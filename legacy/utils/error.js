const formatMessage = (message, errorName) => {
    // todo, normalize error messages
  return message;
};

export const format = (error) => {
  let ret = 'Unknown Error';
  switch (typeof error) {
    case 'string':
      ret = formatMessage(error);
      break;
    case 'object':
      if (error instanceof Error) {
        const name = error.name;
        const message = error.message;
        ret = formatMessage(error, name);
      }
    default:
  }
  return ret;
};
