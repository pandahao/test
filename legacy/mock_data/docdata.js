const DocumentationData = { docs: [
  {
    num: 1,
    title: 'RoboTerraRoboCore Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'ROBOCORE_LAUNCH',
            emitted: 'Right after initializeRoboTerraRobot() if RoboTerraRoboCore object declared',
            source: 'RoboTerraRoboCore object',
            type: ' ROBOCORE_LAUNCH',
            data: 'Number of ports attached (0 - 18)'
          },
          {
            key: 2,
            title: 'ROBOCORE_TERMINATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraRoboCore object',
            type: 'ROBOCORE_TERMINATE',
            data: 'Number of ports attached (0)'
          },
          {
            key: 3,
            title: 'ROBOCORE_TIME_UP',
            emitted: 'objectName.time(RoboTerraTimeUnit length) is called and time is up',
            source: 'RoboTerraRoboCore object',
            type: ' ROBOCORE_TIME_UP',
            data: 'Length of time has just passed in millisecond or second '
          },
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void attach(objectName, portID)',
            content: 'Attach electronics (sensors or actuators) to a specific port on RoboCore and activate it.'
          },
          {
            key: 2,
            title: 'void attach(object, portIDX, portIDY)',
            content: 'Attach RoboterraJoystick to RoboTerraRoboCore.'

          },
          {
            key: 3,
            title: 'void terminate()',
            content: 'Terminate RoboCore so that it will not process any EVENT unless RoboCore is reset.'
          },
          {
            key: 4,
            title: 'void print(char *string)',
            content: 'Print a string that has no more than 50 characters.'
          },
          {
            key: 5,
            title: 'void print(int num)',
            content: 'Print an integter that ranges from -32766 to 32766.'
          },
          {
            key: 6,
            title: 'void print(char *string, int num)',
            content: 'Print a string that has no more than 50 characters followed by an integer that ranges from -32766 to 32766.'
          },
          {
            key: 7,
            title: 'void time(RoboTerraTimeUnit length)',
            content: 'Set after how long ROBOCORE_TIME_UP will fire. Valid arguments: QUARTER_SEC, HALF_SEC, [ONE_SEC, …, TEN_SEC], ONE_MIN, TWO_MIN.'
          }
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'DIO_1 is the port ID where the “button” is attached to “tom”. A RoboTerraButton can be attached to any DIO port on RoboCore.',
                code: 'tom.attach(button, DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, With EVENT information, make decisions by calling Command Functions of each object to take action.',
            content: [
              {
                key: 1,
                text: 'Terminate “tom” so that it will not process any EVENT unless “tom” is reset.',
                code: 'tom.terminate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 2,
    title: 'RoboTerraButton Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'Command Function attach(objectName, portID) or activate() is called',
            source: 'RoboTerraButton object',
            type: 'ACTIVATE',
            data: 'Number of active RoboTerraButton objects (0-9)'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraButton object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraButton objects (0-9)'
          },
          {
            key: 3,
            title: 'BUTTON_PRESS',
            emitted: 'Press and hold an active RoboTerraButton',
            source: 'RoboTerraButton object',
            type: 'BUTTON_PRESS',
            data: 'Number of times a RoboTerraButton pressed (0-32767)'
          },
          {
            key: 4,
            title: 'BUTTON_RELEASE',
            emitted: 'Release an active RoboTerraButton',
            source: 'RoboTerraButton object',
            type: 'BUTTON_RELEASE',
            data: 'Number of times a RoboTerraButton pressed (0-32767)'
          }
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: 'Activate RoboTerraButton so that EVENT related to RoboTerraButton can be detected.'
          },
          {
            key: 2,
            title: 'vvoid deactivate()',
            content: 'Deactivate RoboTerraButton so that EVENT related to RoboTerraButton can no longer be detected.'
          }
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'The “button” can be replaced with any name of your choice to specify the RoboTerraButton attached to RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraButton button;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'DIO_1 is the port ID where the “button” is attached to “tom”. A RoboTerraButton can be attached to any DIO port on RoboCore.',
                code: 'tom.attach(button,DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, Design the algorithm to handle each EVENT.',
            content: [
              {
                key: 1,
                text: 'Check if this EVENT is from the “button”.',
                code: 'if (EVENT.isFrom(button)){newline}'
              },
              {
                key: 2,
                text: 'Check if this EVENT has a type of BUTTON_PRESS.',
                code: 'if (EVENT.isType(BUTTON_PRESS)){newline}'
              },
              {
                key: 3,
                text: 'Check if this EVENT has a type of BUTTON_RELEASE.',
                code: 'if (EVENT.isTYPE(BUTTON_RELEASE)){newline}'
              },
              {
                key: 4,
                text: 'Check if the “button” has been pressed twice.',
                code: 'if (EVENT.isType(BUTTON_PRESS)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 5,
                text: 'Check if the “button” has been released twice.',
                code: 'if (EVENT.isType(BUTTON_RELEASE)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 6,
                text: 'Activate the “button”.',
                code: 'button.activate();'
              },
              {
                key: 7,
                text: 'Deactivate the “button”.',
                code: 'button.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 3,
    title: 'RoboTerraLightSensor Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'Command Function attach(objectName, portID) or activate() is called',
            source: 'RoboTerraLightSensor object',
            type: 'ACTIVATE',
            data: 'Number of active RoboTerraLightSensor objects (0-9)'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraLightSensor object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraLightSensor objects (0-9)'
          },
          {
            key: 3,
            title: 'DARK_ENTER',
            emitted: 'Darkness sensed by RoboTerraLightSensor',
            source: 'RoboTerraLightSensor object',
            type: 'DARK_ENTER',
            data: 'Number of times a RoboTerraLightSensor enters Dark (0-32767)'
          },
          {
            key: 4,
            title: 'DARK_LEAVE',
            emitted: 'Darkness not sensed by RoboTerraLightSensor',
            source: 'RoboTerraLightSensor object',
            type: 'DARK_LEAVE',
            data: 'Number of times a RoboTerraLightSensor leaves Dark (0-32767)'
          }
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: 'Activate RoboTerraButton so that EVENT related to RoboTerraLightSensor can be detected.'
          },
          {
            key: 2,
            title: 'vvoid deactivate()',
            content: 'Deactivate RoboTerraButton so that EVENT related to RoboTerraLightSensor can no longer be detected.'
          }
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'The “eye” can be replaced with any name of your choice to specify the RoboTerraLightSensor attached to RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraLightSensor eye;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'DIO_1 is the port ID where the “eye” is attached to “tom”. A RoboTerraLightSensor can be attached to any DIO port on RoboCore.',
                code: 'tom.attach(eye, DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, Design the algorithm to handle each EVENT.',
            content: [
              {
                key: 1,
                text: 'Check if this EVENT is from the “eye”.',
                code: 'if (EVENT.isFrom(eye)){newline}'
              },
              {
                key: 2,
                text: 'Check if this EVENT has a type of DARK_ENTER.',
                code: 'if (EVENT.isType(DARK_ENTER)){newline}'
              },
              {
                key: 3,
                text: 'Check if this EVENT has a type of DARK_LEAVE.',
                code: 'if (EVENT.isTYPE(DARK_LEAVE)){newline}'
              },
              {
                key: 4,
                text: 'Check if the “eye” has entered Dark twice.',
                code: 'if (EVENT.isType(DARK_ENTER)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 5,
                text: 'Check if the “eye” has left Dark twice.',
                code: 'if (EVENT.isType(DARK_LEAVE)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 6,
                text: 'Activate the “eye”.',
                code: 'eye.activate();'
              },
              {
                key: 7,
                text: 'Deactivate the “eye”.',
                code: 'eye.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 4,
    title: 'RoboTerraSoundSensor Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'Command Function attach(objectName, portID) or activate() is called',
            source: 'RoboTerraSoundSensor object',
            type: 'ACTIVATE',
            data: 'Number of active RoboTerraSoundSensor objects (0-9)'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraSoundSensor object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraSoundSensor objects (0-9)'
          },
          {
            key: 3,
            title: 'SOUND_BEGIN',
            emitted: 'Sound sensed by RoboTerraSoundSensor',
            source: 'RoboTerraButton object',
            type: 'SOUND_BEGIN',
            data: 'Number of times a RoboterraSoundSensor detects the beginning of sound (0-32767)'
          },
          {
            key: 4,
            title: 'SOUND_END',
            emitted: 'Sound no longer sensed by RoboTerraSoundSensor',
            source: 'RoboTerraButton object',
            type: 'SOUND_END',
            data: 'Number of times a RoboterraSoundSensor detects the end of sound (0-32767)'
          }
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: 'Activate RoboTerraSoundSensor so that EVENT related to RoboTerraSoundSensor can be detected.'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: 'Deactivate RoboTerraSoundSensor so that EVENT related to RoboTerraSoundSensor can no longer be detected.'
          }
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'The “ear” can be replaced with any name of your choice to specify the RoboTerraButton attached to RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraSoundSensor ear;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'DIO_1 is the port ID where the “ear” is attached to “tom”. A RoboTerraSoundSensor can be attached to any DIO port on RoboCore.',
                code: 'tom.attach(ear,DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, Design the algorithm to handle each EVENT.',
            content: [
              {
                key: 1,
                text: 'Check if this EVENT is from the “ear”.',
                code: 'if (EVENT.isFrom(ear)){newline}'
              },
              {
                key: 2,
                text: 'Check if this EVENT has a type of SOUND_BEGIN.',
                code: 'if (EVENT.isType(SOUND_BEGIN)){newline}'
              },
              {
                key: 3,
                text: 'Check if this EVENT has a type of SOUND_END.',
                code: 'if (EVENT.isTYPE(SOUND_END)){newline}'
              },
              {
                key: 4,
                text: 'Check if the “ear” has detected sound begin twice.',
                code: 'if (EVENT.isType(SOUND_BEGIN)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 5,
                text: 'Check if the “ear” has detected sound end twice.',
                code: 'if (EVENT.isType(SOUND_END)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 6,
                text: 'Activate the “ear”.',
                code: 'ear.activate();'
              },
              {
                key: 7,
                text: 'Deactivate the “ear”.',
                code: 'ear.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 5,
    title: 'RoboTerraTapeSensor Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'Command Function attach(objectName, portID) or activate() is called',
            source: 'RoboTerraTapeSensor object',
            type: 'ACTIVATE',
            data: 'Number of active RoboTerraTapeSensor objects (0-9)'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraTapeSensor object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraTapeSensor objects (0-9)'
          },
          {
            key: 3,
            title: 'BLACK_TAPE_ENTER',
            emitted: 'Press and hold an active RoboTerraTapeSensor',
            source: 'RoboTerraTapeSensor object',
            type: 'BLACK_TAPE_ENTER',
            data: ' Number of times a RoboTerraTapeSensor enters black tape (0-32767)'
          },
          {
            key: 4,
            title: 'BLACK_TAPE_LEAVE',
            emitted: 'Release an active RoboTerraTapeSensor',
            source: 'RoboTerraTapeSensor object',
            type: 'BLACK_TAPE_LEAVE',
            data: ' Number of times a RoboTerraTapeSensor leaves black tape (0-32767)'
          }
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: 'Activate RoboTerraTapeSensor so that EVENT related to RoboTerraTapeSensor can be detected.'
          },
          {
            key: 2,
            title: 'vvoid deactivate()',
            content: 'Deactivate RoboTerraTapeSensor so that EVENT related to RoboTerraTapeSensor can no longer be detected.'
          }
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'The “foot” can be replaced with any name of your choice to specify the RoboTerraTapeSensor attached to RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraTapeSensor foot;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'DIO_1 is the port ID where the “foot” is attached to “tom”. A RoboTerraTapeSensor can be attached to any DIO port on RoboCore.',
                code: 'tom.attach(foot,DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, Design the algorithm to handle each EVENT.',
            content: [
              {
                key: 1,
                text: 'Check if this EVENT is from the “foot”.',
                code: 'if (EVENT.isFrom(foot)){newline}'
              },
              {
                key: 2,
                text: 'Check if this EVENT has a type of BLACK_TAPE_ENTER.',
                code: 'if (EVENT.isType(BLACK_TAPE_ENTER)){newline}'
              },
              {
                key: 3,
                text: 'Check if this EVENT has a type of BLACK_TAPE_LEAVE.',
                code: 'if (EVENT.isType(BLACK_TAPE_LEAVE)){newline}'
              },
              {
                key: 4,
                text: 'Check if the “foot” has entered black tape twice.',
                code: 'if (EVENT.isType(BLACK_TAPE_ENTER)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 5,
                text: 'Check if the “foot” has left black tape twice.',
                code: 'if (EVENT.isType(BLACK_TAPE_LEAVE)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 6,
                text: 'Activate the “foot”.',
                code: 'foot.activate();'
              },
              {
                key: 7,
                text: 'Deactivate the “foot”.',
                code: 'foot.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 6,
    title: 'RoboTerraServo Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'Command Function attach(objectName, portID) or activate() is called',
            source: 'RoboTerraServo object',
            type: 'ACTIVATE',
            data: 'Number of active RoboTerraServo objects (0-9)'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraServo object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraServo objects (0-9)'
          },
          {
            key: 3,
            title: 'SERVO_MOVE_BEGIN',
            emitted: 'Calling rotate(degree, speed) or resume()',
            source: 'RoboTerraServo object',
            type: 'SERVO_MOVE_BEGIN',
            data: 'EVENT.getData(): Degree of angle a RoboTerraServo is going to rotate to (0-180)newlineEVENT.getData(1): Current Speed of a RoboTerraServo (1 - 10)'
          },
          {
            key: 4,
            title: 'SERVO_INCREASE_END',
            emitted: 'Target angle reached or call pause() when angle increasing (counterclockwise)',
            source: 'RoboTerraServo object',
            type: 'SERVO_INCREASE_END',
            data: 'EVENT.getData(): Degree of angle a RoboTerraServo currently stays at (0-180)newlineEVENT.getData(1): Current Speed of a RoboTerraServo (0)'
          },
          {
            key: 5,
            title: 'SERVO_DECREASE_END',
            emitted: 'Target angle reached or call pause() when angle decreasing (clockwise)',
            source: 'RoboTerraServo object',
            type: 'SERVO_DECREASE_END',
            data: 'EVENT.getData(): Degree of angle a RoboTerraServo currently stays at (0-180)newlineEVENT.getData(1): Current Speed of a RoboTerraServo (0)'
          }
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void activate(int initialAngle)',
            content: 'Activate RoboTerraServo to a initial angle with speed 10.'
          },
          {
            key: 2,
            title: 'void deactivate(int finalAngle)',
            content: 'Deactivate RoboTerraServo to a final angle with speed 10.'
          },
          {
            key: 3,
            title: 'void rotate(int degree, int speed)',
            content: 'Rotate RoboTerraServo to a specified target angle at a specified speed.'
          },
          {
            key: 4,
            title: 'void pause()',
            content: 'Pause RoboTerraServo during its rotation motion.',
          },
          {
            key: 5,
            title: 'void resume()',
            content: 'Resume RoboTerraServo to reach the specified target angle before it is paused.'
          }
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'The “leg” can be replaced with any name of your choice to specify the RoboTerraButton attached to RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraServo leg;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'SERVO_A is the port ID where the “leg” is attached to “tom”. A RoboTerraServo can be attached to any SERVO port on RoboCore.',
                code: 'tom.attach(leg, SERVO_A);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, Design the algorithm to handle each EVENT.',
            content: [
              {
                key: 1,
                text: 'Check if this EVENT is from the “leg”.',
                code: 'if (EVENT.isFrom(leg)){newline}'
              },
              {
                key: 2,
                text: 'Check if this EVENT has a type of SERVO_MOVE_BEGIN.',
                code: 'if (EVENT.isType(SERVO_MOVE_BEGIN)){newline}'
              },
              {
                key: 3,
                text: 'Check if this EVENT has a type of SERVO_INCREASE_END.',
                code: 'if (EVENT.isTYPE(SERVO_INCREASE_END)){newline}'
              },
              {
                key: 4,
                text: 'Check if this EVENT has a type of SERVO_DECREASE_END.',
                code: 'if (EVENT.isType(SERVO_DECREASE_END)){newlineindent}'
              },
              {
                key: 5,
                text: 'Check if RoboTerraServo is going to rotate to 90 degree.',
                code: 'if (EVENT.isType(SERVO_MOVE_BEGIN)){newlineindentif(EVENT.getData()==90){newlineindent}newline}'
              },
              {
                key: 6,
                text: 'Check if RoboTerraServo current speed is 2.',
                code: 'if (EVENT.isType(SERVO_MOVE_BEGIN)){newlineindentif(EVENT.getData(1)==2){newlineindent}newline}'
              },
              {
                key: 7,
                text: 'Activate the “leg”.',
                code: 'leg.activate();'
              },
              {
                key: 8,
                text: 'Deactivate the “leg”.',
                code: 'leg.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 7,
    name: 'Event',
    title: 'RoboTerraLED Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'Command Function attach(objectName, portID) or activate() is called',
            source: 'RoboTerraLED object',
            type: 'ACTIVATE',
            data: 'Number of active RoboTerraLED objects (0-9)'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraLED object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraLED objects (0-9)'
          },
          {
            key: 3,
            title: 'LED_TURNON',
            emitted: 'Calling turnOn() when it is not on or toggle() when it is off',
            source: 'RoboTerraLED object',
            type: 'LED_TURNON',
            data: 'Number of RoboTerraLEDs that is on (0-9)'
          },
          {
            key: 4,
            title: 'LED_TURNOFF',
            emitted: 'Calling turnOff() when it is not on or toggle() when it is off',
            source: 'RoboTerraLED object',
            type: 'LED_TURNOFF',
            data: 'Number of RoboTerraLEDs that is on (0-9)'
          },
          {
            key: 5,
            title: 'SLOWBLINK_BEGIN',
            emitted: 'Calling slowBlink(int num) or slowBlink()',
            source: 'RoboTerraLED object',
            type: 'SLOWBLINK_BEGIN',
            data: 'Number of times a RoboTerraLED is going to blink (1-32767) / Unlimited times (0)'
          },
          {
            key: 6,
            title: 'FASTBLINK_BEGIN',
            emitted: 'Calling fastBlink(int num) or fastBlink()',
            source: 'RoboTerraLED object',
            type: 'FASTBLINK_BEGIN',
            data: 'Number of times a RoboTerraLED is going to blink (1-32767) / Unlimited times (0)'
          },
          {
            key: 7,
            title: 'BLINK_END',
            emitted: 'Finite blinks finished or calling turnOn() or turnOff() or stopBlink() when blinking',
            source: 'RoboTerraLED object',
            type: 'BLINK_END',
            data: 'Number of times a RoboTerraLED has actually blinked (0-32767)'
          }
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: 'Activate RoboTerraLED so that Command Functions related to RoboTerraLED would take action once called.'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: 'Deactivate RoboTerraLED so that Command Functions related to RoboTerraLED would not take action once called.'
          },
          {
            key: 3,
            title: 'void turnOn()',
            content: 'Turn on RoboTerraLED or stop a blinking one and make it stay on.'
          },
          {
            key: 4,
            title: 'void turnOff()',
            content: 'Turn off RoboTerraLED or stop a blinking one and make it stay off.'
          },
          {
            key: 5,
            title: 'void slowBlink(int times)',
            content: 'Make RoboTerraLED blink slowly (once per second) for a specified number of times.'
          },
          {
            key: 6,
            title: 'void slowBlink()',
            content: 'Make RoboTerraLED blink slowly (once per second) for an unlimited number of times or switch from fastBlink() to slowBlink().'
          },
          {
            key: 7,
            title: 'void fastBlink(int times)',
            content: 'Make RoboTerraLED blink fast (once per quarter second) for a specified number of times.'
          },
          {
            key: 8,
            title: 'void fastBlink()',
            content: 'Make RoboTerraLED blink fast (once per quarter second) for an unlimited number of times or switch from slowBlink() to fastBlink().'
          },
          {
            key: 9,
            title: 'void toggle()',
            content: 'Turn on RoboTerraLED if it is already off or turn off RoboTerraLED if it is already on.'
          },
          {
            key: 10,
            title: 'void stopBlink()',
            content: 'Stop RoboTerraLED from blinking and restore its state (either on or off) before blinking.'
          },
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'The “red” can be replaced with any name of your choice to specify the RoboTerraLED attached to RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraLED red;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'DIO_1 is the port ID where the “red” is attached to “tom”. A RoboTerraLED can be attached to any DIO port on RoboCore.',
                code: 'tom.attach(red, DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, Design the algorithm to handle each EVENT.',
            content: [
              {
                key: 1,
                text: 'Check if this EVENT is from the “red”.',
                code: 'if (EVENT.isFrom(red)){newline}',
              },
              {
                key: 2,
                text: 'Check if this EVENT has a type of LED_TURNON.',
                code: 'if(EVENT.isType(LED_TURNON)){newline}'
              },
              {
                key: 3,
                text: 'Check if this EVENT has a type of LED_TURNOFF.',
                code: 'if(EVENT.isType(LED_TURNOFF)){newline}'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 8,
    name: 'Event',
    title: 'RoboTerraJoystick Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'Command Function attach(objectName, portIDX, portIDY) or activate() is called',
            source: 'RoboTerraJoystick object',
            type: 'ACTIVATE',
            data: 'Number of active RoboTerraJoystick (0-1)'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraJoystick object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraJoystick (0-1)'
          },
          {
            key: 3,
            title: 'JOYSTICK_X_UPDATE',
            emitted: 'Push the joystick along X axis of an active RoboTerraJoystick',
            source: 'RoboTerraJoystick object',
            type: 'JOYSTICK_X_UPDATE',
            data: 'X value (-5-5)'
          },
          {
            key: 4,
            title: 'JOYSTICK_Y_UPDATE',
            emitted: 'Push the joystick along Y axis of an active RoboTerraJoystick',
            source: 'RoboTerraJoystick object',
            type: 'JOYSTICK_Y_UPDATE',
            data: 'Y value (-5-5)'
          }
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: 'Activate RoboTerraJoystick so that EVENT related to RoboTerraJoystick can be detected.'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: 'Deactivate RoboTerraJoystick so that EVENT related to RoboTerraJoystick can no longer be detected.'
          }
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'The “joystick” can be replaced with any name of your choice to specify the RoboTerraJoystick attached to RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraJoystick joystick;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'AI_1, AI_2 are the port IDs where the “joystick” is attached to “tom”. A RoboTerraJoystick can be attached to port AI_1 and AI_2 on RoboCore.',
                code: 'tom.attach(joystick, AI_1, AI_2);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, Design the algorithm to handle each EVENT.',
            content: [
              {
                key: 1,
                text: 'Check if this EVENT is from the “joystick”.',
                code: 'if (EVENT.isFrom(joystick)){newline}',
              },
              {
                key: 2,
                text: 'Check if this EVENT has a type of JOYSTICK_X_UPDATE.',
                code: 'if(EVENT.isType(JOYSTICK_X_UPDATE)){newline}'
              },
              {
                key: 3,
                text: 'Check if this EVENT has a type of JOYSTICK_Y_UPDATE.',
                code: 'if(EVENT.isType(JOYSTICK_Y_UPDATE)){newline}'
              },
              {
                key: 4,
                text: 'Check if the X value of the joystick is equal to 2.',
                code: 'if(EVENT.isType(JOYSTICK_X_UPDATE)){newlineindentif (EVENT.getData () == 2){newlineindent}newline}'
              },
              {
                key: 5,
                text: 'Check if the Y value of the joystick is equal to 2.',
                code: 'if(EVENT.isType(JOYSTICK_Y_UPDATE)){newlineindentif (EVENT.getData () == 2){newlineindent}newline}'
              },
              {
                key: 6,
                text: 'Activate the “joystick”.',
                code: 'joystick.activate ();'
              },
              {
                key: 7,
                text: 'Deactivate the “joystick”.',
                code: 'joystick.deactivate ();'
              }
            ]
          }
        ]
      }
    ]
  },

  {
    num: 9,
    name: 'Event',
    title: 'RoboTerraMotor Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'Command Function attach(objectName, portID) or activate() is called',
            source: 'RoboTerraMotor object',
            type: 'ACTIVATE',
            data: 'Number of active RoboTerraMotor (0-2)'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraMotor object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraMotor (0-2)'
          },
          {
            key: 3,
            title: 'MOTOR_SPEED_CHANGE',
            emitted: 'Calling rotate(speedToSet) or resume() when motor is paused',
            source: 'RoboTerraMotor object',
            type: 'MOTOR_SPEED_CHANGE',
            data: 'Speed of RoboTerraMotor (0-10).getData(1):Direction of RoboTerraMotor (1 or 0)'
          },
          {
            key: 4,
            title: 'MOTOR_REVERSE',
            emitted: 'Calling reverse() when motor is rotating',
            source: 'RoboTerraMotor object',
            type: 'MOTOR_REVERSE',
            data: 'Speed of RoboTerraMotor (0-10).getData(1):Direction of RoboTerraMotor (1 or 0)'
          },
          {
            key: 5,
            title: 'MOTOR_SPEED_ZERO',
            emitted: 'Calling pause() when motor is rotating',
            source: 'RoboTerraMotor object',
            type: 'MOTOR_SPEED_ZERO',
            data: 'Speed of RoboTerraMotor (0-10).getData(1):Direction of RoboTerraMotor (1 or 0)'
          }
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: 'Activate RoboTerraMotor so that EVENT related to RoboTerraMotor can be detected.'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: 'Deactivate RoboTerraMotor so that EVENT related to RoboTerraMotor can no longer be detected.'
          },
          {
            key: 3,
            title: 'void rotate(int speedToSet)',
            content: 'Set the speed of the RoboTerraMotor from -10 to 10.'
          },
          {
            key: 4,
            title: 'void reverse',
            content: 'Rotate RoboTerraMotor at the speed set in rotate(int speedToSet) reversely.'
          },
          {
            key: 5,
            title: 'void pause()',
            content: 'Pause RoboTerraMotor during its rotation motion.'
          },
          {
            key: 6,
            title: 'void resume()',
            content: "Resume RoboTerraMotor's motion at its previous speed and direction."
          }
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'The “wheel” can be replaced with any name of your choice to specify the RoboTerraMotor attached to RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraMotor wheel;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'MOTOR_A is the port ID where the “wheel” is attached to “tom”.A RoboTerraMotor can be attached to any MOTOR port on RoboCore.',
                code: 'tom.attach(wheel,MOTOR_A);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, Design the algorithm to handle each EVENT.',
            content: [
              {
                key: 1,
                text: 'Check if this EVENT is from the “wheel”.',
                code: 'if (EVENT.isFrom(wheel)){newline}',
              },
              {
                key: 2,
                text: 'Check if this EVENT has a type of MOTOR_SPEED_CHANGE.',
                code: 'if(EVENT.isType(MOTOR_SPEED_CHANGE)){newline}'
              },
              {
                key: 3,
                text: 'Check if this EVENT has a type of MOTOR_REVERSE.',
                code: 'if(EVENT.isType(MOTOR_REVERSE)){newline}'
              },
              {
                key: 4,
                text: 'Check if this EVENT has a type of MOTOR_SPEED_ZERO.',
                code: 'if(EVENT.isType(MOTOR_SPEED_ZERO)){newline}'
              },
              {
                key: 5,
                text: 'Check if the “wheel” current speed is 2.',
                code: 'if(EVENT.isType(MOTOR_SPEED_CHANGE)){newlineindentif (EVENT.getData () == 2){newlineindent}newline}'
              },
              {
                key: 6,
                text: 'Check the “wheel” current direction.',
                code: 'if(EVENT.isType(MOTOR_SPEED_CHANGE)){newlineindentif (EVENT.getData(1) == 1){newlineindent}newline}'
              },
              {
                key: 7,
                text: 'Activate the wheel.',
                code: 'wheel.activate()'
              },
              {
                key: 8,
                text: 'Deactivate the wheel.',
                code: 'wheel.deactivate()'
              },
              {
                key: 9,
                text: 'Rotate “wheel” with speed of 3.',
                code: 'wheel.rotate (3)'
              },
              {
                key: 10,
                text: 'Reverse the motion of “wheel”.',
                code: 'wheel.reverse()'
              },
              {
                key: 11,
                text: 'Pause the motion of “wheel”',
                code: 'wheel.pause()'
              },
              {
                key: 12,
                text: 'Resume the motion of “wheel”.',
                code: 'wheel.resume ()'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 10,
    name: 'Event',
    title: 'RoboTerraIRTransmitter Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'DEACTIVATE',
            emitted: 'Command Function attach(objectName, portID) or deactivate() is called',
            source: 'RoboTerraIRTransmitter object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraIRTransmitter (0-1)'
          },
          {
            key: 2,
            title: 'ACTIVATE',
            emitted: 'Command Function activate() is called',
            source: 'RoboTerraIRTransmitter object',
            type: 'ACTIVATE',
            data: 'Number of active RoboTerraIRTransmitter (0-1)'
          },
          {
            key: 3,
            title: 'IR_MESSAGE_EMIT',
            emitted: 'Command Function emit(adress, value) is called',
            source: 'RoboTerraIRTransmitter object',
            type: 'IR_MESSAGE_EMIT',
            data: 'Value of the IR signal.getData(1):Address of the IR signal'
          }
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: 'Activate RoboTerraIRTransmitter so that EVENT related to RoboTerraIRTransmitter can be detected.'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: 'Deactivate RoboTerraIRTransmitter so that EVENT related to RoboTerraIRTransmitter can no longer be detected.'
          },
          {
            key: 3,
            title: 'void emit(int value, int address)',
            content: 'Send out an IR message that includes two integers, representing value and address, respectively.'
          }
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'The “trans” can be replaced with any name of your choice to specify the RoboTerraIRTransmitter attached to RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraMotor trans;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'IR_TRAN is the port ID where the “trans” is attached to “tom”.A RoboTerraIRTransmitter can be only attached to IR_TRAN port on RoboCore.',
                code: 'tom.attach(trans, IR_TRAN);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, Design the algorithm to handle each EVENT.',
            content: [
              {
                key: 1,
                text: 'Check if this EVENT is from the “trans”.',
                code: 'if (EVENT.isFrom(trans)){newline}',
              },
              {
                key: 2,
                text: 'Check if this EVENT has a type of IR_MESSAGE_EMIT.',
                code: 'if(EVENT.isType(IR_MESSAGE_EMIT)){newline}'
              },
              {
                key: 3,
                text: 'Check if the value of the IR signal that the “trans” has emitted is 2.',
                code: 'if(EVENT.isType(IR_MESSAGE_EMIT)){newlineindentif (EVENT.getData() == 2){newlineindent}newline}'
              },
              {
                key: 4,
                text: 'Check if the address of the IR signal that the “trans” has emitted is 2.',
                code: 'if(EVENT.isType(IR_MESSAGE_EMIT)){newlineindentif (EVENT.getData(1) == 2){newlineindent}newline}'
              },
              {
                key: 5,
                text: 'Activate the “trans”.',
                code: 'trans.activate();'
              },
              {
                key: 6,
                text: 'Deactivate the “trans”.',
                code: 'trans.deactivate();'
              },
              {
                key: 7,
                text: 'Send a 16-bit integar as an “address” and a 16-bit integar as a "value" through IR communication.',
                code: 'trans.emit (address, value);'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 11,
    name: 'Event',
    title: 'RoboTerraIRReceiver Class',
    content: [
      {
        key: 1,
        name: 'Event',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'Command Function attach(objectName, portID) or activate() is called',
            source: 'RoboTerraIRReceiver object',
            type: 'ACTIVATE',
            data: 'Number of active RoboTerraIRReceiver (0-1)'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraIRReceiver object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraIRTransmitter (0-1)'
          },
          {
            key: 3,
            title: 'IR_MESSAGE_RECIVE',
            emitted: 'IR signal recived by RoboTerraIRReceiver',
            source: 'RoboTerraIRReceiver object',
            type: 'IR_MESSAGE_RECIVE',
            data: 'Value of the IR signal.getData(1):Address of the IR signal'
          },
          {
            key: 4,
            title: 'IR_MESSAGE_REPEAT',
            emitted: 'IR signal recived by RoboTerraIRReceiver is same as last one',
            source: 'RoboTerraIRReceiver object',
            type: 'IR_MESSAGE_REPEAT',
            data: 'Value of the IR signal.getData(1):Address of the IR signal'
          }
        ]
      },
      {
        key: 2,
        name: 'Command Functions',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: 'Activate RoboTerraIRReceiver so that EVENT related to RoboTerraIRReceiver can be detected.'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: 'Deactivate RoboTerraIRReceiver so that EVENT related to RoboTerraIRReceiver can no longer be detected.'
          }
        ]
      },
      {
        key: 3,
        name: 'Example Usage',
        content: [
          {
            key: 1,
            step: 'In Step 1, Name your RoboCore and all electronics attached to it. These name are considered as eventSource of EVENT.',
            content: [
              {
                key: 1,
                text: 'The “Receiver” can be replaced with any name of your choice to specify the RoboTerraIRReceiver attached to RoboTerraRoboCore named “tom”.',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraMotor Receiver;'
              }
            ]
          },
          {
            key: 2,
            step: 'In Step 2, Initialize your robot by attaching electronics.',
            content: [
              {
                key: 1,
                text: 'DIO_1 is the port ID where the “Receiver” is attached to “tom”.A RoboTerraIRTransmitter can be attached to any DIO port on RoboCore.',
                code: 'tom.attach(Receiver, DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: 'In Step 3, Design the algorithm to handle each EVENT.',
            content: [
              {
                key: 1,
                text: 'Check if this EVENT is from the “Receiver”.',
                code: 'if (EVENT.isFrom(Receiver)){newline}',
              },
              {
                key: 2,
                text: 'Check if this EVENT has a type of IR_MESSAGE_RECIVE.',
                code: 'if(EVENT.isType(IR_MESSAGE_RECIVE)){newline}'
              },
              {
                key: 3,
                text: 'Check if this EVENT has a type of IR_MESSAGE_REPEAT.',
                code: 'if(EVENT.isType(IR_MESSAGE_REPEAT)){newline}'
              },
              {
                key: 4,
                text: 'Check if this EVENT has a type of IR_MESSAGE_INTERFERE.',
                code: 'if(EVENT.isType(IR_MESSAGE_INTERFERE)){newline}'
              },
              {
                key: 5,
                text: 'Check if the value of the IR signal the the “Receiver” has recived is 2',
                code: 'if (EVENT.isType(IR_MESSAGE_RECIVE)){newlineindentif (EVENT.getData() == 2){newlineindent}newline}'
              },
              {
                key: 6,
                text: 'Check if the address of the IR signal the the “Receiver” has recived is 2',
                code: 'if (EVENT.isType(IR_MESSAGE_RECIVE)){newlineindentif (EVENT.getData(1) == 2){newlineindent}newline}'
              },
              {
                key: 7,
                text: 'Activate the “Receiver”.',
                code: 'trans.activate();'
              },
              {
                key: 8,
                text: 'Deactivate the “Receiver”.',
                code: 'trans.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  }

] };


export default DocumentationData;
