export const onBoardingNav = {
  0: {
    title: 'WELCOME TO CASTLEROCK!',
    text: 'You’re on your way to becoming a robot master! \nBut first, let’s get a little introduction to the \ndifferent parts of this platform.',
    button: 'NEXT'
  },

  1: {
    title: 'The NAVIGATION BAR',
    text: 'Our navigation bar has three main buttons: \n1) Learn Mode \n2) Free Mode \n3) Settings',
    button: 'NEXT',
    arrowleft: '5%',
    arrowtop: 11,
    color: 'GeneralPalette.'
  },

  2: {
    title: 'SETTINGS',
    text: 'Change your settings \nand account information, logout, or view important \nfiles from the Roboterra team here. ',
    button: 'NEXT',
    arrowleft: '5%',
    arrowtop: 210,
  },

  3: {
    title: 'FREE MODE',
    text: 'Free mode encourages you to use your \nimagination and create your own robots! \nThere’s so much room for you to \nbe creative!',
    button: 'NEXT',
    arrowleft: '5%',
    arrowtop: 145,
  },

  4: {
    title: 'PROJECT MODE',
    text: 'This is the page we’re in now. We’ll be \nable to choose projects and work on \nchallenges to build robots! Try starting the \nTutorial Challenge or checkout its Overview.',
    button: 'NEXT',
    arrowleft: '5%',
    arrowtop: 80,
  },

};

export const onBoardChallenge = {
  0: {
    title: 'Your first challenge!',
    text: 'Wow! Diving in, head first! \nHere we’ll go through the basics of a challenge, \nfrom the parts you see to the buttons \nyou press. Let’s get started!',
    progress: '0',
    button: 'NEXT'
  },

  1: {
    title: 'Type your code on the CODEPAD.',
    text: 'Here is where you’ll do your \nactual coding. You’ll learn more \nabout this later. Exciting stuff!',
    button: 'NEXT'
  },

  2: {
    title: 'Upload code with the RUN BUTTON',
    text: 'RUN lets you input your program \nto your RoboCore. It only activates when your \nRoboCore is connected.',
    button: 'NEXT'
  },

  3: {
    title: 'TOOLBOX DROPDOWN',
    text: "This toolbox has a load \nof functionalities for you to \nchoose from. When stuck, don't \nforget there are resources here \nto check out. Let's have \na look at some of them.",
    button: 'NEXT'
  },

  4: {
    title: 'TOOLBOX',
    text: 'DEBUG activates after you run code. \nClick it when stuck and when you need help \nfinding out why you didn’t pass a challenge. \n\nDOCUMENTATION provides a list of \nhelpful guides and tips for coding. \nClick it when you’re stuck. \n\nRESET CODE allows you to restart to the \nstarting template for that challenge.\n Click it when you need to do a redo. ',
    button: 'NEXT'
  },

  5: {
    title: 'This is the INSTRUCTIONS PANEL',
    text: 'Each project consists of several \nChallenges. Here, you’ll be given \ninstructions to complete until \nyou finish your robot project.',
    button: 'Try a code challenge!'
  },

// BUILD & MENU

  6: {
    title: 'The MENU DRAWER.',
    text: "Let's first explore the menu tab. \nClick Next and let's take a look \nat its contents.",
    button: 'NEXT'
  },

  7: {
    title: 'The MENU DRAWER.',
    text: "Here, you’ll find the list of \nchallenges per project, both locked and \nunlocked. At the bottom, you have the \noption of going back to the Projects List \nat any time or restarting a project. \n\nTake note of 3 icons that \nmean 3 different challenges. \nYou'll see all versions of this as you\ngo through this project.",
    button: 'Try a build challenge!'
  },

// VIRTUAL MONITOR
  8: {
    title: 'Get realtime feedback from the VIRTUAL MONITOR.',
    text: 'This is the Virtual Monitor. \nWhat your physical Robot is experiencing\n will immediately be shown here. \n If any bugs arise, you can fix it in no time!',
    button: 'NEXT'
  },

  9: {
    title: 'Say hello to your very own VIRTUAL ROBOT.',
    text: 'This is the Virtual Robot. \nThink of it as the virtual version of \nyour physical robot. If, for example, you connect \nyour physical button, you can see the virtual \nequivalent of the button here.',
    button: 'NEXT'
  },

  10: {
    title: 'See all events with the EVENT MONITOR.',
    text: 'The Event Monitor shows the print elements, \nlabels, events and any assigned numbers as \nthey happen in real time. Check here \nif you want to see if the events you \ndeclared are registering. ',
    button: 'NEXT'
  },


};
