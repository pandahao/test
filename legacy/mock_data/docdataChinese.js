const DocumentationDataCN = { docs: [
  {
    num: 1,
    title: 'RoboTerraRoboCore类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'ROBOCORE_LAUNCH',
            emitted: 'initializeRoboTerraRobot()内语句全部执行后',
            source: 'RoboTerraRoboCore object',
            type: ' ROBOCORE_LAUNCH',
            data: '被连接的端口数量(0 - 18)'
          },
          {
            key: 2,
            title: 'ROBOCORE_TERMINATE',
            emitted: 'terminate() 指令被调用',
            source: 'RoboTerraRoboCore object',
            type: 'ROBOCORE_TERMINATE',
            data: '0'
          },
          {
            key: 3,
            title: 'ROBOCORE_TIME_UP',
            emitted: 'time(RoboTerraTimeUnitlength) 指令被调用且计时结束时',
            source: 'RoboTerraRoboCore object',
            type: ' ROBOCORE_TIME_UP',
            data: 'RoboCore开机以来计时指令结束的次数'
          },
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void attach(objectName, portID)',
            content: '将电子元件（传感器或执行器）连接到某一个指定的RoboCore端口，并激活它。'
          },
          {
            key: 2,
            title: 'void attach(object, portIDX, portIDY)',
            content: '将摇杆模块连接到RoboCore'

          },
          {
            key: 3,
            title: 'void terminate()',
            content: '终止RoboCore的运行，使得它不再处理任何事件（EVENT），除非RoboCore重置。'
          },
          {
            key: 4,
            title: 'void print(char *string)',
            content: '发送一个不超过50个字符的字符串。'
          },
          {
            key: 5,
            title: 'void print(int num)',
            content: '发送一个-32766到32766之间的整数。'
          },
          {
            key: 6,
            title: 'void print(char *string, int num)',
            content: '发送一个不超过50个字符的字符串和一个-32766到32766之间的整数，整数紧挨在字符串之后'
          },
          {
            key: 7,
            title: 'void time(float length)',
            content: '使RoboCore开始计时结束后发出ROBOCORE_TIME_UP事件，length是时间参数，以秒为单位，可以接收的范围是0.1-120秒, 时间精度0.1秒'
          }
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)',
            content: [
              {
                key: 1,
                text: '给RoboTerraRoboCore 命名为“tom”',
                code: 'RoboTerraRoboCore tom;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'DIO_1是“button”与“tom”连接的端口ID。',
                code: 'tom.attach(button, DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: '根据事件（EVENT）信息，通过调用每一个对象的指令，决定使其执行的动作。',
            content: [
              {
                key: 1,
                text: '终止RoboCore，使得它不再处理任何事件（EVENT），除非RoboCore重置。',
                code: 'tom.terminate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 2,
    title: 'RoboTerraButton类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'attach(name,port)或activate()指令被调用',
            source: 'RoboTerraButton object',
            type: 'ACTIVATE',
            data: '激活的RoboTerraButton对象的数量（0-9）'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'deactivate()指令被调用',
            source: 'RoboTerraButton object',
            type: 'DEACTIVATE',
            data: '激活的RoboTerraButton对象的数量（0-9）'
          },
          {
            key: 3,
            title: 'BUTTON_PRESS',
            emitted: '按下一个激活的萝卜太辣按钮',
            source: 'RoboTerraButton object',
            type: 'BUTTON_PRESS',
            data: '按钮被按下的次数(0-32767)'
          },
          {
            key: 4,
            title: 'BUTTON_RELEASE',
            emitted: '松开一个激活的被按下的萝卜太辣按钮',
            source: 'RoboTerraButton object',
            type: 'BUTTON_RELEASE',
            data: '按下的按钮被松开的次数(0-32767)'
          }
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: '启动RoboTerraButton，使得与之相关的事件（EVENT）能够被探测到。'
          },
          {
            key: 2,
            title: 'vvoid deactivate()',
            content: '停止RoboTerraButton，不再处理与之相关的事件(EVENT)。'
          }
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)。',
            content: [
              {
                key: 1,
                text: '“button” 可以被任何其他名字替代，代指某个连接在被命名为“tom”的RoboCore上的萝卜太辣按钮。',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraButton button;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'DIO_1是“button”与“tom”连接的端口ID。一个萝卜太辣按钮可以与任何RoboCore上的DIO 端口连接。',
                code: 'tom.attach(button,DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: '设计处理每一个事件（EVENT）的算法。',
            content: [
              {
                key: 1,
                text: '查看这个事件（EVENT）是否来自“button”。',
                code: 'if (EVENT.isFrom(button)){newline}'
              },
              {
                key: 2,
                text: '查看这个事件（EVENT）是否是BUTTON_PRESS这个类别。',
                code: 'if (EVENT.isType(BUTTON_PRESS)){newline}'
              },
              {
                key: 3,
                text: '查看这个事件（EVENT）是否是BUTTON_ RELEASE这个类别。',
                code: 'if (EVENT.isTYPE(BUTTON_RELEASE)){newline}'
              },
              {
                key: 4,
                text: '查看“button”是否第二次被按下。',
                code: 'if (EVENT.isType(BUTTON_PRESS)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 5,
                text: '查看“button”是否第二次被松开。',
                code: 'if (EVENT.isType(BUTTON_RELEASE)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 6,
                text: '激活 “button”.',
                code: 'button.activate();'
              },
              {
                key: 7,
                text: '停止 “button”.',
                code: 'button.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 3,
    title: 'RoboTerraLightSensor类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'attach(name,port)或activate()指令被调用',
            source: 'RoboTerraLightSensor object',
            type: 'ACTIVATE',
            data: '激活的RoboTerraLightSensor对象的数量（0-9）'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'deactivate()指令被调用',
            source: 'RoboTerraLightSensor object',
            type: 'DEACTIVATE',
            data: '激活的RoboTerraLightSensor对象的数量（0-9）'
          },
          {
            key: 3,
            title: 'DARK_ENTER',
            emitted: '萝卜太辣光敏传感器处于黑暗环境r',
            source: 'RoboTerraLightSensor object',
            type: 'DARK_ENTER',
            data: '萝卜太辣光敏传感器进入黑暗环境的次数(0-32767)'
          },
          {
            key: 4,
            title: 'DARK_LEAVE',
            emitted: '萝卜太辣光敏传感器处于明亮环境',
            source: 'RoboTerraLightSensor object',
            type: 'DARK_LEAVE',
            data: '萝卜太辣光敏传感器离开黑暗环境的次数(0-32767)'
          }
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: '激活RoboTerraLightSensor，使得与之相关的事件（EVENT）能够被检测到。'
          },
          {
            key: 2,
            title: 'vvoid deactivate()',
            content: '停止RoboTerraLightSensor，不再处理与之相关的事件(EVENT)。'
          }
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)。',
            content: [
              {
                key: 1,
                text: '“eye” 可以被任何其他名字替代，代指某个连接在被命名为“tom”的RoboCore上的萝卜太辣光敏传感器。',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraLightSensor eye;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'DIO_1是“eye”与“tom”连接的端口ID。一个萝卜太辣光敏传感器可以与任何RoboCore上的DIO 端口连接。',
                code: 'tom.attach(eye, DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: '设计处理每一个事件（EVENT）的算法。',
            content: [
              {
                key: 1,
                text: '查看这个事件（EVENT）是否来自“eye”。',
                code: 'if (EVENT.isFrom(eye)){newline}'
              },
              {
                key: 2,
                text: '查看这个事件（EVENT）是否是DARK_ENTER类型。',
                code: 'if (EVENT.isType(DARK_ENTER)){newline}'
              },
              {
                key: 3,
                text: '查看这个事件（EVENT）是否是DARK_LEAVE类型。',
                code: 'if (EVENT.isTYPE(DARK_LEAVE)){newline}'
              },
              {
                key: 4,
                text: '查看“eye”是否第二次进入黑暗环境。',
                code: 'if (EVENT.isType(DARK_ENTER)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 5,
                text: '查看“eye”是否第二次离开黑暗环境。',
                code: 'if (EVENT.isType(DARK_LEAVE)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 6,
                text: '激活“eye”.',
                code: 'eye.activate();'
              },
              {
                key: 7,
                text: '停止 “eye”.',
                code: 'eye.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 4,
    title: 'RoboTerraSoundSensor 类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'attach(name,port)或activate()指令被调用',
            source: 'RoboTerraSoundSensor object',
            type: 'ACTIVATE',
            data: '激活的RoboTerraSoundSensor对象的数量（0-9）'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'deactivate()指令被调用',
            source: 'RoboTerraSoundSensor object',
            type: 'DEACTIVATE',
            data: '激活的RoboTerraSoundSensor对象的数量（0-9）'
          },
          {
            key: 3,
            title: 'SOUND_BEGIN',
            emitted: '萝卜太辣声音传感器探测到声音',
            source: 'RoboTerraButton object',
            type: 'SOUND_BEGIN',
            data: '萝卜太辣声音传感(RoboTerraSoundSensor) 探测到声音开始的次数(0-32767)'
          },
          {
            key: 4,
            title: 'SOUND_END',
            emitted: '萝卜太辣声音传感器不再探测到声音',
            source: 'RoboTerraButton object',
            type: 'SOUND_END',
            data: '萝卜太辣声音传感(RoboTerraSoundSensor) 探测到声音结束的次数(0-32767)'
          }
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: '激活RoboTerraSoundSensor，使得与之相关的事件（EVENT）能够被检测到。'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: '停止RoboTerraSoundSensor，不再处理与之相关的事件(EVENT)。'
          }
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)。',
            content: [
              {
                key: 1,
                text: '“ear” 可以被任何其他名字替代，代指某个连接在被命名为“tom”的RoboCore上的萝卜太辣声音传感器。',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraSoundSensor ear;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'DIO_1是“ear”与“tom”连接的端口ID。RoboTerraSoundSensor可以与RoboCore上的任意DIO 端口连接。',
                code: 'tom.attach(ear,DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: '设计处理每一个事件（EVENT）的算法。',
            content: [
              {
                key: 1,
                text: '查看这个事件（EVENT）是否来自“ear”。',
                code: 'if (EVENT.isFrom(ear)){newline}'
              },
              {
                key: 2,
                text: '查看这个事件（EVENT）是否是SOUND_BEGIN类型。',
                code: 'if (EVENT.isType(SOUND_BEGIN)){newline}'
              },
              {
                key: 3,
                text: '查看这个事件（EVENT）是否是SOUND_END类型。',
                code: 'if (EVENT.isTYPE(SOUND_END)){newline}'
              },
              {
                key: 4,
                text: '查看 “ear”是否第二次探测到声音开始。',
                code: 'if (EVENT.isType(SOUND_BEGIN)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 5,
                text: '查看 “ear”是否第二次探测到声音结束。',
                code: 'if (EVENT.isType(SOUND_END)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 6,
                text: '激活 “ear”.',
                code: 'ear.activate();'
              },
              {
                key: 7,
                text: '停止 “ear”.',
                code: 'ear.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 5,
    title: 'RoboTerraTapeSensor 类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'attach(name,port) 或activate()指令被调用',
            source: 'RoboTerraTapeSensor object',
            type: 'ACTIVATE',
            data: '激活的RoboTerraTapeSensor对象的数量（0-9）'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'deactivate()指令被调用',
            source: 'RoboTerraTapeSensor object',
            type: 'DEACTIVATE',
            data: '激活的RoboTerraTapeSensor对象的数量（0-9）'
          },
          {
            key: 3,
            title: 'BLACK_TAPE_ENTER',
            emitted: '萝卜太辣巡线传感器探测到黑色胶带',
            source: 'RoboTerraTapeSensor object',
            type: 'BLACK_TAPE_ENTER',
            data: ' 萝卜太辣巡线传感(RoboTerraTapeSensor) 进入黑色胶带区域的次数(0-32767)'
          },
          {
            key: 4,
            title: 'BLACK_TAPE_LEAVE',
            emitted: '萝卜太辣巡线传感器没有探测到黑色胶带',
            source: 'RoboTerraTapeSensor object',
            type: 'BLACK_TAPE_LEAVE',
            data: ' 萝卜太辣巡线传感(RoboTerraTapeSensor) 离开黑色胶带区域的次数(0-32767)'
          }
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: '激活RoboTerraTapeSensor，使得与之相关的事件（EVENT）能够被检测到。'
          },
          {
            key: 2,
            title: 'vvoid deactivate()',
            content: '停止RoboTerraTapeSensor，不再处理与之相关的事件(EVENT)。'
          }
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)。',
            content: [
              {
                key: 1,
                text: '“foot”可以被任何其他名字替代，代指某个连接在被命名为“tom”的RoboTerraRoboCore上的萝卜太辣巡线传感器。',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraTapeSensor foot;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'DIO_1是“foot”与“tom”连接的端口ID。RoboTerraTapeSensor可以与RoboCore上的任意DIO 端口连接。',
                code: 'tom.attach(foot,DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: '设计处理每一个事件（EVENT）的算法。',
            content: [
              {
                key: 1,
                text: '查看这个事件（EVENT）是否来自“foot”。',
                code: 'if (EVENT.isFrom(foot)){newline}'
              },
              {
                key: 2,
                text: '查看这个事件（EVENT）是否是BLACK_TAPE_ENTER类型。',
                code: 'if (EVENT.isType(BLACK_TAPE_ENTER)){newline}'
              },
              {
                key: 3,
                text: '查看这个事件（EVENT）是否是BLACK_TAPE_ LEAVE类型。',
                code: 'if (EVENT.isType(BLACK_TAPE_LEAVE)){newline}'
              },
              {
                key: 4,
                text: '查看“foot”是否第二次进入黑色胶带区域。',
                code: 'if (EVENT.isType(BLACK_TAPE_ENTER)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 5,
                text: '查看“foot”是否第二次离开黑色胶带区域。',
                code: 'if (EVENT.isType(BLACK_TAPE_LEAVE)){newlineindentif(EVENT.getData()==2){newlineindent}newline}'
              },
              {
                key: 6,
                text: '激活 “foot”.',
                code: 'foot.activate();'
              },
              {
                key: 7,
                text: '停止 “foot”.',
                code: 'foot.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 6,
    title: 'RoboTerraServo 类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'activate(int angle)指令被调用',
            source: 'RoboTerraServo object',
            type: 'ACTIVATE',
            data: '激活的RoboTerraServo对象的数量（0-4）',
            data:'RoboTerraServo的当前位置角度(0-180)'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'Command Function deactivate() is called',
            source: 'RoboTerraServo object',
            type: 'DEACTIVATE',
            data: 'Number of active RoboTerraServo objects (0-9)'
          },
          {
            key: 3,
            title: 'SERVO_MOVE_BEGIN',
            emitted: 'Calling rotate(degree, speed) or resume()',
            source: 'RoboTerraServo object',
            type: 'SERVO_MOVE_BEGIN',
            data: 'EVENT.getData(): Degree of angle a RoboTerraServo is going to rotate to (0-180)newlineEVENT.getData(1): Current Speed of a RoboTerraServo (1 - 10)'
          },
          {
            key: 4,
            title: 'SERVO_INCREASE_END',
            emitted: 'Target angle reached or call pause() when angle increasing (counterclockwise)',
            source: 'RoboTerraServo object',
            type: 'SERVO_INCREASE_END',
            data: 'EVENT.getData(): Degree of angle a RoboTerraServo currently stays at (0-180)newlineEVENT.getData(1): Current Speed of a RoboTerraServo (0)'
          },
          {
            key: 5,
            title: 'SERVO_DECREASE_END',
            emitted: 'Target angle reached or call pause() when angle decreasing (clockwise)',
            source: 'RoboTerraServo object',
            type: 'SERVO_DECREASE_END',
            data: 'EVENT.getData(): Degree of angle a RoboTerraServo currently stays at (0-180)newlineEVENT.getData(1): Current Speed of a RoboTerraServo (0)'
          }
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void activate(int initialAngle)',
            content: '激活RoboTerraServo并使其以10的速度转到initialAngle角度上，使得与之相关的其他指令一经调用马上执行动作。'
          },
          {
            key: 2,
            title: 'void deactivate(int finalAngle)',
            content: '停止RoboTerraServo并使其以10的速度转到finalAngle角度上，使得与之相关的指令被调用后也不执行动作。'
          },
          {
            key: 3,
            title: 'void rotate(int degree, int speed)',
            content: '按照指定速度将RoboTerraServo 转动至指定角度。'
          },
          {
            key: 4,
            title: 'void pause()',
            content: '暂停RoboTerraServo 的动作。',
          },
          {
            key: 5,
            title: 'void resume()',
            content: '在RoboTerraServo 被暂停后恢复RoboTerraServo 的动作，使其继续向目标角度转动。'
          }
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)。',
            content: [
              {
                key: 1,
                text: '“leg” 可以被任何其他名字替代，代指某个连接在被命名为“tom”的RoboCore上的萝卜太辣舵机。',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraServo leg;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'SERVO_A是“leg”与“tom”连接的端口ID。RoboTerraServo可以与RoboCore上的任意SERVO端口连接。',
                code: 'tom.attach(leg, SERVO_A);'
              }
            ]
          },
          {
            key: 3,
            step: '设计处理每一个事件（EVENT）的算法。',
            content: [
              {
                key: 1,
                text: '查看这个事件（EVENT）是否来自“leg”。',
                code: 'if (EVENT.isFrom(leg)){newline}'
              },
              {
                key: 2,
                text: '查看这个事件（EVENT）是否是SERVO_MOVE_BEGIN类型。',
                code: 'if (EVENT.isType(SERVO_MOVE_BEGIN)){newline}'
              },
              {
                key: 3,
                text: '查看这个事件（EVENT）是否是SERVO_INCREASE_END类型。',
                code: 'if (EVENT.isTYPE(SERVO_INCREASE_END)){newline}'
              },
              {
                key: 4,
                text: '查看这个事件（EVENT）是否是SERVO_DECREASE_END类型。',
                code: 'if (EVENT.isType(SERVO_DECREASE_END)){newlineindent}'
              },
              {
                key: 5,
                text: '查看RoboTerraServo 是否将要转动至90度。',
                code: 'if (EVENT.isType(SERVO_MOVE_BEGIN)){newlineindentif(EVENT.getData()==90){newlineindent}newline}'
              },
              {
                key: 6,
                text: '查看RoboTerraServo 的当前速度是否是2。',
                code: 'if (EVENT.isType(SERVO_MOVE_BEGIN)){newlineindentif(EVENT.getData(1)==2){newlineindent}newline}'
              },
              {
                key: 7,
                text: '激活“leg”，使其转到90度。',
                code: 'leg.activate(90);'
              },
              {
                key: 8,
                text: '停止 “leg”，十七回到0度',
                code: 'leg.deactivate(0);'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 7,
    name: '事件',
    title: 'RoboTerraLED 类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'attach(name,port) 或activate()指令被调用',
            source: 'RoboTerraLED object',
            type: 'ACTIVATE',
            data: '激活的RoboTerraLED对象的数量（0-9）'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'deactivate()指令被调用',
            source: 'RoboTerraLED object',
            type: 'DEACTIVATE',
            data: '激活的RoboTerraLED对象的数量（0-9）'
          },
          {
            key: 3,
            title: 'LED_TURNON',
            emitted: '当LED不处于点亮状态时，调用 turnOn() 指令，或当LED处于熄灭状态时，调用 toggle()指令 ',
            source: 'RoboTerraLED object',
            type: 'LED_TURNON',
            data: '处于点亮状态下的RoboTerraLED的数量(0-9)'
          },
          {
            key: 4,
            title: 'LED_TURNOFF',
            emitted: '当LED不处于熄灭状态时，调用turnOff()指令，或当LED处于点亮状态时，调用toggle() ',
            source: 'RoboTerraLED object',
            type: 'LED_TURNOFF',
            data: '处于点亮状态下的RoboTerraLED的数量(0-9)'
          },
          {
            key: 5,
            title: 'SLOWBLINK_BEGIN',
            emitted: 'slowBlink(int num) 或 slowBlink()指令被调用',
            source: 'RoboTerraLED object',
            type: 'SLOWBLINK_BEGIN',
            data: 'RoboTerraLED将要闪烁的次数(1-32767) / 无限次 (0)'
          },
          {
            key: 6,
            title: 'FASTBLINK_BEGIN',
            emitted: 'fastBlink(int num) 或fastBlink()指令被调用',
            source: 'RoboTerraLED object',
            type: 'FASTBLINK_BEGIN',
            data: 'RoboTerraLED将要闪烁的次数 (1-32767) / 无限次 (0)'
          },
          {
            key: 7,
            title: 'BLINK_END',
            emitted: '指定次数的闪烁结束，或当LED闪烁过程中， turnOn() ， turnOff()，或stopBlink()指令被调用',
            source: 'RoboTerraLED object',
            type: 'BLINK_END',
            data: 'RoboTerraLED已经闪烁的次数(0-32767)'
          }
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: '激活RoboTerraLED，使得与之相关的事件（EVENT）能够被检测到且与之相关的其他指令可以被执行。'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: '停止RoboTerraLED，不再处理与之相关的事件(EVENT)且与之相关的指令被调用后也不执行动作。'
          },
          {
            key: 3,
            title: 'void turnOn()',
            content: '点亮熄灭的RoboTerraLED或停止使其闪烁，而使其保持点亮状态。'
          },
          {
            key: 4,
            title: 'void turnOff()',
            content: '熄灭点亮的RoboTerraLED或停止使其闪烁，而使其保持熄灭状态。'
          },
          {
            key: 5,
            title: 'void slowBlink(int times)',
            content: '使RoboTerraLED缓慢闪烁指定的次数（每秒一次）。'
          },
          {
            key: 6,
            title: 'void slowBlink()',
            content: '使RoboTerraLED开始缓慢闪烁（每秒一次），或从快速闪烁切换成缓慢闪烁。'
          },
          {
            key: 7,
            title: 'void fastBlink(int times)',
            content: '使RoboTerraLED快速闪烁指定的次数（每四分之一秒一次）。'
          },
          {
            key: 8,
            title: 'void fastBlink()',
            content: '使RoboTerraLED开始快速闪烁（每四分之一秒一次），或从缓慢闪烁切换成快速闪烁。'
          },
          {
            key: 9,
            title: 'void toggle()',
            content: '改变RoboTerraLED的当前状态(若当前LED灯点亮则熄灭,若当前LED灯熄灭则点亮)'
          },
          {
            key: 10,
            title: 'void stopBlink()',
            content: '停止RoboTerraLED的闪烁状态，并恢复其在闪烁前的状态（点亮或熄灭）。'
          },
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)。',
            content: [
              {
                key: 1,
                text: '“red” 可以被任何其他名字替代，代指某个连接在被命名为“tom”的RoboCore上的萝卜太辣LED灯。',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraLED red;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'DIO_1是“red”与“tom”连接的端口ID。RoboTerraLED可以被任何RoboCore上的DIO 端口连接。',
                code: 'tom.attach(red, DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: '设计处理每一个事件（EVENT）的算法。',
            content: [
              {
                key: 1,
                text: '查看这个事件（EVENT）是否来自“red”。',
                code: 'if (EVENT.isFrom(red)){newline}',
              },
              {
                key: 2,
                text: '查看这个事件（EVENT）是否是LED_TURNON类型。',
                code: 'if(EVENT.isType(LED_TURNON)){newline}'
              },
              {
                key: 3,
                text: '查看这个事件（EVENT）是否是LED_TURNOFF类型。',
                code: 'if(EVENT.isType(LED_TURNOFF)){newline}'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 8,
    name: '事件',
    title: 'RoboTerraJoystick类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'attach(name,portIDX,portIDY) 或activate()指令被调用',
            source: 'RoboTerraJoystick object',
            type: 'ACTIVATE',
            data: '激活的RoboTerraJoystick对象的数量（0-1）'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'deactivate()指令被调用',
            source: 'RoboTerraJoystick object',
            type: 'DEACTIVATE',
            data: '激活的RoboTerraJoystick对象的数量（0-1）'
          },
          {
            key: 3,
            title: 'JOYSTICK_X_UPDATE',
            emitted: '把RoboTerraJoystick沿X轴推动一段距离',
            source: 'RoboTerraJoystick object',
            type: 'JOYSTICK_X_UPDATE',
            data: 'X轴的位置（-5-5）'
          },
          {
            key: 4,
            title: 'JOYSTICK_Y_UPDATE',
            emitted: '把RoboTerraJoystick沿Y轴推动一段距离',
            source: 'RoboTerraJoystick object',
            type: 'JOYSTICK_Y_UPDATE',
            data: 'Y轴的位置（-5-5）'
          }
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: '启动RoboTerraJoystick，使得与之相关的事件（EVENT）能够被探测到。'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: '停止RoboTerraJoystick，不再处理与之相关的事件(EVENT)。'
          }
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)。',
            content: [
              {
                key: 1,
                text: '“joystick” 可以被任何其他名字替代，代指某个连接在被命名为“tom”的RoboCore上的萝卜太辣摇杆。',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraJoystick joystick;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'AI_1,AI_2是“joystick”与“tom”连接的端口ID。一个萝卜太辣摇杆可以与RoboCore上的AI_1和AI_2 端口连接。',
                code: 'tom.attach(joystick, AI_1, AI_2);'
              }
            ]
          },
          {
            key: 3,
            step: '设计处理每一个事件（EVENT）的算法。',
            content: [
              {
                key: 1,
                text: '查看这个事件（EVENT）是否来自“joystick”。.',
                code: 'if (EVENT.isFrom(joystick)){newline}',
              },
              {
                key: 2,
                text: '查看这个事件（EVENT）是否是JOYSTICK_X_UPDATE这个类别。',
                code: 'if(EVENT.isType(JOYSTICK_X_UPDATE)){newline}'
              },
              {
                key: 3,
                text: '查看这个事件（EVENT）是否是JOYSTICK_Y_UPDATE这个类别。',
                code: 'if(EVENT.isType(JOYSTICK_Y_UPDATE)){newline}'
              },
              {
                key: 4,
                text: '查看摇杆的X坐标值是否是2。',
                code: 'if(EVENT.isType(JOYSTICK_X_UPDATE)){newlineindentif (EVENT.getData () == 2){newlineindent}newline}'
              },
              {
                key: 5,
                text: '查看摇杆的Y坐标值是否是2。',
                code: 'if(EVENT.isType(JOYSTICK_Y_UPDATE)){newlineindentif (EVENT.getData () == 2){newlineindent}newline}'
              },
              {
                key: 6,
                text: '激活“joystick”.',
                code: 'joystick.activate ();'
              },
              {
                key: 7,
                text: '停止“joystick”.',
                code: 'joystick.deactivate ();'
              }
            ]
          }
        ]
      }
    ]
  },

  {
    num: 9,
    name: '事件',
    title: 'RoboTerraMotor 类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'activate()指令被调用',
            source: 'RoboTerraMotor object',
            type: 'ACTIVATE',
            data: '激活的RoboTerraMotor对象的数量（0-2）'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'activate()指令被调用',
            source: 'RoboTerraMotor object',
            type: 'DEACTIVATE',
            data: '激活的RoboTerraMotor对象的数量（0-2）'
          },
          {
            key: 3,
            title: 'MOTOR_SPEED_CHANGE',
            emitted: 'Calling rotate(speedToSet) or resume() when motor is paused',
            source: 'RoboTerraMotor object',
            type: 'MOTOR_SPEED_CHANGE',
            data: 'Speed of RoboTerraMotor (0-10).getData(1):Direction of RoboTerraMotor (1 or 0)'
          },
          {
            key: 4,
            title: 'MOTOR_REVERSE',
            emitted: '电机旋转时调用reverse()',
            source: 'RoboTerraMotor object',
            type: 'MOTOR_REVERSE',
            data: 'RoboTerraMotor速度(0-10).getData(1):RoboTerraMotor方向(1或0)'
          },
          {
            key: 5,
            title: 'MOTOR_SPEED_ZERO',
            emitted: '电机旋转时调用pause()',
            source: 'RoboTerraMotor object',
            type: 'MOTOR_SPEED_ZERO',
            data: 'RoboTerraMotor速度(0-10).getData(1):RoboTerraMotor方向(1或0)'
          }
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: '激活RoboTerraMotor，使得与之相关的其他指令一经调用马上执行动作，与其相关的事件可以被检测到。'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: '停止RoboTerraMotor，使得与之相关的指令被调用后也不执行动作，与其相关的事件也不可以被检测到。'
          },
          {
            key: 3,
            title: 'void rotate(int speedToSet)',
            content: '将RoboTerraMotor 设置到指定速度(-10 - 10)。'
          },
          {
            key: 4,
            title: 'void reverse',
            content: '保持当前RoboTerraMotor速度并使其反转'
          },
          {
            key: 5,
            title: 'void pause()',
            content: '暂停RoboTerraMotor 的运动。'
          },
          {
            key: 6,
            title: 'void resume()',
            content: "在RoboTerraMotor 被暂停后恢复RoboTerraMotor 的动作，使其继续以之前的速度和方向运动。"
          }
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)。',
            content: [
              {
                key: 1,
                text: '“wheel” 可以被任何其他名字替代，代指某个连接在被命名为“tom”的RoboCore上的萝卜太辣舵机。',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraMotor wheel;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'MOTOR_A是“wheel”与“tom”连接的端口ID。RoboTerraMotor可以与RoboCore上的任意MOTOR端口连接。',
                code: 'tom.attach(wheel,MOTOR_A);'
              }
            ]
          },
          {
            key: 3,
            step: '设计处理每一个事件（EVENT）的算法。',
            content: [
              {
                key: 1,
                text: '查看这个事件（EVENT）是否来自“wheel”。',
                code: 'if (EVENT.isFrom(wheel)){newline}',
              },
              {
                key: 2,
                text: '查看这个事件（EVENT）是否是MOTOR_SPEED_CHANGE类型。',
                code: 'if(EVENT.isType(MOTOR_SPEED_CHANGE)){newline}'
              },
              {
                key: 3,
                text: '查看这个事件（EVENT）是否是MOTOR_REVERSE类型。',
                code: 'if(EVENT.isType(MOTOR_REVERSE)){newline}'
              },
              {
                key: 4,
                text: '查看这个事件（EVENT）是否是MOTOR_SPEED_ZERO类型。',
                code: 'if(EVENT.isType(MOTOR_SPEED_ZERO)){newline}'
              },
              {
                key: 5,
                text: '查看RoboTerraMotor 当前的速度是否是2。',
                code: 'if(EVENT.isType(MOTOR_SPEED_CHANGE)){newlineindentif (EVENT.getData () == 2){newlineindent}newline}'
              },
              {
                key: 6,
                text: '查看RoboTerraMotor 当前是否向前转动。.',
                code: 'if(EVENT.isType(MOTOR_SPEED_CHANGE)){newlineindentif (EVENT.getData(1) == 1){newlineindent}newline}'
              },
              {
                key: 7,
                text: '激活 wheel.',
                code: 'wheel.activate()'
              },
              {
                key: 8,
                text: '停止 wheel.',
                code: 'wheel.deactivate()'
              },
              {
                key: 9,
                text: 'Rotate “wheel” with speed of 3.',
                code: 'wheel.rotate (3)'
              },
              {
                key: 10,
                text: 'Reverse the motion of “wheel”.',
                code: 'wheel.reverse()'
              },
              {
                key: 11,
                text: 'Pause the motion of “wheel”',
                code: 'wheel.pause()'
              },
              {
                key: 12,
                text: 'Resume the motion of “wheel”.',
                code: 'wheel.resume ()'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 10,
    name: '事件',
    title: 'RoboTerraIRTransmitter类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'DEACTIVATE',
            emitted: 'attach(name,port) 或deactivate()指令被调用',
            source: 'RoboTerraIRTransmitter object',
            type: 'DEACTIVATE',
            data: '激活的RoboTerraIRTransmitter对象的数量（0-1）'
          },
          {
            key: 2,
            title: 'ACTIVATE',
            emitted: 'activate()指令被调用',
            source: 'RoboTerraIRTransmitter object',
            type: 'ACTIVATE',
            data: '激活的RoboTerraIRTransmitter对象的数量（0-1）'
          },
          {
            key: 3,
            title: 'IR_MESSAGE_EMIT',
            emitted: 'emit(value ,address)指令被调用时',
            source: 'RoboTerraIRTransmitter object',
            type: 'IR_MESSAGE_EMIT',
            data: 'Value of the IR signal.getData(1):Address of the IR signal'
          }
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: '启动RoboTerraIRTransmitter，使得与之相关的事件（EVENT）能够被探测到。'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: '停止RoboTerraIRTransmitter，不再处理与之相关的事件(EVENT)。'
          },
          {
            key: 3,
            title: 'void emit(int value, int address)',
            content: 'RoboTerraIRTransmitter发射一个包含两个整数的红外信号，分别是信号的数值和地址'
          },
          {
            key: 4,
            title: 'void setBeacon(RoboTerraBeaconStatus statusToSet)',
            content: 'RoboTerraIRTransmitter发射一系列红外信息去控制NOC竞赛用龙珠，statusToSet代表需要设置的状态，可选的内容有: ON/OFF/RED/GREEN/YELLOW/BLUE/PURPLE/ORANGE/WHITE，分别代表直接点亮/关闭/点亮红色/点亮绿色/点亮黄色/点亮蓝色/点亮紫色/点亮橙色/点亮白色'
          }
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)。',
            content: [
              {
                key: 1,
                text: '“trans” 可以被任何其他名字替代，代指某个连接在被命名为“tom”的RoboCore上的萝卜太辣红外发射器。',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraMotor trans;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'IR_TRAN是“trans”与“tom”连接的端口ID。一个萝卜太辣红外发射器只可以与任何RoboCore上的IR_TRAN 端口连接。',
                code: 'tom.attach(trans, IR_TRAN);'
              }
            ]
          },
          {
            key: 3,
            step: '设计处理每一个事件（EVENT）的算法。',
            content: [
              {
                key: 1,
                text: '查看这个事件（EVENT）是否来自“trans”。',
                code: 'if (EVENT.isFrom(trans)){newline}',
              },
              {
                key: 2,
                text: '查看这个事件（EVENT）是否是IR_MESSAGE_EMIT这个类别。',
                code: 'if(EVENT.isType(IR_MESSAGE_EMIT)){newline}'
              },
              {
                key: 3,
                text: '查看“trans”发射的红外信号的值是否是2。',
                code: 'if(EVENT.isType(IR_MESSAGE_EMIT)){newlineindentif (EVENT.getData() == 2){newlineindent}newline}'
              },
              {
                key: 4,
                text: '查看“trans”发射的红外信号的地址是否是2。',
                code: 'if(EVENT.isType(IR_MESSAGE_EMIT)){newlineindentif (EVENT.getData(1) == 2){newlineindent}newline}'
              },
              {
                key: 5,
                text: '激活“trans”.',
                code: 'trans.activate();'
              },
              {
                key: 6,
                text: '停止 “trans”.',
                code: 'trans.deactivate();'
              },
              {
                key: 7,
                text: '“trans”通过红外通信发射一个16位的整数value和一个16位的整数address',
                code: 'trans.emit (address, value);'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    num: 11,
    name: '事件',
    title: 'RoboTerraIRReceiver 类',
    content: [
      {
        key: 1,
        name: '事件',
        content: [
          {
            key: 1,
            title: 'ACTIVATE',
            emitted: 'attach(name,port) 或activate()指令被调用',
            source: 'RoboTerraIRReceiver object',
            type: 'ACTIVATE',
            data: '激活的RoboTerraIRReceiver对象的数量（0-1）'
          },
          {
            key: 2,
            title: 'DEACTIVATE',
            emitted: 'deactivate()指令被调用',
            source: 'RoboTerraIRReceiver object',
            type: 'DEACTIVATE',
            data: '激活的RoboTerraIRReceiver对象的数量（0-1）'
          },
          {
            key: 3,
            title: 'IR_MESSAGE_RECIVE',
            emitted: '红外接收器收到新的红外信号',
            source: 'RoboTerraIRReceiver object',
            type: 'IR_MESSAGE_RECIVE',
            data: 'RoboTerraIRReceiver收到的红外信号的值'
          },
          {
            key: 4,
            title: 'IR_MESSAGE_REPEAT',
            emitted: '红外接收器收到与之前相同的红外信号',
            source: 'RoboTerraIRReceiver object',
            type: 'IR_MESSAGE_REPEAT',
            data: 'RoboTerraIRReceiver收到的红外信号的值'
          }
        ]
      },
      {
        key: 2,
        name: '指令',
        content: [
          {
            key: 1,
            title: 'void activate()',
            content: '启动RoboTerraIRReceiver，使得与之相关的事件（EVENT）能够被探测到。'
          },
          {
            key: 2,
            title: 'void deactivate()',
            content: '停止RoboTerraIRReceiver，不再处理与之相关的事件(EVENT)。'
          }
        ]
      },
      {
        key: 3,
        name: '示例',
        content: [
          {
            key: 1,
            step: '给RoboCore和所有将连接在机器人上的电子元件命名。这些名字将会是事件(EVENT)的来源(Event Source)。',
            content: [
              {
                key: 1,
                text: '“receiver” 可以被任何其他名字替代，代指某个连接在被命名为“tom”的RoboCore上的萝卜太辣红外接收器。',
                code: 'RoboTerraRoboCore tom;newlineRoboTerraMotor Receiver;'
              }
            ]
          },
          {
            key: 2,
            step: '通过将电子元件连接到RoboCore上并执行attach(name,port)指令，初始化机器人。',
            content: [
              {
                key: 1,
                text: 'DIO_1是“receiver”与“tom”连接的端口ID。一个萝卜太辣红外接收器可以与任何RoboCore上的DIO 端口连接。',
                code: 'tom.attach(Receiver, DIO_1);'
              }
            ]
          },
          {
            key: 3,
            step: '设计处理每一个事件（EVENT）的算法。',
            content: [
              {
                key: 1,
                text: '查看这个事件（EVENT）是否来自“receiver”。',
                code: 'if (EVENT.isFrom(Receiver)){newline}',
              },
              {
                key: 2,
                text: '查看这个事件（EVENT）是否是IR_MESSAGE_RECEIVE这个类别。',
                code: 'if(EVENT.isType(IR_MESSAGE_RECIVE)){newline}'
              },
              {
                key: 3,
                text: '查看这个事件（EVENT）是否是IR_MESSAGE_REPEAT这个类别。',
                code: 'if(EVENT.isType(IR_MESSAGE_REPEAT)){newline}'
              },
              {
                key: 4,
                text: '查看“receiver”收到的红外信号的值是否是2。',
                code: 'if(EVENT.isType(IR_MESSAGE_INTERFERE)){newline}'
              },
              {
                key: 5,
                text: '查看“receiver”收到的红外信号的地址是否是2。',
                code: 'if (EVENT.isType(IR_MESSAGE_RECIVE)){newlineindentif (EVENT.getData() == 2){newlineindent}newline}'
              },
              {
                key: 6,
                text: '查看“receiver”收到的红外信号的地址是否是2。',
                code: 'if (EVENT.isType(IR_MESSAGE_RECIVE)){newlineindentif (EVENT.getData(1) == 2){newlineindent}newline}'
              },
              {
                key: 7,
                text: '激活 “Receiver”.',
                code: 'trans.activate();'
              },
              {
                key: 8,
                text: '停止 “Receiver”.',
                code: 'trans.deactivate();'
              }
            ]
          }
        ]
      }
    ]
  }

] };


export default DocumentationDataCN;
