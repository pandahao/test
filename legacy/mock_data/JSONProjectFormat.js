/* general notes:
- all quotatation marks must be preceded by a backwards slash. eg: \"Attach\"
- except for overview description, remember to limit the length of your text blocks

- IMPORTANT: ALL FIELDS ARE MANDATORY
*/

/* PROJECT IDENTIFIERS */
const stageID = 1;
const projectID = 1; // If chinese version, add 100. Eg. the chinese version of this project would have projectID 101
const projectName = 'Project Title';
const language = 'LANGUAGE'; // CHINESE or ENGLISH

/* OVERVIEW CONTENT */
const imageLink = 'link to image';
const videoLink = 'link to video';
const modelLink = 'link to 3D model';
const overviewDescription = 'A paragraph or two that describes the project.';
const knowledge =
  [
    'concept 1',
    'concept 2',
    'concept 3',
    'concept 4',
  ];
// parts with icons: button, servo, led, sound, light, tape, transmitter, receiver, motor, controller
// format is {"part" : quantity}
const partsUsed =
  [
    { button: 1 },
    { servo: 3 },
    { tape: 34 },
    { 'custom part': 1 },
    //"Mechanical pieces, nuts and screws" NO
  ];

/* BUILD CHALLENGE */
const challengeOne = {
  level: 1,
  title: 'Build Challenge Title',
  type: 'build',
  content:
  {
    introduction: 'A sentence or two describing this build challenge.',
        // NOTE: number of text lines must match the number of steps
    blocks: [
      'Text that shows up the first time next step is clicked.',
      'Text that shows up the second time next step is clicked.',
      'Text that shows up the third and final time next step is clicked.'
    ],
  },
};

/* QUIZ CHALLENGE */
const challengeTwo = {
  level: 2,
  title: 'Quiz Challenge Title',
  type: 'quiz',
  content:
  {
    introduction: [
                { text: 'Text for the question here.' },
                { code: 'Code for the question here.' },
                { text: 'Text' },
    ],
    blocks: [
      {
        question: 'this is the question text.',
        choices: [
          'Yes',
          'No',
          "I don't know",
          'Daichi is a leaky battery'
        ],
        answer: 1,
        explanation: 'A sentence or two explaining why the answer is correct/why the others are wrong.'
      },
      {
        question: 'Is Daichi a leaky battery?',
        choices: [
          'Yes',
          'No',
          "I don't know",
        ],
        answer: 1,
        explanation: 'A sentence or two explaining why the answer is correct/why the others are wrong.'
      },
    ],
  },
};

/* CODE CHALLENGE */
const challengeThree = {
  level: 3,
  title: 'Code Challenge Title',
  type: 'code',
  content:
  {
    introduction: 'A sentence or two describing this code challenge.',
    conclusion: 'A sentence or two congratulating the user and summarizing this code challenge.',

    resetCode: true,
    start_template: '#include "ROBOTERRA.h"\r\n\r\n/*--------------------- STEP 1 ---------------------*/\r\n// Name your RoboCore and all electronics attached it.\r\n// These names are considered as eventSource of EVENT. \r\nRoboTerraRoboCore tom;\r\n\r\n\r\n\r\n/*--------------------- STEP 2 ---------------------*/\r\n// Initialize your robot by attaching electronics. \r\n// Optionally, define STATE of your RoboCore. \r\nvoid initializeRoboTerraRobot() {\r\n \r\n\r\n\r\n}\r\n\r\n/*--------------------- STEP 3 ---------------------*/\r\n/* Design the algorithm to handle each EVENT. \r\n * Call below functions to get EVENT information.\r\n * a. bool EVENT.isFrom(eventSource)\r\n * b. bool EVENT.isType(eventType)\r\n * c. int EVENT.getData()\r\n * With EVENT information, make decisions by calling\r\n * Command Functions of each object to take action.\r\n */\r\nvoid handleRoboTerraEvent() {\r\n \r\n\r\n\r\n}',
    ending_template: '#include "ROBOTERRA.h"\r\n\r\n/*--------------------- STEP 1 ---------------------*/\r\n// Name your RoboCore and all electronics attached it.\r\n// These names are considered as eventSource of EVENT. \r\nRoboTerraRoboCore tom;\r\n\r\n\r\n\r\n/*--------------------- STEP 2 ---------------------*/\r\n// Initialize your robot by attaching electronics. \r\n// Optionally, define STATE of your RoboCore. \r\nvoid initializeRoboTerraRobot() {\r\n \r\n\r\n\r\n}\r\n\r\n/*--------------------- STEP 3 ---------------------*/\r\n/* Design the algorithm to handle each EVENT. \r\n * Call below functions to get EVENT information.\r\n * a. bool EVENT.isFrom(eventSource)\r\n * b. bool EVENT.isType(eventType)\r\n * c. int EVENT.getData()\r\n * With EVENT information, make decisions by calling\r\n * Command Functions of each object to take action.\r\n */\r\nvoid handleRoboTerraEvent() {\r\n \r\n\r\n\r\n}',

    blocks: [
                { text: 'Text that shows up for one instruction.' },
                { text: 'Text that shows up for one instruction.',
                code: 'Code associated with text above. Same format as start/ending template.' },
    ],

            // Event format: PORT-EVENT-DATA-DATA. If data field is not relevant, use "N"
    checklist: [
      {
        text: 'Checklist event that the user sees / eg. Launch Robocore.',
        event: [
          'ROBOCORE-ROBOCORE_LAUNCH-N-N'
        ]
      },
      {
        text: 'Checklist event that the user sees / eg. Turn on your two LEDs',
        event: [
                        // within each event array, events are not order-dependent
        ]
      },
    ],

    hint: {
      text: 'This is the text of the hint.',
      code: 'This is the code of the hint. Same format as code start/ending template.',
    },
  },
};

export const exampleProject = {
  stageID,
  projectID,
  projectName,
  language,
  imageLink,
  overview: {
    videoLink,
    modelLink,
    description: overviewDescription,
    learn: knowledge,
    parts: partsUsed
  },
  challengeData: [
    challengeOne,
    challengeTwo,
    challengeThree,
  ]
};
