import { S1P1 } from 'legacy_mock/projects/S1P1';
import { S1P1C } from 'legacy_mock/projects/S1P1C';
import { S4P1 } from 'legacy_mock/projects/S4P1';
import { S4P1C } from 'legacy_mock/projects/S4P1C';
import { S5P1 } from 'legacy_mock/projects/S5P1';
import { S5P1C } from 'legacy_mock/projects/S5P1C';
import { S8P1 } from 'legacy_mock/projects/S8P1';
import { S8P1C } from 'legacy_mock/projects/S8P1C';
import { Onboarding } from 'legacy_mock/projects/Onboarding';
import { Onboarding_CHINESE } from 'legacy_mock/projects/Onboarding_CHINESE';
import { OldCastleRock } from 'legacy_mock/projects/OldCastleRock';
import { OldCastleRockC } from 'legacy_mock/projects/OldCastleRockC';
import { S0P1E } from 'legacy_mock/projects/S0P1E';
import { S0P1C } from 'legacy_mock/projects/S0P1C';
import { S0P2E } from 'legacy_mock/projects/S0P2E';
import { S0P2C } from 'legacy_mock/projects/S0P2C';

const ProjectsData = { projects:
[
  Onboarding,
  Onboarding_CHINESE,
  S1P1,
  S1P1C,
  S4P1,
  S4P1C,
  S5P1,
  S5P1C,
  S8P1,
  S8P1C,
  OldCastleRock,
  OldCastleRockC,
  S0P1E,
  S0P1C,
  S0P2E,
  S0P2C
]
};


export default ProjectsData;
