ace.define('ace/mode/doc_comment_highlight_rules', ['require', 'exports', 'module', 'ace/lib/oop', 'ace/mode/text_highlight_rules'], (acequire, exports, module) => {
  const oop = acequire('../lib/oop');
  const TextHighlightRules = acequire('./text_highlight_rules').TextHighlightRules;

  const DocCommentHighlightRules = function () {
    this.$rules = {
      start: [{
        token: 'comment.doc.tag',
        regex: '@[\\w\\d_]+' // TODO: fix email addresses
      },
            DocCommentHighlightRules.getTagRule(),
        {
          defaultToken: 'comment.doc',
          caseInsensitive: true
        }]
    };
  };

  oop.inherits(DocCommentHighlightRules, TextHighlightRules);

  DocCommentHighlightRules.getTagRule = function (start) {
    return {
      token: 'comment.doc.tag.storage.type',
      regex: '\\b(?:TODO|FIXME|XXX|HACK)\\b'
    };
  };

  DocCommentHighlightRules.getStartRule = function (start) {
    return {
      token: 'comment.doc', // doc comment
      regex: '\\/\\*(?=\\*)',
      next: start
    };
  };

  DocCommentHighlightRules.getEndRule = function (start) {
    return {
      token: 'comment.doc', // closing comment
      regex: '\\*\\/',
      next: start
    };
  };


  exports.DocCommentHighlightRules = DocCommentHighlightRules;
});

ace.define('ace/mode/c_cpp_highlight_rules', ['require', 'exports', 'module', 'ace/lib/oop', 'ace/mode/doc_comment_highlight_rules', 'ace/mode/text_highlight_rules'], (acequire, exports, module) => {
  const oop = acequire('../lib/oop');
  const DocCommentHighlightRules = acequire('./doc_comment_highlight_rules').DocCommentHighlightRules;
  const TextHighlightRules = acequire('./text_highlight_rules').TextHighlightRules;
  const cFunctions = exports.cFunctions = '\\b(?:hypot(?:f|l)?|s(?:scanf|ystem|nprintf|ca(?:nf|lb(?:n(?:f|l)?|ln(?:f|l)?))|i(?:n(?:h(?:f|l)?|f|l)?|gn(?:al|bit))|tr(?:s(?:tr|pn)|nc(?:py|at|mp)|c(?:spn|hr|oll|py|at|mp)|to(?:imax|d|u(?:l(?:l)?|max)|k|f|l(?:d|l)?)|error|pbrk|ftime|len|rchr|xfrm)|printf|et(?:jmp|vbuf|locale|buf)|qrt(?:f|l)?|w(?:scanf|printf)|rand)|n(?:e(?:arbyint(?:f|l)?|xt(?:toward(?:f|l)?|after(?:f|l)?))|an(?:f|l)?)|c(?:s(?:in(?:h(?:f|l)?|f|l)?|qrt(?:f|l)?)|cos(?:h(?:f)?|f|l)?|imag(?:f|l)?|t(?:ime|an(?:h(?:f|l)?|f|l)?)|o(?:s(?:h(?:f|l)?|f|l)?|nj(?:f|l)?|pysign(?:f|l)?)|p(?:ow(?:f|l)?|roj(?:f|l)?)|e(?:il(?:f|l)?|xp(?:f|l)?)|l(?:o(?:ck|g(?:f|l)?)|earerr)|a(?:sin(?:h(?:f|l)?|f|l)?|cos(?:h(?:f|l)?|f|l)?|tan(?:h(?:f|l)?|f|l)?|lloc|rg(?:f|l)?|bs(?:f|l)?)|real(?:f|l)?|brt(?:f|l)?)|t(?:ime|o(?:upper|lower)|an(?:h(?:f|l)?|f|l)?|runc(?:f|l)?|gamma(?:f|l)?|mp(?:nam|file))|i(?:s(?:space|n(?:ormal|an)|cntrl|inf|digit|u(?:nordered|pper)|p(?:unct|rint)|finite|w(?:space|c(?:ntrl|type)|digit|upper|p(?:unct|rint)|lower|al(?:num|pha)|graph|xdigit|blank)|l(?:ower|ess(?:equal|greater)?)|al(?:num|pha)|gr(?:eater(?:equal)?|aph)|xdigit|blank)|logb(?:f|l)?|max(?:div|abs))|di(?:v|fftime)|_Exit|unget(?:c|wc)|p(?:ow(?:f|l)?|ut(?:s|c(?:har)?|wc(?:har)?)|error|rintf)|e(?:rf(?:c(?:f|l)?|f|l)?|x(?:it|p(?:2(?:f|l)?|f|l|m1(?:f|l)?)?))|v(?:s(?:scanf|nprintf|canf|printf|w(?:scanf|printf))|printf|f(?:scanf|printf|w(?:scanf|printf))|w(?:scanf|printf)|a_(?:start|copy|end|arg))|qsort|f(?:s(?:canf|e(?:tpos|ek))|close|tell|open|dim(?:f|l)?|p(?:classify|ut(?:s|c|w(?:s|c))|rintf)|e(?:holdexcept|set(?:e(?:nv|xceptflag)|round)|clearexcept|testexcept|of|updateenv|r(?:aiseexcept|ror)|get(?:e(?:nv|xceptflag)|round))|flush|w(?:scanf|ide|printf|rite)|loor(?:f|l)?|abs(?:f|l)?|get(?:s|c|pos|w(?:s|c))|re(?:open|e|ad|xp(?:f|l)?)|m(?:in(?:f|l)?|od(?:f|l)?|a(?:f|l|x(?:f|l)?)?))|l(?:d(?:iv|exp(?:f|l)?)|o(?:ngjmp|cal(?:time|econv)|g(?:1(?:p(?:f|l)?|0(?:f|l)?)|2(?:f|l)?|f|l|b(?:f|l)?)?)|abs|l(?:div|abs|r(?:int(?:f|l)?|ound(?:f|l)?))|r(?:int(?:f|l)?|ound(?:f|l)?)|gamma(?:f|l)?)|w(?:scanf|c(?:s(?:s(?:tr|pn)|nc(?:py|at|mp)|c(?:spn|hr|oll|py|at|mp)|to(?:imax|d|u(?:l(?:l)?|max)|k|f|l(?:d|l)?|mbs)|pbrk|ftime|len|r(?:chr|tombs)|xfrm)|to(?:b|mb)|rtomb)|printf|mem(?:set|c(?:hr|py|mp)|move))|a(?:s(?:sert|ctime|in(?:h(?:f|l)?|f|l)?)|cos(?:h(?:f|l)?|f|l)?|t(?:o(?:i|f|l(?:l)?)|exit|an(?:h(?:f|l)?|2(?:f|l)?|f|l)?)|b(?:s|ort))|g(?:et(?:s|c(?:har)?|env|wc(?:har)?)|mtime)|r(?:int(?:f|l)?|ound(?:f|l)?|e(?:name|alloc|wind|m(?:ove|quo(?:f|l)?|ainder(?:f|l)?))|a(?:nd|ise))|b(?:search|towc)|m(?:odf(?:f|l)?|em(?:set|c(?:hr|py|mp)|move)|ktime|alloc|b(?:s(?:init|towcs|rtowcs)|towc|len|r(?:towc|len))))\\b';

  const c_cppHighlightRules = function () {
    const keywordControls = (
            'break|case|continue|default|do|else|for|goto|if|_Pragma|' +
            'return|switch|while|catch|operator|try|throw|using'
        );

    const storageType = (
            'asm|__asm__|auto|bool|_Bool|char|_Complex|double|enum|float|' +
            '_Imaginary|int|long|short|signed|struct|typedef|union|unsigned|void|' +
            'class|wchar_t|template|char16_t|char32_t'
        );

    const roboTerraTypes = (
            'RoboTerraRoboCore|RoboTerraLED|RoboTerraButton|RoboTerraEventType|' +
            'RoboTerraServo|RoboTerraLightSensor|RoboTerraSoundSensor|RoboTerraTapeSensor|' +
            'RoboTerraJoystick|RoboTerraIRTransmitter|RoboTerraIRReceiver|RoboTerraMotor'
        );

    const storageModifiers = (
            'const|extern|register|restrict|static|volatile|inline|private|' +
            'protected|public|friend|explicit|virtual|export|mutable|typename|' +
            'constexpr|new|delete|alignas|alignof|decltype|noexcept|thread_local'
        );

    const keywordOperators = (
            'and|and_eq|bitand|bitor|compl|not|not_eq|or|or_eq|typeid|xor|xor_eq' +
            'const_cast|dynamic_cast|reinterpret_cast|static_cast|sizeof|namespace'
        );

    const builtinConstants = (
            'NULL|true|false|TRUE|FALSE|nullptr|' +
            // Basic Robocore
            'ROBOCORE_LAUNCH|ROBOCORE_TERMINATE|' +
            // Buttons
            'BUTTON_PRESS|BUTTON_RELEASE|ACTIVATE|DEACTIVATE|' +
            // Light Sensor
            'DARK_ENTER|DARK_LEAVE|' +
            // Sound Sensor
            'SOUND_BEGIN|SOUND_END|' +
            // Tape Sensor
            'BLACK_TAPE_ENTER|BLACK_TAPE_LEAVE|' +
            // Servo
            'SERVO_MOVE_BEGIN|SERVO_INCREASE_END|SERVO_DECREASE_END|' +
            // LED
            'LED_TURNON|LED_TURNOFF|SLOWBLINK_BEGIN|FASTBLINK_BEGIN|BLINK_END'
        );

    const builtinFunctions = (
                'terminate|attach|isFrom|isType|getData|type|' +
                // State Functions
                'define|enter|isIn|wasIn|' +
                // Servo functions
                'activate|deactivate|rotate|pause|resume|' +
                // Led Functions
                'turnOn|turnOff|slowBlink|fastBlink|toggle|stopBlink'
        );

    const coreComponents = (
                'DIO_1|DIO_2|DIO_3|DIO_4|DIO_5|DIO_6|DIO_7|DIO_8|DIO_9|' +
                'MOTOR_A|MOTOR_B|SERVO_A|SERVO_B|SERVO_C|SERVO_D|' +
                'AI_1|AI_2|IR_TRAN'
        );

    const keywordMapper = this.$keywords = this.createKeywordMapper({
      'keyword.control': keywordControls,
      'storage.type': storageType,
      'roboterra.type': roboTerraTypes,
      'storage.modifier': storageModifiers,
      'keyword.operator': keywordOperators,
      'variable.language': 'this',
      'constant.language': builtinConstants,
      'keyword.functions': builtinFunctions,
      'keyword.core': coreComponents
    }, 'identifier');

    const identifierRe = '[a-zA-Z\\$_\u00a1-\uffff][a-zA-Z\d\\$_\u00a1-\uffff]*\\b';
    const escapeRe = /\\(?:['"?\\abfnrtv]|[0-7]{1,3}|x[a-fA-F\d]{2}|u[a-fA-F\d]{4}U[a-fA-F\d]{8}|.)/.source;

    this.$rules = {
      start: [
        {
          token: 'comment',
          regex: '//$',
          next: 'start'
        }, {
          token: 'comment',
          regex: '//',
          next: 'singleLineComment'
        },
        DocCommentHighlightRules.getStartRule('doc-start'),
        {
          token: 'comment', // multi line comment
          regex: '\\/\\*',
          next: 'comment'
        }, {
          token: 'string', // character
          regex: `'(?:${escapeRe}|.)'`
        }, {
          token: 'string.start',
          regex: '"',
          stateName: 'qqstring',
          next: [
                        { token: 'string', regex: /\\\s*$/, next: 'qqstring' },
                        { token: 'constant.language.escape', regex: escapeRe },
                        { token: 'constant.language.escape', regex: /%[^'"\\]/ },
                        { token: 'string.end', regex: '"|$', next: 'start' },
                        { defaultToken: 'string' }
          ]
        }, {
          token: 'string.start',
          regex: 'R"\\(',
          stateName: 'rawString',
          next: [
                        { token: 'string.end', regex: '\\)"', next: 'start' },
                        { defaultToken: 'string' }
          ]
        }, {
          token: 'constant.numeric', // hex
          regex: '0[xX][0-9a-fA-F]+(L|l|UL|ul|u|U|F|f|ll|LL|ull|ULL)?\\b'
        }, {
            token: 'constant.numeric', // float
            regex: '[+-]?\\d+(?:(?:\\.\\d*)?(?:[eE][+-]?\\d+)?)?(L|l|UL|ul|u|U|F|f|ll|LL|ull|ULL)?\\b'
          }, {
              token: 'keyword', // pre-compiler directives
              regex: '#\\s*(?:include|import|pragma|line|define|undef)\\b',
              next: 'directive'
            }, {
              token: 'keyword', // special case pre-compiler directive
              regex: '#\\s*(?:endif|if|ifdef|else|elif|ifndef)\\b'
            }, {
              token: 'roboFunction',
              regex: `(${builtinFunctions})(?=\\()`,
              next: 'lparen'
            }, {
                token: 'roboType',
                regex: roboTerraTypes,
                next: 'variable'
              }, {
                  token: 'variable.call',
                  regex: '[a-z][0-9a-zA-Z]*(?=\\.)'
                }, {
                  token: 'support.function.C99.c',
                  regex: cFunctions
                }, {
                  token: keywordMapper,
                  regex: '[a-zA-Z_$][a-zA-Z0-9_$]*\\b'
                }, {
                  token: 'keyword.operator',
                  regex: '!|\\$|%|&|\\*|\\-\\-|\\-|\\+\\+|\\+|~|==|=|!=|<=|>=|<<=|>>=|>>>=|<>|<|>|!|&&|\\|\\||\\?\\:|\\*=|%=|\\+=|\\-=|&=|\\^=|\\b(?:in|new|delete|typeof|void)'
                }, {
                  token: 'punctuation.operator',
                  regex: '\\?|\\:|\\,|\\;|\\.'
                }, {
                  token: 'paren.lparen',
                  regex: '[[({]'
                }, {
                  token: 'paren.rparen',
                  regex: '[\\])}]'
                }, {
                  token: 'text',
                  regex: '\\s+'
                }
      ],
      comment: [
        {
          token: 'comment', // closing comment
          regex: '.*?\\*\\/',
          next: 'start'
        }, {
          token: 'comment', // comment spanning whole line
          regex: '.+'
        }
      ],
      singleLineComment: [
        {
          token: 'comment',
          regex: /\\$/,
          next: 'singleLineComment'
        }, {
          token: 'comment',
          regex: /$/,
          next: 'start'
        }, {
          defaultToken: 'comment'
        }
      ],
      directive: [
        {
          token: 'constant.other.multiline',
          regex: /\\/
        },
        {
          token: 'constant.other.multiline',
          regex: /.*\\/
        },
        {
          token: 'constant.other',
          regex: '\\s*<.+?>',
          next: 'start'
        },
        {
          token: 'constant.other', // single line
          regex: '\\s*["](?:(?:\\\\.)|(?:[^"\\\\]))*?["]',
          next: 'start'
        },
        {
          token: 'constant.other', // single line
          regex: "\\s*['](?:(?:\\\\.)|(?:[^'\\\\]))*?[']",
          next: 'start'
        },
        {
          token: 'constant.other',
          regex: /[^\\\/]+/,
          next: 'start'
        }
      ],
      lparen: [
        {
          token: 'lparen.function',
          regex: '[[({]',
          next: ['variable', 'start']
        }
      ],
      variable: [
        {
          token: 'variable.declaration',
          regex: '[a-z][0-9a-zA-Z]*',
          next: 'start'
        }, {
          token: 'variable.call',
          regex: '[a-z][0-9a-zA-Z]*',
          next: 'start'
        }, {
          token: 'variable.constants',
          regex: builtinConstants,
          next: 'start'
        }, {
          token: 'variable.empty',
          regex: '(?=\\))',
          next: 'start'
        }
      ]
    };

    this.embedRules(DocCommentHighlightRules, 'doc-',
            [DocCommentHighlightRules.getEndRule('start')]);
    this.normalizeRules();
  };

  oop.inherits(c_cppHighlightRules, TextHighlightRules);

  exports.c_cppHighlightRules = c_cppHighlightRules;
});

ace.define('ace/mode/matching_brace_outdent', ['require', 'exports', 'module', 'ace/range'], (acequire, exports, module) => {
  const Range = acequire('../range').Range;

  const MatchingBraceOutdent = function () {};

  (function () {
    this.checkOutdent = function (line, input) {
      if (!/^\s+$/.test(line))
        return false;

      return /^\s*\}/.test(input);
    };

    this.autoOutdent = function (doc, row) {
      const line = doc.getLine(row);
      const match = line.match(/^(\s*\})/);

      if (!match) return 0;

      const column = match[1].length;
      const openBracePos = doc.findMatchingBracket({ row, column });

      if (!openBracePos || openBracePos.row == row) return 0;

      const indent = this.$getIndent(doc.getLine(openBracePos.row));
      doc.replace(new Range(row, 0, row, column - 1), indent);
    };

    this.$getIndent = function (line) {
      return line.match(/^\s*/)[0];
    };
  }).call(MatchingBraceOutdent.prototype);

  exports.MatchingBraceOutdent = MatchingBraceOutdent;
});

ace.define('ace/mode/behaviour/cstyle', ['require', 'exports', 'module', 'ace/lib/oop', 'ace/mode/behaviour', 'ace/token_iterator', 'ace/lib/lang'], (acequire, exports, module) => {
  const oop = acequire('../../lib/oop');
  const Behaviour = acequire('../behaviour').Behaviour;
  const TokenIterator = acequire('../../token_iterator').TokenIterator;
  const lang = acequire('../../lib/lang');

  const SAFE_INSERT_IN_TOKENS =
        ['text', 'paren.rparen', 'punctuation.operator'];
  const SAFE_INSERT_BEFORE_TOKENS =
        ['text', 'paren.rparen', 'punctuation.operator', 'comment'];

  let context;
  let contextCache = {};
  const initContext = function (editor) {
    let id = -1;
    if (editor.multiSelect) {
      id = editor.selection.index;
      if (contextCache.rangeCount != editor.multiSelect.rangeCount)
        contextCache = { rangeCount: editor.multiSelect.rangeCount };
    }
    if (contextCache[id])
      return context = contextCache[id];
    context = contextCache[id] = {
      autoInsertedBrackets: 0,
      autoInsertedRow: -1,
      autoInsertedLineEnd: '',
      maybeInsertedBrackets: 0,
      maybeInsertedRow: -1,
      maybeInsertedLineStart: '',
      maybeInsertedLineEnd: ''
    };
  };

  const getWrapped = function (selection, selected, opening, closing) {
    const rowDiff = selection.end.row - selection.start.row;
    return {
      text: opening + selected + closing,
      selection: [
        0,
        selection.start.column + 1,
        rowDiff,
        selection.end.column + (rowDiff ? 0 : 1)
      ]
    };
  };

  const CstyleBehaviour = function () {
    this.add('braces', 'insertion', function (state, action, editor, session, text) {
      const cursor = editor.getCursorPosition();
      const line = session.doc.getLine(cursor.row);
      if (text == '{') {
        initContext(editor);
        const selection = editor.getSelectionRange();
        const selected = session.doc.getTextRange(selection);
        if (selected !== '' && selected !== '{' && editor.getWrapBehavioursEnabled()) {
          return getWrapped(selection, selected, '{', '}');
        } else if (CstyleBehaviour.isSaneInsertion(editor, session)) {
          if (/[\]\}\)]/.test(line[cursor.column]) || editor.inMultiSelectMode) {
            CstyleBehaviour.recordAutoInsert(editor, session, '}');
            return {
              text: '{}',
              selection: [1, 1]
            };
          } else {
            CstyleBehaviour.recordMaybeInsert(editor, session, '{');
            return {
              text: '{',
              selection: [1, 1]
            };
          }
        }
      } else if (text == '}') {
        initContext(editor);
        var rightChar = line.substring(cursor.column, cursor.column + 1);
        if (rightChar == '}') {
          const matching = session.$findOpeningBracket('}', { column: cursor.column + 1, row: cursor.row });
          if (matching !== null && CstyleBehaviour.isAutoInsertedClosing(cursor, line, text)) {
            CstyleBehaviour.popAutoInsertedClosing();
            return {
              text: '',
              selection: [1, 1]
            };
          }
        }
      } else if (text == '\n' || text == '\r\n') {
        initContext(editor);
        let closing = '';
        if (CstyleBehaviour.isMaybeInsertedClosing(cursor, line)) {
          closing = lang.stringRepeat('}', context.maybeInsertedBrackets);
          CstyleBehaviour.clearMaybeInsertedClosing();
        }
        var rightChar = line.substring(cursor.column, cursor.column + 1);
        if (rightChar === '}') {
          const openBracePos = session.findMatchingBracket({ row: cursor.row, column: cursor.column + 1 }, '}');
          if (!openBracePos)
            return null;
          var next_indent = this.$getIndent(session.getLine(openBracePos.row));
        } else if (closing) {
          var next_indent = this.$getIndent(line);
        } else {
          CstyleBehaviour.clearMaybeInsertedClosing();
          return;
        }
        const indent = next_indent + session.getTabString();

        return {
          text: `\n${indent}\n${next_indent}${closing}`,
          selection: [1, indent.length, 1, indent.length]
        };
      } else {
        CstyleBehaviour.clearMaybeInsertedClosing();
      }
    });

    this.add('braces', 'deletion', (state, action, editor, session, range) => {
      const selected = session.doc.getTextRange(range);
      if (!range.isMultiLine() && selected == '{') {
        initContext(editor);
        const line = session.doc.getLine(range.start.row);
        const rightChar = line.substring(range.end.column, range.end.column + 1);
        if (rightChar == '}') {
          range.end.column++;
          return range;
        } else {
          context.maybeInsertedBrackets--;
        }
      }
    });

    this.add('parens', 'insertion', (state, action, editor, session, text) => {
      if (text == '(') {
        initContext(editor);
        const selection = editor.getSelectionRange();
        const selected = session.doc.getTextRange(selection);
        if (selected !== '' && editor.getWrapBehavioursEnabled()) {
          return getWrapped(selection, selected, '(', ')');
        } else if (CstyleBehaviour.isSaneInsertion(editor, session)) {
          CstyleBehaviour.recordAutoInsert(editor, session, ')');
          return {
            text: '()',
            selection: [1, 1]
          };
        }
      } else if (text == ')') {
        initContext(editor);
        const cursor = editor.getCursorPosition();
        const line = session.doc.getLine(cursor.row);
        const rightChar = line.substring(cursor.column, cursor.column + 1);
        if (rightChar == ')') {
          const matching = session.$findOpeningBracket(')', { column: cursor.column + 1, row: cursor.row });
          if (matching !== null && CstyleBehaviour.isAutoInsertedClosing(cursor, line, text)) {
            CstyleBehaviour.popAutoInsertedClosing();
            return {
              text: '',
              selection: [1, 1]
            };
          }
        }
      }
    });

    this.add('parens', 'deletion', (state, action, editor, session, range) => {
      const selected = session.doc.getTextRange(range);
      if (!range.isMultiLine() && selected == '(') {
        initContext(editor);
        const line = session.doc.getLine(range.start.row);
        const rightChar = line.substring(range.start.column + 1, range.start.column + 2);
        if (rightChar == ')') {
          range.end.column++;
          return range;
        }
      }
    });

    this.add('brackets', 'insertion', (state, action, editor, session, text) => {
      if (text == '[') {
        initContext(editor);
        const selection = editor.getSelectionRange();
        const selected = session.doc.getTextRange(selection);
        if (selected !== '' && editor.getWrapBehavioursEnabled()) {
          return getWrapped(selection, selected, '[', ']');
        } else if (CstyleBehaviour.isSaneInsertion(editor, session)) {
          CstyleBehaviour.recordAutoInsert(editor, session, ']');
          return {
            text: '[]',
            selection: [1, 1]
          };
        }
      } else if (text == ']') {
        initContext(editor);
        const cursor = editor.getCursorPosition();
        const line = session.doc.getLine(cursor.row);
        const rightChar = line.substring(cursor.column, cursor.column + 1);
        if (rightChar == ']') {
          const matching = session.$findOpeningBracket(']', { column: cursor.column + 1, row: cursor.row });
          if (matching !== null && CstyleBehaviour.isAutoInsertedClosing(cursor, line, text)) {
            CstyleBehaviour.popAutoInsertedClosing();
            return {
              text: '',
              selection: [1, 1]
            };
          }
        }
      }
    });

    this.add('brackets', 'deletion', (state, action, editor, session, range) => {
      const selected = session.doc.getTextRange(range);
      if (!range.isMultiLine() && selected == '[') {
        initContext(editor);
        const line = session.doc.getLine(range.start.row);
        const rightChar = line.substring(range.start.column + 1, range.start.column + 2);
        if (rightChar == ']') {
          range.end.column++;
          return range;
        }
      }
    });

    this.add('string_dquotes', 'insertion', (state, action, editor, session, text) => {
      if (text == '"' || text == "'") {
        initContext(editor);
        const quote = text;
        const selection = editor.getSelectionRange();
        const selected = session.doc.getTextRange(selection);
        if (selected !== '' && selected !== "'" && selected != '"' && editor.getWrapBehavioursEnabled()) {
          return getWrapped(selection, selected, quote, quote);
        } else if (!selected) {
          const cursor = editor.getCursorPosition();
          const line = session.doc.getLine(cursor.row);
          const leftChar = line.substring(cursor.column - 1, cursor.column);
          const rightChar = line.substring(cursor.column, cursor.column + 1);

          const token = session.getTokenAt(cursor.row, cursor.column);
          const rightToken = session.getTokenAt(cursor.row, cursor.column + 1);
          if (leftChar == '\\' && token && /escape/.test(token.type))
            return null;

          const stringBefore = token && /string|escape/.test(token.type);
          const stringAfter = !rightToken || /string|escape/.test(rightToken.type);

          let pair;
          if (rightChar == quote) {
            pair = stringBefore !== stringAfter;
          } else {
            if (stringBefore && !stringAfter)
              return null; // wrap string with different quote
            if (stringBefore && stringAfter)
              return null; // do not pair quotes inside strings
            const wordRe = session.$mode.tokenRe;
            wordRe.lastIndex = 0;
            const isWordBefore = wordRe.test(leftChar);
            wordRe.lastIndex = 0;
            const isWordAfter = wordRe.test(leftChar);
            if (isWordBefore || isWordAfter)
              return null; // before or after alphanumeric
            if (rightChar && !/[\s;,.})\]\\]/.test(rightChar))
              return null; // there is rightChar and it isn't closing
            pair = true;
          }
          return {
            text: pair ? quote + quote : '',
            selection: [1, 1]
          };
        }
      }
    });

    this.add('string_dquotes', 'deletion', (state, action, editor, session, range) => {
      const selected = session.doc.getTextRange(range);
      if (!range.isMultiLine() && (selected == '"' || selected == "'")) {
        initContext(editor);
        const line = session.doc.getLine(range.start.row);
        const rightChar = line.substring(range.start.column + 1, range.start.column + 2);
        if (rightChar == selected) {
          range.end.column++;
          return range;
        }
      }
    });
  };


  CstyleBehaviour.isSaneInsertion = function (editor, session) {
    const cursor = editor.getCursorPosition();
    const iterator = new TokenIterator(session, cursor.row, cursor.column);
    if (!this.$matchTokenType(iterator.getCurrentToken() || 'text', SAFE_INSERT_IN_TOKENS)) {
      const iterator2 = new TokenIterator(session, cursor.row, cursor.column + 1);
      if (!this.$matchTokenType(iterator2.getCurrentToken() || 'text', SAFE_INSERT_IN_TOKENS))
        return false;
    }
    iterator.stepForward();
    return iterator.getCurrentTokenRow() !== cursor.row ||
        this.$matchTokenType(iterator.getCurrentToken() || 'text', SAFE_INSERT_BEFORE_TOKENS);
  };

  CstyleBehaviour.$matchTokenType = function (token, types) {
    return types.indexOf(token.type || token) > -1;
  };

  CstyleBehaviour.recordAutoInsert = function (editor, session, bracket) {
    const cursor = editor.getCursorPosition();
    const line = session.doc.getLine(cursor.row);
    if (!this.isAutoInsertedClosing(cursor, line, context.autoInsertedLineEnd[0]))
      context.autoInsertedBrackets = 0;
    context.autoInsertedRow = cursor.row;
    context.autoInsertedLineEnd = bracket + line.substr(cursor.column);
    context.autoInsertedBrackets++;
  };

  CstyleBehaviour.recordMaybeInsert = function (editor, session, bracket) {
    const cursor = editor.getCursorPosition();
    const line = session.doc.getLine(cursor.row);
    if (!this.isMaybeInsertedClosing(cursor, line))
      context.maybeInsertedBrackets = 0;
    context.maybeInsertedRow = cursor.row;
    context.maybeInsertedLineStart = line.substr(0, cursor.column) + bracket;
    context.maybeInsertedLineEnd = line.substr(cursor.column);
    context.maybeInsertedBrackets++;
  };

  CstyleBehaviour.isAutoInsertedClosing = function (cursor, line, bracket) {
    return context.autoInsertedBrackets > 0 &&
        cursor.row === context.autoInsertedRow &&
        bracket === context.autoInsertedLineEnd[0] &&
        line.substr(cursor.column) === context.autoInsertedLineEnd;
  };

  CstyleBehaviour.isMaybeInsertedClosing = function (cursor, line) {
    return context.maybeInsertedBrackets > 0 &&
        cursor.row === context.maybeInsertedRow &&
        line.substr(cursor.column) === context.maybeInsertedLineEnd &&
        line.substr(0, cursor.column) == context.maybeInsertedLineStart;
  };

  CstyleBehaviour.popAutoInsertedClosing = function () {
    context.autoInsertedLineEnd = context.autoInsertedLineEnd.substr(1);
    context.autoInsertedBrackets--;
  };

  CstyleBehaviour.clearMaybeInsertedClosing = function () {
    if (context) {
      context.maybeInsertedBrackets = 0;
      context.maybeInsertedRow = -1;
    }
  };

  oop.inherits(CstyleBehaviour, Behaviour);

  exports.CstyleBehaviour = CstyleBehaviour;
});

ace.define('ace/mode/folding/cstyle', ['require', 'exports', 'module', 'ace/lib/oop', 'ace/range', 'ace/mode/folding/fold_mode'], (acequire, exports, module) => {
  const oop = acequire('../../lib/oop');
  const Range = acequire('../../range').Range;
  const BaseFoldMode = acequire('./fold_mode').FoldMode;

  const FoldMode = exports.FoldMode = function (commentRegex) {
    if (commentRegex) {
      this.foldingStartMarker = new RegExp(
                this.foldingStartMarker.source.replace(/\|[^|]*?$/, `|${commentRegex.start}`)
            );
      this.foldingStopMarker = new RegExp(
            this.foldingStopMarker.source.replace(/\|[^|]*?$/, `|${commentRegex.end}`)
            );
    }
  };
  oop.inherits(FoldMode, BaseFoldMode);

  (function () {
    this.foldingStartMarker = /(\{|\[)[^\}\]]*$|^\s*(\/\*)/;
    this.foldingStopMarker = /^[^\[\{]*(\}|\])|^[\s\*]*(\*\/)/;
    this.singleLineBlockCommentRe = /^\s*(\/\*).*\*\/\s*$/;
    this.tripleStarBlockCommentRe = /^\s*(\/\*\*\*).*\*\/\s*$/;
    this.startRegionRe = /^\s*(\/\*|\/\/)#?region\b/;
    this._getFoldWidgetBase = this.getFoldWidget;
    this.getFoldWidget = function (session, foldStyle, row) {
      const line = session.getLine(row);

      if (this.singleLineBlockCommentRe.test(line)) {
        if (!this.startRegionRe.test(line) && !this.tripleStarBlockCommentRe.test(line))
          return '';
      }

      const fw = this._getFoldWidgetBase(session, foldStyle, row);

      if (!fw && this.startRegionRe.test(line))
        return 'start'; // lineCommentRegionStart

      return fw;
    };

    this.getFoldWidgetRange = function (session, foldStyle, row, forceMultiline) {
      const line = session.getLine(row);

      if (this.startRegionRe.test(line))
        return this.getCommentRegionBlock(session, line, row);

      var match = line.match(this.foldingStartMarker);
      if (match) {
        var i = match.index;

        if (match[1])
          return this.openingBracketBlock(session, match[1], row, i);

        let range = session.getCommentFoldRange(row, i + match[0].length, 1);

        if (range && !range.isMultiLine()) {
          if (forceMultiline) {
            range = this.getSectionRange(session, row);
          } else if (foldStyle != 'all')
            range = null;
        }

        return range;
      }

      if (foldStyle === 'markbegin')
        return;

      var match = line.match(this.foldingStopMarker);
      if (match) {
        var i = match.index + match[0].length;

        if (match[1])
          return this.closingBracketBlock(session, match[1], row, i);

        return session.getCommentFoldRange(row, i, -1);
      }
    };

    this.getSectionRange = function (session, row) {
      let line = session.getLine(row);
      const startIndent = line.search(/\S/);
      const startRow = row;
      const startColumn = line.length;
      row = row + 1;
      let endRow = row;
      const maxRow = session.getLength();
      while (++row < maxRow) {
        line = session.getLine(row);
        const indent = line.search(/\S/);
        if (indent === -1)
          continue;
        if (startIndent > indent)
          break;
        const subRange = this.getFoldWidgetRange(session, 'all', row);

        if (subRange) {
          if (subRange.start.row <= startRow) {
            break;
          } else if (subRange.isMultiLine()) {
            row = subRange.end.row;
          } else if (startIndent == indent) {
            break;
          }
        }
        endRow = row;
      }

      return new Range(startRow, startColumn, endRow, session.getLine(endRow).length);
    };
    this.getCommentRegionBlock = function (session, line, row) {
      const startColumn = line.search(/\s*$/);
      const maxRow = session.getLength();
      const startRow = row;

      const re = /^\s*(?:\/\*|\/\/|--)#?(end)?region\b/;
      let depth = 1;
      while (++row < maxRow) {
        line = session.getLine(row);
        const m = re.exec(line);
        if (!m) continue;
        if (m[1]) depth--;
        else depth++;

        if (!depth) break;
      }

      const endRow = row;
      if (endRow > startRow) {
        return new Range(startRow, startColumn, endRow, line.length);
      }
    };
  }).call(FoldMode.prototype);
});

ace.define('ace/mode/c_cpp', ['require', 'exports', 'module', 'ace/lib/oop', 'ace/mode/text', 'ace/mode/c_cpp_highlight_rules', 'ace/mode/matching_brace_outdent', 'ace/range', 'ace/mode/behaviour/cstyle', 'ace/mode/folding/cstyle'], (acequire, exports, module) => {
  const oop = acequire('../lib/oop');
  const TextMode = acequire('./text').Mode;
  const c_cppHighlightRules = acequire('./c_cpp_highlight_rules').c_cppHighlightRules;
  const MatchingBraceOutdent = acequire('./matching_brace_outdent').MatchingBraceOutdent;
  const Range = acequire('../range').Range;
  const CstyleBehaviour = acequire('./behaviour/cstyle').CstyleBehaviour;
  const CStyleFoldMode = acequire('./folding/cstyle').FoldMode;

  const Mode = function () {
    this.HighlightRules = c_cppHighlightRules;

    this.$outdent = new MatchingBraceOutdent();
    this.$behaviour = new CstyleBehaviour();

    this.foldingRules = new CStyleFoldMode();
  };
  oop.inherits(Mode, TextMode);

  (function () {
    this.lineCommentStart = '//';
    this.blockComment = { start: '/*', end: '*/' };

    this.getNextLineIndent = function (state, line, tab) {
      let indent = this.$getIndent(line);

      const tokenizedLine = this.getTokenizer().getLineTokens(line, state);
      const tokens = tokenizedLine.tokens;
      const endState = tokenizedLine.state;

      if (tokens.length && tokens[tokens.length - 1].type == 'comment') {
        return indent;
      }

      if (state == 'start') {
        var match = line.match(/^.*[\{\(\[]\s*$/);
        if (match) {
          indent += tab;
        }
      } else if (state == 'doc-start') {
        if (endState == 'start') {
          return '';
        }
        var match = line.match(/^\s*(\/?)\*/);
        if (match) {
          if (match[1]) {
            indent += ' ';
          }
          indent += '* ';
        }
      }

      return indent;
    };

    this.checkOutdent = function (state, line, input) {
      return this.$outdent.checkOutdent(line, input);
    };

    this.autoOutdent = function (state, doc, row) {
      this.$outdent.autoOutdent(doc, row);
    };

    this.$id = 'ace/mode/c_cpp';
  }).call(Mode.prototype);

  exports.Mode = Mode;
});
