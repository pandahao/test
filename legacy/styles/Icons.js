export const IconStyle = {
  position: 'absolute',
  top: '-20%',
  left: '16%',
  width: '50%',
  height: '50%',
  fill: '#ffffff'
};

export const IconDeactivateStyle = {
  position: 'absolute',
  top: '-20%',
  left: '16%',
  width: '50%',
  height: '50%',
  fill: '#B8C0D0'
};
