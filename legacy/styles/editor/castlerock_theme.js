import { EditorPalette } from 'legacy_styles/Colors';
const { identifier, backgroundDark, backgroundLight, comment, langKeyword, roboFunction, variable, numeric, coreKeyword, string } = EditorPalette;
// console.log(EditorPalette);

ace.define('ace/theme/castle-rock', ['require', 'exports', 'module', 'ace/lib/dom'], (require, exports, module) => {
  exports.isDark = true;
  exports.cssClass = 'ace-castle-rock';
  exports.cssText = `.ace-castle-rock .ace_gutter {\
        background: ${backgroundLight};\
        color: ${comment}\
    }\
    .ace-castle-rock .ace_gutter-layer {\
        text-align: center;\
    }\
    .ace-castle-rock .ace_scrollbar {\
    }\
    .ace-castle-rock .ace_print-margin {\
        width: 1px;\
        background: #232323\
    }\
    .ace-castle-rock {\
        background-color: ${backgroundDark};\
        font-weight: 600;\
        font-Family: menlo;\
        line-height: 1.3;\
        color: #F8F8F8\
    }\
    .ace-castle-rock .ace_cursor {\
        color: #A7A7A7\
    }\
    .ace-castle-rock .ace_storage.ace_modifier,\
    .ace-castle-rock .ace_keyword.ace_operator,\
    .ace-castle-rock .ace_operator, \
    .ace-castle-rock .ace_identifier {\
        color: ${identifier}\
    }\
    .ace-castle-rock .ace_keyword.ace_functions {\
        color: ${identifier}
    }\
    .ace-castle-rock .ace_keyword.ace_core {\
        color: ${coreKeyword}
    }\
    .ace-castle-rock .ace_marker-layer .ace_selection {\
        background: rgba(221, 240, 255, 0.20)\
    }\
    .ace-castle-rock.ace_multiselect .ace_selection.ace_start {\
        box-shadow: 0 0 3px 0px #141414;\
    }\
    .ace-castle-rock .ace_marker-layer .ace_step {\
        background: rgb(102, 82, 0)\
    }\
    .ace-castle-rock .ace_marker-layer .ace_bracket {\
        margin: -1px 0 0 -1px;\
        border: 1px solid rgba(255, 255, 255, 0.25)\
    }\
    .ace-castle-rock .ace_marker-layer .ace_active-line {\
        background: rgba(255, 255, 255, 0.031)\
    }\
    .ace-castle-rock .ace_gutter-active-line {\
        background-color: rgba(255, 255, 255, 0.031)\
    }\
    .ace-castle-rock .ace_marker-layer .ace_selected-word {\
        border: 1px solid rgba(221, 240, 255, 0.20)\
    }\
    .ace-castle-rock .ace_invisible {\
        color: rgba(255, 255, 255, 0.25)\
    }\
    .ace-castle-rock .ace_keyword,\
    .ace-castle-rock .ace_meta,\
    .ace-castle-rock .ace_roboType,\
    .ace-castle-rock .ace_keyword.ace_control {\
        color: ${langKeyword}\
    }\
    .ace-castle-rock .ace_constant,\
    .ace-castle-rock .ace_constant.ace_character,\
    .ace-castle-rock .ace_constant.ace_character.ace_escape,\
    .ace-castle-rock .ace_constant.ace_other,\
    .ace-castle-rock .ace_heading,\
    .ace-castle-rock .ace_markup.ace_heading,\
    .ace-castle-rock .ace_support.ace_constant, \
    .ace-castle-rock .ace_entity.ace_name.ace_function,\
    .ace-castle-rock .ace_meta.ace_tag,\
    .ace-castle-rock .ace_variable.ace_constants {\
        color: ${variable}\
    }\
    .ace-castle-rock .ace_constant.ace_numeric {\
        color: ${numeric}\
    }\
    .ace-castle-rock .ace_invalid.ace_illegal {\
        color: #F8F8F8;\
        background-color: rgba(86, 45, 86, 0.75)\
    }\
    .ace-castle-rock .ace_invalid.ace_deprecated {\
        text-decoration: underline;\
        font-style: italic;\
        color: #D2A8A1\
    }\
    .ace-castle-rock .ace_support {\
        color: #9B859D\
    }\
    .ace-castle-rock .ace_fold {\
        background-color: #AC885B;\
        border-color: #F8F8F8\
    }\
    .ace-castle-rock .ace_roboFunction {\
        color: ${roboFunction}\
    }\
    .ace-castle-rock .ace_list,\
    .ace-castle-rock .ace_markup.ace_list,\
    .ace-castle-rock .ace_storage {\
        color: ${identifier}\
    }\
    .ace-castle-rock .ace_string {\
        color: ${string}\
    }\
    .ace-castle-rock .ace_string.ace_regexp {\
        color: #E9C062\
    }\
    .ace-castle-rock .ace_comment {\
        color: ${comment}\
    }\
    .ace-castle-rock .ace_variable,\
    .ace-castle-rock .ace_attach {\
        color: ${coreKeyword}\
    }\
    .ace-castle-rock .ace_xml-pe {\
        color: #494949\
    }\
    .ace-castle-rock .ace_istype {\
        color: #a98cff;\
        background-color: #565371;\
        border: 1px solid #7569a6;\
        border-radius: 2px\
    }\
    .ace-castle-rock .ace_issource {\
        color: #ffa800;\
        background-color: #67603e;\
        border: 1px solid #a07726;\
        border-radius: 2px\
    }\
    .ace-castle-rock .ace_getparam {\
        color: #34a2ec;\
        background-color: #3e5a71;\
        border: 1px solid #3a79a6;\
        border-radius: 2px\
    }\
    .ace-castle-rock .ace_indent-guide {\
    }`;

  const dom = require('../lib/dom');
  dom.importCssString(exports.cssText, exports.cssClass);
});
