export const CastleRockPalette = {
  textDefault: '#686E84',
  background: '#FFFFFF',
  white: '#FFFFFF',
  primary: '#5A5AFF',
  border: '#CFD5E7'
};

export const ChallengePalette = {
  primary: '#5A5AFF',
  white: '#FFFFFF',
  headerIcon: '#686E84',
  headerBar: '#CCD2E5',
  topBar: '#F2F4F8',
  headerSquare: '#5A5AFF',
  navbarBackground: '#E5E8F0',
  navbarIconDefault: '#B7BFD9',
  navbarIconSelected: '#5A5AFF',
  contentBackground: '#F6F7FA',
  contentTest: '#C0C8DE',
  squareShadow: '#BAABF6',
  listDecorator: '#B7BFD9',
  titleBar: 'grey',
  mainBackground: '#FFFFFF',
};

export const ToolbeltPalette = {
  button: '#ffffff',
  text: '#686E84',
  actionIcon: '#21D325',
  backgroundDark: '#292B34',
  backgroundLight: '#343742',
  compileError: '#EE6350',
  challengeError: '#F1A758',
  header: '#3C404E',
};

export const EditorPalette = {
  identifier: '#FFFFFF',
  backgroundDark: '#292B34',
  backgroundLight: '#343742',
  comment: '#7B8296',
  langKeyword: '#FF8A88',
  coreKeyword: '#30E3CA',
  roboFunction: '#FF8A88',
  variable: '#FFDC96',
  numeric: '#E051B5',
  string: '#AEB6FD'
};


export const StatePalette = {
  background: '#F2F4F8',
  text: '#B1BBD7',
  current: '#5557F8',
  previous: '#687088'
};


export const ConsolePalette = {
  events: '#DBBC77',
  data: '#E051B5',
  port: '#30E3CA',
  index: '#CCD2E5',
  eventsBackground: '#FFF8EA',
  dataBackground: '#F9DCF0',
  portBackground: '#D6F9F4'
};

export const RobotPalette = {
  servo: '#F89406',
  servoShadow: '#FFDAC8',
  led: '#87D37C',
  ledShadow: '#2ECC71',
  button: '#59ABE3',
  buttonShadow: '#2574A9',
  tape: '#674172',
  tapeShadow: '#9B59B6',
  light: '#EE4444',
  lightShadow: '#D24D57',
  sound: '#81CFE0',
  soundShadow: '#59ABE3',
  motor: '#F3567E',
  motorShadow: '#FF8A88',
  irt: '#F66E56',
  irtShadow: '#FFDAC8',
  irr: '#9666D7',
  irrShadow: '#8D85FA',
  joy: '#F5D76E',
  joyShadow: '#FFF8EA',
  accel: '#6C7A89',
  accelShadow: '#B8C0D0',
  deactivate: '#FFFFFF',
  deactivateText: '#B8C0D0',
  white: '#FFFFFF'
};

export const GeneralPalette = {
  brightPurple: '#50C3FF',
  darkPurple: '#686E84',
  buttonBackground: '#B8C0D0',
  sideMenuPurple: '#C2C9DF',
  hover: '#D9DEEB',
  sideMenuBG: '#F2F4F8',
  menuBG: '#F6F7FA',
  green: '#21D325',
  underline: '#B8C0D9',
  black: '#000000',
  codepadBG: '#323439',
  white: '#FFFFFF',
  boxShadow: '#686868',
  whiteShadow: '#E2E7F5',
  purpleShadow: '#97acb7',
  greenShadow: '#79B171',
  dark:'#868fac',
};
export const Design = {
  text:'#7d86a4',
  buttonborder: '#bbbfd0',
 settingcolor:'#868fac',
 settingborder:'#e7e9ed',
 grounds:'#BBBBBB',
 buildbg:'#535c85',
 colors:'#979797',
 buildback:'#e7ecfc',
};


//
