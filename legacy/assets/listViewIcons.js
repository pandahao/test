import React from 'react';

export const ListViewSelectedIcon = () => {
  return (
        <svg width="37px" height="36px" viewBox="0 0 37 36">
            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Project-Overview" transform="translate(-1136.000000, -42.000000)">
                    <g id="List-View" transform="translate(1136.000000, 42.000000)">
                        <g transform="translate(12.000000, 12.000000)" id="Hamburger" fill="#686E84">
                            <path d="M4,1 C4,0.44771525 4.45036576,0 4.99679737,0 L13.8044779,0 C14.3549939,0 14.8012752,0.443864822 14.8012752,1 C14.8012752,1.55228475 14.3509095,2 13.8044779,2 L4.99679737,2 C4.44628139,2 4,1.55613518 4,1 Z M4,6 C4,5.44771525 4.45036576,5 4.99679737,5 L13.8044779,5 C14.3549939,5 14.8012752,5.44386482 14.8012752,6 C14.8012752,6.55228475 14.3509095,7 13.8044779,7 L4.99679737,7 C4.44628139,7 4,6.55613518 4,6 Z M4,11 C4,10.4477153 4.45036576,10 4.99679737,10 L13.8044779,10 C14.3549939,10 14.8012752,10.4438648 14.8012752,11 C14.8012752,11.5522847 14.3509095,12 13.8044779,12 L4.99679737,12 C4.44628139,12 4,11.5561352 4,11 Z" />
                            <path d="M0.5,1 C0.5,0.44771525 0.946949899,0 1.50244075,0 L1.99755925,0 C2.55119199,0 3,0.443864822 3,1 C3,1.55228475 2.5530501,2 1.99755925,2 L1.50244075,2 C0.948808011,2 0.5,1.55613518 0.5,1 Z M0.5,6 C0.5,5.44771525 0.946949899,5 1.50244075,5 L1.99755925,5 C2.55119199,5 3,5.44386482 3,6 C3,6.55228475 2.5530501,7 1.99755925,7 L1.50244075,7 C0.948808011,7 0.5,6.55613518 0.5,6 Z M0.5,11 C0.5,10.4477153 0.946949899,10 1.50244075,10 L1.99755925,10 C2.55119199,10 3,10.4438648 3,11 C3,11.5522847 2.5530501,12 1.99755925,12 L1.50244075,12 C0.948808011,12 0.5,11.5561352 0.5,11 Z" transform="translate(1.750000, 6.000000) scale(-1, 1) translate(-1.750000, -6.000000) " />
                        </g>
                    </g>
                </g>
            </g>
        </svg>
    );
};

export const ListViewIcon = () => {
  return (
        <svg width="37px" height="36px" viewBox="0 0 37 36">
            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="All-Projects-List" transform="translate(-1136.000000, -42.000000)">
                    <g id="List-View" transform="translate(1136.000000, 42.000000)">
                        <g transform="translate(12.000000, 12.000000)" id="Hamburger" fill="#D9DEEB">
                            <path d="M4,1 C4,0.44771525 4.45036576,0 4.99679737,0 L13.8044779,0 C14.3549939,0 14.8012752,0.443864822 14.8012752,1 C14.8012752,1.55228475 14.3509095,2 13.8044779,2 L4.99679737,2 C4.44628139,2 4,1.55613518 4,1 Z M4,6 C4,5.44771525 4.45036576,5 4.99679737,5 L13.8044779,5 C14.3549939,5 14.8012752,5.44386482 14.8012752,6 C14.8012752,6.55228475 14.3509095,7 13.8044779,7 L4.99679737,7 C4.44628139,7 4,6.55613518 4,6 Z M4,11 C4,10.4477153 4.45036576,10 4.99679737,10 L13.8044779,10 C14.3549939,10 14.8012752,10.4438648 14.8012752,11 C14.8012752,11.5522847 14.3509095,12 13.8044779,12 L4.99679737,12 C4.44628139,12 4,11.5561352 4,11 Z" />
                            <path d="M0.5,1 C0.5,0.44771525 0.946949899,0 1.50244075,0 L1.99755925,0 C2.55119199,0 3,0.443864822 3,1 C3,1.55228475 2.5530501,2 1.99755925,2 L1.50244075,2 C0.948808011,2 0.5,1.55613518 0.5,1 Z M0.5,6 C0.5,5.44771525 0.946949899,5 1.50244075,5 L1.99755925,5 C2.55119199,5 3,5.44386482 3,6 C3,6.55228475 2.5530501,7 1.99755925,7 L1.50244075,7 C0.948808011,7 0.5,6.55613518 0.5,6 Z M0.5,11 C0.5,10.4477153 0.946949899,10 1.50244075,10 L1.99755925,10 C2.55119199,10 3,10.4438648 3,11 C3,11.5522847 2.5530501,12 1.99755925,12 L1.50244075,12 C0.948808011,12 0.5,11.5561352 0.5,11 Z" transform="translate(1.750000, 6.000000) scale(-1, 1) translate(-1.750000, -6.000000) " />
                        </g>
                    </g>
                </g>
            </g>
        </svg>
    );
};

export const GridViewIcon = () => {
  return (
        <svg width="37px" height="36px" viewBox="0 0 37 36">
            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="All-Projects-List" transform="translate(-1093.000000, -42.000000)">
                    <g id="Grid-View" transform="translate(1093.000000, 42.000000)">
                        <rect id="Rectangle-187" fill="#D9DEEB" x="11" y="11" width="15" height="15" />
                        <rect id="Rectangle-188" fill="#F6F7FA" x="14" y="7" width="3" height="23" />
                        <rect id="Rectangle-188" fill="#F6F7FA" x="20" y="7" width="3" height="23" />
                        <rect id="Rectangle-188" fill="#F6F7FA" transform="translate(18.500000, 15.500000) rotate(90.000000) translate(-18.500000, -15.500000) " x="17" y="4" width="3" height="23" />
                        <rect id="Rectangle-188" fill="#F6F7FA" transform="translate(22.500000, 21.500000) rotate(90.000000) translate(-22.500000, -21.500000) " x="21" y="10" width="3" height="23" />
                    </g>
                </g>
            </g>
        </svg>
    );
};

export const GridViewSelectedIcon = () => {
  return (
        <svg width="37px" height="36px" viewBox="0 0 37 36">
            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Project-Overview" transform="translate(-1093.000000, -42.000000)">
                    <g id="Grid-View" transform="translate(1093.000000, 42.000000)">
                        <rect id="Rectangle-187" fill="#686E84" x="11" y="11" width="15" height="15" />
                        <rect id="Rectangle-188" fill="#FFFFFF" x="14" y="7" width="3" height="23" />
                        <rect id="Rectangle-188" fill="#FFFFFF" x="20" y="7" width="3" height="23" />
                        <rect id="Rectangle-188" fill="#FFFFFF" transform="translate(18.500000, 15.500000) rotate(90.000000) translate(-18.500000, -15.500000) " x="17" y="4" width="3" height="23" />
                        <rect id="Rectangle-188" fill="#FFFFFF" transform="translate(22.500000, 21.500000) rotate(90.000000) translate(-22.500000, -20.500000) " x="21" y="10" width="3" height="23" />
                    </g>
                </g>
            </g>
        </svg>
    );
};

export const BackArrow = () => {
  return (
        <svg width="5px" height="8px" viewBox="0 0 5 8">
            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
                <g id="All-Projects-List" transform="translate(-136.000000, -63.000000)" stroke="#686E84" strokeWidth="2">
                    <polyline id="Polygon-2" transform="translate(138.508000, 67.250000) scale(1, -1) rotate(90.000000) translate(-138.508000, -67.250000) " points="141.016 66 138.508 68.5 136 66" />
                </g>
            </g>
        </svg>
    );
};
