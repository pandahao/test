import React from 'react';
export const RobotIcon = () => {
  return (
        <svg width="45px" height="73px" viewBox="100 76 45 73" version="1.1">
            <defs />
            <g id="Group-17" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(100.000000, 77.000000)" opacity="0.8">
                <path d="M16.4965557,47.184925 C16.9063118,50.1483559 19.4085756,52.3530145 22.3622817,52.3530145 C25.3159878,52.3530145 27.8182516,50.1483559 28.2280077,47.184925 C28.2994865,46.6953868 28.7131428,46.3321061 29.2016098,46.3298893 L42.358801,46.3298893 L39.6278926,32.0110961 C39.4432669,31.0821094 38.624722,30.4230878 37.6897313,30.4506561 L7.03181792,30.4506561 C6.09682717,30.4230878 5.27828227,31.0821094 5.09365665,32.0110961 L2.36274823,46.3268356 L15.5319964,46.3268356 C16.0175272,46.3350394 16.4257761,46.698225 16.4965557,47.184925 L16.4965557,47.184925 Z M29.3342367,35.5808699 C30.9706594,35.5808699 32.2972421,36.9248152 32.2972421,38.5826557 C32.2972421,40.2404963 30.9706594,41.5844415 29.3342367,41.5844415 C27.6978139,41.5844415 26.3712312,40.2404963 26.3712312,38.5826557 C26.3778709,36.9295874 27.7025109,35.593071 29.3342367,35.5930847 L29.3342367,35.5808699 Z M16.1578446,11.9167383 C10.1016146,13.7799597 5.5774065,18.3160949 4.59027728,23.7294652 L18.1996056,23.7294652 C18.1657755,24.8159553 18.5769547,25.8682497 19.3356711,26.6368963 C20.0943875,27.4055429 21.1330873,27.8221037 22.2055408,27.7878308 C23.2779944,27.8221037 24.3166941,27.4055429 25.0754105,26.6368963 C25.834127,25.8682497 26.2453062,24.8159553 26.211476,23.7294652 L40.1342862,23.7294652 C39.1478287,18.319779 34.6291052,13.7861343 28.5766385,11.9155958 L34.2289786,6.26325568 C34.6244473,5.86778695 34.6184512,5.23729798 34.2258791,4.8447259 L32.7719408,3.39078758 C32.3798491,2.99869596 31.7442713,2.99682782 31.353411,3.3876881 L24.4,10.3410991 L24.4,1.00086036 C24.4,0.441583121 23.9499371,-9.85878046e-14 23.3947563,-9.85878046e-14 L21.338577,-9.85878046e-14 C20.7840757,-9.85878046e-14 20.3333333,0.448100447 20.3333333,1.00086036 L20.3333333,10.3410991 L13.3799223,3.3876881 C12.9844536,2.99221937 12.3539646,2.9982155 11.9613926,3.39078758 L10.5074542,4.8447259 C10.1153626,5.23681752 10.1134945,5.87239539 10.5043548,6.26325568 L16.1578391,11.91674 Z M12.4303355,38.5826557 C12.4303355,36.9248152 13.7569183,35.5808699 15.393341,35.5808699 C17.0297638,35.5808699 18.3563465,36.9248152 18.3563465,38.5826557 C18.3563465,40.2404963 17.0297638,41.5844415 15.393341,41.5844415 C13.7604393,41.5861381 12.433965,40.2491312 12.4273213,38.5948705 L12.4303355,38.5826557 Z M44.1281644,49.9912741 L31.1427853,49.9912741 C29.6974313,53.6173844 26.2238641,55.9914691 22.3637888,55.9914691 C18.5037136,55.9914691 15.0301464,53.6173844 13.5847924,49.9912741 L0.593384828,49.9912741 C0.397223852,49.9862431 0.211908045,50.082252 0.101103607,50.2463165 C-0.00970083144,50.410381 -0.0308990355,50.6201507 0.0447917469,50.8035579 C3.52021935,59.0668666 13.1627977,67.5317194 21.611734,71.0159897 C22.0945387,71.2168923 22.6360532,71.2168923 23.1188579,71.0159897 C31.5587515,67.5317194 41.2043441,59.072974 44.6858002,50.8096653 C44.765002,50.6240949 44.7442443,50.4102773 44.6308743,50.2438945 C44.5175044,50.0775118 44.3275189,49.9820408 44.1281644,49.9912741 L44.1281644,49.9912741 Z" id="Combined-Shape" fill="#5557F8" />
            </g>
        </svg>);
};

export const DeleteRobotIcon = () => {
  return (
        <svg width="33px" height="33px" viewBox="106 177 33 33">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-1">
                    <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1" />
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
                    <feColorMatrix values="0 0 0 0 0.581685799   0 0 0 0 0.581685799   0 0 0 0 0.581685799  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1" />
                    <feMerge>
                        <feMergeNode in="shadowMatrixOuter1" />
                        <feMergeNode in="SourceGraphic" />
                    </feMerge>
                </filter>
            </defs>
            <g id="Group" filter="url(#filter-1)" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(110.000000, 179.000000)">
                <rect id="Rectangle-298" fill="#C2C9DF" x="0" y="0" width="25" height="25" rx="12.5" />
                <rect id="Rectangle-320" fill="#FFFFFF" x="7" y="9" width="11" height="12" />
                <rect id="Rectangle-321" fill="#FFFFFF" x="6" y="6" width="13" height="2" />
                <rect id="Rectangle-321" fill="#FFFFFF" x="9" y="5" width="7" height="2" />
            </g>
        </svg>
    );
};

export const LoadRobotIcon = () => {
  return (
        <svg width="33px" height="33px" viewBox="161 177 33 33">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-1">
                    <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1" />
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
                    <feColorMatrix values="0 0 0 0 0.581685799   0 0 0 0 0.581685799   0 0 0 0 0.581685799  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1" />
                    <feMerge>
                        <feMergeNode in="shadowMatrixOuter1" />
                        <feMergeNode in="SourceGraphic" />
                    </feMerge>
                </filter>
            </defs>
            <g id="Group-18" filter="url(#filter-1)" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(165.000000, 179.000000)">
                <rect id="Rectangle-298" fill="#C2C9DF" x="0" y="0" width="25" height="25" rx="12.5" />
                <rect id="Combined-Shape" fill="#FFFFFF" x="7" y="15" width="12" height="3" />
                <rect id="Combined-Shape" fill="#FFFFFF" transform="translate(6.500000, 12.500000) scale(-1, 1) translate(-6.500000, -12.500000) " x="5" y="7" width="3" height="11" />
                <rect id="Combined-Shape" fill="#FFFFFF" transform="translate(19.500000, 12.500000) scale(-1, 1) translate(-19.500000, -12.500000) " x="18" y="7" width="3" height="11" />
                <path d="M13.9999816,12 L11.9999908,12 L10.3039986,12 C10.008,12 9.91100041,11.8019943 10.0889996,11.6249892 L12.6769877,9.13041735 C12.8549869,8.9549123 13.1439855,8.95716236 13.3229847,9.13341744 L15.9099728,11.6249892 C16.088972,11.8012443 15.9919725,12 15.6949738,12 L13.9999816,12 Z" id="Page-1" fill="#FFFFFF" transform="translate(12.999823, 10.500000) rotate(180.000000) translate(-12.999823, -10.500000) " />
                <rect id="Rectangle-328" fill="#FFFFFF" transform="translate(13.000000, 7.000000) rotate(180.000000) translate(-13.000000, -7.000000) " x="12" y="5" width="2" height="4" />
            </g>
        </svg>
    );
};

export const LoadRobotMenu = () => {
  return (
        <svg width="111px" height="35px" viewBox="-7 -7 111 35">
            <defs>
                <rect id="path-1" x="0" y="0" width="97" height="21" rx="10.5" />
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-2">
                    <feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1" />
                    <feGaussianBlur stdDeviation="3.5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
                    <feColorMatrix values="0 0 0 0 0.593670281   0 0 0 0 0.593670281   0 0 0 0 0.593670281  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1" />
                </filter>
            </defs>
            <g id="Group-2" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Rectangle-178">
                    <use fill="black" fillOpacity="1" filter="url(#filter-2)" xlinkHref="#path-1" />
                    <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-1" />
                </g>
                <text id="LOAD" fontFamily=".AppleSystemUIFont" fontSize="14" fontWeight="normal" fill="#686E84">
                    <tspan x="30.6293945" y="16">LOAD</tspan>
                </text>
            </g>
        </svg>
    );
};

export const DeleteRobotMenu = () => {
  return (
        <svg width="111px" height="35px" viewBox="-7 -7 111 35">
            <defs>
                <rect id="path-1" x="0" y="0" width="97" height="21" rx="10.5" />
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-2">
                    <feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1" />
                    <feGaussianBlur stdDeviation="3.5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
                    <feColorMatrix values="0 0 0 0 0.593670281   0 0 0 0 0.593670281   0 0 0 0 0.593670281  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1" />
                </filter>
            </defs>
            <g id="Group-2" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Rectangle-178">
                    <use fill="black" fillOpacity="1" filter="url(#filter-2)" xlinkHref="#path-1" />
                    <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-1" />
                </g>
                <text id="DELETE" fontFamily=".AppleSystemUIFont" fontSize="14" fontWeight="normal" fill="#686E84">
                    <tspan x="23.9472656" y="16">DELETE</tspan>
                </text>
            </g>
        </svg>
    );
};
