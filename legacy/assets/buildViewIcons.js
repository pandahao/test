import React from 'react';

export const Plus = () => {
  return (
      <svg width="17px" height="17px" viewBox="0 0 17 17" version="1.1">
          <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" fontSize="48" fontFamily="StaticBold, Static Bold" fontWeight="bold">
              <g id="15-Build-Circle-Undone" transform="translate(-1085.000000, -34.000000)" fill="#686E84">
                  <g id="Group-6" transform="translate(1077.000000, 13.000000)">
                      <g id="Group">
                          <text id="+">
                              <tspan x="7" y="46">+</tspan>
                          </text>
                      </g>
                  </g>
              </g>
          </g>
      </svg>
    );
};

export const Minus = () => {
  return (
        <svg width="16px" height="5px" viewBox="0 0 16 5" version="1.1">
            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" fontSize="48" fontFamily="StaticBold, Static Bold" fontWeight="bold">
                <text id="-" transform="translate(17.000000, 29.000000) rotate(180.000000) translate(-17.000000, -29.000000) ">
                    <tspan x="8" y="46">-</tspan>
                </text>
            </g>
        </svg>
    );
};

export const Left = () => {
  return (
        <svg width="16px" height="24px" viewBox="0 0 16 24" version="1.1">
            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" fontSize="48" fontFamily="StaticBold, Static Bold" fontWeight="bold">
                <g id="15-Build-Circle-Undone" transform="translate(-1202.000000, -742.000000)" fill="#686E84">
                    <g id="Group-6" transform="translate(1135.000000, 719.000000)">
                        <g id="Group-2" transform="translate(58.000000, 12.000000)">
                            <text style={{ fontFamily: 'StaticBold' }} id="&lt;">
                                <tspan x="14" y="29">&lt;</tspan>
                            </text>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
    );
};

export const Right = () => {
  return (
        <svg width="16px" height="24px" viewBox="0 0 16 24" version="1.1">
            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" fontSize="48" fontFamily="StaticBold, Static Bold" fontWeight="bold">
                <g id="15-Build-Circle-Undone" transform="translate(-1202.000000, -742.000000)" fill="#686E84">
                    <g id="Group-6" transform="translate(1135.000000, 719.000000)">
                        <g id="Group-2" transform="translate(58.000000, 12.000000)">
                            <text style={{ fontFamily: 'StaticBold' }} id="&lt;" transform="translate(17.000000, 29.000000) rotate(180.000000) translate(-17.000000, -29.000000) ">
                                <tspan x="14" y="36">&lt;</tspan>
                            </text>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
    );
};
export const Parts = () =>{
   return(
     <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1">
       <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
         <g id="screen" transform="translate(-1160.000000, -725.000000)">
           <g id="parts-list-button" transform="translate(1160.000000, 725.000000)">
              <use id="continue" stroke="#CCCCCC" mask="url(#mask-2)" strokeWidth="4" fill="#FFFFFF"></use>
              <g id="bolt3-copy" transform="translate(11.000000, 7.000000)" fill="#CCCCCC">
                  <path d="M7.04003924,11 L2.98170501,11.0125957 C2.01858705,11.0130771 2.00818449,11.3795576 2.00078211,11.4926776 C1.99337974,11.6057976 2.01887475,11.9991708 2.98170501,12 L7.04003924,11.9874043 C8.00246833,11.987306 8.00226913,11.5911529 7.99976089,11.4926776 C7.99725265,11.3942024 8.00187829,11.0014209 7.04003924,11 Z" id="Shape"></path>
                  <path d="M7.04003924,13 L2.98170501,13.0125957 C2.01858705,13.0130771 2.00818449,13.3795576 2.00078211,13.4926776 C1.99337974,13.6057976 2.01887475,13.9991708 2.98170501,14 L7.04003924,13.9874043 C8.00246833,13.987306 8.00226913,13.5911529 7.99976089,13.4926776 C7.99725265,13.3942024 8.00187829,13.0014209 7.04003924,13 Z" id="Shape-Copy"></path>
                  <path d="M7.04003924,15 L2.98170501,15.0125957 C2.01858705,15.0130771 2.00818449,15.3795576 2.00078211,15.4926776 C1.99337974,15.6057976 2.01887475,15.9991708 2.98170501,16 L7.04003924,15.9874043 C8.00246833,15.987306 8.00226913,15.5911529 7.99976089,15.4926776 C7.99725265,15.3942024 8.00187829,15.0014209 7.04003924,15 Z" id="Shape-Copy-2"></path>
                  <path d="M7.04003924,17 L2.98170501,17.0125957 C2.01858705,17.0130771 2.00818449,17.3795576 2.00078211,17.4926776 C1.99337974,17.6057976 2.01887475,17.9991708 2.98170501,18 L7.04003924,17.9874043 C8.00246833,17.987306 8.00226913,17.5911529 7.99976089,17.4926776 C7.99725265,17.3942024 8.00187829,17.0014209 7.04003924,17 Z" id="Shape-Copy-3"></path>
                  <polygon id="Shape" points="2 6 2 10 8 10 8 6"></polygon>
                  <polygon id="Shape" points="1.01293945 0.0138549805 0.00408935547 1.00372314 0.000427246094 5.00189209 9.99591064 5.00952148 9.99957275 1.01135254 8.98815918 0.0205078125"></polygon>
              </g>
          </g>
      </g>
  </g>
</svg>
   );
};
export const Help = () =>{
  return(
    <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1">
        {/* <defs>
            <rect id="path-1" x="0" y="0" width="32" height="32" rx="16"></rect>
            <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="32" height="32" fill="white">
            </mask>
        </defs> */}
        <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="screen" transform="translate(-1214.000000, -725.000000)">
                <g id="help-button" transform="translate(1214.000000, 725.000000)">
                    <use id="continue" stroke="#CCCCCC" mask="url(#mask-2)" strokeWidth="4" fill="#FFFFFF"></use>
                    <text id="?" opacity="0.2" fontFamily="StaticBold, Static Bold" fontSize="24" font-weight="bold" fill="#000000">
                        <tspan x="11.996" y="24">?</tspan>
                    </text>
                </g>
            </g>
        </g>
    </svg>
  );
};
export const PartList = () =>{
  return(
    <svg width="830px" height="609px" viewBox="0 0 830 609" version="1.1">
    <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" opacity="0.949999988">
        <g id="screen" transform="translate(-436.000000, -105.000000)" fill="#e7ecfc">
            <path d="M1193.2132,695 L1259.99981,695 C1263.31567,695 1266,692.310381 1266,688.992567 L1266,111.007433 C1266,107.695744 1263.31362,105 1259.99981,105 L442.000187,105 C438.68433,105 436,107.689619 436,111.007433 L436,688.992567 C436,692.304256 438.686375,695 442.000187,695 L1156.7868,695 L1175,713.213203 L1193.2132,695 Z" id="parts-base"></path>
        </g>
    </g>
</svg>
  );
};
