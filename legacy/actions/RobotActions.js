export const DEVICE_UPDATE = 'DEVICE_UPDATE';
export const DEVICE_SETUP = 'DEVICE_SETUP';
export const LAYOUT_SETUP = 'LAYOUT_SETUP';
export const LAYOUT_UPDATE = 'LAYOUT_UPDATE';
export const LAYOUT_RESIZE = 'LAYOUT_RESIZE';

export const EVENT_RECEIVED = 'EVENT_RECEIVED';
export const STATE_RECEIVED = 'STATE_RECEIVED';
export const EVENT_RESET = 'EVENT_RESET';
export const TOGGLE_CONSOLE = 'TOGGLE_CONSOLE';
export const TAKE_SNAPSHOT = 'TAKE_SNAPSHOT';

export const UPDATE_CHECKLIST = 'UPDATE_CHECKLIST';

export const UPDATE_PROJECTS = 'UPDATE_PROJECTS';

import { TOGGLE_CHALLENGE_BANNER, CANCEL_CHECK, SHOW_COMPLETION } from 'legacy_actions/CodepadActions';
import { incrementChallengesCompleted } from 'legacy_actions/ChallengeActions';

// Workflow:
/*
--> Init RoboCore - auto dispatch Init_End Action - In Components Life Cycle - update state
--> Run Button - dispatch Start_Compile Action - update state
--> Compile - auto dispatch Compile_End Action - update state
--> Upload  - auto dispatch Upload_End Action - update state
--> Render VirtualRobot - auto dispatch Update_Layout - update state
--> Update VirtualRobot Info - auto dispatch Update_Robot Action - In Components Life Cycle - update state
*/

const dummyRobot = [];

let order = 0;
let leftOverEvents = undefined;

function checkChallengeEvents(dispatch, state, parts_arr) {
  const currentChallenge = state.currentChallengeSelected;
  const currentSet = state.currentChallengeSet;
  const currentProjectIndex = state.currentProjectIndex;
  const currentProjectID = state.currentProjectID;
  const checklist = state.currentChallengeSet[state.currentChallengeSelected].content.checklist;


  parts_arr.forEach((part) => {
    const event_block = leftOverEvents === undefined ? [...checklist[order].event] : leftOverEvents;
    let event_index = -1;
    const eventToRemove = event_block.find((eventStandard, i) => {
      const standardList = eventStandard.split('-');

      const portMatch = part.port === standardList[0];
      const nameMatch = part.event_name === standardList[1];

      let dataOneMatch = true;
      if (standardList[2] !== 'N') {
        dataOneMatch = part.msg[0].value === parseInt(standardList[2]);
      }
      let dataTwoMatch = true;
      if (standardList[3] && part.msg[1]) {
        if (standardList[3] !== 'N') {
          dataTwoMatch = part.msg[1].value === parseInt(standardList[3]);
        }
      }
            // console.log(portMatch,nameMatch, dataOneMatch,dataTwoMatch);
      const found = (portMatch && nameMatch && dataOneMatch && dataTwoMatch);
      if (found) {
        event_index = i;
      }
      return found;
    });

    let eventFound = eventToRemove ? true : false;

    const finishBlock = false;
    if (eventFound) {
            // console.log(event_block,eventToRemove);
      if (event_block.length === 1) {
        if (order === checklist.length - 1) {
          order++;
          const dum = order;
          dispatch({
            type: 'SET_CHALLENGE_EVENT_ORDER',
            newOrder: dum
          });
          dispatch({
            type: CANCEL_CHECK
          });
          dispatch(incrementChallengesCompleted(currentProjectID));
          order = 0;
                    // When all challenge has been completed
          if (currentChallenge === currentSet.length - 1) {
            dispatch({
              type: UPDATE_PROJECTS,
              currentProjectID
            });
            dispatch({
              type: SHOW_COMPLETION
            });
          }
          else {
            dispatch({
              type: TOGGLE_CHALLENGE_BANNER
            });
          }
        }
        else {
          order++;
          dispatch({
            type: 'SET_CHALLENGE_EVENT_ORDER',
            newOrder: order
          });
        }
      }

      event_block.splice(event_index, 1);
      leftOverEvents = (event_block.length === 0) ? undefined : event_block;
      eventFound = false;
    }
  });
}
export const eventReceived = (parts_arr) => {
  return (dispatch, getState) => {
    const state = getState();
        // Always dispatch EVENT_RECEIVED to update the console
    dispatch({
      type: EVENT_RECEIVED,
      event_info: parts_arr
    });

    if (state.shared.codepad.checkingAnswers) {
      checkChallengeEvents(
        dispatch,
        // Merge state to fix legacy store
        { ...state.projectMode.challenge, ...state.projectMode.project },
        parts_arr.reverse());
    }
  };
};

export const stateReceived = (states_arr) => {
  return (dispatch) => {
    dispatch({
      type: STATE_RECEIVED,
      state_info: states_arr.map((st) => {
        return {
          port: 'ROBOCORE',
          event_name: 'ROBOCORE_PRINT',
          msg: [
            {
              value: st.MSG
            }
          ]
        };
      })
    });
  };
};

export const takeSnapShot = ()=>{
  return (dispatch, getState)=>{
    const state= getState();
    dispatch({
      type : TAKE_SNAPSHOT,
      data : {
        virtualRobot :state.robot.virtualRobot,
        eventHistory  : state.robot.eventHistory
      }
    })
  }
}


export const eventReset = () => {
  return {
    type: EVENT_RESET
  };
};

export const deviceSetup = () => {
  return {
    type: DEVICE_SETUP,
    device_info: dummyRobot
  };
};

export const deviceUpdate = (devices) => {
  return {
    type: DEVICE_UPDATE,
    device_info: devices
  };
};

export const layoutSetup = () => {
  return (dispatch, getState) => {
    const state = getState().shared.app;
    const rows = state.rootLocation === 2 ? (state.docView ? 6 : 8) : 6;
    dispatch({
      type: LAYOUT_SETUP,
      device_info: dummyRobot,
      row: rows
    });
  };
};

export const layoutResize = () => {
  return (dispatch, getState) => {
    const state = getState().shared.app;

    const rows = state.rootLocation === 2 ? (state.docView ? 6 : 8) : 6;
    dispatch({
      type: LAYOUT_RESIZE,
      row: rows
    });
  };
};

export const layoutUpdate = (devices) => {
  return {
    type: LAYOUT_UPDATE,
    device_info: devices
  };
};

export const toggleConsole = () => {
  return {
    type: TOGGLE_CONSOLE
  };
};
