export const DISPLAY_GLOBAL_MESSAGE = 'DISPLAY_GLOBAL_MESSAGE';
export const HIDE_GLOBAL_MESSAGE = 'HIDE_GLOBAL_MESSAGE';

export const displayGlobalMessage = (message, level = 'info') => {
  return {
    type: DISPLAY_GLOBAL_MESSAGE,
    message,
    level,
  };
};

export const hideGlobalMessage = () => {
  return {
    type: HIDE_GLOBAL_MESSAGE
  };
};
