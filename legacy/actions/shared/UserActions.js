export const UPDATE_USER_PROFILE = 'UPDATE_USER_PROFILE';
export const UPDATE_USER_PROFILE_SUCCESS = 'UPDATE_USER_PROFILE_SUCCESS';
export const UPDATE_USER_PROFILE_CANCEL = 'UPDATE_USER_PROFILE_CANCEL';

import { SkylineCloud } from 'legacy_actions/AuthActions';
import { displayGlobalMessage } from 'legacy_actions/shared/RuntimeActions';
import ChangePwdDialog from 'components/Settings/ChangePwdDialog';
import { t } from 'legacy_utils/i18n';
import { format } from 'legacy_utils/error';

export const UpdateUserProfile = () => {
  return {
    type: UPDATE_USER_PROFILE
  };
};
export const cancelUpdateUserProfile = () => {
  return {
    type: UPDATE_USER_PROFILE_CANCEL
  };
};
export const updateUserProfile = (profile) => {
  return (dispatch, getState) => {
    SkylineCloud.updateUserProfile(profile).then(() => {
      dispatch({
        type: UPDATE_USER_PROFILE_SUCCESS,
        profile
      });
      dispatch(displayGlobalMessage(t('User profile updated')));
    }, (err) => {
      dispatch(displayGlobalMessage(t(format(err.message)), 'error'));
    });
  };
};

export const changePassword = () => {
  return (dispatch, getState) => {
    const dialog = new ChangePwdDialog(t('Change Password'));
    dialog.show(({ oldPassword, newPassword }) => {
      SkylineCloud.changePassword(oldPassword, newPassword).then(() => {
        dispatch(displayGlobalMessage(t('Password updated')));
        dialog.close();
      }, (err) => {
        dialog.error(t(format(err.message)));
      });
    });
  };
};
