/*eslint-disable*/
import { SerialEnv, CompilerEndPoint, SkylineCore } from 'config';
import ProtocolReader from 'legacy_lib/ProtocolReader';
import RobotParser from 'legacy_lib/RobotParser';
import { generateTree, compareTrees, printResults, removeComments } from 'legacy_lib/CodeChecker';
// Robot Related
import { deviceSetup, deviceUpdate, layoutSetup, layoutUpdate, eventReset, eventReceived, stateReceived } from './RobotActions';

export const START_CONNECT = 'START_CONNECT';
export const END_CONNECT = 'END_CONNECT'
// Core Related
export const TOGGLE_TOOLBOX = 'TOGGLE_TOOLBOX';
export const HIDE_TOOLBOX = 'HIDE_TOOLBOX';
export const CORE_INIT_SUCCESS = 'CORE_INIT_SUCCESS';
export const CORE_INIT_ERROR = 'CORE_INIT_ERROR';
export const CORE_NULL = 'CORE_NULL';
export const PROCEED_CHECK = 'PROCEED_CHECK';
export const CANCEL_CHECK = 'CANCEL_CHECK';
export const COMPILATION_ERROR = 'COMPILATION_ERROR';

// Pass Challenge Banner Related
export const TOGGLE_CHALLENGE_BANNER = 'TOGGLE_CHALLENGE_BANNER';
export const CLOSE_CHALLENGE_BANNER = 'CLOSE_CHALLENGE_BANNER';


// Error Banner Related
export const TOGGLE_ERROR_BANNER = 'TOGGLE_ERROR_BANNER';
export const CLOSE_ERROR_BANNER = 'CLOSE_ERROR_BANNER';

// Upload Related;
export const UPLOAD_START = 'UPLOAD_START';
export const UPLOAD_ERROR = 'UPLOAD_ERROR';
export const UPDATE_ERROR = 'UPDATE_ERROR';
export const UPLOAD_SUCCESS = 'UPLOAD_SUCCESS';
export const UPLOAD_NOT_AVAILABLE = 'UPLOAD_NOT_AVAILABLE';
export const MAP_SOLUTION = 'MAP_SOLUTION';

// Editing Related
export const SKETCH_CHANGE = 'SKETCH_CHANGE';
export const SKETCH_RESET = 'SKETCH_RESET';

// Console Related
export const UPDATE_NAMES = 'UPDATE_NAMES';
export const RESET_NAMES = 'RESET_NAMES';


// Reset Code Related
export const SHOW_WARNING = 'SHOW_WARNING';
export const CLOSE_WARNING = 'CLOSE_WARNING';

// Passing Project Related
export const CLOSE_COMPLETION = 'CLOSE_COMPLETION';
export const SHOW_COMPLETION = 'SHOW_COMPLETION';

// Mark codepad content as dirty(modified, must explicitly save or discard)
export const MARK_DIRTY = 'MARK_DIRTY';
// clear that dirty status
export const CLEAR_DIRTY = 'CLEAR_DIRTY';

export const init_sketch = '#include "ROBOTERRA.h"\n\n/*--------------------- STEP 1 ---------------------*/\n// Name your RoboCore and all electronics attached it.\n// These names are considered as eventSource of EVENT. \n\nRoboTerraRoboCore tom;\n\n/*--------------------- STEP 2 ---------------------*/\n// Attach electronics to physical ports on RoboCore.   \n\nvoid attachRoboTerraElectronics() {\n\n}\n\n/*--------------------- STEP 3 ---------------------*/\n/* Design your robot algorithm by handling each EVENT. \n * Based on the source, type and data of each EVENT, \n * you can organize your loigc and form program flow.\n * Call Command Functions of instances you defined in\n * STEP 1 for your robot to take physical actions. \n * Calling Command Functions can result in new EVENT. \n */\n\nvoid handleRoboTerraEvent() {\n    if (EVENT.isType(ROBOCORE_LAUNCH)) {\n        // Initialize robot by calling Comand Function  \n    \n    }\n    \n    // Your robot code goes below\n\n\n}';

export const markDirty = () => {
  return {
    type: MARK_DIRTY
  }
}

export const clearDirty = () => {
  return {
    type: CLEAR_DIRTY
  }
}

export const sketchChange = (val) => {
    // console.log('change sketch');
  return {
    type: SKETCH_CHANGE,
    sketch: val
  };
};

export const sketchReset = () => {
  return {
    type: SKETCH_RESET
  };
};

import { user } from 'legacy_actions/AuthActions';

const myHeader = new Headers({
  'Content-Type': 'application/json'
});

// Function to add variable names to store
function updateNames(sketch) {
    // ROBOCOREZ necessary to prevent name confusion in debugging step.
  const name = { ROBOCOREZ: { name: '', type: '' },
        DIO_1: { name: '', type: '' },
        DIO_2: { name: '', type: '' },
        DIO_3: { name: '', type: '' },
        DIO_4: { name: '', type: '' },
        DIO_5: { name: '', type: '' },
        DIO_6: { name: '', type: '' },
        DIO_7: { name: '', type: '' },
        DIO_8: { name: '', type: '' },
        DIO_9: { name: '', type: '' },
        SERVO_A: { name: '', type: '' },
        SERVO_B: { name: '', type: '' },
        SERVO_C: { name: '', type: '' },
        SERVO_D: { name: '', type: '' },
        MOTO_A: { name: '', type: '' },
        MOTO_B: { name: '', type: '' },
        AI_1: { name: '', type: '' },
        AI_2: { name: '', type: '' },
        IR_TRAN: { name: '', type: '' },
        PORT_1: { name: '', type: '' },
        PORT_2: { name: '', type: '' },
        PORT_3: { name: '', type: '' },
        PORT_4: { name: '', type: '' },
        PORT_5: { name: '', type: '' },
        PORT_6: { name: '', type: '' },
        PORT_7: { name: '', type: '' },
    };
  sketch = removeComments(sketch);
  for (const portName in name) {
    const varName = grabVariableName(portName, sketch);
    const varType = grabVariableType(varName, portName, sketch);
    if (name[portName]) {
      name[portName].name = varName;
      name[portName].type = varType;
    }
  }
  return name;
}

// Function to get the type of each variable declared in the code
function grabVariableType(varName, portName, sketch) {
  if (varName === '') return '';
  if (portName === 'ROBOCORE') {
    return 'RoboTerraRoboCore';
  } else {
    const regEx = new RegExp(`RoboTerra[a-zA-Z]*(?=\\s*${varName})`);
    const varType = sketch.match(regEx);
    if (varType === null) {
      return '';
    }
    return varType[0];
  }
}

// Function to get the variable name
function grabVariableName(portName, sketch) {
    // If launching robocore, variable name declared in first step
  if (portName === 'ROBOCORE') {
        // Matches for the RoboTerraRoboCore roboName;
    const result = sketch.match(/RoboTerraRoboCore\s*[a-zA-Z]*(?=;)/);
    if (result === null) {
      return '';
    }
    const wordArray = result.toString().split(/(\s+)/);
    const varName = wordArray[wordArray.length - 1];
    return varName.toString();
  }
    // For all other components, variable declared when you attach to robocore
  else {
        // Matches for the .attach(VARNAME, portName)
    const regEx = new RegExp(`[a-zA-Z]*(?=,\\s*${portName})`);
    const varName = sketch.match(regEx);
    if (varName === null) {
      return '';
    }
    return varName.toString();
  }
}

function parsePorts(portsArr) {
  const parsed = {};
    // console.log(portsArr);
  if (!portsArr) {
    return parsed;
  }
  for (let x = 0; x < portsArr.length; x++) {
    const line = portsArr[x];
        // console.log(line);
    const type = line.substring(line.indexOf(':') + 1);
    const port = line.substring(0, line.indexOf(':'));
    parsed[port] = type;
  }
  return parsed;
}

// Gets the port and variables, checks if user attached to correct ports
function checkNamesTypes(solution_map, user_map) {
  const userTypes = user_map;
  const solutionTypes = solution_map;
  console.log('variable NAMES,', userTypes); // HMM THIS ONE IS CURRENTLY NOT UPDATED YET
  console.log('PORT TYPES:', solutionTypes);
  for (const portName in solutionTypes) {
    const solutionType = solutionTypes[portName].type;
    const userType = userTypes[portName].type;
    if (userType !== solutionType) {
      console.log('userType', userType);
      console.log('solutionType', solutionType);
      return false;
    }
  }
  return true;
}

export const uploadSketch = (checkCode) => {
  const uploadHandler = (dispatch, getState) => {
    dispatch({
      type: CANCEL_CHECK
    });

    const {projectMode, shared} = getState();
    const challengeState = projectMode.challenge;

    let solution_sketch;
    let solution_map;
    if (checkCode) {
      solution_sketch = challengeState.currentChallengeSet[challengeState.currentChallengeSelected].content.ending_template;
      solution_map = updateNames(solution_sketch);
      dispatch({
        type: MAP_SOLUTION,
        names: solution_map
      });
    }
    const user_map = updateNames(getState().shared.codepad.sketch);
    dispatch({
      type: UPDATE_NAMES,
      names: user_map
    });
    let uploadCode = false;
    if (checkCode) {
      const result = checkNamesTypes(solution_map, user_map);
      if (result || shared.app.erraMode) {
        dispatch({
          type: PROCEED_CHECK
        });
        uploadCode = true;
      }
      else {
                // console.log("STOP_CHECK");
        dispatch({
          type: UPLOAD_ERROR,
          error: 'Check your declarations and connections\nto RoboCore in step 1 and step 2.'
        });
        dispatch({
          type: TOGGLE_ERROR_BANNER
        });
        dispatch({
          type: CANCEL_CHECK
        });
        setTimeout(() => {
          dispatch({
            type: UPDATE_ERROR
          });
        }, 2500);
        uploadCode = false;
      }
    } else {
      uploadCode = true;
    }

        // if need to upload, start uploading
    if (uploadCode) {
            // dispatch upload start
      dispatch({
        type: UPLOAD_START
      });
            // console.log('UPLOAD START');
      const payload = {
        fileName: 'sketch',
        fileContent: getState().shared.codepad.sketch
      };

            // Somehow post request payload not carried or ignored if use node-fetch
      window.fetch(CompilerEndPoint, {
        method: 'POST',
        headers: myHeader,
        body: JSON.stringify(payload)
      }).then((res) => {
                // console.log("GOT RESPONSE", res);
                // Handle Upload here
        return res.json();
      }).then((data) => {
                // console.log(data);
        if (data.code === 0) {
                    // Compilation Error
          throw new Error(data.content);
        } else {
          console.log('RESPONSE PARSED', data);
          const hex = window.atob(data.firmware);
          return SkylineCore.upload(hex);
        }
      }).then(() => {
        console.log('UPLOAD_SUCCESS');
        dispatch({
          type: UPLOAD_SUCCESS
        });
        // Reconnect RoboCore after successful upload
        // VERY IMPORTANT
        dispatch(setup());

        dispatch(monitor());
      }, (e) => {
        dispatch({
          type: UPLOAD_ERROR,
          error: e
        });
        dispatch({
          type: TOGGLE_ERROR_BANNER
        });
        setTimeout(() => {
          dispatch({
            type: UPDATE_ERROR
          });
        }, 4000);
        dispatch({
          type: CANCEL_CHECK
        });
      });
    }
  };
  return SerialEnv ? uploadHandler : (dispatch) => {
    dispatch({
      type: UPLOAD_NOT_AVAILABLE
    });
  };
};

// Takes events from robocore and seperate type and port
function parseEvent(theEvent = '', state) {
  const mapping = state.shared.codepad.variableNames; // mapping from port to name and type
  if (theEvent.indexOf('ROBOCORE') != -1) {
    return {
      type: 'ROBOCORE',
      port: 'ROBOCORE',
    };
  } else {
    let underscoreCounter = 0;
    let substrIndex = 0;
    for (substrIndex; substrIndex < theEvent.length; substrIndex++) {
      if (theEvent[substrIndex] === '-') {
        underscoreCounter++;
        if (underscoreCounter === 1) break;
      }
    }
    const port = theEvent.substring(0, substrIndex);
        // console.log("port ", port);
        // console.log(mapping[port]);
    return {
      type: mapping[port].type.toLowerCase(),
      port
    };
  }
}

function parseRoboTerraEventHandler(code) {
  const re = /void handleRoboTerraEvent\(\) ?{((.|\n)*)}/;
  // console.log('code', code);
  const m = re.exec(code);
  // console.log('m from regex', m);
  if (m !== null) {
    if (m.index === re.lastIndex) {
      re.lastIndex++;
    }
    return m[1];
  }
  // console.log('returning empty string in code parse');
  return '';
}

export const startDebug = () => {
  return (dispatch, getState) => {
    const state = getState();
    const currentChallenge = state.projectMode.challenge.currentChallengeSet[state.projectMode.challenge.currentChallengeSelected];
    // console.log('currentChallenge', currentChallenge);
    // console.log('current event order', state.currentEventOrder);
    const event = currentChallenge.content.checklist[state.projectMode.challenge.currentEventOrder].event[0]; // if multiple in set, just check first
    // console.log('event', event);

    const parse = parseEvent(event, state);
    const part = parse.type;
    const port = parse.port;
    const sensorSet = ['roboterralightsensor', 'roboterrasoundsensor', 'roboterratapesensor', 'roboterrabutton'];
    const actuatorSet = ['roboterraservo', 'roboterraled'];
    let msg = '';

    // console.log('current challenge selected', state.currentChallengeSelected);
    // console.log('challenge', state.currentChallengeSet[state.currentChallengeSelected]);
    // console.log('parse', parse);

    if (sensorSet.indexOf(part) != -1) { // part is a sensor
      // HERE: MAKE A CUSTOM MAPPING OF EACH EVENT TYPE TO THE ERROR Message. NEEDS TO BE HARDCODED.
      if (part === 'roboterralightsensor') msg += 'Light sensor is not triggered.\n';
      if (part === 'roboterrasoundsensor') msg += 'Sound sensor is not triggered.\n';
      if (part === 'roboterratapesensor') msg += 'Tape sensor is not triggered.\n';
      if (part === 'roboterrabutton') msg += 'Button is not triggered.\n';
      msg += `Also, please check the physical connection of ${part} on your Robocore.\nIt should be connected to ${port}.`;
            // double check on your robocore that sensor A is connected to port A
    } else { // part is an actuator
      // console.log('ending template:', currentChallenge.content.ending_template);
      const codeGold = parseRoboTerraEventHandler(currentChallenge.content.ending_template);
      const treeGold = { type: 'root', condition: '', content: [] };
      const codeInput = parseRoboTerraEventHandler(state.sketch);
      const treeInput = { type: 'root', condition: '', content: [] };
      // console.log('codeGold', codeGold);
      // console.log('codeInput', codeInput);
      const errorInfo = [];
      const stackTrace = [];
      const errorFound = [false];

      const inputMap = state.shared.codepad.variableNames;

      const goldMap = state.shared.codepad.solutionMapping;
      // console.log('gold map', goldMap);
            // the tree results will be saved in tree.content
      generateTree(codeGold, treeGold.content, goldMap);
      // console.log('treeGold', treeGold);
      generateTree(codeInput, treeInput.content, inputMap);
      // console.log('treeInput', treeInput);
      // console.log(util.inspect(treeGold, false, null));
      // console.log(util.inspect(treeInput, false, null));
      compareTrees(treeGold, treeInput, errorInfo, stackTrace, errorFound);
      msg = printResults(errorInfo, stackTrace, inputMap);

      if (msg === '') {
        msg = `(actuator hardware) Please check the physical connection of ${part} on your Robocore.\nIt should be connected to ${port}.`;
        // double check that on your robocore led A is connected to port A
        // THIS WON'T ACTUALLY EVER HAPPEN BECAUSE IF THE CODE IS CORRECT
        // EVEN IF THE CONNECTIONS ARE WRONG THE CHALLENGE WILL STILL generate
        // THE CORRECT EVENTS AND PASS
        // still good to have if user presses debug because they don't see their
        // light turning on
      }
    }
    dispatch({
      type: UPLOAD_ERROR,
      error: msg
    });
    if (!state.showErrorBanner) {
      dispatch({
        type: TOGGLE_ERROR_BANNER
      });
    }
    setTimeout(() => {
      dispatch({
        type: UPDATE_ERROR
      });
    }, 2500);
  };
};


export const cancelCheck = () => {
  return {
    type: CANCEL_CHECK
  };
};

export const toggleToolbox = () => {
  return {
    type: TOGGLE_TOOLBOX
  };
};

export const hideToolbox = () => {
  return {
    type: HIDE_TOOLBOX
  };
};

export const toggleChallengeBanner = () => {
  return {
    type: TOGGLE_CHALLENGE_BANNER
  };
};

export const closeChallengeBanner = () => {
  return {
    type: CLOSE_CHALLENGE_BANNER
  };
};

export const closeErrorBanner = () => {
  return {
    type: CLOSE_ERROR_BANNER
  };
};

export const resetCode = () => {
  return (dispatch, getState) => {
    const startingTemplate = getState().projectMode.challenge.currentChallengeSet[getState().projectMode.challenge.currentChallengeSelected].content.start_template;
    dispatch({
      type: SKETCH_CHANGE,
      sketch: startingTemplate
    });
  };
};

export const showWarning = () => {
  return {
    type: SHOW_WARNING
  };
};

export const closeWarning = () => {
  return {
    type: CLOSE_WARNING
  };
};

export const closeCompletion = () => {
  return {
    type: CLOSE_COMPLETION
  };
};


// Start monitor events
export const monitor = ()=>{
  return (dispatch)=>{
    // See if initialize robot
    let init = false;
    let parts = [];

    // Listen to open events
    SkylineCore.port.once('open', (e) => {
      console.error('Port Open!!!!!');
      dispatch(eventReset());
      ProtocolReader.reset();
    });

    // Listen to events
    SkylineCore.port.on('data', onData);

    SkylineCore.port.open();
    // Listen to data events and parse incoming protocal data
    function onData(data){

      const flag = ProtocolReader.parse(data);

      if (flag) {
        const raw_message = ProtocolReader.read();
                  // Core Message could be events or states
        const parts_arr = RobotParser(raw_message.events);

        const states_arr = raw_message.states;


        if (parts_arr) {
                      // Make sure update layout only when needed
                      // Dispatch Robot Related Actions Here
          parts_arr.forEach((p) => {
                          // Get all robot device data into this part array
            if (p.event_name === 'ACTIVATE' || p.event_name === 'DEACTIVATE') {
              parts.push(p);
            }
                          // Reset button incase --- reset all robotstates
                          // Also turn on initialization sequence
            if (p.event_name === 'ROBOCORE_LAUNCH') {
                              // Reset event history
              init = true;
            }
          });

          // Intialization sequence
          if (init) {
            // Update Layout
            // Use only active parts
            dispatch(layoutUpdate(parts));
            // Use all events to get most updated parts
            init = false;
            // Clear robot parts array
            parts = [];
          }

          // Update sequence
          dispatch(deviceUpdate(parts_arr));
          // parts_arr.forEach(p=>{
          //     dispatch(eventReceived([p]));
          // });

          dispatch(eventReceived(parts_arr));
        }

        // dispatch state related actions
        if (states_arr) {
          // console.log("state challenge detected : ", states_arr);
          dispatch(stateReceived(states_arr));
        }
      }
    }
  }
}

export const unmountRoboCore = () => {
  return SerialEnv ? (dispatch, getState) => {
    // Tell Others Stop Trying to reconnect
    dispatch({type: END_CONNECT});
    if (getState().shared.codepad.roboCoreStatus.connected) {
      SkylineCore.port.close(err=>{
        if(err){
          console.log(err);
        }
      });
    }
  // SkylineCore.port.close();
  } : () => {

  };
};
// Call a function until func resolves
function callUntilResolve(target, timeout, onSuccess, onError, stop) {
  if (stop()) {
    console.log('STOP RECONNECTING ROBOCORE');
    return;
  } else {
    // console.log(target);
    target().then((success) => {
      onSuccess();
    }, (error) => {
      onError(error);
      setTimeout(() => {
        callUntilResolve(target, timeout, onSuccess, onError, stop);
      }, timeout);
    });
  }
}
export const setupRoboCore = setup;
function setup(){
  return SerialEnv ? (dispatch, getState) => {
    console.log('Initialiting RoboCore');
    // Start reconnecting sequence
    dispatch({type :START_CONNECT})
    // When Core finishes Initialization
    function onConnectSuccess() {
      console.log('PORT INITIALIZED');

      // Port disconnect --- unplug
      SkylineCore.port.once('disconnect',e=>{
          console.log('PORT DISCONNECTED');
          callUntilResolve(SkylineCore.init.bind(SkylineCore), 1000, onConnectSuccess, onConnectFail, stopFunc);
      });
      // Listen to close events
      SkylineCore.port.once('close', (e) => {
        console.log('PORT CLOSED');
        //Remove All Listeners dispite using once
        SkylineCore.port.removeAllListeners('data');
        SkylineCore.port.removeAllListeners('open');
        SkylineCore.port.removeAllListeners('close');
        SkylineCore.port.removeAllListeners('disconnect');
        dispatch(deviceSetup());
        dispatch(layoutSetup());
        dispatch(eventReset());
        dispatch({
          type: CORE_INIT_ERROR,
          error: 'Port Disconnected'
        });
      });

      // Notify state that Core has finished
      dispatch({
        type: CORE_INIT_SUCCESS
      });
    }
    function onConnectFail(err) {
      console.log(err, ' CONNECTION FAILED');
      dispatch({
        type: CORE_INIT_ERROR,
        error: err
      });
    }

    // Stop functions for stopping robocore
    function stopFunc() {
      console.log('Calling Stop Func');
      if(getState().shared.codepad.roboCoreStatus.uploading){
          return true;
      }
      // console.log('STOP ? ', getState().shared.codepad.connectRoboCore);
      return !getState().shared.codepad.connectRoboCore;
      // const currentState = getState().projectMode.challenge;
      // const allState = getState();
      //
      // let showBuildView;
      // if (allState.shared.app.rootLocation !== 2 && currentState.currentChallengeSet.length != 0) {
      //   const type = currentState.currentChallengeSet[currentState.currentChallengeSelected].type;
      //
      //   showBuildView = type === 'build';
      // }
      // else {
      //
      //   showBuildView = false;
      // }
      // const codeChallenge = (allState.shared.app.rootLocation === 0 && currentState.challengeLocation === 3) ? !showBuildView : false;
      // const stopFlag = !(allState.shared.app.rootLocation === 2 || codeChallenge);
      //
      // return stopFlag;
    }
    // let stopFlag =
    // Need to bind this keyword
    callUntilResolve(SkylineCore.init.bind(SkylineCore), 1000, onConnectSuccess, onConnectFail, stopFunc);

  } : (dispatch) => {
    const device_arr = [{
      event_name: 'ACTIVATE',
      state: 0,
      port: 'SERVO_A',
      name: 'servo',
      size: 3,
      msg: [{
        label: 'Target Angle',
        value: 0
      }, {
        label: 'Speed',
        value: 0
      }]
    }, {
      event_name: 'ACTIVATE',
      state: 0,
      port: 'SERVO_B',
      name: 'servo',
      size: 3,
      msg: [{
        label: 'Current Angle',
        value: 0
      }, {
        label: 'Speed',
        value: 0
      }]
    }, {
      event_name: 'ACTIVATE',
      state: 0,
      port: 'DIO_1',
      name: 'button',
      size: 2,
      msg: [{
        label: 'Press Count',
        value: 0
      }]
    }, {
      event_name: 'ACTIVATE',
      state: 0,
      port: 'DIO_2',
      name: 'led',
      size: 2,
      msg: [{
        label: 'Blink Count',
        value: 0
      }]
    }, {
      event_name: 'ACTIVATE',
      state: 0,
      port: 'DIO_3',
      name: 'sound',
      size: 2,
      msg: [{
        label: 'Sound Count',
        value: 0
      }]
    }, {
      event_name: 'ACTIVATE',
      state: 0,
      port: 'DIO_4',
      name: 'tape',
      size: 2,
      msg: [{
        label: 'Tape Count',
        value: 0
      }]
    }, {
      event_name: 'ACTIVATE',
      state: 0,
      port: 'DIO_5',
      name: 'light',
      size: 2,
      msg: [{
        label: 'Light Count',
        value: 0
      }]
    }, {
      event_name: 'ACTIVATE',
      state: 0,
      port: 'DIO_6',
      name: 'irr',
      size: 3,
      msg: [{
        label: 'Value',
        value: 0
      }, {
        label: 'Address',
        value: 0
      }]
    }, {
      event_name: 'ACTIVATE',
      state: 0,
      port: 'MOTOR_A',
      name: 'motor',
      size: 3,
      msg: [{
        label: 'Speed',
        value: 0
      }, {
        label: 'Direction',
        value: 0
      }]
    }];
        // console.log('dispatching');
    dispatch(layoutUpdate(device_arr));
    dispatch(deviceUpdate(device_arr));
    dispatch(eventReceived(
      [
        {
          port: 'ROBOCORE',
          event_name: 'ROBOCORE_LAUNCH',
        }, {
          port: 'DIO_2',
          event_name: 'BUTTON_PRESS',
          msg: [{
            value: 1
          }, {
            value: 2
          }]
        }, {
          port: 'DIO_1',
          event_name: 'LED_TURNON',
          msg: [{
            value: 1
          }]
        }, {
          port: 'DIO_2',
          event_name: 'SERVO_INCREASE_BEGIN',
          msg: [{
            value: 180
          }, {
            value: 3
          }]
        }, {
          port: 'ROBOCORE',
          event_name: 'ROBOCORE_LAUNCH',
        }, {
          port: 'DIO_2',
          event_name: 'BUTTON_PRESS',
          msg: [{
            value: 1
          }, {
            value: 2
          }]
        }, {
          port: 'DIO_1',
          event_name: 'LED_TURNON',
          msg: [{
            value: 1
          }]
        }, {
          port: 'DIO_2',
          event_name: 'SERVO_INCREASE_BEGIN',
          msg: [{
            value: 180
          }, {
            value: 3
          }]
        }, {
          port: 'ROBOCORE',
          event_name: 'ROBOCORE_LAUNCH',
        }, {
          port: 'DIO_2',
          event_name: 'BUTTON_PRESS',
          msg: [{
            value: 1
          }, {
            value: 2
          }]
        }, {
          port: 'DIO_1',
          event_name: 'LED_TURNON',
          msg: [{
            value: 1
          }]
        }, {
          port: 'DIO_2',
          event_name: 'SERVO_INCREASE_BEGIN',
          msg: [{
            value: 180
          }, {
            value: 3
          }]
        }
      ]
        ));
  };
};
