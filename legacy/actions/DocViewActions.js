import DocumentationData from 'legacy_mock/docdata';
import DocumentationDataCN from 'legacy_mock/docdataChinese';
export const DOC_LOAD_SUCCESS = 'DOC_LOAD_SUCCESS';
export const DOC_LOAD_FAILED = 'DOC_LOAD_FAILED';
export const TOGGLE_OPEN = 'TOGGLE_OPEN';
export const TOGGLE_INNER_OPEN = 'TOGGLE_INNER_OPEN';

function loadLocal() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
          if(localStorage.skylineDefaultLanguage === 'CHINESE'){
            resolve(DocumentationDataCN.docs);
          }else{
            resolve(DocumentationData.docs);
          }
        }, 1000);
      });
}


export const loadDocs = () => {
  return (dispatch) => {
    return loadLocal().then(
            load_success => dispatch({
              type: DOC_LOAD_SUCCESS,
              data: { Documentation: load_success },
              status: 'success'
            },

            load_error => dispatch({
              type: DOC_LOAD_FAILED,
              data: undefined,
              error: load_error,
              status: 'error'
            }))
        );
  };
};

export const toggleOpen = (id) => {
  return {
    type: TOGGLE_OPEN,
    num: id
  };
};

export const toggleInnerOpen = (id, name) => {
  return {
    type: TOGGLE_INNER_OPEN,
    num: id,
    node: name
  };
};
