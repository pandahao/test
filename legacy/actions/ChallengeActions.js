import { UPDATE_PROJECTS } from 'legacy_actions/RobotActions';
import { SHOW_COMPLETION, SKETCH_CHANGE } from 'legacy_actions/CodepadActions';
import { SkylineCloud } from 'legacy_actions/AuthActions';

export const TOGGLE_CHALLENGE_VIEW = 'TOGGLE_CHALLENGE_VIEW';
export const SELECT_CHALLENGE = 'SELECT_CHALLENGE';
export const NEXT_CHALLENGE = 'NEXT_CHALLENGE';
export const PREVIOUS_CHALLENGE = 'PREVIOUS_CHALLENGE';
export const SET_BLUR = 'SET_BLUR';
export const SHOW_CHALLENGE_VIEW = 'SHOW_CHALLENGE_VIEW';
export const SET_HINT = 'SET_HINT';
export const SET_ANSWER_SELECTED = 'SET_ANSWER_SELECTED';
export const SET_RESULT = 'SET_RESULT';
export const INITIALIZE_QUIZ_RESULT = 'INITIALIZE_QUIZ_RESULT';
export const INITIALIZE_CHALLENGE_SET = 'INITIALIZE_CHALLENGE_SET';
export const INCREMENT_CHALLENGES_COMPLETED = 'INCREMENT_CHALLENGES_COMPLETED';
export const TOGGLE_BUILD_VIEW = 'TOGGLE_BUILD_VIEW';
export const PROGRESS_NEXT = 'PROGRESS_NEXT';
export const PROGRESS_BACK = 'PROGRESS_BACK';
export const MOVE_POINT = 'MOVE_POINT';
export const INITIALIZE_ANSWERS_SELECTED = 'INITIALIZE_ANSWERS_SELECTED';
export const SET_DELAY_PAGE_CHANGE = 'SET_DELAY_PAGE_CHANGE';
export const RESET_BUILD_PROGRESS = 'RESET_BUILD_PROGRESS';
export const COMPLETED = 'COMPLETED';
export const ONGOING = 'ONGOING';
export const LOCKED = 'LOCKED';

const emptySketch = '#include "ROBOTERRA.h"\n\n/*--------------------- STEP 1 ---------------------*/\n// Name your RoboCore and all electronics attached it.\n// These names are considered as eventSource of EVENT. \n\nRoboTerraRoboCore tom;\n\n/*--------------------- STEP 2 ---------------------*/\n// Attach electronics to physical ports on RoboCore.   \n\nvoid attachRoboTerraElectronics() {\n\n}\n\n/*--------------------- STEP 3 ---------------------*/\n/* Design your robot algorithm by handling each EVENT. \n * Based on the source, type and data of each EVENT, \n * you can organize your loigc and form program flow.\n * Call Command Functions of instances you defined in\n * STEP 1 for your robot to take physical actions. \n * Calling Command Functions can result in new EVENT. \n */\n\nvoid handleRoboTerraEvent() {\n    if (EVENT.isType(ROBOCORE_LAUNCH)) {\n        // Initialize robot by calling Comand Function  \n    \n    }\n    \n    // Your robot code goes below\n\n\n}';

function resolveChallengeSketch(state, challengeId) {
  const history = state.projectMode.project.userProgress.info.projectsOwned.find((p) => {
    return p.projectId === state.projectMode.project.currentProjectID;
  }) || { records: [] };
  const challenge = state.projectMode.challenge.currentChallengeSet[challengeId];
  const record = history.records[0];
  return new Promise((resolve) => {
    if (history.progress > challenge.level) {
      if (record) {
        record.getCodeContent(challenge.id, (e, c) => {
          if (e) {
            resolve(emptySketch);
          } else {
            resolve(c.content);
          }
        });
      } else {
        resolve(challenge.content.ending_template || emptySketch);
      }
    } else {
      if (challenge.content.resetCode) {
        resolve(challenge.content.start_template);
      } else {
        if (challengeId > 1) {
          // OMG! Human will never know what's this mean.
          const lastCodeChallenge = state.projectMode.challenge.currentChallengeSet.slice(0, challengeId - 2).filter(p => {
            return p.type === 'code';
          }).pop();
          if(lastCodeChallenge) {
            record.getCodeContent(lastCodeChallenge.id, (e, c) => {
              resolve(c.content);
            });
          } else {
            resolve(emptySketch);
          }
        } else {
          resolve(emptySketch);
        }
      }
    }
  });
}

export const selectChallenge = (challenge_id) => {
  return (dispatch, getState) => {
    const state = getState();
    challenge_id--;
    const currentChallenge = state.projectMode.challenge.currentChallengeSet[challenge_id];
    if (currentChallenge.type === 'code') {
      dispatch({
        type: 'SET_CHALLENGE_EVENT_ORDER',
        newOrder: 0
      });
      resolveChallengeSketch(state, challenge_id).then((code) => {
                // console.log('GOT CODE! ', code)
        dispatch({
          type: SKETCH_CHANGE,
          sketch: code
        });
      });
    }
    dispatch({
      type: SELECT_CHALLENGE,
      challenge_id
    });
  };
};

export const nextChallenge = () => {
  return (dispatch, getState) => {
    // console.log('GOING TO NEXT');
    const state = getState();
    const proState = state.projectMode.challenge;
    const currentChallenge = proState.currentChallengeSet[proState.currentChallengeSelected + 1];

    resolveChallengeSketch(state, proState.currentChallengeSelected + 1).then((code) => {
            // console.log('GOT CODE! ', code)
      dispatch({
        type: SKETCH_CHANGE,
        sketch: code
      });
    });

    if (currentChallenge.type === 'code') {
      dispatch({
        type: 'SET_CHALLENGE_EVENT_ORDER',
        newOrder: 0
      });
    }

    dispatch({
      type: NEXT_CHALLENGE,
      max: proState.currentChallengeSet.length - 1
    });
  };
};

export const toggleChallengeView = () => {
  return (dispatch) => {
    dispatch({
      type: TOGGLE_CHALLENGE_VIEW
    });
  };
};

export const closeChallengeView = () => {
  return (dispatch) => {
    dispatch({
      type: 'CLOSE_CHALLENGE_VIEW'
    });
  };
};

export const showChallengeView = (status) => {
  return {
    type: SHOW_CHALLENGE_VIEW,
    status
  };
};

export const setBlur = (blur) => {
  return {
    type: SET_BLUR,
    blur
  };
};

export const setHint = (hint) => {
  return {
    type: SET_HINT,
    hint
  };
};

export const setAnswerSelected = (index, question) => {
  return {
    type: SET_ANSWER_SELECTED,
    index,
    question
  };
};

export const initializeAnswersSelected = () => {
  return {
    type: INITIALIZE_ANSWERS_SELECTED,
  };
};

export const setResult = (result, question) => {
  return {
    type: SET_RESULT,
    result,
    question
  };
};

export const checkAnswer = () => {
  return (dispatch, getState) => {
    // console.log('checking answer');
    const state = getState().projectMode;
    const currentProjectIndex = state.challenge.currentProjectIndex;
    const quizResult = state.challenge.quizResult;
    const quizAnswerSelected = state.challenge.quizAnswerSelected;
    const quizAnswer = state.challenge.currentChallengeSet[state.challenge.currentChallengeSelected].content.blocks;
    // GG
    for (const key in quizAnswer) {
      if ('question' in quizAnswer[key]) {
        const questionBlock = quizAnswer[key];
        if (quizAnswerSelected[questionBlock.question] == questionBlock.answer - 1) {
          dispatch(setResult('CORRECT', questionBlock.question));
        } else {
          dispatch(setResult('WRONG', questionBlock.question));
        }
      }
    }
    // console.log('UNLOCK NEXT');
    dispatch(incrementChallengesCompleted(state.project.currentProjectID));
  };
};

export const updateProjects = (currentProjectID) => {
  return (dispatch, getState) => {
    // const currentProjectID = getState().projectMode.challenge.currentProjectID;
    dispatch({
      type: UPDATE_PROJECTS,
      currentProjectID
    });
    dispatch({
      type: SHOW_COMPLETION
    });
    // dispatch(incrementChallengesCompleted(currentProjectID));
  };
};

export const initializeQuizResult = () => {
  return {
    type: INITIALIZE_QUIZ_RESULT,
  };
};


export const incrementChallengesCompleted = (currentProjectID, currentChallenge) => {
  return (dispatch, getState) => {
    //console.log(currentProjectID);
    const state = getState();
    const challenge = state.projectMode.challenge.currentChallengeSet[state.projectMode.challenge.currentChallengeSelected];
    let  currentProgress = state.projectMode.project.userProgress.info.projectsOwned.find((p) => {
      return p.projectId === currentProjectID
    }).progress // NOTE: progress is 1-indexed, and it means the furthest challenge being attempted, not completed

    console.log(`Current progress: ${JSON.stringify(currentProgress)}`);



    SkylineCloud.saveUserProgress(
            state.projectMode.project.currentProjectID,
            challenge.type,
            challenge.level,
            state.shared.codepad.sketch,
            challenge.id
        ).then((record) => {
          // console.log(record);
          // console.log('PROGRESS SAVED!');
        }, (e) => {
          console.warn('save progress error! ', e);
        });
    if(currentChallenge + 1 >= currentProgress)dispatch({
      type: INCREMENT_CHALLENGES_COMPLETED,
      currentProjectID
    });
  };
};

export const initializeChallenges = (status, challengeProgress) => {
  if (!status) {
    return;
  }
  console.log(status);
  return (dispatch, getState) => {
    const state = getState();
    challengeProgress--;

    // Making sure progress is less than the current project length
    if (challengeProgress >= state.projectMode.challenge.currentChallengeSet.length) {
      challengeProgress = state.projectMode.challenge.currentChallengeSet.length - 1;
    }
    // Right now, dispatch since whenever user enters challenge they need a template
    // console.log(state.currentChallengeSet,challengeProgress);

    if (state.projectMode.challenge.currentChallengeSet && state.projectMode.challenge.currentChallengeSet[challengeProgress].type === 'code') {
      dispatch({
        type: 'SET_CHALLENGE_EVENT_ORDER',
        newOrder: 0
      });
      dispatch({
        type: SKETCH_CHANGE,
        sketch: state.projectMode.challenge.currentChallengeSet[challengeProgress].content.start_template
      });
    }
    dispatch({
      type: SELECT_CHALLENGE,
      challenge_id: state.projectMode.project.currentProjectID === 'S0P000' ? 0 : challengeProgress
    });
    dispatch({
      type: 'CHALLENGE_VIEW'
    });
    resolveChallengeSketch(state, challengeProgress).then((code) => {
            // console.log('GOT CODE! ', code)
      dispatch({
        type: SKETCH_CHANGE,
        sketch: code
      });
    });
  };
};

export const initialize_challenge_set = () => {
  return (dispatch, getState) => {
    const state = getState();
    if (state) {
      dispatch({
        type: INITIALIZE_CHALLENGE_SET,
        // Need refactoring.... for everything...
        value: state.projectMode.project.projects.projectsData.find(
          (p) => { return p.projectID === state.projectMode.project.currentProjectID; }
        ).challengeData
      });
    }
  };
};

export const backProgress = () => {
  return {
    type: PROGRESS_BACK
  };
};

// Advance to next progress (step) on buildview
export const nextProgress = (maxpoints) => {
  return (dispatch, getState) => {
    const state = getState();
    const projectsData = state.projectMode.project.projects.projectsData;
    const index = state.projectMode.project.currentProjectIndex;
    //console.log(state)
    const current_challenge = projectsData.find((p) => { return p.projectID === state.projectMode.project.currentProjectID; }).challengeData[state.projectMode.challenge.currentChallengeSelected];

    dispatch({
      type: PROGRESS_NEXT,
      maxpoints: current_challenge.content.blocks.length
    });

    const currentProjectIDProgress = state.projectMode.project.userProgress.info.projectsOwned.find((p) => { return p.projectId === state.projectMode.project.currentProjectID; }).progress;

    if (state.projectMode.challenge.currentProgress === (current_challenge.content.blocks.length - 1) && currentProjectIDProgress === state.projectMode.challenge.currentChallengeSelected + 1) {
      // console.log('UNLOCK NEXT PROGRESS');
      dispatch(incrementChallengesCompleted(state.projectMode.project.currentProjectID));
    }
  };
};

export const resetBuildProgress = () => {
  return {
    type: RESET_BUILD_PROGRESS
  };
};

export const goToPoint = (point) => {
  return {
    type: MOVE_POINT,
    point
  };
};

export const setPageDelay = (delay) => {
  return {
    type: SET_DELAY_PAGE_CHANGE,
    delay
  };
};
