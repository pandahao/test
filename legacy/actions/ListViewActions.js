import { SkylineCloud, SET_USER } from 'legacy_actions/AuthActions';
import { loadModel } from 'legacy_lib/build/ModelManager';
import _ from 'underscore';
import Promise from 'bluebird';

export const PROJECT_LOAD_SUCCESS = 'PROJECT_LOAD_SUCCESS';
export const PROJECT_LOAD_FAILED = 'PROJECT_LOAD_FAILED';
export const LOAD_SINGLE_PROJECT = 'LOAD_SINGLE_PROJECT';
export const SELECT_PROJECT = 'SELECT_PROJECT';
export const TOGGLE_GRID_VIEW = 'TOGGLE_GRID_VIEW';
export const CLOSE_GRID_VIEW = 'CLOSE_GRID_VIEW';
export const BEGIN_PROJECT = 'BEGIN_PROJECT';
export const RESTART_PROJECT = 'RESTART_PROJECT';
export const CLOSE_RESET = 'CLOSE_RESET';
export const SHOW_RESET = 'SHOW_RESET';
export const SET_LOADING = 'SET_LOADING';


const isProjectsLoaded = (state) => {
  return state.projectMode.project.projects.status === 'success';
};

function loadOwnedProjects(userProjects, language) {
  // Loading Projects
  const tasks = [];
  userProjects.forEach((userprj) => {
    tasks.push(new Promise((resolve, reject) => {
      SkylineCloud.loadProject(userprj.projectId, language).then((prj) => {
        resolve(prj);
      }, (err) => {
        resolve(`${userprj.projectId}_${language === 'CHINESE' ? 'zh_CN' : 'en_US'}`);
      });
    }));
  });
  return Promise.all(tasks);
}

export const languageToLocale = (lang) => {
  switch (lang) {
    case 'CHINESE':
      return 'zh_CN';
    case 'ENGLISH':
      return 'en_US';
    default:
      return 'en_US';
  }
};

export const loadSingleProject = (projectID) => {
  return (dispatch, getState) => {
    SkylineCloud.loadProjectChallenge(projectID).then((data) => {
      dispatch({
        type: LOAD_SINGLE_PROJECT,
        data,
        projectID: data[0].keyname_project
      });
    }, (error) => {
      console.log(error, 'FALI');
      dispatch({
        type: LOAD_SINGLE_PROJECT,
        error
      });
    });
  };
};

export const loadProjects = () => {
  return (dispatch, getState) => {
    if (isProjectsLoaded(getState())) {
      return null;
    }
    return SkylineCloud.getUserProfile()
        .then((userInfo) => {
          dispatch({
            type: SET_USER,
            user: { info: userInfo }
          });
          return loadOwnedProjects(userInfo.projectsOwned, getState().shared.app.language);
        })
        .then((projectsData) => {
          dispatch({
            type: PROJECT_LOAD_SUCCESS,
            data: { projectsData },
            status: 'success'
          });
        },
        (err) => {
          console.log(err);
          dispatch({
            type: PROJECT_LOAD_FAILED,
            data: undefined,
            error: err,
            status: 'error'
          });
        });
  };
};

export const restartProject = (projectIndex) => {
  return {
    type: RESTART_PROJECT,
    projectIndex
  };
};

export const showReset = () => {
  return {
    type: SHOW_RESET
  };
};

export const closeReset = () => {
  return {
    type: CLOSE_RESET
  };
};

export const selectProject = (projectID) => {
  return (dispatch, getState) => {
    let projectIndex = 0;
    const state = getState().projectMode.project;
    const projectsData = state.projects.projectsData;
    for (let i = 0; i < projectsData.length; i++) {
      if (projectsData[i].projectID === projectID) {
        projectIndex = i;
        break;
      }
    }
    dispatch({
      type: SELECT_PROJECT,
      projectID,
      projectIndex
    });
  };
};

export const beginProject = (projectID) => {
  return {
    type: BEGIN_PROJECT,
    projectID
  };
};

export const toggleGridView = () => {
  return {
    type: TOGGLE_GRID_VIEW
  };
};

export const closeGridView = () => {
  return {
    type: CLOSE_GRID_VIEW
  };
};

export const setLoading = (loading) => {
  return {
    type: SET_LOADING,
    loading
  };
};
