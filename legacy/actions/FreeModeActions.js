import { SkylineCloud } from 'legacy_actions/AuthActions';
import { displayGlobalMessage } from 'legacy_actions/shared/RuntimeActions';
import { sketchChange, sketchReset, clearDirty, init_sketch } from 'legacy_actions/CodepadActions';
import SaveRobotDialog from 'components/freemode/SaveRobotDialog';
import ConfirmDialog from 'legacy_components/common/ConfirmDialog';
import { t } from 'legacy_utils/i18n';
import { format } from 'legacy_utils/error';
import { freemode } from 'legacy_actions/AppActions';

import { generateExternalGraph } from '../../src/dnd-editor/utils/code-gen';

export const LOAD_ROBOTS = 'LOAD_ROBOTS';
export const LOAD_ROBOTS_SUCCESS = 'LOAD_ROBOTS_SUCCESS';
export const LOAD_ROBOTS_FAIL = 'LOAD_ROBOTS_FAILED';
export const LOAD_ROBOT_SUCCESS = 'LOAD_ROBOT_SUCCESS';
export const DELETE_ROBOT_SUCCESS = 'DELETE_ROBOT_SUCCESS';
export const NEW_ROBOT = 'NEW_ROBOT';
export const SAVE_ROBOT_SUCCESS = 'SAVE_ROBOT_SUCCESS';
export const SAVE_ROBOT_FAIL = 'SAVE_ROBOT_FAIL';
export const CHANGE_FREEMODE_VIEW = 'CHANGE_FREEMODE_VIEW';
export const TOGGLE_DOCUMENTATION = 'TOGGLE_DOCUMENTATION';

export const OPEN_SAVE_EXISTING = 'OPEN_SAVE_EXISTING';

export const openSaveExistingFreeModeSessionDialog = () => {
  return dispatch => {
    dispatch({
      type: 'OPEN_SAVE_EXISTING'
    });
  };
};

export const CLOSE_SAVE_EXISTING = 'CLOSE_SAVE_EXISTING';

export const closeSaveExistingFreeModeSessionDialog = () => {
  return dispatch => {
    dispatch({
      type: 'CLOSE_SAVE_EXISTING'
    });
  };
};

const codeToRobot = (code, content) => {
  return {
    id: code.id,
    name: code.keyname,
    created: code.dt_create * 1000,
    updated: code.dt_update * 1000,
    content,
    raw: code // keep the reference so we can lazy load the content
  };
};

export const changeFreeModeView = newView => {
  return (dispatch, getState) => {
    // switch view first
    dispatch(freemode());
    dispatch({
      type: CHANGE_FREEMODE_VIEW,
      view: isDirty(getState()) ? 'editor' : newView
    });
    // Nullity of robots can't be use to distinguish if user has no codes on records, or if API call loading codes hasn't been called. So we are going to load codes every time upon entering freemode
    dispatch(loadRobots());
  };
};

export const loadRobotsFail = error => {
  return {
    type: LOAD_ROBOTS_FAIL,
    error
  };
};

export const loadRobotsSuccess = items => {
  return {
    type: LOAD_ROBOTS_SUCCESS,
    items: items.map(codeToRobot)
  };
};

export const loadRobots = () => {
  return dispatch => {
    dispatch({
      type: LOAD_ROBOTS
    });
    SkylineCloud.getUserCodes().then(
      codes => {
        dispatch(loadRobotsSuccess(codes));
      },
      err => {
        dispatch(loadRobotsFail(err));
        dispatch(displayGlobalMessage(t(format(err.message)), 'error'));
      }
    );
  };
};

export const deleteRobot = robot => {
  return dispatch => {
    const confirm = new ConfirmDialog(`${t('Delete Robot')} ${robot.name} ?`);
    confirm.show(() => {
      SkylineCloud.deleteUserCode(robot.id).then(
        () => {
          confirm.close();
          dispatch({
            type: DELETE_ROBOT_SUCCESS,
            robot
          });
          dispatch(sketchReset());
        },
        err => {
        }
      );
    });
  };
};

export const loadRobot = robot => {
  return saveEditingRobotThen(_loadRobot(robot));
};

const _loadRobot = robot => {
  return dispatch => {
    robot.raw.getCodeContent((err, content) => {
      if (err) {
      } else {
        dispatch({
          type: LOAD_ROBOT_SUCCESS,
          robot: Object.assign({}, robot, {
            content
          })
        });
        dispatch(sketchChange(content));
        dispatch(changeFreeModeView('editor'));
      }
    });
  };
};

export const toggleDocumentation = state => {
  return {
    type: TOGGLE_DOCUMENTATION
  };
};

export const saveRobotSuccess = robot => {
  return {
    type: SAVE_ROBOT_SUCCESS,
    robot
  };
};

export const saveRobotFail = err => {
  return {
    type: SAVE_ROBOT_FAIL,
    err
  };
};

export const saveRobot = (dispatch, state) => {
  const current = state.freeMode.editingRobot.item;

  const tech = current.raw.tech;
  const code = tech === 'dnd'
    ? generateExternalGraph(state.dndEditor.code)
    : state.shared.codepad.sketch;

  // save right away
  SkylineCloud.updateUserCode(current.id, current.name, code, current.raw.tech).then(
    code => {
      dispatch(saveRobotSuccess(codeToRobot(code, state.shared.codepad.sketch)));
      dispatch(clearDirty());
      dispatch(displayGlobalMessage(t('Robot saved')));
    },
    err => {
      dispatch(saveRobotFail(err));
    }
  );
};

export const createRobot = () => {
  return saveEditingRobotThen(_createRobot);
};

const _createRobot = (dispatch, getState) => {
  const dialog = new SaveRobotDialog();
  const sketch = init_sketch;
  dispatch(sketchReset());
  dialog.show((value, tech) => {
    const ctype = 'freeedit';
    const cid_challenge = 1;
    SkylineCloud.createUserCode(value, sketch, tech, ctype, cid_challenge).then(
      code => {
        dispatch(saveRobotSuccess(codeToRobot(code, tech === 'dnd' ? '' : sketch)));
        dispatch(changeFreeModeView('editor'));
        dialog.close();
      },
      err => {
        dialog.error(t(format(err.message)));
      }
    );
  });
};

export const saveCurrentRobot = () => {
  return (dispatch, getState) => {
    const state = getState();
    if (state.freeMode.editingRobot.item.id == null) {
      // new robot
      createRobot(dispatch, state);
    } else {
      saveRobot(dispatch, state);
    }
  };
};

export const saveAsCurrentRobot = () => {
  return (dispatch, getState) => {
    // popup dialog
    const state = getState();
    const dialog = new SaveRobotDialog(t('Save program as'));
    dialog.show((value, tech) => {
      const ctype = 'freeedit';
      const cid_challenge = 1;
      SkylineCloud.createUserCode(value, state.shared.codepad.sketch, tech, ctype, cid_challenge).then(
        code => {
          dispatch(saveRobotSuccess(codeToRobot(code, state.shared.codepad.sketch)));
          dispatch(clearDirty());
          dialog.close();
          dispatch(saveCurrentRobot());
        },
        err => {
          dialog.error(t(format(err.message)));
        }
      );
    });
  };
};

const getEditingRobot = state => {
  return state.freeMode.editingRobot.item;
};

const isDirty = state => {
  return state.shared.codepad.dirty === true;
};

export const saveRobotThunk = () => {
  return (dispatch, getState) => {
    const state = getState();
    const current = state.freeMode.editingRobot.item;
    return SkylineCloud.updateUserCode(
      current.id,
      current.name,
      state.shared.codepad.sketch,
      current.raw.tech
    ).then(
      code => {
        dispatch(saveRobotSuccess(codeToRobot(code, state.shared.codepad.sketch)));
        dispatch(clearDirty());
        return code;
      },
      err => {
        dispatch(saveRobotFail(err));
        return err;
      }
    );
  };
};

const needsUpdate = getState => {
  return getEditingRobot(getState()).id && isDirty(getState());
};

export const appendAction = (precedingAction, succeedingAction) => {
  return dispatch => {
    return dispatch(precedingAction).then(() => {
      return dispatch(succeedingAction);
    });
  };
};

export const ignoreAction = actionCreator => {
  return Promise.resolve(null);
};

export const justAction = actionCreator => {
  return actionCreator;
};

export const doBeforeClose = actionCreator => {
  return appendAction(actionCreator, closeSaveExistingFreeModeSessionDialog());
};

export const doAfterClose = actionCreator => {
  return appendAction(closeSaveExistingFreeModeSessionDialog(), actionCreator);
};

export const skipSave = clearDirty;

export const saveEditingRobotThen = actionCreator => {
  return (dispatch, getState) => {
    if (needsUpdate(getState)) {
      dispatch(saveRobotThunk())
        .then(data => {
          dispatch(actionCreator);
        })
        .catch(err => {
        });
    } else {
      dispatch(actionCreator);
    }
  };
};

export const promptSaveEditingRobot = actionCreator => {
  return (dispatch, getState) => {
    if (needsUpdate(getState)) {
      dispatch(openSaveExistingFreeModeSessionDialog());
    } else {
      dispatch(actionCreator);
    }
  };
};

export const promptSaveEditingRobotBeforeCodeVault = () => {
  return promptSaveEditingRobot(changeFreeModeView('list'));
};

export const newRobot = () => {
  return dispatch => {
    dispatch({
      type: NEW_ROBOT
    });
    dispatch(sketchReset());
    dispatch(displayGlobalMessage(t('Robot reset')));
  };
};
