export const SELECT_SETTING = 'SELECT_SETTING'
export const SAVE_PROFILE = 'SAVE_PROFILE'
export const CHANGE_PROFILE = 'CHANGE_PROFILE'

export const ACCOUNT = 'Account'
export const PROFILE = 'Profile'
export const LANGUAGE = 'Language'
export const VERSION = 'Version'
export const SEND = 'Send'

export const selectSetting = (setting_id) => {
  return {
    type: SELECT_SETTING,
    setting_id
  };
};

export const changeProfile = () => {
  return {
    type: CHANGE_PROFILE
  };
};

export const saveProfile = () => {
  return {
    type: SAVE_PROFILE
  };
};
