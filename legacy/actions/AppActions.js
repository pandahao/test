export const TOGGLE_DOC_VIEW = 'TOGGLE_DOC_VIEW';
export const CLOSE_DOC_VIEW = 'CLOSE_DOC_VIEW';

export const SET_ONBOARDING = 'SET_ONBOARDING';

export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';

export const SET_LANGUAGE = 'SET_LANGUAGE';

export const [CHINESE, ENGLISH] = ['CHINESE', 'ENGLISH']

export const SUPPORTED_LANGUAGES = [CHINESE, ENGLISH];

export const REQUEST_PROJECTS_RELOAD = 'REQUEST_PROJECTS_RELOAD';

import { LAYOUT_RESIZE } from 'legacy_actions/RobotActions';

export const changeLanguage = (language) => {
  return (dispatch) => {

    dispatch({
      type: CHANGE_LANGUAGE,
      language
    })

    dispatch(requestProjectsReload())
  }
};

export const requestProjectsReload = () => {
  return {
    type: REQUEST_PROJECTS_RELOAD
  }
}

export const setLanguage = (language) => {
  return {
    type: SET_LANGUAGE,
    language
  };
};


export const homeView = () => {
  return {
    type: 'HOME_VIEW'
  };
};

export const projectView = () => {
  return {
    type: 'PROJECT_VIEW'
  };
};

export const settingsView = () => {
  return {
    type: 'SETTINGS_VIEW'
  };
};

export const freemode = () => {
  return {
    type: 'FREEMODE'
  };
};

export const gridView = () => {
  return {
    type: 'GRID_VIEW'
  };
};

export const listView = () => {
  return {
    type: 'LIST_VIEW'
  };
};

export const overview = () => {
  return {
    type: 'OVERVIEW'
  };
};

export const challengeView = () => {
  return {
    type: 'CHALLENGE_VIEW'
  };
};

export const setOnboarding = (value) => {
  return {
    type: 'SET_ONBOARDING',
    value
  };
};

export const setTutorialOnboard = (value) => {
  return {
    type: 'SET_TUTORIAL_ONBOARD',
    value
  };
};

export const toggleDocView = () => {
  return {
    type: TOGGLE_DOC_VIEW
  };
};

export function turnOnErraMode() {
  return {
    type: 'TURN_ON_ERRA'
  };
}

export function turnOffErraMode() {
  return {
    type: 'TURN_OFF_ERRA'
  };
}

export const closeDoc = () => {
  return (dispatch, getState) => {
    dispatch({
      type: CLOSE_DOC_VIEW
    });
    // if (getState().freemode) {
    //   dispatch({
    //     type: LAYOUT_RESIZE,
    //     row: getState().docView ? 6 : 8
    //   });
    // }
  };
};
