import {ChromeEnv} from 'config';
import Cloud from 'legacy_lib/cloud/index';
import { t } from 'legacy_utils/i18n';
import { push } from 'react-router-redux';
import { displayGlobalMessage } from 'legacy_actions/shared/RuntimeActions';

export const LOG_IN = 'LOG_IN';
export const LOG_OUT = 'LOG_OUT';
export const PASSWORD_CHANGE = 'PASSWORD_CHANGE';
export const USERNAME_CHANGE = 'USERNAME_CHANGE';
export const NEWPASSWORD_CHANGE = 'NEWPASSWORD_CHANGE';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const SET_USER = 'SET_USER';
export const BEGIN_LOGIN = 'BEGIN_LOGIN';

/* locations */
export const LOGIN = 0;
export const REGISTER_ONE = 1;
export const REGISTER_TWO = 2;
export const PASSWORD = 3;
export const LICENSE = 4;
export const CONFIRM = 5;
export const RESET = 6;

export const SkylineCloud = new Cloud();

export const setAuthLocation = (loc) => {
  return {
    type: loc
  };
};

export const logOut = () => {
  return (dispatch) => {
    if(ChromeEnv){
      localStorage.clear();
      dispatch(push('/'));
      dispatch({
        type: LOG_OUT
      })
    }else{
      SkylineCloud.logout()
      .then(() => {
        dispatch(push('/'));
        dispatch({
          type: LOG_OUT
        });
      },
      (err) => {
        dispatch({
          type: 'AUTH_ERROR',
          err
        });
        dispatch(displayGlobalMessage(t(format(err.message)), 'error'));
      });
    }
  };
};

export const resendResetRequest = () => {
  return (dispatch, getState) => {
    const username = getState().auth.username;
    SkylineCloud.sendResetPasswordRequest(username);
  };
};
export const sendResetRequest = (username) => {
  return (dispatch, getState) => {
    // const username = getState().auth.username;
    SkylineCloud.sendResetPasswordRequest(username)
        .then(() => {
          dispatch({
            type: RESET
          });
        }, (err) => {
          dispatch({
            type: 'AUTH_ERROR',
            err: err.message
          });
          dispatch(displayGlobalMessage(t(format(err.message)), 'error'));
        });
  };
};
export const resetPassword = () => {
  return (dispatch, getState) => {
    const state = getState().auth;
    const username = state.username;
    const confirmCode = state.registerAccount.confirmCode;
    const newPass = state.newPassword;
    SkylineCloud.resetPassword(username, confirmCode, newPass)
        .then(() => {
          dispatch({
            type: LOGIN
          });
        }, (err) => {
          dispatch({
            type: 'AUTH_ERROR',
            err: err.message
          });
          dispatch(displayGlobalMessage(t(format(err.message)), 'error'));
        });
  };
};

export const newPassChange = (val) => {
  return {
    type: NEWPASSWORD_CHANGE,
    val
  };
};
export const passChange = (val) => {
  return {
    type: PASSWORD_CHANGE,
    val
  };
};

export const userChange = (val) => {
  return {
    type: USERNAME_CHANGE,
    val
  };
};

export const beginLogin = () => {
  return {
    type: BEGIN_LOGIN
  };
};

export const checkLogin = () => {
  return (dispatch) => {
    SkylineCloud.isAuthenticated()
        .then(() => {
          dispatch({
            type: SET_USER,
            user: { info: { projectsOwned: [] } }
          });
          dispatch({
            type: LOG_IN
          });
          dispatch(push('home'));
        }, () => {});
  };
};
export const checkCredentials = (username, password) => {
  return (dispatch) => {
    if (!username) {
      dispatch(displayGlobalMessage(t('Please provide an email'), 'error'));
      dispatch({
        type: 'AUTH_ERROR',
        err: t('Please provide an email')
      });
    } else if (!password) {
      dispatch({
        type: 'AUTH_ERROR',
        err: t('Please provide a password')
      });
      dispatch(displayGlobalMessage(t('Please provide a password'), 'error'));
    } else {
      SkylineCloud.login(username, password)
            .then(() => {
              dispatch({
                type: SET_USER,
                user: { info: { projectsOwned: [] } }
              });
              dispatch({
                type: LOG_IN
              });
              dispatch(push('home'));
            }, (err) => {
                // error: UserNotFoundException  User does not exist.
                // Error: can not find current user
              if (err.name === 'UserNotConfirmedException') {
                SkylineCloud.resendUserConfirmation(username);
                dispatch({
                  type: CONFIRM
                });
                dispatch({
                  type: 'AUTH_ERROR',
                  err: t('Your account is not confirmed! We have sent you an email with confirmation code, please confirm your account with the code we sent you.')
                });
                dispatch(displayGlobalMessage(t('Your account is not confirmed! We have sent you an email with confirmation code, please confirm your account with the code we sent you.'), 'error'));
              } else {
                    // BUG  LOG FAILED, SHOULD UPDATE PASSWORD TO EMPTY.
                dispatch({
                  type: PASSWORD_CHANGE,
                  val: ''
                });
                dispatch({
                  type: 'AUTH_ERROR',
                  err: t('Login failed. Please check your account and password.')
                });
                dispatch(displayGlobalMessage(t('Login failed. Please check your account and password.'), 'error'));
              }
            });
    }
  };
};

export const getPassword = (email) => {
  return (dispatch) => {
    SkylineCloud.requestPassword(email).then(
            (response) => {
              dispatch({
                type: LOGIN
              });
              dispatch({
                type: 'AUTH_ERROR',
                err: 'Please check for email to reset password'
              });
              dispatch(displayGlobalMessage(t('Please check for email to reset password'), 'error'));
            },
            (err) => {
              dispatch({
                type: 'AUTH_ERROR',
                err
              });
              dispatch(displayGlobalMessage(t(format(err.message)), 'error'));
            }
        );
  };
};

export const resetError = () => {
  return {
    type: 'RESET_ERROR'
  };
};
function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export const registerUser = () => {
  return (dispatch, getState) => {
    const state = getState().auth;
    const newAccount = state.registerAccount;
    if (!newAccount.username) {
      dispatch({
        type: 'AUTH_ERROR',
        err: t('Please provide an email')
      });
      dispatch(displayGlobalMessage(t('Please provide an email'), 'error'));
    } else if (!validateEmail(newAccount.email)) {
      dispatch({
        type: 'AUTH_ERROR',
        err: t('Please provide a valid email')
      });
      dispatch(displayGlobalMessage(t('Please provide a valid email'), 'error'));
    } else if (!newAccount.password || newAccount.password.length < 8) {
      dispatch({
        type: 'AUTH_ERROR',
        err: t('Password needs to be longer than 8 characters')
      });
      dispatch(displayGlobalMessage(t('Password needs to be longer than 8 characters'), 'error'));
    } else {
      SkylineCloud.createAccount(newAccount)
            .then(() => {
                // console.log(newUser);
              dispatch({
                type: PASSWORD_CHANGE,
                val: ''
              })
              dispatch(setAuthLocation(LOGIN));
            }, (registerError) => {
              // console.log(registerError);
              dispatch({
                type: 'AUTH_ERROR',
                err: registerError.message || t('An unexpected error happened, please try again later.')
              });
              dispatch(displayGlobalMessage(t('An unexpected error happened, please try again later.'), 'error'));
            });
    }
  };
};
export const confirmUser = () => {
  return (dispatch, getState) => {
    const state = getState().auth;
    const username = state.registerAccount.username || state.username;

    if (!state.registerAccount.confirmCode) {
      dispatch({
        type: 'AUTH_ERROR',
        err: t('Please provide confirmation code!')
      });
      dispatch(displayGlobalMessage(t('Please provide confirmation code!'), 'error'));
    } else {
      SkylineCloud.confirmNewUser(username, state.registerAccount.confirmCode)
            .then(() => {
              dispatch({
                type: 'AUTH_ERROR',
                err: t('Account confirmed! Please login now!')
              });
              dispatch({
                type: LOGIN
              });
            }, (err) => {
              dispatch({
                type: 'AUTH_ERROR',
                err: err.message
              });
              dispatch(displayGlobalMessage(t(format(err.message)), 'error'));
            });
    }
  };
};
export const resendCode = () => {
  return (dispatch, getState) => {
    SkylineCloud.resendUserConfirmation(getState().auth.registerAccount.username)
        .then(() => {
          dispatch({
            type: 'AUTH_ERROR',
            err: t('Code Sent!')
          });
        }, (err) => {
          dispatch({
            type: 'AUTH_ERROR',
            err: err.message
          });
          dispatch(displayGlobalMessage(t(format(err.message)), 'error'));
        });
  };
};


export const confirmChange = (value) => {
  return {
    type: 'UPDATE_CONFIRM',
    value
  };
};

export const updateUsername = (value) => {
  return {
    type: 'UPDATE_USERNAME',
    value
  };
};

export const updateEmail = (value) => {
  return {
    type: 'UPDATE_EMAIL',
    value
  };
};

export const updatePass = (value) => {
  return {
    type: 'UPDATE_PASS',
    value
  };
};

export const updateFirstname = (value) => {
  return {
    type: 'UPDATE_FIRSTNAME',
    value
  };
};

export const updateLastname = (value) => {
  return {
    type: 'UPDATE_LASTNAME',
    value
  };
};

export const updateLang = (value) => {
  return {
    type: 'UPDATE_LANG',
    value
  };
};

export const updateBday = (value) => {
  return {
    type: 'UPDATE_BDAY',
    value
  };
};

export const clearInfo = () => {
  return {
    type: 'CLEAR_INFO'
  };
};

export const setCheck = (bool) => {
  return {
    type: 'SET_CHECK',
    bool
  };
};
