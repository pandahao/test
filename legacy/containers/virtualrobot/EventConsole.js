import { connect } from 'react-redux';

import Console from 'legacy_components/virtualrobot/Console';
import { toggleConsole } from 'legacy_actions/RobotActions';

const mapStateToProps = (state) => {
  return {
    history: state.robot.eventHistory,
    show: state.robot.showConsole,
    freemode: state.shared.app.rootLocation === 2,
    variableName: state.shared.codepad.variableNames,
    docView: state.shared.app.docView,
    virtualrobotblur: state.shared.codepad.roboCoreStatus.virtualrobotblur
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleConsole: () => {
      dispatch(toggleConsole());
    }
  };
};

const EventConsole = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Console);

export default EventConsole;
