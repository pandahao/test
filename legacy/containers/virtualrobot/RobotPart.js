import { connect } from 'react-redux';

import Part from 'legacy_components/virtualrobot/Part';


const mapStateToProps = (state, ownProps) => {
  return {
    hasDevice: state.robot.virtualRobot[ownProps.port].hasDevice,
    device: state.robot.virtualRobot[ownProps.port].device
  };
};

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


const RobotPart = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Part);

export default RobotPart;
