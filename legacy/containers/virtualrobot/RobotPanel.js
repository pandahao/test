import { connect } from 'react-redux';
import { setupRoboCore, unmountRoboCore } from 'legacy_actions/CodepadActions';
import Robot from 'legacy_components/virtualrobot/Robot';


const mapStateToProps = (state) => {
  return {
    layout: state.robot.robotLayout.layout,
    freemode: state.shared.app.rootLocation === 2,
    docView: state.shared.app.docView
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    connect: () => {
      dispatch(setupRoboCore());
    },
    disconnect: () => {
      dispatch(unmountRoboCore());
    }
  };
};

const RobotPanel = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Robot);

export default RobotPanel;
