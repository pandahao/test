import { connect } from 'react-redux';

import VirtualRobot from 'legacy_components/virtualrobot/VirtualRobot';
import { monitor } from 'legacy_actions/CodepadActions';
import { takeSnapShot } from 'legacy_actions/RobotActions';
const mapStateToProps = (state, ownProps) => {
  return {
    freemode: state.shared.app.rootLocation === 2,
    virtualrobotblur: state.shared.codepad.roboCoreStatus.virtualrobotblur,
    blurFromListDisplay: state.projectMode.project.challengelistBlur,
    uploading: state.shared.codepad.roboCoreStatus.uploading,
    codeCompiled: state.shared.codepad.roboCoreStatus.codeCompiled,
    docView: state.shared.app.docView,
    // Snapshot related
    snapshotVariableName: state.shared.codepad.variableNames,
    snapshotRobot : state.robot.snapshot.virtualRobot,
    snapshotEvent : state.robot.snapshot.eventHistory

  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    monitor : ()=>{
      dispatch(monitor());
    },
    takeSnapShot : ()=>{
      dispatch(takeSnapShot());
    }
  };
};


const VirtualRobotContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(VirtualRobot);

export default VirtualRobotContainer;
