import { connect } from 'react-redux';
import BuildView from 'legacy_components/dashboard/buildview/BuildView';

const mapStateToProps = (state) => {
  return {
    blurFromListDisplay: state.projectMode.challenge.challengelistBlur,
    projectId: state.projectMode.project.currentProjectID,
    level: state.projectMode.challenge.currentChallengeSelected + 1,
    tutorialMode : state.shared.app.tutorialMode,
    language : state.shared.app.language
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  };
};

const BuildViewContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(BuildView);

export default BuildViewContainer;
