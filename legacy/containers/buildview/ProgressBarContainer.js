import { connect } from 'react-redux';

import ProgressBar from 'legacy_components/dashboard/buildview/ProgressBar';

const mapStateToProps = (state, ownProps) => {
  const finalState = { ...state.projectMode.challenge, ...state.projectMode.project };
  const index = finalState.currentProjectIndex;
  const projectsData = finalState.projects.projectsData;

  const current_challenge = projectsData[index].challengeData[finalState.currentChallengeSelected];
  return {
    progress: current_challenge.content.text.length,
    width: finalState.width,
    currentProgress: finalState.currentProgress
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  };
};

const ProgressBarContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ProgressBar);

export default ProgressBarContainer;
