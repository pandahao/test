import { connect } from 'react-redux';

import SquareButton from 'legacy_components/dashboard/buildview/SquareButton';
import { backProgress, nextProgress } from 'legacy_actions/ChallengeActions';


const mapStateToProps = (state, ownProps) => {
  // const finalState = { ...state.projectMode.challenge, ...state.projectMode.project };
  // console.log(finalState);
  return {
    // currentProgress: finalState.currentProgress,
    // buildLength: finalState.currentChallengeSet[finalState.currentChallengeSelected].content.blocks.length
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    back: () => {
      dispatch(backProgress());
    },
    next: () => {
      dispatch(nextProgress());
    }
  };
};

const SquareButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(SquareButton);

export default SquareButtonContainer;
