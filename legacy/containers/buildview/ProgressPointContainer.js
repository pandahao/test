import { connect } from 'react-redux';

import ProgressPoint from 'legacy_components/dashboard/buildview/ProgressPoint';
import { goToPoint } from 'legacy_actions/ChallengeActions';

const mapStateToProps = (state, ownProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    goToPoint: (point) => {
      dispatch(goToPoint(point));
    }
  };
};

const ProgressPointContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ProgressPoint);

export default ProgressPointContainer;
