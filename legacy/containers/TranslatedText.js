import { connect } from 'react-redux';

import TranslatedTextComponent from 'legacy_components/TranslatedTextComponent';

const mapStateToProps = (state) => {
  return {
    language: state.shared.app.language,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
  };
};

const TranslatedText = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TranslatedTextComponent);

export default TranslatedText;
