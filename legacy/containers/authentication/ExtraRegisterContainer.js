import { connect } from 'react-redux';

import ExtraRegister from 'legacy_components/authentication/ExtraRegister';
import { setCheck, updateFirstname, updateLastname, updateBday, updateLang, setAuthLocation, LICENSE, resetError, REGISTER_ONE } from 'legacy_actions/AuthActions';

const mapStateToProps = (state, ownProps) => {
    // console.log(state.registerAccount);
  return {
    authLocation: state.auth.authLocation,
    errorMessage: state.auth.loggedIn.error,
    isChecked: state.auth.isChecked,
    lan: state.shared.app.language
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    nextStep: () => {
      dispatch(setAuthLocation(LICENSE));
      dispatch(resetError());
    },
    proceed: () => {
      dispatch(setAuthLocation(REGISTER_ONE));
    },
    firstChange: (event) => {
      dispatch(updateFirstname(event.target.value));
    },
    lastChange: (event) => {
      dispatch(updateLastname(event.target.value));
    },
    birthChange: (event) => {
      dispatch(updateBday(event.target.value));
    },
    langChange: (event) => {
      dispatch(updateLang(event.target.value));
    },
    setCheck: (event, isInputChecked) => {
      dispatch(setCheck(isInputChecked));
    }
  };
};

const ExtraRegisterContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ExtraRegister);

export default ExtraRegisterContainer;
