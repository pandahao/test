import { connect } from 'react-redux';
import ResetPassword from 'legacy_components/authentication/ResetPassword';

import { resetPassword, newPassChange, confirmChange, resendResetRequest, setAuthLocation, resetError, clearInfo, LOGIN } from 'legacy_actions/AuthActions';

const mapStateToProps = (state, ownProps) => {
  return {
    errorMessage: state.auth.loggedIn.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    cancel: () => {
      dispatch(setAuthLocation(LOGIN));
      dispatch(resetError());
      dispatch(clearInfo());
    },
    confirmChange: (ev) => {
      dispatch(confirmChange(ev.target.value));
    },
    newPassChange: (ev) => {
      dispatch(newPassChange(ev.target.value));
    },
    resetPassword: () => {
      dispatch(resetPassword());
    },
    resendCode: () => {
      dispatch(resendResetRequest());
    }
  };
};


const ResetPasswordContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ResetPassword);

export default ResetPasswordContainer;
