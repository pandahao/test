import { connect } from 'react-redux';

import Register from 'legacy_components/authentication/Register';
import { updateUsername, updateFirstname, updateLastname, registerUser, setAuthLocation, updateEmail, updatePass, passChange, resetError, clearInfo, checkProceed, LOGIN } from 'legacy_actions/AuthActions';

const mapStateToProps = (state) => {
  return {
    authLocation: state.auth.authLocation,
    errorMessage: state.auth.loggedIn.error,
    lastName: state.auth.registerAccount.lastName,
    firstName: state.auth.registerAccount.firstName,
    email: state.auth.registerAccount.email,
    password: state.auth.registerAccount.password
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    cancel: () => {
      dispatch(setAuthLocation(LOGIN));
      dispatch(resetError());
      dispatch(clearInfo());
    },
    login: () => {
      dispatch(setAuthLocation(LOGIN));
    },
    register: () => {
      dispatch(resetError());
      dispatch(registerUser());
    },
    emailChange: (ev) => {
      dispatch(updateEmail(ev.target.value));
      dispatch(updateUsername(ev.target.value));
    },
    updatePass: (ev) => {
      dispatch(updatePass(ev.target.value));
    },
    updateFirstname: (ev) => {
      dispatch(updateFirstname(ev.target.value));
    },
    updateLastname: (ev) => {
      dispatch(updateLastname(ev.target.value));
    },
    pass1Change: (ev) => {
      dispatch(updatePass(ev.target.value));
    },
    pass2Change: (ev) => {
      dispatch(passChange(ev.target.value));
    },
    // updateAction: (e, actionType) => {
    //   return (e) => {
    //     dispatch({
    //       type: actionType,
    //       value: e.target.value
    //     })
    //   }
    // }
  };
};

const RegisterContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Register);

export default RegisterContainer;
