import { connect } from 'react-redux';

import LoginButton from 'legacy_components/authentication/LoginButton';

import { checkCredentials, beginLogin } from 'legacy_actions/AuthActions';

const mapStateToProps = (state, ownProps) => {
  return {
    username: state.auth.username,
    password: state.auth.password,
    fetchStatus: state.auth.loggedIn.status,
    loggedIn: state.auth.loggedIn.signedIn
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (user, pass) => {
      dispatch(beginLogin());
      dispatch(checkCredentials(user, pass));
    }
  };
};

const LoginButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(LoginButton);

export default LoginButtonContainer;
