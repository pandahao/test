import { connect } from 'react-redux';
import Confirm from 'legacy_components/authentication/Confirm';

import { confirmChange, confirmUser, resendCode } from 'legacy_actions/AuthActions';

const mapStateToProps = (state, ownProps) => {
  return {
    errorMessage: state.auth.loggedIn.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    confirmChange: (ev) => {
      dispatch(confirmChange(ev.target.value));
    },
    confirmUser: () => {
      dispatch(confirmUser());
    },
    resendCode: () => {
      dispatch(resendCode());
    }
  };
};


const ConfirmContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Confirm);

export default ConfirmContainer;
