import { connect } from 'react-redux';

import OnBoarding from 'legacy_components/onBoarding/initialOnBoard/OnBoarding';
import { setOnboarding } from 'legacy_actions/AppActions';

const mapStateToProps = (state, ownProps) => {
  return {
    onBoarding: state.projectMode.project.userProgress.info.onBoardProgress,
    language: state.shared.app.language
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    nextOnBoarding: (nextOnBoardingVal) => {
      dispatch(setOnboarding(nextOnBoardingVal));
    },
    previousOnBoarding: (value) => {
      dispatch(setOnboarding(value));
    },
    exitOnBoarding: (value) => {
      dispatch(setOnboarding(value));
    }
  };
};

const OnBoardingContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(OnBoarding);

export default OnBoardingContainer;
