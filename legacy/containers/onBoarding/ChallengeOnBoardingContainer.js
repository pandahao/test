import { connect } from 'react-redux';

import ChallengeOnBoarding from 'legacy_components/onBoarding/tutorialOnBoarding/ChallengeOnBoarding';
import { setTutorialOnboard } from 'legacy_actions/AppActions';
import { toggleChallengeView, closeChallengeView } from 'legacy_actions/ChallengeActions';
import { toggleToolbox, hideToolbox } from 'legacy_actions/CodepadActions';

const mapStateToProps = (state, ownProps) => {
  return {
    currentStep: state.shared.app.tutorialOnBoard,
    currentChallenge: state.projectMode.challenge.currentChallengeSelected,
    language: state.shared.app.language
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    nextOnBoarding: (nextOnBoardingVal) => {
      if (nextOnBoardingVal === 7 || nextOnBoardingVal === 8) {
        dispatch(toggleChallengeView());
      }
      else if (nextOnBoardingVal === 4 || nextOnBoardingVal === 5) {
        dispatch(toggleToolbox());
      }

      dispatch(setTutorialOnboard(nextOnBoardingVal));
    },
    previousOnBoarding: (value) => {
      if (value === 6 || value === 7) {
        dispatch(toggleChallengeView());
      }
      else if (value === 3 || value === 4) {
        dispatch(toggleToolbox());
      }
      dispatch(setTutorialOnboard(value));
    },
    exitOnBoarding: (value) => {
      if (value === 8) {
        dispatch(closeChallengeView());
      }
      else if (value === 6) {
        dispatch(hideToolbox());
      }
      dispatch(setTutorialOnboard(value));
    }
  };
};

const ChallengeOnBoardingContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ChallengeOnBoarding);

export default ChallengeOnBoardingContainer;
