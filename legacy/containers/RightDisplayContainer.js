import { connect } from 'react-redux';
import RightDisplay from 'legacy_components/RightDisplay';

const mapStateToProps = (state, ownProps) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  return {
    level: proState.currentChallengeSelected + 1,
    challengeType : proState.currentChallengeSet[proState.currentChallengeSelected].type,
    docView: state.shared.app.docView,
    tutorialMode : state.shared.app.tutorialMode
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
  };
};

const RightDisplayContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(RightDisplay);

export default RightDisplayContainer;
