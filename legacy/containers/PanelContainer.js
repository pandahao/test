import { connect } from 'react-redux';
import React from 'react';
import ChallengeModeContainer from 'legacy_containers/ChallengeModeContainer';
import FreemodeContainer from 'containers/freemode/FreemodeContainer';
import SettingsView from 'components/Settings/SettingsView';
import Home from 'containers/Home';


const Panel = ({view}) => {

  switch (view) {
    case 1:
      return (
        <SettingsView />
      )
    case 2:
      return (<FreemodeContainer />)
    case 0:
      return (  <ChallengeModeContainer />)
    case 'HOME_VIEW':
      return (<Home />)
    default:
      return (<Home />)

  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    view: state.shared.app.rootLocation
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
  };
};

const PanelContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Panel);

export default PanelContainer;
