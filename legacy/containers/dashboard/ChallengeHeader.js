import { connect } from 'react-redux';
import Header from 'legacy_components/dashboard/challenge/Header';
import { toggleChallengeView, setBlur } from 'legacy_actions/ChallengeActions';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  const projectsData = proState.projects.projectsData;
  return {
    title: projectsData.find((p) => { return p.projectID === proState.currentProjectID; }).projectName,
    showContent: proState.showChallengeContent,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    toggleContent: (blur) => {
      dispatch(toggleChallengeView());
            // dispatch(setBlur(blur));
    }
  };
};

const ChallengeHeader = connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);

export default ChallengeHeader;
