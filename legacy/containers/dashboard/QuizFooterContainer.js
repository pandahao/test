import { connect } from 'react-redux';
import QuizFooter from 'legacy_components/dashboard/challenge/QuizFooter.js';
import { checkAnswer, nextChallenge, updateProjects } from 'legacy_actions/ChallengeActions';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  const currentProject = proState.projects.projectsData.find((p) => { return p.projectID === proState.currentProjectID; });

  return {
    challengeCompleted: proState.userProgress.info.projectsOwned.find((p) => { return p.projectId === proState.currentProjectID; }).progress > currentProject.challengeData[proState.currentChallengeSelected].level,
    lastChallenge: proState.currentChallengeSelected === proState.currentChallengeSet.length - 1,
    // currentProjectIndex: proState.currentProjectIndex,
    currentProjectID: state.projectMode.project.currentProjectID
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    checkAnswer: () => {
      dispatch(checkAnswer());
    },
    nextChallenge: () => {
      dispatch(nextChallenge());
    },
    completeProject: (currentProjectID) => {
      dispatch(updateProjects(currentProjectID));
    }
  };
};
const QuizFooterContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(QuizFooter);

export default QuizFooterContainer;
