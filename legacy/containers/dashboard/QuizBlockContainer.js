import { connect } from 'react-redux';
import QuizBlock from 'legacy_components/dashboard/challenge/QuizBlock';
import { setAnswerSelected } from 'legacy_actions/ChallengeActions';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };


  const index = proState.currentProjectIndex;
  const currentProject = proState.projects.projectsData[index];

  return {
    quizResult: proState.quizResult,
    quizAnswerSelected: proState.quizAnswerSelected,
    challengeCompleted: proState.userProgress.info.projectsOwned.find((p) => { return p.projectId === proState.currentProjectID; }).progress > currentProject.challengeData[proState.currentChallengeSelected].level,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setAnswerSelected: (event, value) => {
            // format is "index:question"
            // look for first ":" and substr there to get value and question
      const question = value.substring(value.indexOf(':') + 1);
      const index = value.substring(0, value.indexOf(':'));
      dispatch(setAnswerSelected(index, question));
    },
  };
};
const QuizBlockContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(QuizBlock);

export default QuizBlockContainer;
