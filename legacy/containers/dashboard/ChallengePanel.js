import { connect } from 'react-redux';
import Panel from '../../components/dashboard/challenge/Panel';
import { setBlur, setPageDelay } from '../../actions/ChallengeActions';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  return {
    showChallengeContent: proState.showChallengeContent,
    openDocs: state.shared.app.docView,
    pageDelay: proState.delayedPageChange
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    delayedPageChange: () => {
      setTimeout(() => {
        dispatch(setPageDelay(false));
      }, 1000);
    },
    immediatePageChange: () => {
      dispatch(setPageDelay(true));
    },
        // setBlur : (blur)=>{
        //     dispatch(setBlur(blur));
        // }
  };
};

const ChallengePanel = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Panel);

export default ChallengePanel;
