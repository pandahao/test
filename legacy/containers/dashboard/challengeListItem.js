import { connect } from 'react-redux';
import ListItem from 'legacy_components/dashboard/challenge/ListItem.js';
import { initializeQuizResult, initializeAnswersSelected, selectChallenge, toggleChallengeView, setBlur, toggleBuildView, setHint, goToPoint } from 'legacy_actions/ChallengeActions';
import { closeChallengeBanner } from 'legacy_actions/CodepadActions';

const mapStateToProps = (state, ownProps) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  const currentProject = proState.projects.projectsData.find((p) => {
    return p.projectID === proState.currentProjectID;
  });
  const thisProject = proState.userProgress.info.projectsOwned.find((p) => {
    return p.projectId === proState.currentProjectID;
  });
  return {
    currentChallengeSelected: proState.currentChallengeSelected,
    selected: proState.currentChallengeSelected === ownProps.level - 1,
    challengeCompleted: thisProject.progress > currentProject.challengeData[proState.currentChallengeSelected].level,
    challengeProgress: thisProject.progress
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: (type, challengeCompleted, currentChallengeSelected) => {
      if (type === 'quiz') {
        if (!challengeCompleted) {
          dispatch(initializeQuizResult());
          dispatch(initializeAnswersSelected());
        }
      }
      if (ownProps.level - 1 === currentChallengeSelected) {
        dispatch(toggleChallengeView());
      }
      else {
        if (type === 'build') {
          dispatch(goToPoint(1));
        }
        dispatch(selectChallenge(ownProps.level));
        dispatch(toggleChallengeView());
        dispatch(setHint(false));
        dispatch(closeChallengeBanner());
      }
    }
  };
};

const ChallengeList = connect(
    mapStateToProps,
    mapDispatchToProps
)(ListItem);

export default ChallengeList;
