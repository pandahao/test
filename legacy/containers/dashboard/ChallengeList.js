import { connect } from 'react-redux';
import List from 'legacy_components/dashboard/challenge/List.js';
import { setBlur } from 'legacy_actions/ChallengeActions';
import { showReset } from 'legacy_actions/ListViewActions';
import { gridView } from 'legacy_actions/AppActions';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  return {
    challenges: proState.currentChallengeSet,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    openProjectListView: () => {
      dispatch(gridView());
      dispatch(setBlur(false));
    },
    restartProject: () => {
      dispatch(showReset());
    }
  };
};

const ChallengeList = connect(
    mapStateToProps,
    mapDispatchToProps
)(List);

export default ChallengeList;
