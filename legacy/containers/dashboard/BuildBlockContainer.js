import { connect } from 'react-redux';
import BuildBlock from 'legacy_components/dashboard/challenge/BuildBlock.js';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  return {
    currentProgress: proState.currentProgress,
    progress: proState.userProgress.info.projectsOwned.find((p) => { return p.projectId === proState.currentProjectID; }).progress
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  };
};
const BuildBlockContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(BuildBlock);

export default BuildBlockContainer;
