import { connect } from 'react-redux';
import HintBlock from 'legacy_components/dashboard/challenge/HintBlock';
import { setHint } from 'legacy_actions/ChallengeActions';

const mapStateToProps = (state) => {
  return {
    hint : state.projectMode.challenge.currentChallengeSet[state.projectMode.challenge.currentChallengeSelected].content.hint,
    hintStatus : state.projectMode.challenge.hintStatus
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    toggleHint : (status) => {
      dispatch(setHint(!status));
    }
  }
}

const HintBlockContainer = connect(mapStateToProps, mapDispatchToProps)(HintBlock);

export default HintBlockContainer;
