import { connect } from 'react-redux';
import ContentHeader from 'legacy_components/dashboard/challenge/ContentHeader';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };


  const currentChallenge = proState.currentChallengeSelected;
  const index = proState.currentProjectIndex;
  const projectsData = proState.projects.projectsData;
  const challengeContent = projectsData[index].challengeData[currentChallenge];
  return {
    type: challengeContent.type,
    level: challengeContent.level,
    title: challengeContent.title,
    progress: proState.userProgress.info.projectsOwned.find((p) => { return p.projectId === proState.currentProjectID; }).progress
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
  };
};

const ContentHeaderContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ContentHeader);

export default ContentHeaderContainer;
