import { connect } from 'react-redux';
import CodeFooter from 'legacy_components/dashboard/challenge/CodeFooter.js';
import { setHint } from 'legacy_actions/ChallengeActions';

const mapStateToProps = (state, ownProps) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  const currentChallengeSelected = proState.currentChallengeSelected;
  return {
    challenge: proState.currentChallengeSet[currentChallengeSelected],
    completed: currentChallengeSelected < proState.userProgress.info.projectsOwned.find((p) => { return p.projectId === proState.currentProjectID; }).progress - 1,
    currentEventOrder: proState.currentEventOrder,
    checklist: proState.currentChallengeSet[currentChallengeSelected].content.checklist,
    hint: proState.currentChallengeSet[currentChallengeSelected].content.hint,
    hintStatus: proState.hintStatus
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    toggleHint: (hintStatus) => {
      dispatch(setHint(!hintStatus));
    }
  };
};

const CodeFooterContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CodeFooter);

export default CodeFooterContainer;
