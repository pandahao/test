import { connect } from 'react-redux';
import Content from 'legacy_components/dashboard/challenge/Content';
import { nextChallenge, updateProjects, resetBuildProgress } from 'legacy_actions/ChallengeActions';


const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  const currentChallenge = proState.currentChallengeSelected;
  const challengeSet = proState.currentChallengeSet;
  return {
    challenge: challengeSet[currentChallenge],
    currentChallenge,
    currentChallengeSet: challengeSet,
    currentProjectID: state.projectMode.project.currentProjectID,
    currentProgress: proState.currentProgress, // build progress
    showChallengeContent: proState.showChallengeContent,
    show: proState.delayedPageChange,
    quizResult: proState.quizResult,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    nextChallenge: (currentChallenge, currentChallengeSet, currentProjectID) => {
      if (currentChallenge === currentChallengeSet.length - 1) {
        dispatch(updateProjects(currentProjectID));
        dispatch(resetBuildProgress());
      }
      else {
        dispatch(nextChallenge());
        dispatch(resetBuildProgress());
      }
    }
  };
};

const ChallengeContent = connect(
    mapStateToProps,
    mapDispatchToProps
)(Content);

export default ChallengeContent;
