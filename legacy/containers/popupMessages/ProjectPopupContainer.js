import { connect } from 'react-redux';

import ProjectPopup from 'legacy_components/popupMessages/ProjectPopup';
import { freemode, closeDoc, gridView } from 'legacy_actions/AppActions';
import { layoutResize } from 'legacy_actions/RobotActions';
import { initializeChallenges, setBlur, initialize_challenge_set } from 'legacy_actions/ChallengeActions';
import { closeCompletion } from 'legacy_actions/CodepadActions';
import { selectProject, beginProject } from 'legacy_actions/ListViewActions';

const mapStateToProps = (state, ownProps) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  const projectsData = proState.projects.projectsData;
  const currentProject = proState.projects.projectsData.find((p) => { return p.projectID === proState.currentProjectID; });
  if (ownProps.moreUnlock) {
    const nextProject = proState.userProgress.info.projectsOwned[proState.currentProjectIndex + 1];
    return {
      next: nextProject.projectId,
      nextSet: projectsData[proState.currentProjectIndex + 1].challengeData,
      nextStatus: nextProject.status,
      nextProgress: nextProject.progress,
      projectName: currentProject.projectName
    };
  }
  else {
    return {
      projectName: currentProject.projectName
    };
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
      goToFreemode: () => {
      dispatch(freemode());
      dispatch(layoutResize());
      dispatch(setBlur(false));
      dispatch(closeDoc());
      dispatch(closeCompletion());
    },
    nextProject: (projectID, challengeData, status, challengeProgress) => {
      dispatch(closeCompletion());
      dispatch(selectProject(projectID));
      dispatch(initialize_challenge_set(challengeData));
      dispatch(initializeChallenges(status, challengeProgress));
      if (status === 'UNLOCKED') {
        dispatch(beginProject(projectID));
      }
    },
    closePopup: () => {
      dispatch(closeCompletion());
    },
    viewProjects: () => {
      dispatch(gridView());
      dispatch(closeDoc());
      dispatch(setBlur(false));
      dispatch(closeCompletion());
    }
  };
};

const ProjectPopupContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ProjectPopup);

export default ProjectPopupContainer;
