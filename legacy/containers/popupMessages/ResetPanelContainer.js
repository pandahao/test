import { connect } from 'react-redux';

import ResetPanel from 'legacy_components/popupMessages/ResetPanel';
import { closeReset, restartProject } from 'legacy_actions/ListViewActions';
import { closeWarning, resetCode, cancelCheck } from 'legacy_actions/CodepadActions';
import { selectChallenge, setBlur, toggleChallengeView } from 'legacy_actions/ChallengeActions';

const mapStateToProps = (state, ownProps) => {
  return {
    resetWarning: state.shared.codepad.resetWarning,
    currentProjectIndex: state.projectMode.project.currentProjectIndex
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    closeWarning: () => {
      dispatch(closeWarning());
    },
    resetCode: () => {
      dispatch(resetCode());
      dispatch(cancelCheck());
      dispatch(closeWarning());
    },
    closeReset: () => {
      dispatch(closeReset());
    },
    resetProject: (currentProjectIndex) => {
      dispatch(restartProject(currentProjectIndex));
      dispatch(selectChallenge(1));
      dispatch(toggleChallengeView());
            // dispatch(setBlur(false));
      dispatch(closeReset());
    }
  };
};

const ResetPanelContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ResetPanel);

export default ResetPanelContainer;
