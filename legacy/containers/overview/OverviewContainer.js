import { connect } from 'react-redux';
import Overview from 'legacy_components/overview/Overview';
import { gridView } from 'legacy_actions/AppActions';

const mapStateToProps = (state, ownProps) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  const currentProject = proState.projects.projectsData.find((p) => {
    return p.projectID === proState.currentProjectID;
  });
  return {
    overview: currentProject.overview,
    userStatus: proState.userProgress.info.projectsOwned,
    projectID: proState.currentProjectID,
    projectName: currentProject.projectName,
    currentChallenge: currentProject.currentChallenge,
    challengeData: currentProject.challengeData
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    openProjectListView: () => {
      dispatch(gridView());
    }
  };
};

const OverviewContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Overview); // connect this component to the OverviewContainer

export default OverviewContainer;
