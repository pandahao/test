import { connect } from 'react-redux';
import OverviewColumn from 'legacy_components/overview/OverviewColumn';

const mapStateToProps = (state, ownProps) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  const currentProject = proState.projects.projectsData.find((p) => { return p.projectID === proState.currentProjectID; });

  return {
    overview: currentProject.overview,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  };
};

const OverviewColumnContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OverviewColumn); // connect this component to the OverviewContainer

export default OverviewColumnContainer;
