import { connect } from 'react-redux';
import OverviewVideo from 'legacy_components/overview/OverviewVideo';
import { selectProject, beginProject, setLoading } from 'legacy_actions/ListViewActions';
import { loadSingleProject } from 'legacy_actions/ListViewActions';
import { layoutResize } from 'legacy_actions/RobotActions';
import { showChallengeView, initializeChallenges, initialize_challenge_set, setBlur, goToPoint } from 'legacy_actions/ChallengeActions';
import { loadModel } from 'legacy_lib/build/ModelManager';

const mapStateToProps = (state, ownProps) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  const currentProject = proState.projects.projectsData.find((p) => { return p.projectID === proState.currentProjectID; });

  return {
    gridview: proState.gridView,
    loading: proState.loading,
    overview: currentProject.overview,
    currentProject: proState.userProgress.info.projectsOwned.find((p) => { return p.projectId === proState.currentProjectID; }),
    projectID: proState.currentProjectID,
    projectName: currentProject.projectName,
    currentChallenge: currentProject.currentChallenge,
    challengeData: currentProject.challengeData
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    goToProject: (projectID, status, challengeData, challengeProgress) => {
      dispatch(setLoading(projectID));
      dispatch(loadSingleProject(projectID));
      loadModel(projectID).then(() => {
        dispatch(setLoading(-1));
        dispatch(selectProject(projectID));
        dispatch(initialize_challenge_set(challengeData));
        dispatch(initializeChallenges(status, challengeProgress));
        dispatch(goToPoint(1));
        dispatch(showChallengeView(status));
        dispatch(layoutResize());
        if (status === 'UNLOCKED') {
          dispatch(beginProject(projectID));
        }
      });
    }
  };
};

const OverviewVideoContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OverviewVideo); // connect this component to the OverviewContainer

export default OverviewVideoContainer;
