import { connect } from 'react-redux';

import GlobalMessageComponent from 'legacy_components/common/GlobalMessage';
import { hideGlobalMessage } from 'legacy_actions/shared/RuntimeActions';

const mapStateToProps = (state, ownProps) => {
  return {
    message: state.shared.runtime.message.message,
    level: state.shared.runtime.message.level,
    open: state.shared.runtime.message.open,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onClose: () => {
      dispatch(hideGlobalMessage());
    }
  };
};

const GlobalMessage = connect(
    mapStateToProps,
    mapDispatchToProps,
)(GlobalMessageComponent);

export default GlobalMessage;
