import { connect } from 'react-redux';
import ChallengeMode from 'legacy_components/ChallengeMode';

const mapStateToProps = (state) => {
  return {
    view: state.shared.app.challengeLocation,
    docView: state.shared.app.docView,
    toolboxStatus: state.shared.codepad.showToolbox
  };
};

const mapDispatchToProps = () => {
  return {
  };
};

const ChallengeModeContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ChallengeMode);

export default ChallengeModeContainer;
