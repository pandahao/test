import { connect } from 'react-redux';

import CastleRock from 'legacy_components/CastleRock';
import { loadProjects } from 'legacy_actions/ListViewActions';


const mapStateToProps = (state, ownProps) => {
  let moreUnlock = false;
  let userOnBoard;
  let index;
  const blur = false;
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };
    // REFACTOR BY MOVING THIS FUNCTION OUT
  if (!(proState.projects.projectsData === undefined || proState.userProgress.info === undefined)) {
    const projectsData = proState.projects.projectsData;
    index = proState.currentProjectIndex;
    if (index !== proState.userProgress.info.projectsOwned.length - 1) {
      if (proState.userProgress.info.projectsOwned[index + 1].status === 'LOCKED') {
        moreUnlock = true;
      }
    }
  }
  return {
    user: proState.userProgress.info,
    resetWarning: state.shared.codepad.resetWarning,
    completionPopup: state.shared.codepad.completionPopup,
    resetProject: proState.resetProject,
    loggedIn: state.auth.loggedIn.signedIn,
    tutorialMode: state.shared.app.tutorialMode,
    moreUnlock,
    index,
    blur
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadProjects: () => {
      dispatch(loadProjects());
    }
  };
};


const CastleRockApp = connect(
    mapStateToProps,
    mapDispatchToProps
)(CastleRock);

export default CastleRockApp;
