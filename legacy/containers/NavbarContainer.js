import { connect } from 'react-redux';
import Navbar from 'legacy_components/Navbar';
import {
  closeDoc,
  projectView,
  settingsView,
  gridView,
  homeView,
  turnOffErraMode,
  turnOnErraMode
} from 'legacy_actions/AppActions';
import { layoutResize } from 'legacy_actions/RobotActions';
import { changeFreeModeView } from 'legacy_actions/FreeModeActions';
// import { setBlur, sketchReset } from 'legacy_actions/ChallengeActions';

const mapStateToProps = state => {
  return {
    view: state.shared.app.rootLocation,
    freemode: state.shared.app.rootLocation === 2,
    robot: state.freeMode.editingRobot,
    erraMode: state.shared.app.erraMode,
    tutorialMode: state.shared.app.tutorialMode
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    goToHome: () => {
      dispatch(projectView());
      dispatch(gridView());
      dispatch(closeDoc());
      dispatch(layoutResize());
      // dispatch(setBlur(false));
    },
    openProjectListView: () => {
      dispatch(projectView());
      dispatch(gridView());
      dispatch(layoutResize());
      // dispatch(setBlur(false));
      dispatch(closeDoc());
      dispatch(turnOffErraMode());
    },

    gotoFreemode: () => {
      dispatch(closeDoc());
      dispatch(changeFreeModeView('list'));
      // Reset Sketch
      // dispatch(sketchReset());
      // dispatch(layoutResize());
      // dispatch(changeFreeModeView('list'));
      // dispatch(setBlur(false));
    },
    gotoErraProjects() {
      dispatch(projectView());
      dispatch(gridView());
      dispatch(layoutResize());
      dispatch(closeDoc());
      dispatch(turnOnErraMode());
    },
    goToSettings: () => {
      dispatch(settingsView());
      // dispatch(setBlur(false));
    },
    goToWelcome: () => {
      dispatch(homeView());
    }
  };
};

const NavbarContainer = connect(mapStateToProps, mapDispatchToProps)(Navbar);

export default NavbarContainer;
