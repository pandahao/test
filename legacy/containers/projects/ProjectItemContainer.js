import { connect } from 'react-redux';
import ProjectItem from 'legacy_components/projects/ProjectItem';
import { closeProjectListView, overview } from 'legacy_actions/AppActions';
import { selectProject, beginProject, setLoading } from 'legacy_actions/ListViewActions';
import { loadSingleProject } from 'legacy_actions/ListViewActions';
import { layoutResize } from '../../actions/RobotActions';
import { showChallengeView, initializeChallenges, setBlur, initialize_challenge_set, goToPoint } from '../../actions/ChallengeActions';
import { loadModel } from 'legacy_lib/build/ModelManager';


const mapStateToProps = (state, ownProps) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  return {
    gridView: state.shared.app.challengeLocation === 0,
    userStatus: proState.userProgress.info.projectsOwned,
    loading: proState.loading,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    projectOverview: (projectID) => {
      dispatch(selectProject(projectID));
      dispatch(overview());
    },
    goToProject: (projectID, status, challengeData, challengeProgress) => {
      dispatch(setLoading(projectID));
      dispatch(loadSingleProject(projectID));
      loadModel(projectID).then(() => {
        dispatch(setLoading(-1));
        dispatch(selectProject(projectID));
        dispatch(initialize_challenge_set(challengeData));
        dispatch(initializeChallenges(status, challengeProgress));
        dispatch(goToPoint(1));
        dispatch(showChallengeView(status));
        dispatch(layoutResize());
        if (status === 'UNLOCKED') {
          dispatch(beginProject(projectID));
        }
      });
    }
  };
};

const ProjectItemContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProjectItem);

export default ProjectItemContainer;
