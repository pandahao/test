import { connect } from 'react-redux';
import ProjectsViewHeader from 'legacy_components/projects/ProjectsViewHeader';
import { gridView, listView } from 'legacy_actions/AppActions';

// Grabs the state to determine which list to show
const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  return {
    projectListView: proState.challengeLocation === 1,
    gridView: state.shared.app.challengeLocation === 0
        // robotView : state.robotView
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    goBack: () => {
            // go back dispatch
    },
    listView: () => {
      dispatch(listView());
    },
    toggleGridView: () => {
      dispatch(gridView());
    },
    blockView: () => {
            // Dispatch action to change view style
    }

  };
};
const ProjectsViewHeaderContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProjectsViewHeader);

export default ProjectsViewHeaderContainer;
