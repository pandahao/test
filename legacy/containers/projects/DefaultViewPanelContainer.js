import { connect } from 'react-redux';
import DefaultViewPanel from 'legacy_components/projects/DefaultViewPanel';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };
  return {
    projects: proState.projects.projectsData,
    gridView: state.shared.app.challengeLocation === 0
        // robotView : state.robotView
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  };
};

const DefaultViewPanelContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DefaultViewPanel);

export default DefaultViewPanelContainer;
