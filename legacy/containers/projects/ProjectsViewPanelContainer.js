import { connect } from 'react-redux';
import ProjectsViewPanel from 'legacy_components/projects/ProjectsViewPanel';
import { loadProjects } from 'legacy_actions/ListViewActions';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  return {
    projectStatus: proState.projects.status,
    userStatus: proState.userProgress.status,
        // userProjects: state.user['info']['projectsOwned'],
    projects: proState.projects.projectsData,
    projectListView: proState.challengeLocation === 1,
    gridView: state.shared.app.challengeLocation === 0
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    loadProjects: () => {
      dispatch(loadProjects());
    }

  };
};
const ProjectsViewPanelContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProjectsViewPanel);

export default ProjectsViewPanelContainer;
