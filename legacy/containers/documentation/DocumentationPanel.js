import { connect } from 'react-redux';
import DocPanel from 'legacy_components/documentation/DocPanel.js';
import { loadDocs } from 'legacy_actions/DocViewActions';

const mapStateToProps = (state) => {
  return {
    status: state.shared.doc.docs.status,
    docs: state.shared.doc.docs.Documentation,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadDocs: () => {
      dispatch(loadDocs());
    }
  };
};

const DocumentationPanel = connect(
    mapStateToProps,
    mapDispatchToProps
)(DocPanel);

export default DocumentationPanel;
