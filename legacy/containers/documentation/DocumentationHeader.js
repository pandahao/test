import { connect } from 'react-redux';
import DocHeader from 'legacy_components/documentation/DocHeader';
import { toggleDocView } from 'legacy_actions/AppActions';
import { layoutResize } from 'legacy_actions/RobotActions';

import { toggleDocumentation } from 'legacy_actions/FreeModeActions';

const mapStateToProps = (state) => {
  return {
    freemode: state.shared.app.rootLocation === 2
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleContent: (freemode) => {
      dispatch(toggleDocView());
      dispatch(toggleDocumentation(false));
      // if (freemode) {
      //   dispatch(layoutResize());
      // }
    }
  };
};

const DocumentationHeader = connect(
    mapStateToProps,
    mapDispatchToProps
)(DocHeader);

export default DocumentationHeader;
