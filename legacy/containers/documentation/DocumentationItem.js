import { connect } from 'react-redux';
import DocItem from 'legacy_components/documentation/DocItem';
import { toggleOpen, toggleInnerOpen } from 'legacy_actions/DocViewActions';

const mapStateToProps = (state) => {
  return {
    opendocs: state.shared.doc.docOpen
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onClick: (id) => {
      dispatch(toggleOpen(id));
    },
    onExtraClick: (id, name) => {
      dispatch(toggleInnerOpen(id, name));
    }
  };
};

const DocumentationItem = connect(
    mapStateToProps,
    mapDispatchToProps
)(DocItem);

export default DocumentationItem;
