import { connect } from 'react-redux';
import ChallengeView from 'legacy_components/ChallengeView';
import { hideToolbox } from 'legacy_actions/CodepadActions';

const mapStateToProps = (state, ownProps) => {
  return {
    docView: state.shared.app.docView,
    toolboxStatus: state.shared.codepad.showToolbox,
    blurContent: false//state.projectMode.challenge.showChallengeContent === false
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    closeToolbox: () => {
      dispatch(hideToolbox());
    }
  };
};

const ChallengeViewContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ChallengeView);

export default ChallengeViewContainer;
