import Cloud from 'legacy_lib/cloud';


/*
Available APIs

login :
    params : username, password
    returns : Promise
updateUser :
    params : updatedPromise
    returns : Promise
isAuthenticated :
    params : null,
    returns : Promise
loadProjects :
    params : projectId,
    returns : Promise
logout :
    params : null,
    returns : Promise
*/


const CastleRock = new Cloud();


// User Login
CastleRock.logIn('username', 'password').then((user) => {
  console.log(user);
}, (err) => {
  console.log(err);
});
