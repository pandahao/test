export const Dictionary = {};
Dictionary.PortsID = {
  0: 'ROBOCORE',
    // 0:'TXRX',
    // 1:'SERIAL_TX',
    // 2:"POWER",
  3: 'IR_TRAN',
    // 4:'USB',
  5: 'MOTOR_A',
  6: 'MOTOR_B',
  10: 'SERVO_A',
  9: 'SERVO_B',
  8: 'SERVO_C',
  7: 'SERVO_D',
  11: 'DIO_1',
  12: 'DIO_2',
  13: 'DIO_3',
  19: 'DIO_4',
  18: 'DIO_5',
  17: 'DIO_6',
  16: 'DIO_7',
  15: 'DIO_8',
  14: 'DIO_9',
  21: 'AI_1',
  20: 'AI_2',
  31: 'PORT_1',
  32: 'PORT_2',
  33: 'PORT_3',
  34: 'PORT_4',
  35: 'PORT_5',
  36: 'PORT_6',
  37: 'PORT_7'
};

Dictionary.DevicesID = {
  1: 'robocore',
  10: 'button',
  11: 'tape',
  12: 'light',
  13: 'magnet',
  14: 'sound',
  30: 'irr',
  100: 'led',
  110: 'irt',
  120: 'servo',
  130: 'motor',
  40: 'joy',
  41: 'accel',
};

Dictionary.EventsID = {
  0: 'NULL',
  1: 'ROBOCORE_LAUNCH',
  2: 'ROBOCORE_TERMINATE',
  3: 'ROBOCORE_TIME_UP',
    // 3  : "VIRTUAL_ROBOT_MISSED",
    // 4  : "EVENT_HANDLE_MISSED",
  10: 'DEACTIVATE',
  11: 'ACTIVATE',
  100: 'BUTTON_PRESS',
  101: 'BUTTON_RELEASE',
  102: 'BLACK_TAPE_ENTER',
  103: 'BLACK_TAPE_LEAVE',
  104: 'DARK_ENTER',
  105: 'DARK_LEAVE',
  106: 'SOUND_BEGIN',
  107: 'SOUND_END',
  108: 'IR_MESSAGE_REPEAT',
  109: 'IR_MESSAGE_RECEIVE',
  110: 'IR_MESSAGE_INTERFERE',
  200: 'LED_TURNON',
  201: 'LED_TURNOFF',
  202: 'SLOWBLINK_BEGIN',
  203: 'FASTBLINK_BEGIN',
  204: 'BLINK_END',
  205: 'SERVO_MOVE_BEGIN',
  206: 'SERVO_INCREASE_END',
  207: 'SERVO_DECREASE_END',
  208: 'MOTOR_SPEED_CHANGE',
  209: 'MOTOR_SPEED_ZERO',
  210: 'MOTOR_REVERSE',
  211: 'IR_MESSAGE_EMIT',
    // 212: "IR_MESSAGE_EMIT"
  111: 'JOYSTICK_X_UPDATE',
  112: 'JOYSTICK_Y_UPDATE',
  113: 'ACCELEROMETER_X_UPDATE',
  114: 'ACCELEROMETER_Y_UPDATE',
};


export const MessageReader = {
  robocore() {
    return ['OMGLOL'];
  },
  button(e) {
        // var re = null;
    switch (e) {
      case 10:
        return ['Button'];
      case 11:
        return ['Button'];
      default:
        return ['Press Count'];
    }
  },
  magnet() {
    return ['Magnet Count'];
  },
  led(e) {
    switch (e) {
      case 10:
        return ['LED'];
      case 11:
        return ['LED'];
      case 204:
        return ['Blink Count'];
      case 200:
        return ['LED'];
      case 201:
        return ['LED'];
      default:
        return ['Blink Target'];
    }
  },
  sound(e) {
    switch (e) {
      case 10:
        return ['Sound'];
      case 11:
        return ['Sound'];
      default:
        return ['Sound Count'];
    }
  },
  tape(e) {
    switch (e) {
      case 10:
        return ['Tape'];
      case 11:
        return ['Tape'];
      default:
        return ['Tape Count'];
    }
  },
  light(e) {
    switch (e) {
      case 10:
        return ['Light'];
      case 11:
        return ['Light'];
      default:
        return ['Dark Count'];
    }
  },
    // Input Event ID
  servo(e) {
    switch (e) {
      case 10:
        return ['Servo', 'Speed'];
      case 11:
        return ['Servo', 'Speed'];
      case 205:
        return ['Target Angle', 'Speed'];
      default:
        return ['Current Angle', 'Speed'];
    }
  },
  motor(e) {
    switch (e) {
      case 10:
        return ['Motor', 'Direction'];
      case 11:
        return ['Motor', 'Direction'];
      default:
        return ['Speed', 'Direction'];
    }
  },
  irr(e) {
    switch (e) {
      case 10:
        return ['Irr', 'Address'];
      case 11:
        return ['Irr', 'Address'];
      default:
        return ['Value', 'Address'];
    }
  },
  irt() {
    return ['Irt', 'Address'];
  },
  joy() {
    return ['Joystick'];
  },
  accel() {
    return ['Value'];
  }
};
