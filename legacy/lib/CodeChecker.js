// responsible for creating a tree-like javascript object representation of if/else/elseif/noprefix blocks of code

/*	remove all comments from code as a preprocessing step before tokenizing		*/
export function removeComments(code) {
  code = code.replace(/[ {5}]/g, '');
  let buffer = '';
  let inlineComment = false;
  let blockComment = 0; // can be nested; int keeps track of the layer of nesting
  for (let i = 0; i < code.length; i++) {
    	// determine if start of comment
    if (code[i] === '/' && i < code.length - 1) {
      if (code[i + 1] === '/') inlineComment = true;
      if (code[i + 1] === '*') blockComment += 1;
    }
        // only add character to buffer and process if not in a comment
    if (!inlineComment && blockComment === 0) {
      buffer += code[i];
    }
        // determine if end of comment
    if (inlineComment && code[i] === '\n') inlineComment = false;
    if (blockComment && code[i] === '*' && i < code.length - 1 && code[i + 1] === '/') {
      blockComment -= 1;
      i += 1; // to skip over the '/' that we peeked forward at
    }
  }
  return buffer;
}

/*	takes a section of code and tokenizes it into its parent if/else/elseif/noprefix blocks of code
		eg. by parent i mean nested if-statements will be tokenized only as a block only in its highest level
		returns an array of all the tokenized blocks	*/
function tokenize(code) {
  code = removeComments(code);
  const tokens = [];
  code = code.replace(/\s/g, ''); // removes all whitespace
  let buffer = '';
  let bracketCounter = 0;
  for (let i = 0; i < code.length; i++) {
    buffer += code[i];
    	// count open and closed brackets to find complete blocks. a complete block will be:
    	// 1) when open brackets are completely closed (if nested)
    	// 2) when we reach the end of a statement (denoted by ;) and it is outside of any brackets
    if (code[i] === '{') bracketCounter += 1;
    if (code[i] === '}') {
      if (bracketCounter === 1) {
        tokens.push({ type: determineBlockType(buffer), content: buffer });
        buffer = '';
      }
      bracketCounter -= 1;
    }
    if (code[i] === ';' && bracketCounter === 0) {
      tokens.push({ type: determineBlockType(buffer), content: buffer });
    }
  }
  return tokens;
}

/*	given a block of code, returns whether it has an if/else/elseif prefix or
		is a base case without a prefix.	*/
function determineBlockType(code) {
  code = code.toLowerCase();
  if (code.indexOf('elseif') === 0) return 'elseif';
  if (code.indexOf('if') === 0) return 'if';
  if (code.indexOf('else') === 0) return 'else';
  else return 'base';
}

/*	type can be if, else, elseif.
		condition is the condition of the if/else/elseif statement. empty string if a base block.
		content is the content of an expression  */
function makeBlock(type, condition, content) {
  const newBlock = { type, condition, content };
  return newBlock;
}

/*	type can be if, else, elseif, and this function will parse out and return the
		condition and content. eg. if(condition){content};	*/
function parse(type, code) {
  const condAndContent = { condition: '', content: '' };
  let re;
  if (type === 'if') re = /if\((.*?)\){(.*)}/;
  if (type === 'else') re = /else{(.*)}/;
  if (type === 'elseif') re = /elseif\((.*?)\){(.*)}/;
  const str = code;
  let m;

  if ((m = re.exec(str)) !== null) {
    if (m.index === re.lastIndex) {
      re.lastIndex++;
    }
        // the different indicies of m represent the different capture groups
        // m[0] would be the entire string captured
    if (type === 'else') {
      condAndContent.condition = '';
      if (m) {
        condAndContent.content = m[1];
      }
    } else { // for "if" and "elseif"
      if (m) {
        condAndContent.condition = m[1];
        condAndContent.content = m[2];
      }
    }
  }
  return condAndContent;
}

/* Given a map with elements of format {PORT : {name, type}},
   give a name and return the port associated with it
*/
function getPort(map, name) {
  for (const port in map) {
    if (map[port].name === name) return port;
  }
  console.log(`name does not exist in map so returning name back: ${name}`);
  return name;
    // shouldn't ever happen if the map was created correctly
}

/* Given a map with elements of format {PORT : {name, type}},
   give a port and return the name associated with it
*/
function getName(map, port) {
  if (port in map) return map[port].name;
  else {
    console.log(`port does not exist in map so returning port back: ${port}`);
    return port;
  }
}

// to detect cases like the following:
// correct:
// servo1.turn(50)
// servo2.turn(100)
// user:
// servo1.turn(100)
// servo2.turn(50)

// eg. maps led.isOn() to DIO_1.isOn();
function toggleNamePort(content, map, toggleFn) {
  const delimiter = content.indexOf('.');
  if (delimiter === -1) return content; // is a straight boolean/expression
  else return `${toggleFn(map, content.substring(0, delimiter))}.${content.substring(delimiter + 1)}`;
}

// for every port in map, get the name at that port, and replace all those names in code with that port
function variablesToPorts(code, map) {
  for (const port in map) {
    const variable = map[port].name;
    if (variable.length > 0) {
      const re = new RegExp(variable, 'g');
      code = code.replace(re, port);
    }
  }
  return code;
}

function portsToVariables(code, map) {
  for (const port in map) {
    const variable = map[port].name;
    const re = new RegExp(port, 'g');
    code = code.replace(re, variable);
  }
  return code;
}

function generateTreeRecursive(code, currContent, map) {
  const tokens = tokenize(code);
  for (let i = 0; i < tokens.length; i++) {
    const token = tokens[i];
    if (token.type === 'base') {
      const baseContent = [];
            // if base, the content will just be the statement string and not an array
      currContent.push(makeBlock(token.type, '', token.content));
    } else {
      const condAndContent = parse(token.type, token.content);
      const newBlock = makeBlock(token.type, condAndContent.condition, []); // fill in with new block info
      currContent.push(newBlock);
            // level[0]++; //can print this out to see the current depth of the nested if statements/recursive calls
      generateTreeRecursive(condAndContent.content, newBlock.content, map);
            // level[0]--; //recursive backtracking
    }
  }
}

/*	main recursive function to generate parse from code. populates content of tokensObject.  */
export function generateTree(code, currContent, map) {
  code = variablesToPorts(code, map);
  generateTreeRecursive(code, currContent, map);
}

/*	this function will compare two trees and attempt to print out helpful error messages of when input
        deviates from the gold standard. note that it cannot capture all errors because when the content array
    is of differing lengths, it has to stop to avoid crashing with out-of-bounds exception.		*/
export function compareTrees(gold, input, error, stackTrace, errorFound) {
    // if we already found an error, just return
  if (errorFound[0]) return true;
    // if type is not equal return false
  if (gold.type.toLowerCase() != input.type.toLowerCase()) return false;
  if (gold.type === 'base') {
    return gold.content.toLowerCase() === input.content.toLowerCase();
  }
  else if (gold.condition.toLowerCase() != input.condition.toLowerCase()) return false;
    // add everything in gold to a set here
  const goldSet = new Set();
  for (let a = 0; a < gold.content.length; a++) {
    goldSet.add(gold.content[a]);
  }
  for (let b = 0; b < gold.content.length; b++) {
      // updates to true if any input content returns true
    let hasMatch = false;
    for (let c = 0; c < input.content.length; c++) {
      if (!errorFound[0]) stackTrace.push(gold.content[b]);
      const result = compareTrees(gold.content[b], input.content[c], error, stackTrace, errorFound);
      if (!errorFound[0]) stackTrace.pop();
            // if compareTrees is true, set hasMatch to true and break
      if (result) {
        hasMatch = true;
        break;
      }
    }
      // if all input does not result in a true, then this gold is false, we add an error, and return false.
    if (!hasMatch && !errorFound[0]) {
      errorFound[0] = true;
      error.push(gold.content[b]);
      error.push(gold.content.length > input.content.length ? true : false);
      return false;
    } else {
      goldSet.delete(gold.content[b]);
    }
  }
    // if we check all gold content without throwing an error
  return true;
}

export function printResults(errorInfo, stackTrace, map) {
  let msg = '';
  if (errorInfo.length === 0) {
    msg = 'No errors could be detected. \nPlease check with your instructor for further help.';
  } else {
    const type = errorInfo[0].type;
        // let condition = toggleNamePort(errorInfo[0].condition, map, getName);
        // let content = toggleNamePort(errorInfo[0].content, map, getName);
    if (type === 'base') {
      const content = portsToVariables(errorInfo[0].content, map);
      msg += `You are missing a block with:\n\n${content}`;
      console.log(`You are missing a block with:\n\n${content}`);
    } else {
      const condition = portsToVariables(errorInfo[0].condition, map);
      msg += `You are missing an ${type} statement \nwith the condition: \n${condition}`;
      console.log(`You are missing an ${type} statement with the condition: ${condition}`);
    }
    if (stackTrace.length === 0) {
      msg += '\n\nIt belongs in the root.';
      console.log('It belongs in the root.');
    } else {
      msg += '\n\nIt belongs in: \n\n';
      for (let i = 0; i < stackTrace.length; i++) {
        let indentation = '';
        for (let j = 0; j < i; j++) indentation += '    ';
        msg += indentation;
        msg += (`${stackTrace[i].type}(${portsToVariables(stackTrace[i].condition, map)}) {` + `\n`);
        if (i === stackTrace.length - 1) {
          msg += (`${indentation}    [missing code here]\n`);
        }
      }
      for (let i = stackTrace.length - 1; i >= 0; i--) {
        let indentation = '';
        for (let j = 0; j < i; j++) indentation += '    ';
        msg += `${indentation}}\n`;
      }
      console.log(msg);
    }
  }
  return msg;
}
