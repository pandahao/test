const THREE = require('./Boots.js');
// Scene box limits
const UPPER_BOUND = 200;
const LOWER_BOUND = -200;

// Obj square starting point
const START_X = 0;
const START_Y = 0;
const START_Z = 50;
const BIG_PART_DIST = 60;
const SMALL_PART_DIST = 20;
const HORIZON = 300;

const genRandomInt = (up = UPPER_BOUND, lo = LOWER_BOUND) => {
  return Math.floor(Math.random() * (up - lo)) + lo;
};

// Interval
const ZONE_UP = 200;
const ZONE_LO = 150;
const genRandomZone = (up = ZONE_UP, lo = ZONE_LO) => {
  const flag = Math.random();
  if (flag > 0.5) {
    return genRandomInt(up, lo);
  } else {
    return genRandomInt(-lo, -up);
  }
};


function* locationMaker(xIncre, zIncre, startX, startZ, yOffset, horizon = HORIZON) {
  let x = startX;
  const y = START_Y + yOffset;
  let z = startZ;
    // let first = true;
  while (true) {
    if (Math.abs(x) < horizon) {
      x += xIncre;
    } else {
      z += zIncre;
      x = startX;
    }
    yield new THREE.Vector3(x, y, z);
  }
}

class LocationGenerator {
  constructor() {
    this.zone1 = locationMaker(-30, -30, -150, 10, 5, 200);
    this.zone2 = locationMaker(60, -35, -150, -300, 0, 300);
    this.zone3 = locationMaker(50, -50, 150, -100, 0, 200);

    this.zone4 = locationMaker(150, -150, 300, 50, 0, 300);

    this.zone2X = locationMaker(70, 15, -100, -250, 10, 30);
    this.zone2XL = locationMaker(70, 0, -150, -250, 0, 300);
  }
  generate(name) {
        // console.log(name);
    const $ = this;
    const nameArr = name.split('_');
    switch (nameArr[0]) {
      case 'Connect':
        return $.zone1.next().value;
      case 'Build':
        switch (name) {
          case 'Build_Bar_I_120.STL':
            return $.zone2X.next().value;
          case 'Build_Bar_S_110.STL':
            return $.zone2X.next().value;
          case 'Build_Bar_U_150.STL':
            return $.zone2XL.next().value;
          case 'Build_Plate_U_90.STL':
            return $.zone2XL.next().value;
          default:
            return $.zone2.next().value;
        }

      case 'Sense':
        return $.zone3.next().value;
      case 'Act':
        return $.zone3.next().value;
      case 'Drive':
        return $.zone3.next().value;
      case 'Think':
        return $.zone4.next().value;
      default :
        return $.zone4.next().value;
    }
  }
}


module.exports = LocationGenerator;
