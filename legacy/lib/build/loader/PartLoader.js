// Loading Process
/*
    1. load json then get all the parts needed in this model
    2. load all the needed parts
    3. generate scene - model
*/
const THREE = require('../Boots.js');
const LOADER = new THREE.STLLoader();

const EventEmitter = require('events').EventEmitter;

const _ = require('underscore');

// Loader class
class PartLoader extends EventEmitter {
  constructor(loaderList, dict) {
    super();
    this.dict = dict;
    this.list = loaderList;
  }
  load() {
    const $ = this;
    Promise.all($.list.map((p) => {
      return $._loadPart(`/static/models/${p}`);
    })).then((parts) => {
      $.emit('done', null);
    }, (err) => {
      $.emit('error', err);
    });
  }
  _loadPart(path) {
    const $ = this;
    const host ='http://gbscn1.roboterra.com.cn/cr';
    return new Promise((resolve, reject) => {
      LOADER.load(`${host}${path}`, (g) => {
        if (g) {
          $.dict[_.last(path.split('/'))] = g;
          resolve();
        } else {
          reject(new Error('Can not load model!'));
        }
      });
    });
  }
}

module.exports = PartLoader;
