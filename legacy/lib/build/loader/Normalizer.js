const _ = require('underscore');

const normalizer = (name) => {

  let nameArr = name.split('.')[0].split('_');
  let nameIdentifier = _.last(nameArr);
  let needReduce = false;
  switch (nameIdentifier) {
    case 'Blue':
      needReduce = true;
      break;
    case 'Red':
      needReduce = true;
      break;
    case 'Silver':
      needReduce = true;
      break;
    case 'Green':
      needReduce = true;
      break;
    case 'Yellow':
      needReduce = true;
      break;
    default:
  }
  if (needReduce) {
    nameArr.pop();
  }
  return `${nameArr.join('_')}.STL`

};

module.exports = normalizer;