// Loading Process
/*
    1. load json then get all the parts needed in this model
    2. load all the needed parts
    3. generate scene - model
*/

const _ = require('underscore');
const PartLoader = require('./PartLoader.js');
const normalizer = require('./Normalizer.js');

export default class ModelLoader {
  constructor(modelData, partDict) {
    this.modelData = modelData;
    this.partDict = partDict;
  }

  loadModel() {
    return new Promise((resolve, reject) => {
      const $ = this;
      // Calculate parts that needs to be loaded again
      const partsNeedLoad = [];
      // Remove name extension
      getPartArr($.modelData).map((p) => {
        return normalizer(p);
      }).forEach((part) => {
        if (!$.partDict[part]) {
          partsNeedLoad.push(part);
        }
      });
      const loader = new PartLoader(partsNeedLoad, $.partDict);
      loader.load();
      loader.on('done', () => {
        resolve();
      });
      loader.on('error', (err) => {
        reject(err);
      });
    });
  }
}

// Calcuate all the parts that being used in this model
function getPartArr(data, arr = []) {
  if (data.type !== 'Mesh') {
    if (data.children) {
      data.children.forEach((ch) => {
        arr = _.union(arr, getPartArr(ch));
      });
    }
    return arr;
  } else {
    return [data.name];
  }
}
