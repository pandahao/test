const THREE = require('./Boots.js');
import { Animation } from './Utils.js';
const LocationGenerator = require('./LocationGenerator.js');


function parseContext(context, project) {
  if (!context) {
    return undefined;
  }
  const preAssembled = context.split('>');
  const obj = [];
  preAssembled.forEach((ch) => {
    const challenge = project[`C${ch}`];
    const bigPart = new THREE.Object3D();
        // console.log(challenge,ch);
        // Add all parts to this big part
    challenge.steps.forEach((step) => {
      step.parts.forEach((part) => {
        bigPart.add(part);
      });
    });

        // Context of Context
    if (challenge.context) {
      const subContext = parseContext(challenge.context, project);
      subContext.forEach((bPart) => {
        bigPart.add(bPart);
      });
    }
    obj.push(bigPart);
  });
  return obj;
}

// a goes first
function merge(a, b) {
  if (a.length === 0) {
    return b;
  }

  if (b.length === 0) {
    return a;
  }
  return [a.shift(), b.shift()].concat(merge(a, b));
}

export default class BuildChallenge {
  constructor(challenge, scene, project) {
        // Keep a reference of the scene obj and challenge obj
    this.scene = scene;
    this.challenge = challenge;
    this.locationGenerator = new LocationGenerator();
    const $ = this;

        // Animation queue
    this.aniQueue = [];
    this.isAnimating = false;

        // Put all parts to canvas
    this.currentStep = 0;

    challenge.steps.sort((a, b) => {
      return a.index - b.index;
    });

        // All context parts treated as a step of challenge (array of objs)
    const cParts = parseContext(challenge.context, project);

    const contextQueue = cParts ? cParts.map((bp) => {
            // Add Context parts to the scene
      this.scene.add(bp);
      return [new Animation(bp, $.locationGenerator)];
    }) : [];

        // Push obj to the scene
    const stepQueue = [];
    challenge.steps.forEach((step, index) => {
      stepQueue.push([]);
      step.parts.forEach((part) => {
                // console.log(part.name);
        stepQueue[index].push(new Animation(part, $.locationGenerator));
                // Add part to the scene
        this.scene.add(part);
      });
    });

        // An 2-degree array keep track of all objects in the scene
    let head;
    if (contextQueue.length > 1) {
      head = contextQueue.shift();
    }
    this.objs = merge(contextQueue, stepQueue);
    if (head) {
      this.objs.unshift(head);
    }
  }

    // Move forward
  forward() {
    if (this.currentStep >= this.objs.length) {
      console.log('End');
      return;
    }
    if (this.isAnimating) {
      console.log('Animation in progress');
      return;
    }
    this.isAnimating = true;

        // console.log('Current index : ', this.currentStep);
    if (!this.objs[this.currentStep]) {
      console.log('Invalid Index or Missing Group');
      return;
    }
    this.objs[this.currentStep].forEach((obj) => {
            // console.log(obj.mesh.name);
      this.aniQueue.push(obj.play());
    });
    this.currentStep += 1;
  }
    // Move backward
  backward() {
    if (this.currentStep <= 0) {
      console.log('Beginning');
      return;
    }

    if (this.isAnimating) {
      console.log('Animation in progress');
      return;
    }

    this.currentStep -= 1;

    this.isAnimating = true;
    if (!this.objs[this.currentStep]) {
      console.log('Invalid Index or Missing Group');
      return;
    }
    console.log('Current index : ', this.currentStep);
    this.objs[this.currentStep].forEach((obj) => {
      this.aniQueue.push(obj.back());
    });
  }
    // animation hook
  animate() {
    if (this.aniQueue.length === 0) {
      this.isAnimating = false;
      return;
    }
    const doneList = [];
    this.aniQueue.forEach((aniFunc, index) => {
      if (aniFunc()) {
        doneList.push(index);
      }
    });
    doneList.reverse().forEach((i) => {
      this.aniQueue.splice(i, 1);
    });
  }

    // teleport an object
  _teleport(obj, forward) {
    if (forward) {
      obj.mesh.position.set(...obj.destp);
      obj.mesh.rotation.set(...obj.destr);
    } else {
      obj.mesh.position.set(...obj.origin);
    }
  }

}
