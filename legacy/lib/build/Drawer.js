const THREE = require('./Boots.js');

import BuildChallenge from './BuildChallenge.js';
import { makeScene } from './Scene';

import { getModel } from './ModelManager';

// const ZOOM_FACTOR = 0.01;

const Drawer = (projectId, level) => {
  // Create the scene
  const scene = new THREE.Scene();

  // Scene objs
  const project = getModel(projectId);

  if (!project) {
    throw new Error('PROJECT DATA NOT LOADED!');
  }

  // Create challenge instance
  const buildChallenge = new BuildChallenge(project[`C${level}`], scene, project);

  // Setup the scene
  const { camera, controls, renderer } = makeScene(scene);

  // Handle window resize
  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
  }
  // Add window resize handler
  window.addEventListener('resize', onWindowResize);

  // Suppose 60FPS
  function render() {
    // Start animation loop
    requestAnimationFrame(render);

    buildChallenge.animate();

    // Clear frame buffer
    // renderer.clear();

    // Update Controls
    // controls.update();

    // Render frame
    renderer.render(scene, camera);
  }

  // Return object functions -- SkylineHandlers
  function onUnmont() {
    // window.SkylineInBuild = false;
    window.removeEventListener('resize', onWindowResize);
  }
  function forward() {
    buildChallenge.forward();
  }
  function backward() {
    buildChallenge.backward();
  }
  function zoom(zoomFactor){
    camera.zoom = 1 + (0.5 - zoomFactor);
    camera.updateProjectionMatrix();
  }
  function reset(){
    controls.reset();
  }

  return {
    render,
    forward,
    backward,
    zoom,
    reset,
    onUnmont
  };
};

export default Drawer;
