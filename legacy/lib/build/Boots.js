const THREE = require('three');
/**
 * @author mrdoob / http://mrdoob.com/
 */

THREE.ColladaLoader = function (manager) {
  this.manager = (manager !== undefined) ? manager : THREE.DefaultLoadingManager;
};

THREE.ColladaLoader.prototype = {

  constructor: THREE.ColladaLoader,

  load(url, onLoad, onProgress, onError) {
    function getBaseUrl(url) {
      const parts = url.split('/');
      parts.pop();
      return `${parts.length < 1 ? '.' : parts.join('/')}/`;
    }

    const scope = this;

    const loader = new THREE.FileLoader(scope.manager);
    loader.load(url, (text) => {
      onLoad(scope.parse(text, getBaseUrl(url)));
    }, onProgress, onError);
  },

  options: {

    set convertUpAxis(value) {
      console.log('ColladaLoder.options.convertUpAxis: TODO');
    }

  },

  setCrossOrigin(value) {
    this.crossOrigin = value;
  },

  parse(text, baseUrl) {
    function getElementsByTagName(xml, name) {
			// Non recursive xml.getElementsByTagName() ...

      const array = [];
      const childNodes = xml.childNodes;

      for (let i = 0, l = childNodes.length; i < l; i++) {
        const child = childNodes[i];

        if (child.nodeName === name) {
          array.push(child);
        }
      }

      return array;
    }

    function parseFloats(text) {
      if (text.length === 0) return [];

      const parts = text.trim().split(/\s+/);
      const array = new Array(parts.length);

      for (let i = 0, l = parts.length; i < l; i++) {
        array[i] = parseFloat(parts[i]);
      }

      return array;
    }

    function parseInts(text) {
      if (text.length === 0) return [];

      const parts = text.trim().split(/\s+/);
      const array = new Array(parts.length);

      for (let i = 0, l = parts.length; i < l; i++) {
        array[i] = parseInt(parts[i]);
      }

      return array;
    }

    function parseId(text) {
      return text.substring(1);
    }

		// asset

    function parseAsset(xml) {
      return {
        unit: parseAssetUnit(getElementsByTagName(xml, 'unit')[0]),
        upAxis: parseAssetUpAxis(getElementsByTagName(xml, 'up_axis')[0])
      };
    }

    function parseAssetUnit(xml) {
      return xml !== undefined ? parseFloat(xml.getAttribute('meter')) : 1;
    }

    function parseAssetUpAxis(xml) {
      return xml !== undefined ? xml.textContent : 'Y_UP';
    }

		// library

    function parseLibrary(xml, libraryName, nodeName, parser) {
      const library = getElementsByTagName(xml, libraryName)[0];

      if (library !== undefined) {
        const elements = getElementsByTagName(library, nodeName);

        for (let i = 0; i < elements.length; i++) {
          parser(elements[i]);
        }
      }
    }

    function buildLibrary(data, builder) {
      for (const name in data) {
        const object = data[name];
        object.build = builder(data[name]);
      }
    }

		// get

    function getBuild(data, builder) {
      if (data.build !== undefined) return data.build;

      data.build = builder(data);

      return data.build;
    }

		// image

    const imageLoader = new THREE.ImageLoader();
    imageLoader.setCrossOrigin(this.crossOrigin);

    function parseImage(xml) {
      const data = {
        init_from: getElementsByTagName(xml, 'init_from')[0].textContent
      };

      library.images[xml.getAttribute('id')] = data;
    }

    function buildImage(data) {
      if (data.build !== undefined) return data.build;

      let url = data.init_from;

      if (baseUrl !== undefined) url = baseUrl + url;

      return imageLoader.load(url);
    }

    function getImage(id) {
      return getBuild(library.images[id], buildImage);
    }

		// effect

    function parseEffect(xml) {
      const data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'profile_COMMON':
            data.profile = parseEffectProfileCOMMON(child);
            break;

        }
      }

      library.effects[xml.getAttribute('id')] = data;
    }

    function parseEffectProfileCOMMON(xml) {
      const data = {
        surfaces: {},
        samplers: {}
      };

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'newparam':
            parseEffectNewparam(child, data);
            break;

          case 'technique':
            data.technique = parseEffectTechnique(child);
            break;

        }
      }

      return data;
    }

    function parseEffectNewparam(xml, data) {
      const sid = xml.getAttribute('sid');

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'surface':
            data.surfaces[sid] = parseEffectSurface(child);
            break;

          case 'sampler2D':
            data.samplers[sid] = parseEffectSampler(child);
            break;

        }
      }
    }

    function parseEffectSurface(xml) {
      const data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'init_from':
            data.init_from = child.textContent;
            break;

        }
      }

      return data;
    }

    function parseEffectSampler(xml) {
      const data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'source':
            data.source = child.textContent;
            break;

        }
      }

      return data;
    }

    function parseEffectTechnique(xml) {
      const data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'constant':
          case 'lambert':
          case 'blinn':
          case 'phong':
            data.type = child.nodeName;
            data.parameters = parseEffectParameters(child);
            break;

        }
      }

      return data;
    }

    function parseEffectParameters(xml) {
      const data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'emission':
          case 'diffuse':
          case 'specular':
          case 'shininess':
          case 'transparency':
            data[child.nodeName] = parseEffectParameter(child);
            break;

        }
      }

      return data;
    }

    function parseEffectParameter(xml) {
      const data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'color':
            data[child.nodeName] = parseFloats(child.textContent);
            break;

          case 'float':
            data[child.nodeName] = parseFloat(child.textContent);
            break;

          case 'texture':
            data[child.nodeName] = { id: child.getAttribute('texture'), extra: parseEffectParameterTexture(child) };
            break;

        }
      }

      return data;
    }

    function parseEffectParameterTexture(xml) {
      let data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'extra':
            data = parseEffectParameterTextureExtra(child);
            break;

        }
      }

      return data;
    }

    function parseEffectParameterTextureExtra(xml) {
      const data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'technique':
            data[child.nodeName] = parseEffectParameterTextureExtraTechnique(child);
            break;

        }
      }

      return data;
    }

    function parseEffectParameterTextureExtraTechnique(xml) {
      const data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'repeatU':
          case 'repeatV':
          case 'offsetU':
          case 'offsetV':
            data[child.nodeName] = parseFloat(child.textContent);
            break;

          case 'wrapU':
          case 'wrapV':
            data[child.nodeName] = parseInt(child.textContent);
            break;

        }
      }

      return data;
    }

    function buildEffect(data) {
      return data;
    }

    function getEffect(id) {
      return getBuild(library.effects[id], buildEffect);
    }

		// material

    function parseMaterial(xml) {
      const data = {
        name: xml.getAttribute('name')
      };

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'instance_effect':
            data.url = parseId(child.getAttribute('url'));
            break;

        }
      }

      library.materials[xml.getAttribute('id')] = data;
    }

    function buildMaterial(data) {
      const effect = getEffect(data.url);
      const technique = effect.profile.technique;

      let material;

      switch (technique.type) {

        case 'phong':
        case 'blinn':
          material = new THREE.MeshPhongMaterial();
          break;

        case 'lambert':
          material = new THREE.MeshLambertMaterial();
          break;

        default:
          material = new THREE.MeshBasicMaterial();
          break;

      }

      material.name = data.name;

      function getTexture(textureObject) {
        const sampler = effect.profile.samplers[textureObject.id];

        if (sampler !== undefined) {
          const surface = effect.profile.surfaces[sampler.source];

          const texture = new THREE.Texture(getImage(surface.init_from));

          const extra = textureObject.extra;

          if (extra !== undefined && extra.technique !== undefined) {
            const technique = extra.technique;

            texture.wrapS = technique.wrapU ? THREE.RepeatWrapping : THREE.ClampToEdgeWrapping;
            texture.wrapT = technique.wrapV ? THREE.RepeatWrapping : THREE.ClampToEdgeWrapping;

            texture.offset.set(technique.offsetU, technique.offsetV);
            texture.repeat.set(technique.repeatU, technique.repeatV);
          } else {
            texture.wrapS = THREE.RepeatWrapping;
            texture.wrapT = THREE.RepeatWrapping;
          }

          texture.needsUpdate = true;

          return texture;
        }

        console.error('ColladaLoder: Undefined sampler', textureObject.id);

        return null;
      }

      const parameters = technique.parameters;

      for (const key in parameters) {
        const parameter = parameters[key];

        switch (key) {
          case 'diffuse':
            if (parameter.color) material.color.fromArray(parameter.color);
            if (parameter.texture) material.map = getTexture(parameter.texture);
            break;
          case 'specular':
            if (parameter.color && material.specular)
              material.specular.fromArray(parameter.color);
            break;
          case 'shininess':
            if (parameter.float && material.shininess)
              material.shininess = parameter.float;
            break;
          case 'emission':
            if (parameter.color && material.emissive)
              material.emissive.fromArray(parameter.color);
            break;
          case 'transparency':
            if (parameter.float)
              material.opacity = parameter.float;
            if (parameter.float !== 1)
              material.transparent = true;
            break;
        }
      }

      return material;
    }

    function getMaterial(id) {
      return getBuild(library.materials[id], buildMaterial);
    }

		// camera

    function parseCamera(xml) {
      const data = {
        name: xml.getAttribute('name')
      };

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'optics':
            data.optics = parseCameraOptics(child);
            break;

        }
      }

      library.cameras[xml.getAttribute('id')] = data;
    }

    function parseCameraOptics(xml) {
      for (let i = 0; i < xml.childNodes.length; i++) {
        const child = xml.childNodes[i];

        switch (child.nodeName) {

          case 'technique_common':
            return parseCameraTechnique(child);

        }
      }

      return {};
    }

    function parseCameraTechnique(xml) {
      const data = {};

      for (let i = 0; i < xml.childNodes.length; i++) {
        const child = xml.childNodes[i];

        switch (child.nodeName) {

          case 'perspective':
          case 'orthographic':

            data.technique = child.nodeName;
            data.parameters = parseCameraParameters(child);

            break;

        }
      }

      return data;
    }

    function parseCameraParameters(xml) {
      const data = {};

      for (let i = 0; i < xml.childNodes.length; i++) {
        const child = xml.childNodes[i];

        switch (child.nodeName) {

          case 'xfov':
          case 'yfov':
          case 'xmag':
          case 'ymag':
          case 'znear':
          case 'zfar':
          case 'aspect_ratio':
            data[child.nodeName] = parseFloat(child.textContent);
            break;

        }
      }

      return data;
    }

    function buildCamera(data) {
      let camera;

      switch (data.optics.technique) {

        case 'perspective':
          camera = new THREE.PerspectiveCamera(
						data.optics.parameters.yfov,
						data.optics.parameters.aspect_ratio,
						data.optics.parameters.znear,
						data.optics.parameters.zfar
					);
          break;

        case 'orthographic':
          camera = new THREE.OrthographicCamera(/* TODO */);
          break;

        default:
          camera = new THREE.PerspectiveCamera();
          break;

      }

      camera.name = data.name;

      return camera;
    }

    function getCamera(id) {
      return getBuild(library.cameras[id], buildCamera);
    }

		// light

    function parseLight(xml) {
      let data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'technique_common':
            data = parseLightTechnique(child);
            break;

        }
      }

      library.lights[xml.getAttribute('id')] = data;
    }

    function parseLightTechnique(xml) {
      const data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'directional':
          case 'point':
          case 'spot':
          case 'ambient':

            data.technique = child.nodeName;
            data.parameters = parseLightParameters(child);

        }
      }

      return data;
    }

    function parseLightParameters(xml) {
      const data = {};

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'color':
            var array = parseFloats(child.textContent);
            data.color = new THREE.Color().fromArray(array);
            break;

          case 'falloff_angle':
            data.falloffAngle = parseFloat(child.textContent);
            break;

          case 'quadratic_attenuation':
            var f = parseFloat(child.textContent);
            data.distance = f ? Math.sqrt(1 / f) : 0;
            break;

        }
      }

      return data;
    }

    function buildLight(data) {
      let light;

      switch (data.technique) {

        case 'directional':
          light = new THREE.DirectionalLight();
          break;

        case 'point':
          light = new THREE.PointLight();
          break;

        case 'spot':
          light = new THREE.SpotLight();
          break;

        case 'ambient':
          light = new THREE.AmbientLight();
          break;

      }

      if (data.parameters.color) light.color.copy(data.parameters.color);
      if (data.parameters.distance) light.distance = data.parameters.distance;

      return light;
    }

    function getLight(id) {
      return getBuild(library.lights[id], buildLight);
    }

		// geometry

    function parseGeometry(xml) {
      const data = {
        name: xml.getAttribute('name'),
        sources: {},
        vertices: {},
        primitives: []
      };

      const mesh = getElementsByTagName(xml, 'mesh')[0];

      for (let i = 0; i < mesh.childNodes.length; i++) {
        const child = mesh.childNodes[i];

        if (child.nodeType !== 1) continue;

        const id = child.getAttribute('id');

        switch (child.nodeName) {

          case 'source':
            data.sources[id] = parseGeometrySource(child);
            break;

          case 'vertices':
						// data.sources[ id ] = data.sources[ parseId( getElementsByTagName( child, 'input' )[ 0 ].getAttribute( 'source' ) ) ];
            data.vertices = parseGeometryVertices(child);
            break;

          case 'polygons':
            console.log('ColladaLoader: Unsupported primitive type: ', child.nodeName);
            break;

          case 'lines':
          case 'linestrips':
          case 'polylist':
          case 'triangles':
            data.primitives.push(parseGeometryPrimitive(child));
            break;

          default:
            console.log(child);

        }
      }

      library.geometries[xml.getAttribute('id')] = data;
    }

    function parseGeometrySource(xml) {
      const data = {
        array: [],
        stride: 3
      };

      for (let i = 0; i < xml.childNodes.length; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'float_array':
            data.array = parseFloats(child.textContent);
            break;

          case 'technique_common':
            var accessor = getElementsByTagName(child, 'accessor')[0];

            if (accessor !== undefined) {
              data.stride = parseInt(accessor.getAttribute('stride'));
            }
            break;

          default:
            console.log(child);

        }
      }

      return data;
    }

    function parseGeometryVertices(xml) {
      const data = {};

      for (let i = 0; i < xml.childNodes.length; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        data[child.getAttribute('semantic')] = parseId(child.getAttribute('source'));
      }

      return data;
    }

    function parseGeometryPrimitive(xml) {
      const primitive = {
        type: xml.nodeName,
        material: xml.getAttribute('material'),
        inputs: {},
        stride: 0
      };

      for (let i = 0, l = xml.childNodes.length; i < l; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'input':
            var id = parseId(child.getAttribute('source'));
            var semantic = child.getAttribute('semantic');
            var offset = parseInt(child.getAttribute('offset'));
            primitive.inputs[semantic] = { id, offset };
            primitive.stride = Math.max(primitive.stride, offset + 1);
            break;

          case 'vcount':
            primitive.vcount = parseInts(child.textContent);
            break;

          case 'p':
            primitive.p = parseInts(child.textContent);
            break;

        }
      }

      return primitive;
    }

    const DEFAULT_LINEMATERIAL = new THREE.LineBasicMaterial();
    const DEFAULT_MESHMATERIAL = new THREE.MeshPhongMaterial();

    function buildGeometry(data) {
      const group = {};

      const sources = data.sources;
      const vertices = data.vertices;
      const primitives = data.primitives;

      if (primitives.length === 0) return group;

      for (let p = 0; p < primitives.length; p++) {
        const primitive = primitives[p];
        const inputs = primitive.inputs;

        const geometry = new THREE.BufferGeometry();

        if (data.name) geometry.name = data.name;

        for (const name in inputs) {
          const input = inputs[name];

          switch (name)	{

            case 'VERTEX':
              for (const key in vertices) {
                geometry.addAttribute(key.toLowerCase(), buildGeometryAttribute(primitive, sources[vertices[key]], input.offset));
              }
              break;

            case 'NORMAL':
              geometry.addAttribute('normal', buildGeometryAttribute(primitive, sources[input.id], input.offset));
              break;

            case 'COLOR':
              geometry.addAttribute('color', buildGeometryAttribute(primitive, sources[input.id], input.offset));
              break;

            case 'TEXCOORD':
              geometry.addAttribute('uv', buildGeometryAttribute(primitive, sources[input.id], input.offset));
              break;

          }
        }

        let object;

        switch (primitive.type) {

          case 'lines':
            object = new THREE.LineSegments(geometry, DEFAULT_LINEMATERIAL);
            break;

          case 'linestrips':
            object = new THREE.Line(geometry, DEFAULT_LINEMATERIAL);
            break;

          case 'triangles':
          case 'polylist':
            object = new THREE.Mesh(geometry, DEFAULT_MESHMATERIAL);
            break;

        }

        group[primitive.material] = object;
      }

      return group;
    }

    function buildGeometryAttribute(primitive, source, offset) {
      const indices = primitive.p;
      const stride = primitive.stride;
      const vcount = primitive.vcount;

      function pushVector(i) {
        let index = indices[i + offset] * sourceStride;
        const length = index + sourceStride;

        for (; index < length; index++) {
          array.push(sourceArray[index]);
        }
      }

      let maxcount = 0;

      let sourceArray = source.array;
      let sourceStride = source.stride;

      let array = [];

      if (primitive.vcount !== undefined) {
        let index = 0;

        for (var i = 0, l = vcount.length; i < l; i++) {
          const count = vcount[i];

          if (count === 4) {
            var a = index + stride * 0;
            var b = index + stride * 1;
            var c = index + stride * 2;
            const d = index + stride * 3;

            pushVector(a); pushVector(b); pushVector(d);
            pushVector(b); pushVector(c); pushVector(d);
          } else if (count === 3) {
            var a = index + stride * 0;
            var b = index + stride * 1;
            var c = index + stride * 2;

            pushVector(a); pushVector(b); pushVector(c);
          } else {
            maxcount = Math.max(maxcount, count);
          }

          index += stride * count;
        }

        if (maxcount > 0) {
          console.log('ColladaLoader: Geometry has faces with more than 4 vertices.');
        }
      } else {
        for (var i = 0, l = indices.length; i < l; i += stride) {
          pushVector(i);
        }
      }

      return new THREE.Float32Attribute(array, sourceStride);
    }

    function getGeometry(id) {
      return getBuild(library.geometries[id], buildGeometry);
    }

		// nodes

    const matrix = new THREE.Matrix4();
    const vector = new THREE.Vector3();

    function parseNode(xml) {
      const data = {
        name: xml.getAttribute('name'),
        matrix: new THREE.Matrix4(),
        nodes: [],
        instanceCameras: [],
        instanceLights: [],
        instanceGeometries: [],
        instanceNodes: []
      };

      for (let i = 0; i < xml.childNodes.length; i++) {
        const child = xml.childNodes[i];

        if (child.nodeType !== 1) continue;

        switch (child.nodeName) {

          case 'node':
            parseNode(child);
            data.nodes.push(child.getAttribute('id'));
            break;

          case 'instance_camera':
            data.instanceCameras.push(parseId(child.getAttribute('url')));
            break;

          case 'instance_light':
            data.instanceLights.push(parseId(child.getAttribute('url')));
            break;

          case 'instance_geometry':
            data.instanceGeometries.push(parseNodeInstanceGeometry(child));
            break;

          case 'instance_node':
            data.instanceNodes.push(parseId(child.getAttribute('url')));
            break;

          case 'matrix':
            var array = parseFloats(child.textContent);
            data.matrix.multiply(matrix.fromArray(array).transpose()); // .transpose() when Z_UP?
            break;

          case 'translate':
            var array = parseFloats(child.textContent);
            vector.fromArray(array);
            data.matrix.multiply(matrix.makeTranslation(vector.x, vector.y, vector.z));
            break;

          case 'rotate':
            var array = parseFloats(child.textContent);
            var angle = THREE.Math.degToRad(array[3]);
            data.matrix.multiply(matrix.makeRotationAxis(vector.fromArray(array), angle));
            break;

          case 'scale':
            var array = parseFloats(child.textContent);
            data.matrix.scale(vector.fromArray(array));
            break;

          case 'extra':
            break;

          default:
            console.log(child);

        }
      }

      if (xml.getAttribute('id') !== null) {
        library.nodes[xml.getAttribute('id')] = data;
      }

      return data;
    }

    function parseNodeInstanceGeometry(xml) {
      const data = {
        id: parseId(xml.getAttribute('url')),
        materials: {}
      };

      for (let i = 0; i < xml.childNodes.length; i++) {
        const child = xml.childNodes[i];

        if (child.nodeName === 'bind_material') {
          const instances = child.getElementsByTagName('instance_material');

          for (let j = 0; j < instances.length; j++) {
            const instance = instances[j];
            const symbol = instance.getAttribute('symbol');
            const target = instance.getAttribute('target');

            data.materials[symbol] = parseId(target);
          }

          break;
        }
      }

      return data;
    }

    function buildNode(data) {
      const objects = [];

      const matrix = data.matrix;
      const nodes = data.nodes;
      const instanceCameras = data.instanceCameras;
      const instanceLights = data.instanceLights;
      const instanceGeometries = data.instanceGeometries;
      const instanceNodes = data.instanceNodes;

      for (var i = 0, l = nodes.length; i < l; i++) {
        objects.push(getNode(nodes[i]).clone());
      }

      for (var i = 0, l = instanceCameras.length; i < l; i++) {
        objects.push(getCamera(instanceCameras[i]).clone());
      }

      for (var i = 0, l = instanceLights.length; i < l; i++) {
        objects.push(getLight(instanceLights[i]).clone());
      }

      for (var i = 0, l = instanceGeometries.length; i < l; i++) {
        const instance = instanceGeometries[i];
        const geometries = getGeometry(instance.id);

        for (const key in geometries) {
          var object = geometries[key].clone();

          if (instance.materials[key] !== undefined) {
            object.material = getMaterial(instance.materials[key]);
          }

          objects.push(object);
        }
      }

      for (var i = 0, l = instanceNodes.length; i < l; i++) {
        objects.push(getNode(instanceNodes[i]).clone());
      }

      var object;

      if (nodes.length === 0 && objects.length === 1) {
        object = objects[0];
      } else {
        object = new THREE.Group();

        for (var i = 0; i < objects.length; i++) {
          object.add(objects[i]);
        }
      }

      object.name = data.name;
      matrix.decompose(object.position, object.quaternion, object.scale);

      return object;
    }

    function getNode(id) {
      return getBuild(library.nodes[id], buildNode);
    }

		// visual scenes

    function parseVisualScene(xml) {
      const data = {
        name: xml.getAttribute('name'),
        children: []
      };

      const elements = getElementsByTagName(xml, 'node');

      for (let i = 0; i < elements.length; i++) {
        data.children.push(parseNode(elements[i]));
      }

      library.visualScenes[xml.getAttribute('id')] = data;
    }

    function buildVisualScene(data) {
      const group = new THREE.Group();
      group.name = data.name;

      const children = data.children;

      for (let i = 0; i < children.length; i++) {
        group.add(buildNode(children[i]));
      }

      return group;
    }

    function getVisualScene(id) {
      return getBuild(library.visualScenes[id], buildVisualScene);
    }

		// scenes

    function parseScene(xml) {
      const instance = getElementsByTagName(xml, 'instance_visual_scene')[0];
      return getVisualScene(parseId(instance.getAttribute('url')));
    }

    console.time('ColladaLoader');

    if (text.length === 0) {
      return { scene: new THREE.Scene() };
    }

    console.time('ColladaLoader: DOMParser');

    const xml = new DOMParser().parseFromString(text, 'application/xml');

    console.timeEnd('ColladaLoader: DOMParser');

    const collada = getElementsByTagName(xml, 'COLLADA')[0];

		// metadata

    const version = collada.getAttribute('version');
    console.log('ColladaLoader: File version', version);

    const asset = parseAsset(getElementsByTagName(collada, 'asset')[0]);

		//

    let library = {
      images: {},
      effects: {},
      materials: {},
      cameras: {},
      lights: {},
      geometries: {},
      nodes: {},
      visualScenes: {}
    };

    console.time('ColladaLoader: Parse');

    parseLibrary(collada, 'library_images', 'image', parseImage);
    parseLibrary(collada, 'library_effects', 'effect', parseEffect);
    parseLibrary(collada, 'library_materials', 'material', parseMaterial);
    parseLibrary(collada, 'library_cameras', 'camera', parseCamera);
    parseLibrary(collada, 'library_lights', 'light', parseLight);
    parseLibrary(collada, 'library_geometries', 'geometry', parseGeometry);
    parseLibrary(collada, 'library_nodes', 'node', parseNode);
    parseLibrary(collada, 'library_visual_scenes', 'visual_scene', parseVisualScene);

    console.timeEnd('ColladaLoader: Parse');

    console.time('ColladaLoader: Build');

    buildLibrary(library.images, buildImage);
    buildLibrary(library.effects, buildEffect);
    buildLibrary(library.materials, buildMaterial);
    buildLibrary(library.cameras, buildCamera);
    buildLibrary(library.lights, buildLight);
    buildLibrary(library.geometries, buildGeometry);
    buildLibrary(library.nodes, buildNode);
    buildLibrary(library.visualScenes, buildVisualScene);

    console.timeEnd('ColladaLoader: Build');

		// console.log( library );

    const scene = parseScene(getElementsByTagName(collada, 'scene')[0]);

    if (asset.upAxis === 'Z_UP') {
      scene.rotation.x = -Math.PI / 2;
    }

    scene.scale.multiplyScalar(asset.unit);

    console.timeEnd('ColladaLoader');

		// console.log( scene );

    return {
      animations: [],
      kinematics: { joints: [] },
      scene
    };
  }

};
/**
 * @author kovacsv / http://kovacsv.hu/
 * @author mrdoob / http://mrdoob.com/
 */

THREE.STLExporter = function () {};

THREE.STLExporter.prototype = {

  constructor: THREE.STLExporter,

  parse: (function () {
    const vector = new THREE.Vector3();
    const normalMatrixWorld = new THREE.Matrix3();

    return function parse(scene) {
      let output = '';

      output += 'solid exported\n';

      scene.traverse((object) => {
        if (object instanceof THREE.Mesh) {
          const geometry = object.geometry;
          const matrixWorld = object.matrixWorld;

          if (geometry instanceof THREE.Geometry) {
            const vertices = geometry.vertices;
            const faces = geometry.faces;

            normalMatrixWorld.getNormalMatrix(matrixWorld);

            for (let i = 0, l = faces.length; i < l; i++) {
              const face = faces[i];

              vector.copy(face.normal).applyMatrix3(normalMatrixWorld).normalize();

              output += `\tfacet normal ${vector.x} ${vector.y} ${vector.z}\n`;
              output += '\t\touter loop\n';

              const indices = [face.a, face.b, face.c];

              for (let j = 0; j < 3; j++) {
                vector.copy(vertices[indices[j]]).applyMatrix4(matrixWorld);

                output += `\t\t\tvertex ${vector.x} ${vector.y} ${vector.z}\n`;
              }

              output += '\t\tendloop\n';
              output += '\tendfacet\n';
            }
          }
        }
      });

      output += 'endsolid exported\n';

      return output;
    };
  }())

};


/**
 * @author qiao / https://github.com/qiao
 * @author mrdoob / http://mrdoob.com
 * @author alteredq / http://alteredqualia.com/
 * @author WestLangley / http://github.com/WestLangley
 * @author erich666 / http://erichaines.com
 */
/* global THREE, console */

(function () {
  function OrbitConstraint(object) {
    this.object = object;

		// "target" sets the location of focus, where the object orbits around
		// and where it pans with respect to.
	    // #####
    this.target = new THREE.Vector3();
        // this.target = new THREE.Vector3().addVectors(object.position, object.getWorldDirection());

		// Limits to how far you can dolly in and out ( PerspectiveCamera only )
    this.minDistance = 0;
    this.maxDistance = Infinity;

		// Limits to how far you can zoom in and out ( OrthographicCamera only )
    this.minZoom = 0;
    this.maxZoom = Infinity;

		// How far you can orbit vertically, upper and lower limits.
		// Range is 0 to Math.PI radians.
    this.minPolarAngle = 0; // radians
    this.maxPolarAngle = Math.PI; // radians

		// How far you can orbit horizontally, upper and lower limits.
		// If set, must be a sub-interval of the interval [ - Math.PI, Math.PI ].
    this.minAzimuthAngle = -Infinity; // radians
    this.maxAzimuthAngle = Infinity; // radians

		// Set to true to enable damping (inertia)
		// If damping is enabled, you must call controls.update() in your animation loop
    this.enableDamping = false;
    this.dampingFactor = 0.25;

		// //////////
		// internals

    const scope = this;

    const EPS = 0.000001;

		// Current position in spherical coordinate system.
    let theta;
    let phi;

		// Pending changes
    let phiDelta = 0;
    let thetaDelta = 0;
    let scale = 1;
    const panOffset = new THREE.Vector3();
    let zoomChanged = false;

		// API

    this.getPolarAngle = function () {
      return phi;
    };

    this.getAzimuthalAngle = function () {
      return theta;
    };

    this.rotateLeft = function (angle) {
      thetaDelta -= angle;
    };

    this.rotateUp = function (angle) {
      phiDelta -= angle;
    };

		// pass in distance in world space to move left
    this.panLeft = function () {
      const v = new THREE.Vector3();

      return function panLeft(distance) {
        const te = this.object.matrix.elements;

				// get X column of matrix
        v.set(te[0], te[1], te[2]);
        v.multiplyScalar(-distance);

        panOffset.add(v);
      };
    }();

		// pass in distance in world space to move up
    this.panUp = function () {
      const v = new THREE.Vector3();

      return function panUp(distance) {
        const te = this.object.matrix.elements;

				// get Y column of matrix
        v.set(te[4], te[5], te[6]);
        v.multiplyScalar(distance);

        panOffset.add(v);
      };
    }();

		// pass in x,y of change desired in pixel space,
		// right and down are positive
    this.pan = function (deltaX, deltaY, screenWidth, screenHeight) {
      if (scope.object instanceof THREE.PerspectiveCamera) {
				// perspective
        const position = scope.object.position;
        const offset = position.clone().sub(scope.target);
        let targetDistance = offset.length();

				// half of the fov is center to top of screen
        targetDistance *= Math.tan((scope.object.fov / 2) * Math.PI / 180.0);

				// we actually don't use screenWidth, since perspective camera is fixed to screen height
        scope.panLeft(2 * deltaX * targetDistance / screenHeight);
        scope.panUp(2 * deltaY * targetDistance / screenHeight);
      } else if (scope.object instanceof THREE.OrthographicCamera) {
				// orthographic
        scope.panLeft(deltaX * (scope.object.right - scope.object.left) / screenWidth);
        scope.panUp(deltaY * (scope.object.top - scope.object.bottom) / screenHeight);
      } else {
				// camera neither orthographic or perspective
        console.warn('WARNING: OrbitControls.js encountered an unknown camera type - pan disabled.');
      }
    };

    this.dollyIn = function (dollyScale) {
      if (scope.object instanceof THREE.PerspectiveCamera) {
        scale /= dollyScale;
      } else if (scope.object instanceof THREE.OrthographicCamera) {
        scope.object.zoom = Math.max(this.minZoom, Math.min(this.maxZoom, this.object.zoom * dollyScale));
        scope.object.updateProjectionMatrix();
        zoomChanged = true;
      } else {
        console.warn('WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled.');
      }
    };

    this.dollyOut = function (dollyScale) {
      if (scope.object instanceof THREE.PerspectiveCamera) {
        scale *= dollyScale;
      } else if (scope.object instanceof THREE.OrthographicCamera) {
        scope.object.zoom = Math.max(this.minZoom, Math.min(this.maxZoom, this.object.zoom / dollyScale));
        scope.object.updateProjectionMatrix();
        zoomChanged = true;
      } else {
        console.warn('WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled.');
      }
    };

    this.update = function () {
      const offset = new THREE.Vector3();

			// so camera.up is the orbit axis
      const quat = new THREE.Quaternion().setFromUnitVectors(object.up, new THREE.Vector3(0, 1, 0));
      const quatInverse = quat.clone().inverse();

      const lastPosition = new THREE.Vector3();
      const lastQuaternion = new THREE.Quaternion();

      return function () {
        const position = this.object.position;

        offset.copy(position).sub(this.target);

				// rotate offset to "y-axis-is-up" space
        offset.applyQuaternion(quat);

				// angle from z-axis around y-axis

        theta = Math.atan2(offset.x, offset.z);

				// angle from y-axis

        phi = Math.atan2(Math.sqrt(offset.x * offset.x + offset.z * offset.z), offset.y);

        theta += thetaDelta;
        phi += phiDelta;

				// restrict theta to be between desired limits
        theta = Math.max(this.minAzimuthAngle, Math.min(this.maxAzimuthAngle, theta));

				// restrict phi to be between desired limits
        phi = Math.max(this.minPolarAngle, Math.min(this.maxPolarAngle, phi));

				// restrict phi to be betwee EPS and PI-EPS
        phi = Math.max(EPS, Math.min(Math.PI - EPS, phi));

        let radius = offset.length() * scale;

				// restrict radius to be between desired limits
        radius = Math.max(this.minDistance, Math.min(this.maxDistance, radius));

				// move target to panned location
        this.target.add(panOffset);

        offset.x = radius * Math.sin(phi) * Math.sin(theta);
        offset.y = radius * Math.cos(phi);
        offset.z = radius * Math.sin(phi) * Math.cos(theta);

				// rotate offset back to "camera-up-vector-is-up" space
        offset.applyQuaternion(quatInverse);

        position.copy(this.target).add(offset);

        this.object.lookAt(this.target);

        if (this.enableDamping === true) {
          thetaDelta *= (1 - this.dampingFactor);
          phiDelta *= (1 - this.dampingFactor);
        } else {
          thetaDelta = 0;
          phiDelta = 0;
        }

        scale = 1;
        panOffset.set(0, 0, 0);

				// update condition is:
				// min(camera displacement, camera rotation in radians)^2 > EPS
				// using small-angle approximation cos(x/2) = 1 - x^2 / 8

        if (zoomChanged ||
					 lastPosition.distanceToSquared(this.object.position) > EPS ||
				    8 * (1 - lastQuaternion.dot(this.object.quaternion)) > EPS) {
          lastPosition.copy(this.object.position);
          lastQuaternion.copy(this.object.quaternion);
          zoomChanged = false;

          return true;
        }

        return false;
      };
    }();
  }


	// This set of controls performs orbiting, dollying (zooming), and panning. It maintains
	// the "up" direction as +Y, unlike the TrackballControls. Touch on tablet and phones is
	// supported.
	//
	//    Orbit - left mouse / touch: one finger move
	//    Zoom - middle mouse, or mousewheel / touch: two finger spread or squish
	//    Pan - right mouse, or arrow keys / touch: three finter swipe

  THREE.OrbitControls = function (object, domElement) {
    const constraint = new OrbitConstraint(object);

    this.domElement = (domElement !== undefined) ? domElement : document;

		// API

    Object.defineProperty(this, 'constraint', {

      get() {
        return constraint;
      }

    });

    this.getPolarAngle = function () {
      return constraint.getPolarAngle();
    };

    this.getAzimuthalAngle = function () {
      return constraint.getAzimuthalAngle();
    };

		// Set to false to disable this control
    this.enabled = true;

		// center is old, deprecated; use "target" instead
    this.center = this.target;

		// This option actually enables dollying in and out; left as "zoom" for
		// backwards compatibility.
		// Set to false to disable zooming
    this.enableZoom = true;
    this.zoomSpeed = 1.0;

		// Set to false to disable rotating
    this.enableRotate = true;
    this.rotateSpeed = 1.0;

		// Set to false to disable panning
    this.enablePan = true;
    this.keyPanSpeed = 7.0;	// pixels moved per arrow key push

		// Set to true to automatically rotate around the target
		// If auto-rotate is enabled, you must call controls.update() in your animation loop
    this.autoRotate = false;
    this.autoRotateSpeed = 2.0; // 30 seconds per round when fps is 60

		// Set to false to disable use of the keys
    this.enableKeys = true;

		// The four arrow keys
    this.keys = { LEFT: 37, UP: 38, RIGHT: 39, BOTTOM: 40 };

		// Mouse buttons
    this.mouseButtons = { ORBIT: THREE.MOUSE.LEFT, ZOOM: THREE.MOUSE.MIDDLE, PAN: THREE.MOUSE.RIGHT };

		// //////////
		// internals

    const scope = this;

    const rotateStart = new THREE.Vector2();
    const rotateEnd = new THREE.Vector2();
    const rotateDelta = new THREE.Vector2();

    const panStart = new THREE.Vector2();
    const panEnd = new THREE.Vector2();
    const panDelta = new THREE.Vector2();

    const dollyStart = new THREE.Vector2();
    const dollyEnd = new THREE.Vector2();
    const dollyDelta = new THREE.Vector2();

    const STATE = { NONE: -1, ROTATE: 0, DOLLY: 1, PAN: 2, TOUCH_ROTATE: 3, TOUCH_DOLLY: 4, TOUCH_PAN: 5 };

    let state = STATE.NONE;

		// for reset

    this.target0 = this.target.clone();
    this.position0 = this.object.position.clone();
    this.zoom0 = this.object.zoom;

		// events

    const changeEvent = { type: 'change' };
    const startEvent = { type: 'start' };
    const endEvent = { type: 'end' };

		// pass in x,y of change desired in pixel space,
		// right and down are positive
    function pan(deltaX, deltaY) {
      const element = scope.domElement === document ? scope.domElement.body : scope.domElement;

      constraint.pan(deltaX, deltaY, element.clientWidth, element.clientHeight);
    }

    this.update = function () {
      if (this.autoRotate && state === STATE.NONE) {
        constraint.rotateLeft(getAutoRotationAngle());
      }

      if (constraint.update() === true) {
        this.dispatchEvent(changeEvent);
      }
    };

    this.reset = function () {
      state = STATE.NONE;

      this.target.copy(this.target0);
      this.object.position.copy(this.position0);
      this.object.zoom = this.zoom0;

      this.object.updateProjectionMatrix();
      this.dispatchEvent(changeEvent);

      this.update();
    };

    function getAutoRotationAngle() {
      return 2 * Math.PI / 60 / 60 * scope.autoRotateSpeed;
    }

    function getZoomScale() {
      return Math.pow(0.95, scope.zoomSpeed);
    }

    function onMouseDown(event) {
      if (scope.enabled === false) return;

      event.preventDefault();

      if (event.button === scope.mouseButtons.ORBIT) {
        if (scope.enableRotate === false) return;

        state = STATE.ROTATE;

        rotateStart.set(event.clientX, event.clientY);
      } else if (event.button === scope.mouseButtons.ZOOM) {
        if (scope.enableZoom === false) return;

        state = STATE.DOLLY;

        dollyStart.set(event.clientX, event.clientY);
      } else if (event.button === scope.mouseButtons.PAN) {
        if (scope.enablePan === false) return;

        state = STATE.PAN;

        panStart.set(event.clientX, event.clientY);
      }

      if (state !== STATE.NONE) {
        document.addEventListener('mousemove', onMouseMove, false);
        document.addEventListener('mouseup', onMouseUp, false);
        scope.dispatchEvent(startEvent);
      }
    }

    function onMouseMove(event) {
      if (scope.enabled === false) return;

      event.preventDefault();

      const element = scope.domElement === document ? scope.domElement.body : scope.domElement;

      if (state === STATE.ROTATE) {
        if (scope.enableRotate === false) return;

        rotateEnd.set(event.clientX, event.clientY);
        rotateDelta.subVectors(rotateEnd, rotateStart);

				// rotating across whole screen goes 360 degrees around
        constraint.rotateLeft(2 * Math.PI * rotateDelta.x / element.clientWidth * scope.rotateSpeed);

				// rotating up and down along whole screen attempts to go 360, but limited to 180
        constraint.rotateUp(2 * Math.PI * rotateDelta.y / element.clientHeight * scope.rotateSpeed);

        rotateStart.copy(rotateEnd);
      } else if (state === STATE.DOLLY) {
        if (scope.enableZoom === false) return;

        dollyEnd.set(event.clientX, event.clientY);
        dollyDelta.subVectors(dollyEnd, dollyStart);

        if (dollyDelta.y > 0) {
          constraint.dollyIn(getZoomScale());
        } else if (dollyDelta.y < 0) {
          constraint.dollyOut(getZoomScale());
        }

        dollyStart.copy(dollyEnd);
      } else if (state === STATE.PAN) {
        if (scope.enablePan === false) return;

        panEnd.set(event.clientX, event.clientY);
        panDelta.subVectors(panEnd, panStart);

        pan(panDelta.x, panDelta.y);

        panStart.copy(panEnd);
      }

      if (state !== STATE.NONE) scope.update();
    }

    function onMouseUp(/* event */) {
      if (scope.enabled === false) return;

      document.removeEventListener('mousemove', onMouseMove, false);
      document.removeEventListener('mouseup', onMouseUp, false);
      scope.dispatchEvent(endEvent);
      state = STATE.NONE;
    }

    function onMouseWheel(event) {
      if (scope.enabled === false || scope.enableZoom === false || state !== STATE.NONE) return;

      event.preventDefault();
      event.stopPropagation();

      let delta = 0;

      if (event.wheelDelta !== undefined) {
				// WebKit / Opera / Explorer 9

        delta = event.wheelDelta;
      } else if (event.detail !== undefined) {
				// Firefox

        delta = -event.detail;
      }

      if (delta > 0) {
        constraint.dollyOut(getZoomScale());
      } else if (delta < 0) {
        constraint.dollyIn(getZoomScale());
      }

      scope.update();
      scope.dispatchEvent(startEvent);
      scope.dispatchEvent(endEvent);
    }

    function onKeyDown(event) {
			/*
			if ( scope.enabled === false || scope.enableKeys === false || scope.enablePan === false ) return;

			switch ( event.keyCode ) {

				case scope.keys.UP:
					pan( 0, scope.keyPanSpeed );
					scope.update();
					break;

				case scope.keys.BOTTOM:
					pan( 0, - scope.keyPanSpeed );
					scope.update();
					break;

				case scope.keys.LEFT:
					pan( scope.keyPanSpeed, 0 );
					scope.update();
					break;

				case scope.keys.RIGHT:
					pan( - scope.keyPanSpeed, 0 );
					scope.update();
					break;

			}
			*/

    }

    function touchstart(event) {
      if (scope.enabled === false) return;

      switch (event.touches.length) {

        case 1:	// one-fingered touch: rotate

          if (scope.enableRotate === false) return;

          state = STATE.TOUCH_ROTATE;

          rotateStart.set(event.touches[0].pageX, event.touches[0].pageY);
          break;

        case 2:	// two-fingered touch: dolly

          if (scope.enableZoom === false) return;

          state = STATE.TOUCH_DOLLY;

          var dx = event.touches[0].pageX - event.touches[1].pageX;
          var dy = event.touches[0].pageY - event.touches[1].pageY;
          var distance = Math.sqrt(dx * dx + dy * dy);
          dollyStart.set(0, distance);
          break;

        case 3: // three-fingered touch: pan

          if (scope.enablePan === false) return;

          state = STATE.TOUCH_PAN;

          panStart.set(event.touches[0].pageX, event.touches[0].pageY);
          break;

        default:

          state = STATE.NONE;

      }

      if (state !== STATE.NONE) scope.dispatchEvent(startEvent);
    }

    function touchmove(event) {
      if (scope.enabled === false) return;

      event.preventDefault();
      event.stopPropagation();

      const element = scope.domElement === document ? scope.domElement.body : scope.domElement;

      switch (event.touches.length) {

        case 1: // one-fingered touch: rotate

          if (scope.enableRotate === false) return;
          if (state !== STATE.TOUCH_ROTATE) return;

          rotateEnd.set(event.touches[0].pageX, event.touches[0].pageY);
          rotateDelta.subVectors(rotateEnd, rotateStart);

					// rotating across whole screen goes 360 degrees around
          constraint.rotateLeft(2 * Math.PI * rotateDelta.x / element.clientWidth * scope.rotateSpeed);
					// rotating up and down along whole screen attempts to go 360, but limited to 180
          constraint.rotateUp(2 * Math.PI * rotateDelta.y / element.clientHeight * scope.rotateSpeed);

          rotateStart.copy(rotateEnd);

          scope.update();
          break;

        case 2: // two-fingered touch: dolly

          if (scope.enableZoom === false) return;
          if (state !== STATE.TOUCH_DOLLY) return;

          var dx = event.touches[0].pageX - event.touches[1].pageX;
          var dy = event.touches[0].pageY - event.touches[1].pageY;
          var distance = Math.sqrt(dx * dx + dy * dy);

          dollyEnd.set(0, distance);
          dollyDelta.subVectors(dollyEnd, dollyStart);

          if (dollyDelta.y > 0) {
            constraint.dollyOut(getZoomScale());
          } else if (dollyDelta.y < 0) {
            constraint.dollyIn(getZoomScale());
          }

          dollyStart.copy(dollyEnd);

          scope.update();
          break;

        case 3: // three-fingered touch: pan

          if (scope.enablePan === false) return;
          if (state !== STATE.TOUCH_PAN) return;

          panEnd.set(event.touches[0].pageX, event.touches[0].pageY);
          panDelta.subVectors(panEnd, panStart);

          pan(panDelta.x, panDelta.y);

          panStart.copy(panEnd);

          scope.update();
          break;

        default:

          state = STATE.NONE;

      }
    }

    function touchend(/* event */) {
      if (scope.enabled === false) return;

      scope.dispatchEvent(endEvent);
      state = STATE.NONE;
    }

    function contextmenu(event) {
      event.preventDefault();
    }

    this.dispose = function () {
      this.domElement.removeEventListener('contextmenu', contextmenu, false);
      this.domElement.removeEventListener('mousedown', onMouseDown, false);
      this.domElement.removeEventListener('mousewheel', onMouseWheel, false);
      this.domElement.removeEventListener('MozMousePixelScroll', onMouseWheel, false); // firefox

      this.domElement.removeEventListener('touchstart', touchstart, false);
      this.domElement.removeEventListener('touchend', touchend, false);
      this.domElement.removeEventListener('touchmove', touchmove, false);

      document.removeEventListener('mousemove', onMouseMove, false);
      document.removeEventListener('mouseup', onMouseUp, false);

      window.removeEventListener('keydown', onKeyDown, false);
    };

    this.domElement.addEventListener('contextmenu', contextmenu, false);

    this.domElement.addEventListener('mousedown', onMouseDown, false);
    this.domElement.addEventListener('mousewheel', onMouseWheel, false);
    this.domElement.addEventListener('MozMousePixelScroll', onMouseWheel, false); // firefox

    this.domElement.addEventListener('touchstart', touchstart, false);
    this.domElement.addEventListener('touchend', touchend, false);
    this.domElement.addEventListener('touchmove', touchmove, false);

    window.addEventListener('keydown', onKeyDown, false);

		// force an update at start
    this.update();
  };

  THREE.OrbitControls.prototype = Object.create(THREE.EventDispatcher.prototype);
  THREE.OrbitControls.prototype.constructor = THREE.OrbitControls;

  Object.defineProperties(THREE.OrbitControls.prototype, {

    object: {

      get() {
        return this.constraint.object;
      }

    },

    target: {

      get() {
        return this.constraint.target;
      },

      set(value) {
        console.warn('THREE.OrbitControls: target is now immutable. Use target.set() instead.');
        this.constraint.target.copy(value);
      }

    },

    minDistance: {

      get() {
        return this.constraint.minDistance;
      },

      set(value) {
        this.constraint.minDistance = value;
      }

    },

    maxDistance: {

      get() {
        return this.constraint.maxDistance;
      },

      set(value) {
        this.constraint.maxDistance = value;
      }

    },

    minZoom: {

      get() {
        return this.constraint.minZoom;
      },

      set(value) {
        this.constraint.minZoom = value;
      }

    },

    maxZoom: {

      get() {
        return this.constraint.maxZoom;
      },

      set(value) {
        this.constraint.maxZoom = value;
      }

    },

    minPolarAngle: {

      get() {
        return this.constraint.minPolarAngle;
      },

      set(value) {
        this.constraint.minPolarAngle = value;
      }

    },

    maxPolarAngle: {

      get() {
        return this.constraint.maxPolarAngle;
      },

      set(value) {
        this.constraint.maxPolarAngle = value;
      }

    },

    minAzimuthAngle: {

      get() {
        return this.constraint.minAzimuthAngle;
      },

      set(value) {
        this.constraint.minAzimuthAngle = value;
      }

    },

    maxAzimuthAngle: {

      get() {
        return this.constraint.maxAzimuthAngle;
      },

      set(value) {
        this.constraint.maxAzimuthAngle = value;
      }

    },

    enableDamping: {

      get() {
        return this.constraint.enableDamping;
      },

      set(value) {
        this.constraint.enableDamping = value;
      }

    },

    dampingFactor: {

      get() {
        return this.constraint.dampingFactor;
      },

      set(value) {
        this.constraint.dampingFactor = value;
      }

    },

		// backward compatibility

    noZoom: {

      get() {
        console.warn('THREE.OrbitControls: .noZoom has been deprecated. Use .enableZoom instead.');
        return !this.enableZoom;
      },

      set(value) {
        console.warn('THREE.OrbitControls: .noZoom has been deprecated. Use .enableZoom instead.');
        this.enableZoom = !value;
      }

    },

    noRotate: {

      get() {
        console.warn('THREE.OrbitControls: .noRotate has been deprecated. Use .enableRotate instead.');
        return !this.enableRotate;
      },

      set(value) {
        console.warn('THREE.OrbitControls: .noRotate has been deprecated. Use .enableRotate instead.');
        this.enableRotate = !value;
      }

    },

    noPan: {

      get() {
        console.warn('THREE.OrbitControls: .noPan has been deprecated. Use .enablePan instead.');
        return !this.enablePan;
      },

      set(value) {
        console.warn('THREE.OrbitControls: .noPan has been deprecated. Use .enablePan instead.');
        this.enablePan = !value;
      }

    },

    noKeys: {

      get() {
        console.warn('THREE.OrbitControls: .noKeys has been deprecated. Use .enableKeys instead.');
        return !this.enableKeys;
      },

      set(value) {
        console.warn('THREE.OrbitControls: .noKeys has been deprecated. Use .enableKeys instead.');
        this.enableKeys = !value;
      }

    },

    staticMoving: {

      get() {
        console.warn('THREE.OrbitControls: .staticMoving has been deprecated. Use .enableDamping instead.');
        return !this.constraint.enableDamping;
      },

      set(value) {
        console.warn('THREE.OrbitControls: .staticMoving has been deprecated. Use .enableDamping instead.');
        this.constraint.enableDamping = !value;
      }

    },

    dynamicDampingFactor: {

      get() {
        console.warn('THREE.OrbitControls: .dynamicDampingFactor has been renamed. Use .dampingFactor instead.');
        return this.constraint.dampingFactor;
      },

      set(value) {
        console.warn('THREE.OrbitControls: .dynamicDampingFactor has been renamed. Use .dampingFactor instead.');
        this.constraint.dampingFactor = value;
      }

    }

  });
}());
/**
 * @author aleeper / http://adamleeper.com/
 * @author mrdoob / http://mrdoob.com/
 * @author gero3 / https://github.com/gero3
 *
 * Description: A THREE loader for STL ASCII files, as created by Solidworks and other CAD programs.
 *
 * Supports both binary and ASCII encoded files, with automatic detection of type.
 *
 * Limitations:
 *  Binary decoding supports "Magics" color format (http://en.wikipedia.org/wiki/STL_(file_format)#Color_in_binary_STL).
 *  There is perhaps some question as to how valid it is to always assume little-endian-ness.
 *  ASCII decoding assumes file is UTF-8. Seems to work for the examples...
 *
 * Usage:
 *  var loader = new THREE.STLLoader();
 *  loader.load( './models/stl/slotted_disk.stl', function ( geometry ) {
 *    scene.add( new THREE.Mesh( geometry ) );
 *  });
 *
 * For binary STLs geometry might contain colors for vertices. To use it:
 *  // use the same code to load STL as above
 *  if (geometry.hasColors) {
 *    material = new THREE.MeshPhongMaterial({ opacity: geometry.alpha, vertexColors: THREE.VertexColors });
 *  } else { .... }
 *  var mesh = new THREE.Mesh( geometry, material );
 */


THREE.STLLoader = function (manager) {
  this.manager = (manager !== undefined) ? manager : THREE.DefaultLoadingManager;
};

THREE.STLLoader.prototype = {

  constructor: THREE.STLLoader,

  load(url, onLoad, onProgress, onError) {
    const scope = this;

    const loader = new THREE.FileLoader(scope.manager);
    loader.setResponseType('arraybuffer');
    loader.load(url, (text) => {
      onLoad(scope.parse(text));
    }, onProgress, onError);
  },

  parse(data) {
    const isBinary = function () {
      let expect, face_size, n_faces, reader;
      reader = new DataView(binData);
      face_size = (32 / 8 * 3) + ((32 / 8 * 3) * 3) + (16 / 8);
      n_faces = reader.getUint32(80, true);
      expect = 80 + (32 / 8) + (n_faces * face_size);

      if (expect === reader.byteLength) {
        return true;
      }

 			// some binary files will have different size from expected,
 			// checking characters higher than ASCII to confirm is binary
      const fileLength = reader.byteLength;
      for (let index = 0; index < fileLength; index++) {
        if (reader.getUint8(index, false) > 127) {
          return true;
        }
      }

      return false;
    };

    let binData = this.ensureBinary(data);

        // OMGGGGGGG
        /*

        // isBinary seems to be broken!
 		return isBinary()
 			? this.parseBinary( binData )
 			: this.parseASCII( this.ensureString( data ) );
        */

    return this.parseASCII(this.ensureString(data));
  },

  parseBinary(data) {
    const reader = new DataView(data);
    const faces = reader.getUint32(80, true);

    let r, g, b, hasColors = false, colors;
    let defaultR, defaultG, defaultB, alpha;

 		// process STL header
 		// check for default color in header ("COLOR=rgba" sequence).

    for (let index = 0; index < 80 - 10; index++) {
      if ((reader.getUint32(index, false) == 0x434F4C4F /* COLO*/) &&
 				(reader.getUint8(index + 4) == 0x52 /* 'R'*/) &&
 				(reader.getUint8(index + 5) == 0x3D /* '='*/)) {
        hasColors = true;
        colors = new Float32Array(faces * 3 * 3);

        defaultR = reader.getUint8(index + 6) / 255;
        defaultG = reader.getUint8(index + 7) / 255;
        defaultB = reader.getUint8(index + 8) / 255;
        alpha = reader.getUint8(index + 9) / 255;
      }
    }

    const dataOffset = 84;
    const faceLength = 12 * 4 + 2;

    let offset = 0;

    const geometry = new THREE.BufferGeometry();

    const vertices = new Float32Array(faces * 3 * 3);
    const normals = new Float32Array(faces * 3 * 3);

    for (let face = 0; face < faces; face++) {
      const start = dataOffset + face * faceLength;
      const normalX = reader.getFloat32(start, true);
      const normalY = reader.getFloat32(start + 4, true);
      const normalZ = reader.getFloat32(start + 8, true);

      if (hasColors) {
        const packedColor = reader.getUint16(start + 48, true);

        if ((packedColor & 0x8000) === 0) {
 					// facet has its own unique color

          r = (packedColor & 0x1F) / 31;
          g = ((packedColor >> 5) & 0x1F) / 31;
          b = ((packedColor >> 10) & 0x1F) / 31;
        } else {
          r = defaultR;
          g = defaultG;
          b = defaultB;
        }
      }

      for (let i = 1; i <= 3; i++) {
        const vertexstart = start + i * 12;

        vertices[offset] = reader.getFloat32(vertexstart, true);
        vertices[offset + 1] = reader.getFloat32(vertexstart + 4, true);
        vertices[offset + 2] = reader.getFloat32(vertexstart + 8, true);

        normals[offset] = normalX;
        normals[offset + 1] = normalY;
        normals[offset + 2] = normalZ;

        if (hasColors) {
          colors[offset] = r;
          colors[offset + 1] = g;
          colors[offset + 2] = b;
        }

        offset += 3;
      }
    }

    geometry.addAttribute('position', new THREE.BufferAttribute(vertices, 3));
    geometry.addAttribute('normal', new THREE.BufferAttribute(normals, 3));

    if (hasColors) {
      geometry.addAttribute('color', new THREE.BufferAttribute(colors, 3));
      geometry.hasColors = true;
      geometry.alpha = alpha;
    }

    return geometry;
  },

  parseASCII(data) {
    let geometry, length, normal, patternFace, patternNormal, patternVertex, result, text;
    geometry = new THREE.Geometry();
    patternFace = /facet([\s\S]*?)endfacet/g;

    while ((result = patternFace.exec(data)) !== null) {
      text = result[0];
      patternNormal = /normal[\s]+([\-+]?[0-9]+\.?[0-9]*([eE][\-+]?[0-9]+)?)+[\s]+([\-+]?[0-9]*\.?[0-9]+([eE][\-+]?[0-9]+)?)+[\s]+([\-+]?[0-9]*\.?[0-9]+([eE][\-+]?[0-9]+)?)+/g;

      while ((result = patternNormal.exec(text)) !== null) {
        normal = new THREE.Vector3(parseFloat(result[1]), parseFloat(result[3]), parseFloat(result[5]));
      }

      patternVertex = /vertex[\s]+([\-+]?[0-9]+\.?[0-9]*([eE][\-+]?[0-9]+)?)+[\s]+([\-+]?[0-9]*\.?[0-9]+([eE][\-+]?[0-9]+)?)+[\s]+([\-+]?[0-9]*\.?[0-9]+([eE][\-+]?[0-9]+)?)+/g;

      while ((result = patternVertex.exec(text)) !== null) {
        geometry.vertices.push(new THREE.Vector3(parseFloat(result[1]), parseFloat(result[3]), parseFloat(result[5])));
      }

      length = geometry.vertices.length;

      geometry.faces.push(new THREE.Face3(length - 3, length - 2, length - 1, normal));
    }

    geometry.computeBoundingBox();
    geometry.computeBoundingSphere();

    return geometry;
  },

  ensureString(buf) {
    if (typeof buf !== 'string') {
      const array_buffer = new Uint8Array(buf);
      let str = '';
      for (let i = 0; i < buf.byteLength; i++) {
        str += String.fromCharCode(array_buffer[i]); // implicitly assumes little-endian
      }
      return str;
    } else {
      return buf;
    }
  },

  ensureBinary(buf) {
    if (typeof buf === 'string') {
      const array_buffer = new Uint8Array(buf.length);
      for (let i = 0; i < buf.length; i++) {
        array_buffer[i] = buf.charCodeAt(i) & 0xff; // implicitly assumes little-endian
      }
      return array_buffer.buffer || array_buffer;
    } else {
      return buf;
    }
  }

};

if (typeof DataView === 'undefined') {
  DataView = function (buffer, byteOffset, byteLength) {
    this.buffer = buffer;
    this.byteOffset = byteOffset || 0;
    this.byteLength = byteLength || buffer.byteLength || buffer.length;
    this._isString = typeof buffer === 'string';
  };

  DataView.prototype = {

    _getCharCodes(buffer, start, length) {
      start = start || 0;
      length = length || buffer.length;
      const end = start + length;
      const codes = [];
      for (let i = start; i < end; i++) {
        codes.push(buffer.charCodeAt(i) & 0xff);
      }
      return codes;
    },

    _getBytes(length, byteOffset, littleEndian) {
      let result;

 			// Handle the lack of endianness
      if (littleEndian === undefined) {
        littleEndian = this._littleEndian;
      }

 			// Handle the lack of byteOffset
      if (byteOffset === undefined) {
        byteOffset = this.byteOffset;
      } else {
        byteOffset = this.byteOffset + byteOffset;
      }

      if (length === undefined) {
        length = this.byteLength - byteOffset;
      }

 			// Error Checking
      if (typeof byteOffset !== 'number') {
        throw new TypeError('DataView byteOffset is not a number');
      }

      if (length < 0 || byteOffset + length > this.byteLength) {
        throw new Error('DataView length or (byteOffset+length) value is out of bounds');
      }

      if (this.isString) {
        result = this._getCharCodes(this.buffer, byteOffset, byteOffset + length);
      } else {
        result = this.buffer.slice(byteOffset, byteOffset + length);
      }

      if (!littleEndian && length > 1) {
        if (Array.isArray(result) === false) {
          result = Array.prototype.slice.call(result);
        }

        result.reverse();
      }

      return result;
    },

 		// Compatibility functions on a String Buffer

    getFloat64(byteOffset, littleEndian) {
      let b = this._getBytes(8, byteOffset, littleEndian),

        sign = 1 - (2 * (b[7] >> 7)),
        exponent = ((((b[7] << 1) & 0xff) << 3) | (b[6] >> 4)) - ((1 << 10) - 1),

 			// Binary operators such as | and << operate on 32 bit values, using + and Math.pow(2) instead
        mantissa = ((b[6] & 0x0f) * Math.pow(2, 48)) + (b[5] * Math.pow(2, 40)) + (b[4] * Math.pow(2, 32)) +
 							(b[3] * Math.pow(2, 24)) + (b[2] * Math.pow(2, 16)) + (b[1] * Math.pow(2, 8)) + b[0];

      if (exponent === 1024) {
        if (mantissa !== 0) {
          return NaN;
        } else {
          return sign * Infinity;
        }
      }

      if (exponent === -1023) {
 				// Denormalized
        return sign * mantissa * Math.pow(2, -1022 - 52);
      }

      return sign * (1 + mantissa * Math.pow(2, -52)) * Math.pow(2, exponent);
    },

    getFloat32(byteOffset, littleEndian) {
      let b = this._getBytes(4, byteOffset, littleEndian),

        sign = 1 - (2 * (b[3] >> 7)),
        exponent = (((b[3] << 1) & 0xff) | (b[2] >> 7)) - 127,
        mantissa = ((b[2] & 0x7f) << 16) | (b[1] << 8) | b[0];

      if (exponent === 128) {
        if (mantissa !== 0) {
          return NaN;
        } else {
          return sign * Infinity;
        }
      }

      if (exponent === -127) {
 				// Denormalized
        return sign * mantissa * Math.pow(2, -126 - 23);
      }

      return sign * (1 + mantissa * Math.pow(2, -23)) * Math.pow(2, exponent);
    },

    getInt32(byteOffset, littleEndian) {
      const b = this._getBytes(4, byteOffset, littleEndian);
      return (b[3] << 24) | (b[2] << 16) | (b[1] << 8) | b[0];
    },

    getUint32(byteOffset, littleEndian) {
      return this.getInt32(byteOffset, littleEndian) >>> 0;
    },

    getInt16(byteOffset, littleEndian) {
      return (this.getUint16(byteOffset, littleEndian) << 16) >> 16;
    },

    getUint16(byteOffset, littleEndian) {
      const b = this._getBytes(2, byteOffset, littleEndian);
      return (b[1] << 8) | b[0];
    },

    getInt8(byteOffset) {
      return (this.getUint8(byteOffset) << 24) >> 24;
    },

    getUint8(byteOffset) {
      return this._getBytes(1, byteOffset)[0];
    }

  };
}


THREE.TrackballControls = function (object, domElement) {
  const _this = this;
  const STATE = { NONE: -1, ROTATE: 0, ZOOM: 1, PAN: 2, TOUCH_ROTATE: 3, TOUCH_ZOOM_PAN: 4 };

  this.object = object;
  this.domElement = (domElement !== undefined) ? domElement : document;

	// API

  this.enabled = true;

  this.screen = { left: 0, top: 0, width: 0, height: 0 };

  this.rotateSpeed = 1.0;
  this.zoomSpeed = 1.2;
  this.panSpeed = 0.3;

  this.noRotate = false;
  this.noZoom = false;
  this.noPan = false;

  this.staticMoving = false;
  this.dynamicDampingFactor = 0.2;

  this.minDistance = 0;
  this.maxDistance = Infinity;

  this.keys = [65 /* A*/, 83 /* S*/, 68];

	// internals

	// ####
  this.target = new THREE.Vector3();
    // this.target = new THREE.Vector3().addVectors(object.position, object.getWorldDirection());

  const EPS = 0.000001;

  const lastPosition = new THREE.Vector3();

  let _state = STATE.NONE,
    _prevState = STATE.NONE,

    _eye = new THREE.Vector3(),

    _movePrev = new THREE.Vector2(),
    _moveCurr = new THREE.Vector2(),

    _lastAxis = new THREE.Vector3(),
    _lastAngle = 0,

    _zoomStart = new THREE.Vector2(),
    _zoomEnd = new THREE.Vector2(),

    _touchZoomDistanceStart = 0,
    _touchZoomDistanceEnd = 0,

    _panStart = new THREE.Vector2(),
    _panEnd = new THREE.Vector2();

	// for reset
  this.target0 = this.target.clone();
  this.position0 = this.object.position.clone();
  this.up0 = this.object.up.clone();

	// events

  const changeEvent = { type: 'change' };
  const startEvent = { type: 'start' };
  const endEvent = { type: 'end' };


	// methods

  this.handleResize = function () {
    if (this.domElement === document) {
      this.screen.left = 0;
      this.screen.top = 0;
      this.screen.width = window.innerWidth;
      this.screen.height = window.innerHeight;
    } else {
      const box = this.domElement.getBoundingClientRect();
			// adjustments come from similar code in the jquery offset() function
      const d = this.domElement.ownerDocument.documentElement;
      this.screen.left = box.left + window.pageXOffset - d.clientLeft;
      this.screen.top = box.top + window.pageYOffset - d.clientTop;
      this.screen.width = box.width;
      this.screen.height = box.height;
    }
  };

  this.handleEvent = function (event) {
    if (typeof this[event.type] == 'function') {
      this[event.type](event);
    }
  };

  const getMouseOnScreen = (function () {
    const vector = new THREE.Vector2();

    return function getMouseOnScreen(pageX, pageY) {
      vector.set(
				(pageX - _this.screen.left) / _this.screen.width,
				(pageY - _this.screen.top) / _this.screen.height
			);

      return vector;
    };
  }());

  const getMouseOnCircle = (function () {
    const vector = new THREE.Vector2();

    return function getMouseOnCircle(pageX, pageY) {
      vector.set(
				((pageX - _this.screen.width * 0.5 - _this.screen.left) / (_this.screen.width * 0.5)),
				((_this.screen.height + 2 * (_this.screen.top - pageY)) / _this.screen.width) // screen.width intentional
			);

      return vector;
    };
  }());

  this.rotateCamera = (function () {
    let axis = new THREE.Vector3(),
      quaternion = new THREE.Quaternion(),
      eyeDirection = new THREE.Vector3(),
      objectUpDirection = new THREE.Vector3(),
      objectSidewaysDirection = new THREE.Vector3(),
      moveDirection = new THREE.Vector3(),
      angle;

    return function rotateCamera() {
      moveDirection.set(_moveCurr.x - _movePrev.x, _moveCurr.y - _movePrev.y, 0);
      angle = moveDirection.length();

      if (angle) {
        _eye.copy(_this.object.position).sub(_this.target);

        eyeDirection.copy(_eye).normalize();
        objectUpDirection.copy(_this.object.up).normalize();
        objectSidewaysDirection.crossVectors(objectUpDirection, eyeDirection).normalize();

        objectUpDirection.setLength(_moveCurr.y - _movePrev.y);
        objectSidewaysDirection.setLength(_moveCurr.x - _movePrev.x);

        moveDirection.copy(objectUpDirection.add(objectSidewaysDirection));

        axis.crossVectors(moveDirection, _eye).normalize();

        angle *= _this.rotateSpeed;
        quaternion.setFromAxisAngle(axis, angle);

        _eye.applyQuaternion(quaternion);
        _this.object.up.applyQuaternion(quaternion);

        _lastAxis.copy(axis);
        _lastAngle = angle;
      } else if (!_this.staticMoving && _lastAngle) {
        _lastAngle *= Math.sqrt(1.0 - _this.dynamicDampingFactor);
        _eye.copy(_this.object.position).sub(_this.target);
        quaternion.setFromAxisAngle(_lastAxis, _lastAngle);
        _eye.applyQuaternion(quaternion);
        _this.object.up.applyQuaternion(quaternion);
      }

      _movePrev.copy(_moveCurr);
    };
  }());


  this.zoomCamera = function () {
    let factor;

    if (_state === STATE.TOUCH_ZOOM_PAN) {
      factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
      _touchZoomDistanceStart = _touchZoomDistanceEnd;
      _eye.multiplyScalar(factor);
    } else {
      factor = 1.0 + (_zoomEnd.y - _zoomStart.y) * _this.zoomSpeed;

      if (factor !== 1.0 && factor > 0.0) {
        _eye.multiplyScalar(factor);

        if (_this.staticMoving) {
          _zoomStart.copy(_zoomEnd);
        } else {
          _zoomStart.y += (_zoomEnd.y - _zoomStart.y) * this.dynamicDampingFactor;
        }
      }
    }
  };

  this.panCamera = (function () {
    let mouseChange = new THREE.Vector2(),
      objectUp = new THREE.Vector3(),
      pan = new THREE.Vector3();

    return function panCamera() {
      mouseChange.copy(_panEnd).sub(_panStart);

      if (mouseChange.lengthSq()) {
        mouseChange.multiplyScalar(_eye.length() * _this.panSpeed);

        pan.copy(_eye).cross(_this.object.up).setLength(mouseChange.x);
        pan.add(objectUp.copy(_this.object.up).setLength(mouseChange.y));

        _this.object.position.add(pan);
        _this.target.add(pan);

        if (_this.staticMoving) {
          _panStart.copy(_panEnd);
        } else {
          _panStart.add(mouseChange.subVectors(_panEnd, _panStart).multiplyScalar(_this.dynamicDampingFactor));
        }
      }
    };
  }());

  this.checkDistances = function () {
    if (!_this.noZoom || !_this.noPan) {
      if (_eye.lengthSq() > _this.maxDistance * _this.maxDistance) {
        _this.object.position.addVectors(_this.target, _eye.setLength(_this.maxDistance));
        _zoomStart.copy(_zoomEnd);
      }

      if (_eye.lengthSq() < _this.minDistance * _this.minDistance) {
        _this.object.position.addVectors(_this.target, _eye.setLength(_this.minDistance));
        _zoomStart.copy(_zoomEnd);
      }
    }
  };

  this.update = function () {
    _eye.subVectors(_this.object.position, _this.target);

    if (!_this.noRotate) {
      _this.rotateCamera();
    }

    if (!_this.noZoom) {
      _this.zoomCamera();
    }

    if (!_this.noPan) {
      _this.panCamera();
    }

    _this.object.position.addVectors(_this.target, _eye);

    _this.checkDistances();

    _this.object.lookAt(_this.target);

    if (lastPosition.distanceToSquared(_this.object.position) > EPS) {
      _this.dispatchEvent(changeEvent);

      lastPosition.copy(_this.object.position);
    }
  };

  this.reset = function () {
    _state = STATE.NONE;
    _prevState = STATE.NONE;

    _this.target.copy(_this.target0);
    _this.object.position.copy(_this.position0);
    _this.object.up.copy(_this.up0);

    _eye.subVectors(_this.object.position, _this.target);

    _this.object.lookAt(_this.target);

    _this.dispatchEvent(changeEvent);

    lastPosition.copy(_this.object.position);
  };

	// listeners

  function keydown(event) {
    if (_this.enabled === false) return;

    window.removeEventListener('keydown', keydown);

    _prevState = _state;

    if (_state !== STATE.NONE) {
      return;
    } else if (event.keyCode === _this.keys[STATE.ROTATE] && !_this.noRotate) {
      _state = STATE.ROTATE;
    } else if (event.keyCode === _this.keys[STATE.ZOOM] && !_this.noZoom) {
      _state = STATE.ZOOM;
    } else if (event.keyCode === _this.keys[STATE.PAN] && !_this.noPan) {
      _state = STATE.PAN;
    }
  }

  function keyup(event) {
    if (_this.enabled === false) return;

    _state = _prevState;

    window.addEventListener('keydown', keydown, false);
  }

  function mousedown(event) {
    if (_this.enabled === false) return;

    event.preventDefault();
    event.stopPropagation();

    if (_state === STATE.NONE) {
      _state = event.button;
    }

    if (_state === STATE.ROTATE && !_this.noRotate) {
      _moveCurr.copy(getMouseOnCircle(event.pageX, event.pageY));
      _movePrev.copy(_moveCurr);
    } else if (_state === STATE.ZOOM && !_this.noZoom) {
      _zoomStart.copy(getMouseOnScreen(event.pageX, event.pageY));
      _zoomEnd.copy(_zoomStart);
    } else if (_state === STATE.PAN && !_this.noPan) {
      _panStart.copy(getMouseOnScreen(event.pageX, event.pageY));
      _panEnd.copy(_panStart);
    }

    document.addEventListener('mousemove', mousemove, false);
    document.addEventListener('mouseup', mouseup, false);

    _this.dispatchEvent(startEvent);
  }

  function mousemove(event) {
    if (_this.enabled === false) return;

    event.preventDefault();
    event.stopPropagation();

    if (_state === STATE.ROTATE && !_this.noRotate) {
      _movePrev.copy(_moveCurr);
      _moveCurr.copy(getMouseOnCircle(event.pageX, event.pageY));
    } else if (_state === STATE.ZOOM && !_this.noZoom) {
      _zoomEnd.copy(getMouseOnScreen(event.pageX, event.pageY));
    } else if (_state === STATE.PAN && !_this.noPan) {
      _panEnd.copy(getMouseOnScreen(event.pageX, event.pageY));
    }
  }

  function mouseup(event) {
    if (_this.enabled === false) return;

    event.preventDefault();
    event.stopPropagation();

    _state = STATE.NONE;

    document.removeEventListener('mousemove', mousemove);
    document.removeEventListener('mouseup', mouseup);
    _this.dispatchEvent(endEvent);
  }

  function mousewheel(event) {
    if (_this.enabled === false) return;

    event.preventDefault();
    event.stopPropagation();

    let delta = 0;

    if (event.wheelDelta) {
			// WebKit / Opera / Explorer 9

      delta = event.wheelDelta / 40;
    } else if (event.detail) {
			// Firefox

      delta = -event.detail / 3;
    }

    _zoomStart.y += delta * 0.01;
    _this.dispatchEvent(startEvent);
    _this.dispatchEvent(endEvent);
  }

  function touchstart(event) {
    if (_this.enabled === false) return;

    switch (event.touches.length) {

      case 1:
        _state = STATE.TOUCH_ROTATE;
        _moveCurr.copy(getMouseOnCircle(event.touches[0].pageX, event.touches[0].pageY));
        _movePrev.copy(_moveCurr);
        break;

      default: // 2 or more
        _state = STATE.TOUCH_ZOOM_PAN;
        var dx = event.touches[0].pageX - event.touches[1].pageX;
        var dy = event.touches[0].pageY - event.touches[1].pageY;
        _touchZoomDistanceEnd = _touchZoomDistanceStart = Math.sqrt(dx * dx + dy * dy);

        var x = (event.touches[0].pageX + event.touches[1].pageX) / 2;
        var y = (event.touches[0].pageY + event.touches[1].pageY) / 2;
        _panStart.copy(getMouseOnScreen(x, y));
        _panEnd.copy(_panStart);
        break;

    }

    _this.dispatchEvent(startEvent);
  }

  function touchmove(event) {
    if (_this.enabled === false) return;

    event.preventDefault();
    event.stopPropagation();

    switch (event.touches.length) {

      case 1:
        _movePrev.copy(_moveCurr);
        _moveCurr.copy(getMouseOnCircle(event.touches[0].pageX, event.touches[0].pageY));
        break;

      default: // 2 or more
        var dx = event.touches[0].pageX - event.touches[1].pageX;
        var dy = event.touches[0].pageY - event.touches[1].pageY;
        _touchZoomDistanceEnd = Math.sqrt(dx * dx + dy * dy);

        var x = (event.touches[0].pageX + event.touches[1].pageX) / 2;
        var y = (event.touches[0].pageY + event.touches[1].pageY) / 2;
        _panEnd.copy(getMouseOnScreen(x, y));
        break;

    }
  }

  function touchend(event) {
    if (_this.enabled === false) return;

    switch (event.touches.length) {

      case 0:
        _state = STATE.NONE;
        break;

      case 1:
        _state = STATE.TOUCH_ROTATE;
        _moveCurr.copy(getMouseOnCircle(event.touches[0].pageX, event.touches[0].pageY));
        _movePrev.copy(_moveCurr);
        break;

    }

    _this.dispatchEvent(endEvent);
  }

  function contextmenu(event) {
    event.preventDefault();
  }

  this.dispose = function () {
    this.domElement.removeEventListener('contextmenu', contextmenu, false);
    this.domElement.removeEventListener('mousedown', mousedown, false);
    this.domElement.removeEventListener('mousewheel', mousewheel, false);
    this.domElement.removeEventListener('MozMousePixelScroll', mousewheel, false); // firefox

    this.domElement.removeEventListener('touchstart', touchstart, false);
    this.domElement.removeEventListener('touchend', touchend, false);
    this.domElement.removeEventListener('touchmove', touchmove, false);

    document.removeEventListener('mousemove', mousemove, false);
    document.removeEventListener('mouseup', mouseup, false);

    window.removeEventListener('keydown', keydown, false);
    window.removeEventListener('keyup', keyup, false);
  };

  this.domElement.addEventListener('contextmenu', contextmenu, false);
  this.domElement.addEventListener('mousedown', mousedown, false);
  this.domElement.addEventListener('mousewheel', mousewheel, false);
  this.domElement.addEventListener('MozMousePixelScroll', mousewheel, false); // firefox

  this.domElement.addEventListener('touchstart', touchstart, false);
  this.domElement.addEventListener('touchend', touchend, false);
  this.domElement.addEventListener('touchmove', touchmove, false);

  window.addEventListener('keydown', keydown, false);
  window.addEventListener('keyup', keyup, false);

  this.handleResize();

	// force an update at start
  this.update();
};

THREE.TrackballControls.prototype = Object.create(THREE.EventDispatcher.prototype);
THREE.TrackballControls.prototype.constructor = THREE.TrackballControls;


module.exports = THREE;
