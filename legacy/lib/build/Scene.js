const THREE = require('./Boots.js');
const renderer = new THREE.WebGLRenderer({ antialias: true, preserveDrawingBuffer: true });

export const makeScene = (scene) => {
  const WIDTH = window.innerWidth * 0.68;
  const HEIGHT = window.innerHeight;
  renderer.setSize(WIDTH + 20, HEIGHT);
  // Background color
  renderer.setClearColor(0xffffff, 1);
  document.getElementById('the-canvas').appendChild(renderer.domElement);

  const camera = new THREE.PerspectiveCamera(75, WIDTH / HEIGHT, 0.1, 16000);

  camera.position.z = 300;
  camera.position.x = -100;
  camera.position.y = 300;
  const camerLookAt = new THREE.Vector3(0, 0, 0);
  camera.lookAt(camerLookAt);

  const light0 = new THREE.DirectionalLight(0xffffff);
  light0.intensity = 0.5;
  light0.position.set(20, 100, 15);
  light0.target.position.copy(scene.position);
  scene.add(light0);

  const light1 = new THREE.DirectionalLight(0xffffff);
  light1.intensity = 0.4;
  light1.position.set(20, -100, 15);
  light1.target.position.copy(scene.position);
  scene.add(light1);

  // Ambient Light
  const ambientLight = new THREE.AmbientLight(0x95a5a6); // soft white light
  scene.add(ambientLight);

  const controls = new THREE.OrbitControls(camera, renderer.domElement);
  controls.enableDamping = true;
  controls.dampingFactor = 0.25;
  controls.enableZoom = true;
  controls.minDistance = 50;
  controls.maxDistance = 450;

  const grid = new THREE.GridHelper(3000, 300, 0xf8f8f8, 0xf8f8f8);

  scene.add(grid);
  return { camera, controls, renderer };
};
