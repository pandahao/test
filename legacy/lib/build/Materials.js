const THREE = require('./Boots.js');
const { outlineV, outlineF } = require('./shader/index.js');


const WIRE = false;
const VISIBLE = true;

const DarkOutline = new THREE.ShaderMaterial({
  uniforms: {
    offset: {
      type: 'f',
      value: 0.01
    },
    outlineColor: {
      type: 'c',
      value: new THREE.Color(0, 0, 0)
    }
  },
  fragmentShader: outlineF,
  vertexShader: outlineV,
  visible: VISIBLE
});


const BlueMetal = new THREE.MeshPhongMaterial({
  name: 'BlueMetal',
  color: 0x4183D7,
  wireframe: WIRE,
  wireframeLinewidth: 1,
  vertexColors: 0x4183D7,
  shininess: 60,
  shading: THREE.SmoothShading,
  visible: VISIBLE
});

const RedMetal = new THREE.MeshPhongMaterial({
  name: 'RedMetal',
  color: 0xc0392b,
  wireframe: WIRE,
  wireframeLinewidth: 1,
  vertexColors: 0xe74c3c,
  shininess: 60,
  shading: THREE.SmoothShading,
  visible: VISIBLE
});

const SilverMetal = new THREE.MeshPhongMaterial({
  name: 'SilverMetal',
  color: 0xbdc3c7,
  wireframe: WIRE,
  wireframeLinewidth: 1,
  vertexColors: 0xecf0f1,
  shininess: 60,
  shading: THREE.SmoothShading,
  visible: VISIBLE
});

const BareMetal = new THREE.MeshPhongMaterial({
  name: 'BareMetal',
  color: 0x6C7A89,
  wireframe: WIRE,
  wireframeLinewidth: 1,
  vertexColors: 0x95A5A6,
  shininess: 60,
  shading: THREE.SmoothShading,
  visible: VISIBLE
});

const BlackPlastic = new THREE.MeshLambertMaterial({
  name: 'BlackPlastic',
  color: 0x22313F,
  wireframe: WIRE,
  wireframeLinewidth: 1,
  vertexColors: 0x2C3E50,
});

const YellowMetal = new THREE.MeshPhongMaterial({
  name: 'YellowMetal',
  color: 0xEB974E,
  wireframe: WIRE,
  wireframeLinewidth: 1,
  vertexColors: 0xEB9532,
  shininess: 60,
  shading: THREE.SmoothShading,
  visible: VISIBLE
});

const WhitePlastic = new THREE.MeshLambertMaterial({
  color: 0xD2D7D3,
  wireframe: WIRE,
  name: 'WhitePlastic',
  wireframeLinewidth: 1,
  vertexColors: 0xDADFE1,
  visible: VISIBLE
});


// Randomly choose on from opts
function randChoose(opts) {
  return opts[Math.floor(Math.random() * opts.length)];
}

const getMaterial = (name) => {
  const nameArr = name.split('_');
  const prefix = nameArr[0];
  const suffix = nameArr.pop();
  switch (prefix) {
    case 'Think':
      switch (name) {
        case 'Think_Controller_Robocore.STL':
          return WhitePlastic;
        case 'Think_BatteryCase.STL':
          return BlackPlastic;
        default :
      }
    case 'Act' :
      if (name == 'Act_Servo.STL') {
        return BlackPlastic;
      }
      resolveColor(suffix);
    case 'Build':
      return resolveColor(suffix);
    case 'Connect':
      if (name.indexOf('Standoff') >= 0) {
        return YellowMetal;
      }
      if (name.indexOf('Spacer') >= 0) {
        return WhitePlastic;
      }
      return BareMetal;
    case 'Drive':
      if (name.indexOf('BallTransfer') >= 0) {
        return SilverMetal;
      }
      if (name.indexOf('RoboTerraMotor') >= 0) {
        return SilverMetal;
      }
      // RoboTerra Electronics
      if (name.indexOf('RoboTerra') >= 0) {
        return WhitePlastic;
      }
      if (name.indexOf('Washer') >= 0) {
        return WhitePlastic;
      }
      if (name.indexOf('WheelTire') >= 0) {
        return BlackPlastic;
      }
      if (name.indexOf('Wire_3Pos') >= 0) {
        return BlackPlastic;
      }
      if (name.indexOf('Spacer') >= 0) {
        return WhitePlastic;
      }
      return BareMetal;
    case 'Sense':
      return WhitePlastic;
    default:
      return randChoose([BlueMetal, SilverMetal, RedMetal]);
  }
};
// Resolve part color according to part name suffix
function resolveColor(suffix) {
  switch (suffix) {
    case 'Silver.STL':
      return SilverMetal;
    case 'Blue.STL':
      return BlueMetal;
    case 'Red.STL' :
      return RedMetal;
    default :
      return WhitePlastic;
            // return randChoose([BlueMetal,SilverMetal,RedMetal]);
  }
}


module.exports = getMaterial;


//
