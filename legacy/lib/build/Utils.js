const THREE = require('./Boots.js');
const normalizer = require('./loader/Normalizer.js');
const getMaterial = require('./Materials');

// animation duration in number of frames
export const ANIMATION_DURATION = 60;

// Parse entire model and add it to the scene
export const parseModel = (loadedObj, geoDict) => {
  // Scene objs
  function parseScene(node, offset) {
    const newOffset = new THREE.Matrix4();
    newOffset.fromArray(node.matrix);
    newOffset.multiplyMatrices(offset, newOffset);

    switch (node.type) {
      case 'Scene':
        const model = Object.create(null);
        node.children.forEach((child) => {
          const parsed = parseScene(child, newOffset);
          if (parsed) {
            model[`C${parsed.level}`] = parsed;
          }
        });
        return model;
      case 'Group':
        switch (node.name[0]) {
          case 'C':
            let cIndex, context;
            [cIndex, context] = node.name.split('_');
            const challenge = Object.create(null);

            cIndex = parseInt(cIndex.slice(1));
            challenge.context = context;
            challenge.level = cIndex;
            challenge.steps = [];

            node.children.forEach((child) => {
              const parsedStep = parseScene(child, newOffset);
              if (parsedStep) {
                challenge.steps.push(parsedStep);
              }
            });
            return challenge;
          case 'S' :
            const sIndex = parseInt(node.name.slice(1));
            const step = Object.create(null);
            step.index = sIndex;
            step.parts = [];
            node.children.forEach((child) => {
              const parsedMesh = parseScene(child, newOffset);
              if (parsedMesh) {
                step.parts.push(parsedMesh);
              }
            });
            return step;
          default:
            console.warn('Unknown Group', node.name);
            return undefined;
        }
      // Loading all mesh to
      case 'Mesh':
        const normalName = normalizer(node.name);
        const nodeClone = new THREE.Mesh(geoDict[normalName], getMaterial(node.name));
        nodeClone.applyMatrix(newOffset);
        nodeClone.name = normalName;
        return nodeClone;
      default:
        console.warn('Unknown node type', node.type);
        return undefined;
    }
  }
  // Parse scene obj data
  const project = parseScene(loadedObj, new THREE.Matrix4());
  return project;
};

// Animate an obj
const animateFactory = (obj, rev) => {
  let frameCount = 0;
  if (!rev) {
    return () => {
      if (frameCount === ANIMATION_DURATION) {
        obj.mesh.position.set(...obj.destp);
        obj.mesh.rotation.set(...obj.destr);
        return true;
      } else {
        obj.mesh.position.x += obj.dx;
        obj.mesh.position.y += obj.dy;
        obj.mesh.position.z += obj.dz;
        obj.mesh.rotation.x += obj.drx;
        obj.mesh.rotation.y += obj.dry;
        obj.mesh.rotation.z += obj.drz;
        frameCount++;
        return false;
      }
    };
  } else {
    return () => {
      if (frameCount === ANIMATION_DURATION) {
        obj.mesh.position.set(...obj.origin);
        return true;
      } else {
        obj.mesh.position.x -= obj.dx;
        obj.mesh.position.y -= obj.dy;
        obj.mesh.position.z -= obj.dz;
        obj.mesh.rotation.x -= obj.drx;
        obj.mesh.rotation.y -= obj.dry;
        obj.mesh.rotation.z -= obj.drz;
        frameCount++;
        return false;
      }
    };
  }
};

// Animation class
export class Animation {
  constructor(target, locationGenerator) {
    const destr = target.rotation.clone();
    const destp = target.position.clone();
    const loc = locationGenerator.generate(target.name);

    target.position.set(loc.x, loc.y, loc.z);

    target.rotation.set(0, 0, 0);
    this.aniObj = {
      mesh: target,
      origin: [target.position.x, target.position.y, target.position.z],
      destr: [destr.x, destr.y, destr.z],
      destp: [destp.x, destp.y, destp.z],

      dx: (destp.x - target.position.x) / ANIMATION_DURATION,
      dy: (destp.y - target.position.y) / ANIMATION_DURATION,
      dz: (destp.z - target.position.z) / ANIMATION_DURATION,
      drx: destr.x / ANIMATION_DURATION,
      dry: destr.y / ANIMATION_DURATION,
      drz: destr.z / ANIMATION_DURATION
    };
  }

  play() {
    return animateFactory(this.aniObj, false);
  }
  back() {
    return animateFactory(this.aniObj, true);
  }
}
