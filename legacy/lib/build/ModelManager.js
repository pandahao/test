const THREE = require('./Boots.js');

import { parseModel } from './Utils.js';

import ModelLoader from './loader/ModelLoader';
const SkylineModelGeometry = require('./loader/PartMap');

import { SkylineService } from '../cloud';

window.SkylineModels = Object.create(null);

export function getModel(projectId) {
  const origin = window.SkylineModels[projectId];
  return origin ? parseModel(origin, SkylineModelGeometry) : null;
}

// Load model from remote json
export function loadModel(projectId) {
  return new Promise((resolve, reject) => {
    SkylineService.project.findRobot(projectId, (err, robot) => {
      if (err) {
        reject(err);
      } else if (!robot) {
        resolve('NO MODEL FOUND');
      } else if (!robot.object) {
        resolve('NO MODEL FOUND');
      } else {
        const modelData = robot.object;
        if (modelData) {
          const loader = new ModelLoader(modelData, SkylineModelGeometry);
          if (!getModel(projectId)) {
            loader.loadModel().then(() => {
              _saveModel(projectId, modelData);
              resolve();
            }, (err) => {
              reject('ERROR LOADING MODEL', err);
            });
          } else {
            resolve();
          }
        } else {
          reject('ERROR MODEL EMPTY');
        }
      }
    });
  });
}


function _saveModel(projectId, model) {
  window.SkylineModels[projectId] = model;
}
