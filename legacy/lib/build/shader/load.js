const fs = require('fs');


const dirs = fs.readdirSync('./');

console.log(dirs);

const res = {};
dirs.forEach((d) => {
  let [name, suf] = d.split('.');

  if (suf !== 'js') {
    res[name] = fs.readFileSync(d).toString();
  }
});

fs.writeFileSync('index.js', `const Shaders = ${JSON.stringify(res, null, 2)}\n module.exports = Shaders`);
