uniform float offset;


void main(){

	vec4 mvPosition;
    vec3 displaced = position + (normal * offset);

	mvPosition = modelViewMatrix * vec4(displaced, 1.0);

	gl_Position = projectionMatrix * mvPosition;
}
