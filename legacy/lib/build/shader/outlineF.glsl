uniform vec3 outlineColor;
void main(){
    vec4 color = vec4(outlineColor,0.1);
    gl_FragColor = color;
}
