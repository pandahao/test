import { Dictionary, MessageReader } from './Lookup';
function combineAndRead(b, a) {
  if (a === 255) {
    return b + a * 256 - 65536;
  }
  return b + a * 256;
}
const RobotParser = (device_arr) => {
    // console.log(device_arr);
  if (!device_arr) {
    return undefined;
  }
  const part_arr = [];
  device_arr.forEach((d_obj) => {
    const device = d_obj[0];
    const part = Object.create(null);
    part.port = Dictionary.PortsID[device.PORT];
    part.name = Dictionary.DevicesID[device.ID];
    part.state = device.MSG[0];
    part.event_name = Dictionary.EventsID[device.MSG[1]] || 'UNKNOWN_EVENT';


    const labels = MessageReader[part.name](device.MSG[1]);
        // console.log(labels);

    part.msg = [
      {
        label: labels[0],
        value: combineAndRead(device.MSG[2], device.MSG[3])
      }
    ];
    console.log(device.MSG);
    if (device.MSG.length === 6) {
      part.size = 3;
      part.msg.push({
        label: labels[1],
        value: combineAndRead(device.MSG[4], device.MSG[5])
      });
    } else {
      part.size = 2;
    }

    part_arr.push(part);
  });
  return part_arr;
};


export default RobotParser;


//
