import { SerialEnv, CompilerEndPoint, SkylineCore } from 'config';

const myHeader = new Headers({
  'Content-Type': 'application/json'
});

export default class RoboCore {
  constructor() {

  }
  upload(sketch) {
    const payload = {
      fileName: 'sketch',
      fileContent: sketch
    };
    return new Promise((resolve, reject) => {
      window.fetch(CompilerEndPoint, {
        method: 'POST',
        headers: myHeader,
        body: JSON.stringify(payload)
      }).catch((err) => {
        console.log('COMPILATION_ERROR');
        reject(err);
      }).then((res) => {
        console.log('GOT RESPONSE');
                // Handle Upload here
        return res.json();
      }).then((data) => {
        console.log('RESPONSE PARSED', data);
        const hex = window.atob(data.firmware);
        return SkylineCore.upload(hex);
      }).then(() => {
        console.log('SUCCESS UPLOAD');
        resolve();
      }, (e) => {
        console.log('UPLOAD_ERROR');
        reject(e);
      });
    });
  }
}
