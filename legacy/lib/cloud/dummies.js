export const dummy = {
  firstName: 'Mika',
  lastName: 'Reyes',
  email: 'mika.reyes@roboterra.com',
  password: 'ITalkInMySleep',
  language: 'English',
  birthday: '12/06/1994',
  onBoardProgress: 0,
  projectsOwned: [
    {
      status: 'ONGOING',
      projectId: 'S0P001',
      progress: 1
    },
    {
      status: 'ONGOING',
      projectId: 'S0P002',
      progress: 1
    },
    {
      status: 'ONGOING',
      projectId: 'S1P001',
      progress: 1
    },
    {
      status: 'ONGOING',
      projectId: 'S5P001',
      progress: 1
    },
    {
      status: 'ONGOING',
      projectId: 'S4P001',
      progress: 1
    },
    {
      status: 'ONGOING',
      projectId: 'S8P001',
      progress: 1,
    },
    {
      status: 'ONGOING',
      projectId: 'S9P001',
      progress: 1,
    }
  ]
};

export const Mika = {
  firstName: 'Mika',
  lastName: 'Reyes',
  email: 'mika.reyes@roboterra.com',
  password: 'ITalkInMySleep',
  language: 'English',
  birthday: '12/06/1994',
  onBoardProgress: 0,
  projectsOwned: [
    {
      status: 'ONGOING',
      projectId: 0,
      progress: 4
    },
    {
      status: 'ONGOING',
      projectId: 1,
      progress: 20
    },
    {
      status: 'ONGOING',
      projectId: 6,
      progress: 1,
    },
    {
      status: 'ONGOING',
      projectId: 7,
      progress: 1,
    },
    {
      status: 'ONGOING',
      projectId: 8,
      progress: 1,
    }
  ]
};
//
// export const Mika = {
//     firstName: 'Mika',
//     lastName: 'Reyes',
//     email : 'mika.reyes@roboterra.com',
//     password: 'ITalkInMySleep',
//     language: 'English',
//     birthday: '12/06/1994',
//     onBoardProgress: 0,
//     projectsOwned:[
//         {
//             "status" : "ONGOING",
//             "projectId" : 0,
//             "progress" : 4
//         },
//         {
//             "status" : "ONGOING",
//             "projectId" : 1,
//             "progress" : 20
//         },
//         {
//             "status" : "ONGOING",
//             "projectId" : 2,
//             "progress" : 1
//         },
//     ]
// };
//
// export const Daichi = {
//     firstName: 'Daichi',
//     lastName: 'Onda',
//     email : 'daichi.onda@roboterra.com',
//     password: 'ISleepOnTheFloor',
//     language: 'English',
//     birthday: '10/01/1995',
//     onBoardProgress: 0,
//     projectsOwned:[
//         {
//             "status" : "ONGOING",
//             "projectId" : 0,
//             "progress" : 4
//         },
//         {
//             "status" : "ONGOING",
//             "projectId" : 1,
//             "progress" : 20
//         },
//         {
//             "status" : "ONGOING",
//             "projectId" : 2,
//             "progress" : 1
//         },
//     ]
// };
// export const Jason = {
//     firstName: 'Jason',
//     lastName: 'Chen',
//     email : 'jason.chen@roboterra.com',
//     password: 'ISleepOnTheFloor',
//     language: 'English',
//     birthday: '10/01/1995',
//     onBoardProgress: 0,
//     projectsOwned:[
//         {
//             "status" : "ONGOING",
//             "projectId" : 0,
//             "progress" : 4
//         },
//         {
//             "status" : "ONGOING",
//             "projectId" : 1,
//             "progress" : 20
//         },
//         {
//             "status" : "ONGOING",
//             "projectId" : 2,
//             "progress" : 1
//         },
//     ]
// };
//
// export const dummies = [
//     Mika,
//     Daichi
// ];
