/* eslint global-require: "off"*/
import Promise from 'bluebird';
import { SkylineServiceConfig } from 'config';
export const SkylineService = require('skyline_service').createService(SkylineServiceConfig);

Promise.promisifyAll(Object.getPrototypeOf(SkylineService.user));
Promise.promisifyAll(Object.getPrototypeOf(SkylineService.project));
Promise.promisifyAll(Object.getPrototypeOf(SkylineService.projectChallenge));


export default class Cloud {
  constructor() {
    const $ = this;
    $.user = undefined;
    $.progress = undefined;
    $.projects = undefined;
  }
  logout() {
    const $ = this;
    return new Promise((resolve, reject) => {
      $.user.signOut(() => {});
      resolve();
    });
  }
  login(username, password) {
    const $ = this;
    return new Promise((resolve, reject) => {
      SkylineService.user.signInAsync(username, password)
      .catch((error) => {
        console.log(error);
        throw (error);
      })
      .then((cognitoUser) => {
        Promise.promisifyAll(Object.getPrototypeOf(cognitoUser));
        $.user = cognitoUser;
        resolve();
      }, (error) => {
        reject(error);
      });
    });
  }
  loadProject(projectId, language) {
    const $ = this;
    return new Promise((resolve) => {
      const pro = $.projects.find((p) => {
        return p.id === projectId;
      });
      if (pro) {
        resolve(pro);
      } else {
        resolve();
      }
    });
  }

  loadProjectChallenge(pid) {
    const $ = this;
    return new Promise((resolve, reject) => {
      const project = $.projects.find((p) => {
        return p.id === pid;
      });
      if (project && project.challengeData) {
        resolve(project.challengeData);
      } else {
        SkylineService.projectChallenge.findById(pid, (err, challengeData) => {
          if (err) {
            reject(err);
          } else {
            resolve(challengeData);
          }
        });
      }
    });
  }

  isAuthenticated() {
    const $ = this;
    return new Promise((resolve, reject) => {
      SkylineService.user.getCurrentAsync()
      .then((cognitoUser) => {
        Promise.promisifyAll(Object.getPrototypeOf(cognitoUser));
        $.user = cognitoUser;
        resolve();
      }, (err) => {
        reject(err);
      });
    });
  }
  createAccount(pendingUser) {
    return new Promise((resolve, reject) => {
      SkylineService.user.signUp(
        pendingUser.email,
        pendingUser.password,
        pendingUser.email,
        // User profile
        {},
        (err, newUser) => {
          if (err) {
            reject(err);
          } else {
            resolve(newUser);
          }
        }
      );
    });
  }
  confirmNewUser(username, code) {
    return new Promise((resolve, reject) => {
      SkylineService.user.confirm(username, code, (err, user) => {
        if (err) {
          reject(err);
        } else {
          resolve(user);
        }
      });
    });
  }
  resendUserConfirmation(username) {
    return new Promise((resolve, reject) => {
      SkylineService.user.sendConfirmationCode(username, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
  sendResetPasswordRequest(username) {
    return SkylineService.user.forgetPasswordAsync(username);
  }
  resetPassword(username, confirmCode, newPass) {
    return new Promise((resolve, reject) => {
      SkylineService.user.confirmPassword(username, confirmCode, newPass, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  changePassword(oldPassword, newPassword) {
    return this.user.changePasswordAsync(oldPassword, newPassword);
  }

  getUserProfile() {
    return this._fetchUserData();
  }

  updateUserProfile(userProfile) {
    return this.user.updateUserProfileAsync(userProfile);
  }

  saveUserProgress(projectID, challengeType, level, codeContent, challengeID) {
    return this.user.createUserProgressAsync(projectID, challengeType, level, codeContent, 'cpp', challengeID);
  }

  getUserCodes() {
    return this.user.getUserCodesAsync();
  }

  updateUserCode(codeId, codeName, codeContent, codeLanguage) {
    return this.user.updateUserCodeAsync(codeId, codeName, codeContent, codeLanguage);
  }

  createUserCode(codeName, content, language = 'cpp', ctype = 'freeedit', cid_challenge) {
    return this.user.createUserCodeAsync(codeName, content, language, ctype, cid_challenge);
  }

  deleteUserCode(codeId) {
    return this.user.deleteUserCodeAsync(codeId);
  }

  _fetchUserData() {
    const $ = this;
    return new Promise((resolve, reject) => {
      $.user.getUserProgressAsync()
      .then((userProgress) => {
        $.progress = userProgress;
        return SkylineService.project.listAllAsync([
          'projectID',
          'projectName',
          'stageID',
          'overview',
          'numChallenges',
          'language',
          'imageLink'
        ]);
      }).catch((error) => {
        alert('Error Fetching all projects');
        throw (error);
      })
      .then((projectData) => {
        $.projects = projectData;
        return $._createUserProfile();
      })
      .then((profile) => {
        resolve(profile);
      })
      .catch((error) => {
        localStorage.clear();
        location.href = location.pathname;
        reject(error);
      });
    });
  }
  _createUserProfile() {
    const $ = this;
    return new Promise((resolve, reject) => {
      const profile = Object.create(null);
      $.user.getUserProfileAsync().then((userProfile) => {
        Object.assign(profile, userProfile, {
          onBoardProgress: 5
        });
        return $.user.getUserProgressAsync();
      }).then((data) => {
        // Changed by JiangShang
        profile.projectsOwned = $.projects.map((p) => {
          const projectId = p.id;
          const records = $.progress.filter((d) => {
            return d.pid_project === p.id;
          });
          if (records.length === 0) {
            return {
              projectId,
              status: 'UNLOCKED',
              progress: 1,
              records: []
            };
          }
          const status = records[0].order_challenge >= p.cnt_challenge ? 'COMPLETED' : 'ONGOING';
          let progress = records[0].order_challenge || 1;
          progress += 1;
          return {
            projectId,
            status,
            progress,
            records
          };
        });
        resolve(profile);
      }, (err) => {
        console.log(err);
        reject(err);
      });
    });
  }
}
