/*
States:
0 => rdy to read

1 => expecting device count
2 => expecting device id
3 => expecting device port
4 => expecting device state(depreacted)
5 => expecting device msg length
6 => expecting device msg
7 => expecting device end

10 => expecting serial msg length
11 => expecting serial msg
12 => expecting serial end
*/
const ProtocolReader = {};
ProtocolReader.state = 'STATE_0';

ProtocolReader.device_count = 0;
ProtocolReader.device_msg_count = 0;
ProtocolReader.devices = Object.create(null);

// Save parsed device into an arr;
ProtocolReader.device_arr = [];
ProtocolReader.serial_arr = [];

ProtocolReader.serial_msg_count = 0;
ProtocolReader.serial = Object.create(null);


ProtocolReader.parsed_devices = undefined;
ProtocolReader.parsed_serial = undefined;

ProtocolReader.incoming_device = false;
ProtocolReader.incoming_serial = false;

ProtocolReader.stateMachine = {
  STATE_0(n) {
                // console.log("EXPECTING START!")
    if (n === 240) {
      ProtocolReader.state = 'STATE_1';
    } else if (n === 241) {
                    // console.log("INCOMING SERIAL MESSAGE");
      ProtocolReader.state = 'STATE_10';
    } else {
      console.log(`ERROR: Unexpected Starting Byte: ${n}`);
    }
    return false;
  },
  STATE_1(n) {
                // console.log("TOTAL_DEVICES: "+n);
    ProtocolReader.devices.TOTAL_DEVICES = n;
    if (n === 0) {
      ProtocolReader.state = 'STATE_7';
    } else {
      ProtocolReader.state = 'STATE_2';
    }
    return false;
  },
  STATE_2(n) {
                // console.log("DEVICE_ID: "+n);
    ProtocolReader.devices[ProtocolReader.device_count] = Object.create(null);
    ProtocolReader.devices[ProtocolReader.device_count].ID = n;
    ProtocolReader.state = 'STATE_3';
    return false;
  },
  STATE_3(n) {
                // console.log("DEVICE_PORT: "+n);
    ProtocolReader.devices[ProtocolReader.device_count].PORT = n;

                // Omit Deprecated STATE_4;
    ProtocolReader.state = 'STATE_5';
    return false;
  },
    // Deprecated State 4
  STATE_4(n) {
    const info = n.toString(2);
                // console.log("DEVICE_INFO: "+info);
    ProtocolReader.devices[ProtocolReader.device_count].PRIMARY_STATE = (info[0] === '1');
    ProtocolReader.devices[ProtocolReader.device_count].SECONDARY_STATE = (info[1] === '1');
    ProtocolReader.state = 'STATE_5';
    return false;
  },
  STATE_5(n) {
                // console.log("DEVICE_MSG_LENGTH: "+n);
    ProtocolReader.devices[ProtocolReader.device_count].MSG_LENGTH = n;

                /**
                var msg = new ArrayBuffer(n);
                ProtocolReader.devices[ProtocolReader.device_count].MSG = new Uint8Array(msg);
                **/

    ProtocolReader.devices[ProtocolReader.device_count].MSG = [];

    ProtocolReader.state = 'STATE_6';
    return false;
  },
  STATE_6(n) {
                // console.log("Reading Message: "+ProtocolReader.device_msg_count);
                // console.log(n);
    const device = ProtocolReader.devices[ProtocolReader.device_count];
    ProtocolReader.device_msg_count += 1;
    if (ProtocolReader.device_msg_count < device.MSG_LENGTH) {
      device.MSG.push(n);
                    // ProtocolReader.device_msg_count += 1;
    } else if (ProtocolReader.device_msg_count === device.MSG_LENGTH) {
                    // console.log("DEVICE_MSG: "+ device.MSG);
      device.MSG.push(n);
      ProtocolReader.device_msg_count = 0;
      ProtocolReader.device_count += 1;
                    // console.log("NOW WHAT? "+ ProtocolReader.device_count +" vs " +ProtocolReader.devices.TOTAL_DEVICES);
      if (ProtocolReader.device_count === ProtocolReader.devices.TOTAL_DEVICES) {
        ProtocolReader.state = 'STATE_7';
                        // ProtocolReader.stateMachine.STATE_7(n);
      } else {
        ProtocolReader.state = 'STATE_2';
                        // ProtocolReader.stateMachine.STATE_2(n);
      }
    }
    return false;
  },
  STATE_7(n) {
                // console.log("EXPECTING END!");
    if (n !== 255) {
      console.log(`ERROR: Unexpected Ending Byte: ${n}`);
    }
    ProtocolReader.device_count = 0;
    ProtocolReader.device_msg_count = 0;
    ProtocolReader.parsed_devices = ProtocolReader.devices;

    ProtocolReader.device_arr.push(ProtocolReader.parsed_devices);

    ProtocolReader.incoming_device = true;
    ProtocolReader.devices = Object.create(null);
    ProtocolReader.state = 'STATE_0';
    return true;
  },
  STATE_10(n) {
    // console.log("SERIAL MSG LENGTH: "+n);
    ProtocolReader.serial.MSG_LENGTH = n;
    ProtocolReader.serial.MSG = '';
    // ProtocolReader.serial.MSG_ARR = [];
    ProtocolReader.state = 'STATE_11';
    return false;
  },
  STATE_11(n) {
    ProtocolReader.serial_msg_count += 1;
    ProtocolReader.serial.MSG += String.fromCharCode(n);
                // ProtocolReader.serial.MSG_ARR.push(n);
    if (ProtocolReader.serial_msg_count >= ProtocolReader.serial.MSG_LENGTH) {
                    // console.log("SERIAL MSG: "+ProtocolReader.serial.MSG);
      ProtocolReader.state = 'STATE_12';
    }
    return false;
  },
  STATE_12(n) {
    // console.log("EXPECING END!");
    if (n !== 255) {
      console.log(`ERROR: Unexpected Ending Byte: ${n}`);
    }
    ProtocolReader.serial_msg_count = 0;
    ProtocolReader.parsed_serial = ProtocolReader.serial;

    ProtocolReader.serial_arr.push(ProtocolReader.parsed_serial);
    ProtocolReader.incoming_serial = true;
    ProtocolReader.serial = Object.create(null);
    ProtocolReader.state = 'STATE_0';
    return true;
  }
};
ProtocolReader.parse = function (data) {
    // console.log("Reading!");
  let done = false;
    // debugger;
  for (let n = 0; n < data.length; n++) {
        // console.log(data[n]);
    if (ProtocolReader.stateMachine[ProtocolReader.state](data[n])) {
      done = true;
    }
  }
  return done;
};

ProtocolReader.read = function () {
  const res = {};
  if (ProtocolReader.incoming_device) {
    ProtocolReader.incoming_device = false;
    const devices = ProtocolReader.device_arr;
    ProtocolReader.device_arr = [];
    res.events = devices;
  }
  if (ProtocolReader.incoming_serial) {
    ProtocolReader.incoming_serial = false;
    const serials = ProtocolReader.serial_arr;
    ProtocolReader.serial_arr = [];
    res.states = serials;
  }
  return res;
};

ProtocolReader.reset = function () {
  console.warn('Reset ProtocalReader State!');
  ProtocolReader.state = 'STATE_0';
  ProtocolReader.device_arr = [];
  ProtocolReader.serial_arr = [];
  ProtocolReader.parsed_devices = undefined;
  ProtocolReader.parsed_serial = undefined;
};


export default ProtocolReader;


//
