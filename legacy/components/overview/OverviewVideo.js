import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import FlatButton from 'material-ui/FlatButton';
import TranslatedText from 'legacy_containers/TranslatedText';
import CircularProgress from 'material-ui/CircularProgress';
import {ChromeEnv} from 'config';
const style = {
  videosection: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%'
  },

  video: {
    width: '960px',
    height: '480px',
    backgroundColor: 'black',
    textAlign: 'center',
    margin: '20px',
  },

  continue: {
    backgroundColor: GeneralPalette.brightPurple,
    //boxShadow: `0px 2px 0px 0px${GeneralPalette.boxShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    width: '150px',
    lineHeight: '35px',
    paddingTop: '2px'
  },

  unclickable: {
    backgroundColor: GeneralPalette.darkPurple,
    boxShadow: `0px 2px 0px 0px${GeneralPalette.boxShadow}`,
    borderRadius: '20px',
    color: GeneralPalette.underline,
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    width: '150px',
    lineHeight: '35px',
    paddingTop: '2px',
    cursor: 'default'
  },

};


function buttonText(status) {
  if (status.toUpperCase() === 'ONGOING') {
    return 'CONTINUE';
  }
  else if (status.toUpperCase() === 'UNLOCKED') {
    return 'START';
  }
  else if (status.toUpperCase() === 'COMPLETED') {
    return 'VIEW';
  }
  else if (status.toUpperCase() === 'LOCKED') {
    return null;
  }
}


function button(status, gridView, goToProject, projectID, challengeData, challengeProgress) {
  if (status === 'ONGOING' || status === 'UNLOCKED' || status === 'COMPLETED') {
    return (
            <FlatButton style={style.continue} onClick={() => goToProject(projectID, status, challengeData, challengeProgress)}>
                <TranslatedText text={buttonText(status)} />
            </FlatButton>
        );
  }
  else if (status === 'LOCKED') {
    return (
            <div style={style.unclickable}> <TranslatedText text={'LOCKED'} /> </div>
        );
  }
}


const OverviewVideo = ({ loading, openProjectListView, gridView, overview, projectName, projectID, currentProject, challengeData, goToProject }) => {
  return (
        <div style={style.videosection}>
            {
              ChromeEnv ? <div style={{margin:'6em'}}></div>  :<object data={overview.videoLink} style={style.video} />
            }
            <div>
                {loading == projectID ? <CircularProgress size={30} color={GeneralPalette.brightPurple} /> : button(currentProject.status, gridView, goToProject, projectID, challengeData, currentProject.progress)}
            </div>
        </div>


    );
};

export default OverviewVideo;
