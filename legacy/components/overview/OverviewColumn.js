import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import FlatButton from 'material-ui/FlatButton';
import { Button, Transmitter, Receiver, Controller, LED, Light, Motor, Servo, Sound, Tape } from 'legacy_assets/overviewIcons';
import TranslatedText from 'legacy_containers/TranslatedText';

const style = {
  column: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    width: '100%',
    paddingBottom: '50px',
    borderTop: `0.1px solid${GeneralPalette.buttonBackground}`,
  },

  learning: {
    width: '50%',
    height: '100px',
    padding: '20px',
    paddingBottom: '50px',
    borderRight: `0.05px solid${GeneralPalette.buttonBackground}`,
  },

  parts: {
    width: '50%',
    height: '100px',
    padding: '20px',
    paddingBottom: '50px',
  },

  columnheader: {
    fontFamily: 'staticbold',
    fontSize: '1.5em',
    textAlign: 'center'
  },

  list: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: '10px',
    justifyContent: 'center',
    paddingBottom: '100px'
  },
  learnitem: {
    backgroundColor: 'white',
    width: 'auto',
    margin: '2px',
    padding: '5px',
  },
  partsitem: {
    backgroundColor: 'white',
    width: 'auto',
    margin: '2px',
    padding: '2px 10px 2px 10px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: '35px'
  },
  icon: {
    height: '35px',
    width: '35px',
    fill: GeneralPalette.darkPurple,
    paddingRight: '2px'
  }

};


function partIcon(partsbullet) {
  let icon;
  for (const k in partsbullet) {
    icon = k.toUpperCase();
  }
  if (icon === 'BUTTON') {
    return (
            <div style={style.icon}><Button /></div>
        );
  }
  else if (icon === 'LED') {
    return (
            <div style={style.icon}><LED /></div>
        );
  }
  else if (icon === 'RECEIVER') {
    return (
            <div style={style.icon}><Receiver /></div>
        );
  }
  else if (icon === 'TRANSMITTER') {
    return (
            <div style={style.icon}><Transmitter /></div>
        );
  }
  else if (icon === 'CONTROLLER') {
    return (
            <div style={style.icon}><Controller /></div>
        );
  }
  else if (icon === 'MOTOR') {
    return (
            <div style={style.icon}><Motor /></div>
        );
  }
  else if (icon === 'SERVO') {
    return (
            <div style={style.icon}><Servo /></div>
        );
  }
  else if (icon === 'LIGHT') {
    return (
            <div style={style.icon}><Light /></div>
        );
  }
  else if (icon === 'TAPE') {
    return (
            <div style={style.icon}><Tape /></div>
        );
  }
  else if (icon === 'SOUND') {
    return (
            <div style={style.icon}><Sound /></div>
        );
  }
  else {
    return (
            null
        );
  }
}

function partText(partsbullet) {
  let text;
  let value;
  for (const k in partsbullet) {
    text = k;
    value = partsbullet[k];
  }
  const robopartsArray = ['ROBOCORE', 'BUTTON', 'LED', 'IRRECEIVER', 'IRTRANSMITTER', 'CONTROLLER', 'MOTOR', 'SERVO', 'LIGHTSENSOR', 'TAPESENSOR', 'SOUNDSENSOR'];
  if (robopartsArray.indexOf(text.toUpperCase()) > -1) {
    text = `RoboTerra${text}`;
  }
  return value === 1 ? text : `${value} x ${text}`;
}

const OverviewColumn = ({ overview }) => {
  return (
        <div style={style.column}>
            <div style={style.learning}>
                <div style={style.columnheader}> <TranslatedText text={'KNOWLEDGE'} /> </div>
                    <div style={style.list}>
                        {overview.learn.map((learnbullet) => {
                          return (<div style={style.learnitem} key={`learn${overview.learn.indexOf(learnbullet)}`}>{learnbullet}</div>);
                        })}
                    </div>
            </div>
            <div style={style.parts}>
                <div style={style.columnheader}> <TranslatedText text={'HARDWARE'} /> </div>
                    <div style={style.list}>
                        {overview.parts.map((partsbullet) => {
                          return (
                                <div key={`parts${overview.parts.indexOf(partsbullet)}`} style={style.partsitem}>
                                    {partIcon(partsbullet)}
                                    {partText(partsbullet)}
                                </div>
                            );
                        })}
                    </div>
            </div>
        </div>


    );
};

export default OverviewColumn;
