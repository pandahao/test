import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import FlatButton from 'material-ui/FlatButton';
import OverviewVideoContainer from 'legacy_containers/overview/OverviewVideoContainer';
import OverviewColumnContainer from 'legacy_containers/overview/OverviewColumnContainer';
import TranslatedText from 'legacy_containers/TranslatedText';

const style = {
  all: {
    width: '100%',
  },
  root: {
    width: '80%',
    margin: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontFamily: 'sfns',
    color: GeneralPalette.darkPurple,
    marginLeft: '12%',
    paddingTop: '20px',
    fontSize: '14px',
  },
  title: {
    fontSize: '2.5em',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textTransform: 'uppercase',
    color: GeneralPalette.darkPurple,
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
    padding: '30px',
  },

  backbutton: {
    backgroundColor: GeneralPalette.whiteShadow,
    //boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: GeneralPalette.darkPurple,
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    width: '150px',
    lineHeight: '35px',
    paddingTop: '2px'
  },

  dummy: {
    backgroundColor: 'white',
    boxShadow: '0px 0px 20px 0px lightgrey',
    border: 'none',
    borderRadius: '20px',
    color: GeneralPalette.darkPurple,
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    width: '150px',
    textTransform: 'uppercase',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    height: '30px',
    visibility: 'hidden'
  },

  description: {
    textIndent: '30px',
    textAlign: 'left',
    width: '100%',
    margin: '20px',
    padding: '20px'
  },

};


const Overview = ({ openProjectListView, projectName, overview }) => {
  return (
        <div style={style.all}>
        <div style={style.root}>
            <div style={style.header}>
                <FlatButton onClick={openProjectListView} style={style.backbutton}> <TranslatedText text={'Back to Projects'} /> </FlatButton>
                <div style={style.title}>{projectName}</div>
                <button style={style.dummy}> <TranslatedText text={'DUMMY'} /> </button>
            </div>
            <OverviewVideoContainer />
            <div style={style.description}>{overview.description}</div>
            <OverviewColumnContainer />
        </div>
        </div>


    );
};

export default Overview;
