import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import ChallengePanel from 'legacy_containers/dashboard/ChallengePanel';
import ChallengeHeader from 'legacy_containers/dashboard/ChallengeHeader';

const style = {
  panel: {
    display: 'flex',
    backgroundColor: GeneralPalette.menuBG,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: '100px',
    width: '100%',
  },
  title: {
    padding: '50px',
    color: GeneralPalette.darkPurple,
    fontSize: '40px',
    paddingTop: '0px',
  },
  root: {
    display: 'inline-block',
    width: '100%',
    height: '100vh',
    verticalAlign: 'top',
    backgroundColor: 'white',
    zIndex: 0,
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],

  },
};

class Dashboard extends React.Component {
  render() {
    return (
      <div style={style.root}>
        <ChallengeHeader />
        <ChallengePanel />
      </div>
    );
  }
}


export default Dashboard;
