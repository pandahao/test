import React from 'react';
import _ from 'underscore';
import Radium from 'radium';
import { GeneralPalette,Design } from 'legacy_styles/Colors';
import Box from './Box';
import ControlPad from './ControlPad';
import Drawer from 'legacy_lib/build/Drawer';

import ZoomSliderComponent from './ZoomSliderComponent';

import { t } from 'legacy_utils/i18n';
import { TrackPad, Mouse} from 'legacy_assets';
import{ Parts,Help,PartList} from 'legacy_assets/buildViewIcons';
import FlatButton from 'material-ui/FlatButton';
import ActionSettingsOverscan from 'material-ui/svg-icons/action/settings-overscan';
import {zIndex} from 'styles/zIndex';
import TutorialPopup from 'components/TutorialPopup';

class BuildView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showHelp : true,
      showList : false,
      zoomHandler : ()=>{},
      resetHandler : ()=>{}
    }
  }
  componentDidMount() {
    const $ = this;
    window.SkylineHandlers = Drawer(this.props.projectId, this.props.level);
    window.SkylineHandlers.render();

    // The React way to update handlers
    $.setState({
      zoomHandler : window.SkylineHandlers.zoom,
      resetHandler : window.SkylineHandlers.reset
    })

    window.setTimeout(()=>{
      $.setState({showHelp :false})
    },3000)
  }
  componentWillUnmount() {
    window.SkylineHandlers.onUnmont();
  }
  render() {
    const tutorialMode = this.props.tutorialMode === 6;
    const _style = {
      root: {
        height: '100%',
        position: 'absolute',
        backgroundColor: 'white',
        zIndex: tutorialMode ? zIndex.medium+1 : zIndex.medium -1 ,
        width: '66.67%',
        WebkitFilter: this.props.blurFromListDisplay ? 'blur(3px)' : '',
      },
      canvas: {
        position: 'absolute',
        width: '105%',
        height: '100%',
        backgroundColor: GeneralPalette.brightPurple
      },
      instruction : {
        position : 'absolute',
        display : 'flex',
        justifyContent :'space-around',
        zIndex : this.state.showHelp ? zIndex.high : zIndex.low,
        bottom : '18%',
        right : '10%',
        width : '80%',
        height : '7em',
        borderRadius : '10px',
        backgroundColor:Design.buildback,
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        fontSize : '24px',
        color : 'white',
        transition : 'opacity 0.5s',
        opacity : this.state.showHelp ? 1 : 0
      },
      listPicture:{
        width:'400px',
        height:'400px',
        right:'445px',
        position:'fixed',
        bottom:'280px',
        zIndex : this.state.showList ? zIndex.high : zIndex.low,
        transition : 'opacity 0.5s',
        opacity : this.state.showList ? 1 : 0,
      },
      imgs:{
        right:'10px',
        position:'fixed',
        bottom:'100px',
      },
      zoom:{
        position:'absolute',
        bottom: '2em',
        width : '100%',
        display : 'flex',
        justifyContent : 'space-around'
      },
      slide:{
        width : '50%'
      },
      buttonGroup:{
        position :'absolute',
        bottom : '2em',
        width: '8em',
        right : '-0.4em',
        display : 'flex',
        fontFamily : 'StaticBold',
        color : Design.colors,
        justifyContent : 'space-around'
      },
      helpButton:{
        width:'31px',
        height:'31px',
        borderRadius:'25px',
        border:`1px solid${Design.colors}`,
        backgroundColor:GeneralPalette.white,
        textAlign:'center',
        lineHeight:'30px',
        cursor : 'pointer',
        ':hover': {
          backgroundColor:Design.buildbg,
        }
      },
      resetIcon:{
        marginTop : '3.5px',
        marginLeft: '0.7px'
      }
    };
    return (
      <div style={_style.root}>
          {
            tutorialMode ? <TutorialPopup mode={'LEFT'}/> : null
          }
          <div style={_style.instruction}>
            <span style={{marginTop:'2em'}}>
              <TrackPad/>
            </span>
            <span style={{width:'50%',marginTop:'2em'}}>{t('Use your trackpad or mouse to rotate and zoom the 3D model')}</span>
              <span style={{marginTop:'2em'}}>
              <Mouse/>
            </span>
          </div>
          <div style={_style.canvas}>
              <div id="the-canvas" />;
          </div>
          <div style={_style.zoom}>
            <div style={_style.slide}>
              <ZoomSliderComponent
                handler={this.state.zoomHandler}
              />
            </div>
          </div>
        <div style={_style.listPicture}>
           <img src={
             this.props.language ==='CHINESE' ? "./static/media/part_cn.png" : "./static/media/part_en.png"
           } style={_style.imgs}/>
        </div>
        <div style={_style.buttonGroup}>
          <div style={_style.helpButton}
            onMouseEnter={()=>{
              this.setState({showList : true})
            }}
            onMouseLeave={()=>{
              this.setState({showList : false})
            }}
          >
            <Parts/>
          </div>
          <div style={_style.helpButton} key="help"
            onMouseEnter={()=>{
              this.setState({showHelp : true})
            }}
            onMouseLeave={()=>{
              this.setState({showHelp : false})
            }}
          >
            ?
          </div>
          <div style={_style.helpButton} key="restore"
            onClick={this.state.resetHandler}
          >
            <div style={_style.resetIcon}>
              <ActionSettingsOverscan color={Design.colors}/>
            </div>
          </div>
        </div>
    </div>
    );
  }
}

export default Radium(BuildView);
