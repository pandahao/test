import React from 'react';
import TranslatedText from 'legacy_containers/TranslatedText'; //* *

const Box = ({ title, style }) => {
  const _style = {
    box: {
      display: 'inline-block',
      width: '48%',
      height: '100%',
      backgroundColor: 'white',
      borderRadius: '10px',
      boxShadow: '0px 0px 9px 0px grey',
    },
    label: {
      fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
      fontSize: '14px',
      paddingLeft: '16px',
      marginTop: '14px'
    },
  };
  return (
        <div style={Object.assign({}, _style.box, style)}>
            <div style={_style.label}>
            {title}
            </div>
        </div>
    );
};

export default Box;
