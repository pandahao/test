import React from 'react';
import FlatButton from 'material-ui/FlatButton';

const ArrowButton = ({ style }) => {
  const _style = {
    arrow: {
      width: 0,
      height: 0,
      borderLeft: '6px solid transparent',
      borderRight: '6px solid transparent',
      borderBottom: '10px solid darkgrey',
      margin: 'auto',
    },
    circle: {
      minWidth: '10px',
      width: '100%',
      height: '100%',
      backgroundColor: 'white',
      borderRadius: '100%',
      boxShadow: '0px 0px 9px 0px grey',
      display: 'block',
      cursor: 'pointer'
    }
  };
  return (
        <FlatButton style={Object.assign({}, _style.circle, style)}>
            <div style={_style.arrow} />
        </FlatButton>
    );
};

export default ArrowButton;
