import React, {Component} from 'react';
import Slider from 'material-ui/Slider';


class ZoomSliderComponent extends Component{
  constructor(props){
    super(props);
    this.state = {
      value : 0.5
    }
  }
  handleChange(event,value){
    const {handler} = this.props;
    handler(value);
    this.setState({
      value
    })
  }
  render(){
    // const {defaultValue} = this.props;
    return (
      <Slider
        defaultValue={0.5}
        value={this.state.value}
        onChange={this.handleChange.bind(this)}
      />
    )
  }
}

export default ZoomSliderComponent;
