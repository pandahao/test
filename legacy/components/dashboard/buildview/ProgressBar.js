import React from 'react';
import ProgressPointContainer from 'legacy_containers/buildview/ProgressPointContainer';

const num = 4;
let spacing = ((window.innerWidth * 0.6667 * 0.7) - (num * 30)) / total;// (83*5/num)/614;
let total = 0;
let fill = 0;
let points = [];

class ProgressBar extends React.Component {
  updateWidth() {
    total = this.props.progress - 1;
    this.recalculate();
    this.setState({ width: window.innerWidth });
  }
  componentDidMount() {
    this.updateWidth();
    window.addEventListener('resize', this.updateWidth.bind(this));
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWidth);
  }
  recalculate() {
        // calculate spacing based on width of progress bar and progress points
    spacing = ((window.innerWidth * 0.6667 * 0.7) - ((this.props.progress - this.props.currentProgress) * 30) - (this.props.currentProgress * 35)) / total;
        // calculate filled white progress based on progress points passed
    fill = ((this.props.currentProgress) * 35) + spacing * (this.props.currentProgress - 1);
  }
  render() {
    this.recalculate();
    const _style = {
      line: {
        display: 'inline-block',
        textAlign: 'right'
      },
      progress: {
        position: 'relative',
        display: 'inline-block',
        width: '70%',
        height: '25px',
        backgroundColor: 'grey',
        zIndex: 1,
        borderRadius: '18px',
        marginRight: '3%',
        textAlign: 'left'
      },
      completed: {
        position: 'absolute',
        display: 'inline-block',
        width: `${fill}px`,
        verticalAlign: 'middle',
        height: '30px',
        marginTop: '-2px',
        backgroundColor: 'white',
        zIndex: 2,
        borderRadius: '18px',
        marginRight: '3%',
        boxShadow: '0px 0px 7px 0px grey',
      }
    };
    points = [];
    for (let i = 1; i <= this.props.progress; i += 1) {
      const correctwidth = spacing + (this.props.currentProgress >= i ? 35 : 30);
      if (i === 1) {
        points.push(
                    <div style={_style.line} key={i}>
                        <ProgressPointContainer id={i} highlighted />
                    </div>
                );
      }
      else {
        points.push(
                    <div style={_style.line} key={i}>
                            <div style={{ width: `${correctwidth}px` }} />
                            <ProgressPointContainer id={i} highlighted={i <= this.props.currentProgress} />
                    </div>
                );
      }
    }
    return (
            <div style={_style.progress}>
                <div style={_style.completed} />
                {points}
            </div>
        );
  }
}

export default ProgressBar;
