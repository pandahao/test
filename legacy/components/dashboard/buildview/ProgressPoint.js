import React from 'react';

const ProgressPoint = ({ highlighted }) => {
  const _style = {
    point: {
      position: 'relative',
      width: highlighted ? '35px' : '30px',
      height: highlighted ? '35px' : '30px',
      backgroundColor: 'white',
      borderRadius: highlighted ? '35px' : '30px',
      boxShadow: '0px 0px 7px 0px grey',
      display: 'inline-block',
      zIndex: 100,
      verticalAlign: 'middle',
      marginTop: '-4px',
      cursor: 'pointer'
    },
    dot: {
      position: 'relative',
      width: '22px',
      height: '22px',
      backgroundColor: highlighted ? 'white' : 'lightgrey',
      borderRadius: '22px',
      margin: 'auto',
      marginTop: '4px'
    }
  };
  return (
        <div style={_style.point} >
            <div style={_style.dot} />
        </div>
    );
};

export default ProgressPoint;
