import React from 'react';
import ArrowButton from './ArrowButton';

const ControlPad = () => {
  const _style = {
    left: {
      position: 'relative',
      width: '28%',
      height: '40%',
      transform: 'translate(35%, 0%) rotate(-90deg)'
    },
    right: {
      position: 'relative',
      width: '28%',
      height: '40%',
      transform: 'translate(197%, -102%) rotate(90deg)'
    },
    top: {
      position: 'relative',
      width: '28%',
      height: '40%',
      transform: 'translate(114%,10%)'
    },
    bottom: {
      position: 'relative',
      width: '28%',
      height: '40%',
      transform: 'translate(115%, -110%) rotate(180deg)'
    },
    controlpad: {
      display: 'inline-block',
      verticalAlign: 'middle',
      width: '66%',
      marginLeft: '12%',
      height: '100%',
    }
  };
  return (
        <div style={_style.controlpad}>
            <ArrowButton style={_style.top} />
            <ArrowButton style={_style.left} />
            <ArrowButton style={_style.right} />
            <ArrowButton style={_style.bottom} />
        </div>
    );
};

export default ControlPad;
