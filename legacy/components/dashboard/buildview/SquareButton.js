import _ from 'underscore';
import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';
import TranslatedText from 'legacy_containers/TranslatedText';

const SquareButton = ({ style, icon, back, next}) => {
  const grayedLeft = false
  const grayedRight = false
  const _style = {
    smallbox: {
      width: '120px',
      flexDirection: 'row',
      alignItems: 'flex-start',
      justifyContent: 'space-around',
      fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
      fontSize: '12px',
      height: '25px',
      borderRadius: '5px',
      cursor: 'pointer',
      paddingBottom: '30px',
    },
    left: {
      display: grayedLeft ? 'default' : 'flex',
      backgroundColor: grayedLeft ? GeneralPalette.darkPurple : GeneralPalette.white,
      boxShadow: grayedLeft ? `0px 2px 1px 0px${GeneralPalette.menuBG}` : `0px 2px 1px 0px${GeneralPalette.whiteShadow}`,
      color: grayedLeft ? GeneralPalette.buttonBackground : GeneralPalette.darkPurple,
    },
    right: {
      display: grayedRight ? 'default' : 'flex',
      backgroundColor: grayedRight ? GeneralPalette.darkPurple : GeneralPalette.white,
      boxShadow: grayedRight ? `0px 2px 1px 0px${GeneralPalette.menuBG}` : `0px 2px 1px 0px${GeneralPalette.whiteShadow}`,
      color: grayedRight ? GeneralPalette.buttonBackground : GeneralPalette.darkPurple,
    },
    bold: {
      fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
      fontSize: '25px',
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: '-20px',
      width: '16px',
      height: '16px'
    },
  };

  const clickHandler = _.throttle((goBack) => {
    if (goBack) {
      back();
      if (window.SkylineHandlers) {
        window.SkylineHandlers.backward();
      }
    } else {
      next();
      if (window.SkylineHandlers) {
        window.SkylineHandlers.forward();
      }
    }
  }, 2000);

  switch (icon) {
    case 'plus':
      return (<FlatButton style={Object.assign({}, _style.smallbox, style)}>
            <div style={_style.bold}>+</div>
        </FlatButton>);
    case 'minus':
      return (<FlatButton style={Object.assign(_style.smallbox, style)}>
            <div style={_style.bold}>-</div>
        </FlatButton>);
    case 'left':
      return (
            <FlatButton style={Object.assign({}, _style.smallbox, _style.left)} onClick={clickHandler.bind(null, true)}>
                <span>&lt;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span><TranslatedText text={'previous step'} /></span>
            </FlatButton>);
    case 'right':
      return (
            <FlatButton style={Object.assign({}, _style.smallbox, _style.right)} onClick={clickHandler.bind(null, false)}>
                <span><TranslatedText text={'next step'} /></span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&gt; </span>
            </FlatButton>);
  }
};

export default SquareButton;
