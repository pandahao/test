import React from 'react';
import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';
import ChecklistBlock from './ChecklistBlock';
import HintBlock from 'legacy_components/dashboard/challenge/HintBlock';
import TranslatedText from 'legacy_containers/TranslatedText';

const _style = {
  root: {
    paddingBottom: '50px',
  },
  checklistheader: {
    width: '100%',
    padding: '30px 20px 10px 28px',
    color: GeneralPalette.buttonBackground,
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    letterSpacing: '0.2em',
    fontSize: '15px',
    fontWeight: 100
  },
  checklistContent: {
    width: '100%',
    paddingBottom: '30px',
    lineHeight: '23px',
  },
  button: {
    color: GeneralPalette.buttonBackground,
    letterSpacing: '0.2em',
    fontSize: '15px',
    outline: 'none',
    cursor: 'pointer',
    backgroundColor: 'white',
    boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
    border: 'none',
    borderRadius: '20px',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    width: '150px',
    lineHeight: '35px',
    paddingTop: '2px',
    display: 'flex',
    justifyContent: 'center',
  }
};

class CodeFooter extends React.Component {
  render() {
    let id = -1;
    return (
            <div style={_style.root}>
                <div style={_style.checklistheader}>
                    <TranslatedText text={'CHECKLIST'} />
                </div>
                <table style={_style.checklistContent}>
                    <tbody>
                    {

                        this.props.checklist ? this.props.checklist.map((block, index) => {
                          id++;
                          return (<ChecklistBlock {...block} order={index} currentOrder={this.props.currentEventOrder} key={id} completed={this.props.completed} />);
                        }) : null
                    }
                    </tbody>
                </table>
                {
                    this.props.hint ?
                    <HintBlock hint={this.props.hint} hintStatus={this.props.hintStatus} toggleHint={this.props.toggleHint} />
                    : null
                }
            </div>
        );
  }
}


export default CodeFooter;
