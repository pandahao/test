import React from 'react';
import ChallengeListItem from 'legacy_containers/dashboard/challengeListItem';
import { GeneralPalette } from 'legacy_styles/Colors';
import FlatButton from 'material-ui/FlatButton';
import TranslatedText from 'legacy_containers/TranslatedText';


// import Radium from 'radium';
const style = {
  root: {
    position: 'fixed',
    bottom: '0%',
    top: '0%',
    height: '100%',
    marginTop: '4em',
    backgroundColor: GeneralPalette.menuBG,
    width: '28%',
    display: 'flex',
    flexDirection: 'column',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif']
  },
  table: {
    marginTop: '2em',
    textAlign: 'center',
    width: '100%',
    borderCollapse: 'collapse',
    marginLeft: '2%',
    zIndex: 2,
    marginBottom: '100%',
  },
  buttongroup: {
    display: 'flex',
    position: 'fixed',
    bottom: '0%',
    marginLeft: '0px',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '28%',
    zIndex: 10,
    paddingBottom: '1%',
    paddingTop: '1%',
    color: GeneralPalette.buttonBackground,
    backgroundColor: GeneralPalette.menuBG,
    letterSpacing: '0.1em',
    boxShadow: `-0.5px 0px 15px 0px${GeneralPalette.buttonBackground}`,
  },
  button: {
    outline: 'none',
    cursor: 'pointer',
    backgroundColor: 'white',
    boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
    border: 'none',
    borderRadius: '20px',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    padding: '0px 35px 0px 35px',
    marginBottom: '15px',
    color: GeneralPalette.darkPurple,
    letterSpacing: '0.1em',
    width: '250px'
  },
  restart: {
    outline: 'none',
    position: 'relative',
    cursor: 'pointer',
    backgroundColor: GeneralPalette.white,
    boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
    border: 'none',
    borderRadius: '20px',
    fontSize: '10px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    lineHeight: '20px',
    padding: '0px 5px 0px 5px',
    marginBottom: '0px',
    color: GeneralPalette.buttonBackground,
    letterSpacing: '0.1em',
    width: '150px'
  },
  tableonly: {
    overflowX: 'hidden',
    overflowY: 'scroll',
  }

};

class List extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { challenges, openProjectListView, restartProject } = this.props;

    return (
            <div style={style.root}>

                <div style={style.tableonly}>
                    <table style={style.table}>
                    <tbody>
                    {this.props.challenges.map((ch) => {
                      return (<ChallengeListItem key={ch.level}{...ch} />);
                    })}
                    </tbody>
                    </table>
                </div>
                <div style={style.buttongroup}>
                    <FlatButton style={style.button} onClick={this.props.openProjectListView}>
                        <TranslatedText text={'All Projects'} />
                    </FlatButton>

                        {/* <FlatButton style = {style.restart} onClick={this.props.restartProject}>
                            <TranslatedText text={"RESTART PROJECT"}/>
                        </FlatButton>
                         */}


                </div>
            </div>

        );
  }
}


export default List;
