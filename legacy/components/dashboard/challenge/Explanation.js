import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import { CheckBox, XBox } from 'legacy_assets/index';
import InlineCodeText from 'legacy_components/dashboard/challenge/InlineCodeText';

const style = {
  root: {
    textAlign: 'left',
    margin: '20px 0px 40px -10px',
    fontWeight: 'bold',
    backgroundColor: GeneralPalette.menuBG,
    padding: '10px 10px 10px 10px',
    borderRadius: '5px',
    lineHeight: '25px'
  },
  check: {
    paddingRight: '10px'
  },
};

const Explanation = ({ result, text }) => {
  console.log('result', result);
  return (
        <div style={style.root}><span style={style.check}> {result ? <XBox /> : <CheckBox />} </span>
            <InlineCodeText text={text} />
        </div>
   );
};

export default Explanation;
