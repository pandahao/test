import React from 'react';
import { ChallengePalette, GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';

const style = {
  bold: {
    fontSize: 'auto',
    fontFamily: 'sfns',
    fontWeight: 900
  },
  text: {
    fontFamily: 'sfns',
    lineHeight: '25px',
    fontWeight: 'normal',

  },
  block: {
    paddingTop: '5%'
  },
  focus: {
    textAlign: 'left',
    margin: '0px 0px 40px 0px',
    fontWeight: 'bold',
    backgroundColor: GeneralPalette.menuBG,
    padding: '10px 10px 10px 10px',
    borderRadius: '5px',
    lineHeight: '25px',
    width: '90%'
  }
};

const BuildBlock = ({ num, block, currentProgress, progress }) => {
  return (
       <div>
       {(currentProgress - 1) >= num ?
          <div>
            <div style={Object.assign({}, style.text, ((currentProgress - 1) === num || currentProgress === progress.points ? style.focus : null))}>
                <span style={style.bold}>{num < 9 ? 0 : null}{num + 1}.</span>
                <span key={num} > {block} </span>
            </div>
            <div style={style.block} />
          </div>
        : null}
        </div>
    );
};

export default BuildBlock;
