import React, { PropTypes } from 'react';

import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import ContentClear from 'material-ui/svg-icons/content/clear';
import NavigationArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';

const Header = ({ showContent, toggleContent, title }) => {
  const style = {
    root: {
      width: '28%',
      background: GeneralPalette.menuBG,
      position: 'absolute',
      boxShadow: `0px 0px 20px -5px${GeneralPalette.buttonBackground}`,
      zIndex: 2
    },
    bar: {
      display: 'flex',
      alignItems: 'center',
      height: '64px',
      marginLeft: '1%',
      zIndex: 100,
      cursor: 'pointer',
    },
    project_title: {
      // marginLeft: '1.0%',
      // marginTop:'1.2em',
      color: GeneralPalette.darkPurple,
      letterSpacing: '.39vw',
      fontSize: '14px',
      fontFamily: 'staticbold',
      textTransform: 'uppercase',
    }
  };

  return (
        <div style={style.root}>
            <div style={style.bar} onClick={toggleContent.bind(null, showContent)}>
                <IconButton>
                    {showContent ? <NavigationMenu color={CastleRockPalette.textDefault} /> :
                    <NavigationArrowBack color={CastleRockPalette.textDefault} />}
                </IconButton>
                <span style={style.project_title}>
                    {title}
                </span>
            </div>
        </div>
    );
};


export default Header;
