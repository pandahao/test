import React from 'react';
import { ChallengePalette } from 'legacy_styles/Colors';
import { CheckBox, EmptyCheckBox, Donut } from 'legacy_assets/index';

const style = {

  root: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: '25px',
    width: '90%',
    verticalAlign: 'top',
  },
  icon: {
    verticalAlign: 'top',
    marginRight: '20px',
  },
  group: {
    display: 'flex',
    flex: 'wrap',
    flexDirection: 'row',
    paddingLeft: '2px'
  },
  text: {
    width: '90%',
    verticalAlign: 'top',
  }
};

const ChecklistBlock = ({ currentOrder, order, text, completed }) => {
  let eventIcon = null;
  if (order < currentOrder || completed) {
    eventIcon = <CheckBox />;
  }
  else {
    eventIcon = <Donut />;
  }


  return (
        text ?
        <tr style={style.root} key={text}>
            <td style={style.group}>
                <div style={style.icon}>{eventIcon}</div>
                {text}
            </td>
        </tr>
        : null
    );
};


export default ChecklistBlock;
