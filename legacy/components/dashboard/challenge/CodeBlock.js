import React from 'react';
import { ChallengePalette, GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import AceEditor from 'react-ace';
import InlineCodeText from 'legacy_components/dashboard/challenge/InlineCodeText';

const style = {
  numtext: {
    display: 'inline-block',
  },
  bolds: {
    fontSize: 'auto',
    fontFamily: 'sfns',
    fontWeight: 900,
  },
  editor: {
    backgroundColor: ToolbeltPalette.backgroundDark,
    padding: '5px',
    width: '100%',
    borderRadius: '3px',
    marginTop: '3%',
    fontFamily: 'menlo',
    fontSize: '12px',
    color: GeneralPalette.white,
    zIndex: 0,
    //pointerEvents: 'none',
  },
  text: {
    fontFamily: 'sfns',
    lineHeight: '25px',
    fontWeight: 'normal',
  },
  block: {
    paddingTop: '5%',

  },
  line: {
    display: 'inline-block',
  }

    // <div style = {style.editor}>{block.code}</div>
};

const calcLine = (code) => {
  const count = (code.match(/\n/g) || []).length + 1;
  const height = count * 20;
  return `${height}px`;
};

const CodeBlock = ({ num, block }) => {
  return (
        <div>
            { block.text ? (
                <div style={style.numtext}>
                    <div style={style.text}>
                        {num >= 0 ? <span style={style.bolds}>{(num + 1) < 9 ? 0 : null}{num + 1}.</span> : null}
                        <InlineCodeText text={block.text} />
                    </div>
                </div>)
                : null
            }
            { block.code ? (
                <div style={style.editor}>
                    <AceEditor
                      name={num.toString()}
                      width="100%"
                      height={calcLine(block.code)}
                      readOnly
                      showGutter={false}
                      theme="castle-rock"
                      mode="c_cpp"
                      fontSize={12}
                      value={block.code}
                      wrapEnable={true}
                      editorProps={{$backscrolling:true}}
                      onLoad={(editor) => {
                      editor.getSession().setUseWrapMode(false);
                      }}
                    />
                </div>
            ) : null }
            { block.textTwo ? (
                <div style={style.numtext}>
                    <div style={style.text}>
                        <InlineCodeText text={block.text} />
                    </div>
                </div>)
            : null }
            <div style={style.block} />
        </div>
    );
};

export default CodeBlock;
