/*eslint-disable*/
import React from 'react';
import ReactDOM from 'react-dom';
import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';
import ContentHeaderContainer from 'legacy_containers/dashboard/ContentHeaderContainer';
import ContentBlock from './ContentBlock';
import CodeBlock from './CodeBlock';
import CodeFooterContainer from 'legacy_containers/dashboard/CodeFooterContainer';
import QuizFooterContainer from 'legacy_containers/dashboard/QuizFooterContainer';
import FlatButton from 'material-ui/FlatButton';
import InlineCodeText from 'legacy_components/dashboard/challenge/InlineCodeText';
import TranslatedText from 'legacy_containers/TranslatedText';

class Content extends React.Component {

  render() {
    const { challenge, currentProgress, currentChallenge, currentChallengeSet, currentProjectID, quizResult, show, nextChallenge } = this.props;

    const style = {
      root: {
        background: ChallengePalette.mainBackground,
        color: CastleRockPalette.textDefault,
        fontFamily: 'sfns',
        paddingRight: '30px',
        fontSize: '15px',
        overflowX: 'hidden',
      },
      head: {
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        paddingLeft: '8px',
        fontSize: '20px',
      },
      header: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginLeft: '30px',
        marginRight: '10px',
        marginTop: '80px',
        marginBottom: '20px',
        fontSize: '10px',
        overflow: 'hidden',
        color: ChallengePalette.headerBar,
        letterSpacing: '3px',
        fontFamily: 'staticbold',
        zIndex: 10
      },
      introduction: {
        backgroundColor: ChallengePalette.introductionBackground,
        padding: '0px 0px 30px 30px',
        lineHeight: '25px',
      },
      contentheader: {
        width: '100%',
        padding: '30px 20px 0px 30px',
        color: GeneralPalette.buttonBackground,
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        letterSpacing: '0.15em',
        fontSize: '15px',
        borderTop: `2px solid${GeneralPalette.menuBG}`,
        marginBottom: '20px',
      },
      content: {
        width: '93%',
        marginTop: 'none',
        padding: '0px 80px 20px 30px',
        borderBottom: challenge.type === 'quiz' || 'build' ? null : `2px solid${GeneralPalette.menuBG}`,
      },
      button: {
        backgroundColor: GeneralPalette.green,
        boxShadow: `0px 3px 1px 0px${GeneralPalette.greenShadow}`,
        border: 'none',
        borderRadius: '20px',
        color: GeneralPalette.white,
        fontSize: '15px',
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        textAlign: 'center',
        width: '70%',
        minWidth: '10px',
        lineHeight: '35px',
        paddingTop: '2px',
        letterSpacing: '0.1em',
        margin: '10px 0px 80px 30px'
      },
      bottom: {
        background: ChallengePalette.mainBackground,
        width: '130%'
      }
    };

    let titleWording;
    if (challenge.type === 'code') {
      titleWording = <TranslatedText text={'INSTRUCTIONS'} />;
    } else if (challenge.type === 'quiz') {
      titleWording = <TranslatedText text={'KNOWLEDGE CHECK'} />;
    } else if (challenge.type === 'build') {
      titleWording = <TranslatedText text={'INSTRUCTIONS'} />;
    }
    let id = -1;
    let id2 = -1000;
    const buttonText = (currentChallenge === currentChallengeSet.length - 1) ?
      <TranslatedText text={'COMPLETE PROJECT'} /> : <TranslatedText text={'NEXT CHALLENGE'} />;

    return (
            show ?
              <div style={style.root}>
                <div style={style.head}><ContentHeaderContainer /></div>
                <div style={style.introduction}>
                  {
                    challenge.type === 'quiz' ? challenge.content.introduction.map((block) => {
                      id2++;
                      return <CodeBlock num={id2} key={id2} block={block} />;
                    }) : challenge.content.introduction
                }
                </div>

                <div style={style.contentheader}>
                  {titleWording}
                </div>

                <div style={style.content}>
                  {
                        challenge.content.blocks ? challenge.content.blocks.map((block) => {
                          id++;
                          return (<ContentBlock type={challenge.type} block={block} key={id} num={id} />);
                        }) : null
                    }
                </div>

                {challenge.type === 'quiz' ?
                  <QuizFooterContainer />
                    : null
                }

                {challenge.type === 'code' ?
                  <CodeFooterContainer />
                    : null}

                {challenge.type === 'build' &&
                currentProgress === challenge.content.blocks.length ?
                  <div style={style.bottom}>
                    <FlatButton style={style.button} onClick={() => nextChallenge(currentChallenge, currentChallengeSet, currentProjectID)}>{buttonText}</FlatButton>
                  </div>
                    : null}
              </div>
            : null

        );
  }
  componentDidUpdate() {
    // if (ReactDOM.findDOMNode(this) !== null & this.props.challenge.type === 'build' && this.props.currentProgress > 1) {
    //   ReactDOM.findDOMNode(this).scrollIntoView({ behavior: 'smooth', block: 'end' });
    // }
    // if (ReactDOM.findDOMNode(this) !== null & this.props.challenge.type === 'quiz' && this.props.currentProjectID) {
    //   ReactDOM.findDOMNode(this).scrollIntoView({ behavior: 'smooth', block: 'end' });
    // }
  }
}

export default Content;
