import React from 'react';
import CodeBlock from './CodeBlock';
import QuizBlockContainer from '../../../containers/dashboard/QuizBlockContainer';
import BuildBlockContainer from 'legacy_containers/dashboard/BuildBlockContainer';

import 'legacy_styles/c_cpp';
import 'legacy_styles/editor/castlerock_theme';

const ContentBlock = ({ block, type, num }) => {
  const style = {
    root: {
      fontFamily: 'sfns',
      paddingTop: '0px',
    },
    code: {
      display: block.type === 'code' ? 'inline' : 'none',
    },
    quiz: {
      display: block.type === 'quiz' ? 'inline' : 'none',
      question: {
        fontWeight: 900,
      },
      answer: {
        fontStyle: 'italic',
      }
    },
    build: {
      display: block.type === 'build' ? 'inline' : 'none'
    },
    radiobutton: {
      marginTop: '4px',
      color: 'red',
      selected: {
        color: 'green'
      }
    }
  };

  const calcLine = (code) => {
    const count = (code.match(/\n/g) || []).length + 1;
    const height = count * 20;
    return `${height}px`;
  };

  return (
        <div style={style.root}>
        { type === 'code' ? <CodeBlock num={num} block={block} /> : null}
        { type === 'quiz' ? <QuizBlockContainer num={num} block={block} /> : null}
        { type === 'build' ? <BuildBlockContainer num={num} block={block} /> : null}
        </div>
    );
};


export default ContentBlock;
