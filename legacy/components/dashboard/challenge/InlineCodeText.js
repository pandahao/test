import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';

const style = {
  code: {
    color: GeneralPalette.darkPurple,
    padding: '2px 1px 2px 6px',
    fontFamily: 'menlo',
    fontSize: 12,
    letterSpacing: -0.8,
    borderRadius: '2px',
    backgroundColor: GeneralPalette.menuBG,
    border: `1px solid${GeneralPalette.buttonBackground}`,
    wordWrap:'break-word'
  },
};

const lexText = (text) => {
  let result = [];
  result = text.split('~');
  return result;
};

const InlineCodeText = ({ text }) => {
  if (text !== undefined) {
    var InlineCodeText = lexText(text);
  }
  let mapKey = 0;
  return (
        <span>
        {InlineCodeText.map((str) => {
          mapKey++;
          if (InlineCodeText.indexOf(str) % 2 === 0) {
            return <span key={mapKey}> {str} </span>;
          }
          else {
            return <span key={mapKey} style={style.code}> {str}&nbsp;< /span>;
          }
        })}
        </span>
   );
};

export default InlineCodeText;
