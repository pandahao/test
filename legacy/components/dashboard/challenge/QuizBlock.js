import React from 'react';
import { ChallengePalette, GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import RadioButton from 'material-ui/RadioButton';
import RadioButtonGroup from 'material-ui/RadioButton/RadioButtonGroup';
import Explanation from './Explanation.js';


const style = {
  root: {
    backgroundColor: ChallengePalette.contentBackground,
    width: '100%',
    padding: '10px 10px 20px 20px',
  },
  quiz: {
    question: {
      fontWeight: 900,
      marginBottom: '20px',
      lineHeight: '25px',
    },
  },
  label: {
    color: GeneralPalette.darkPurple,
    marginLeft: '-5px',
    zIndex: 1
  },
  labelcorrect: {
    marginLeft: '-5px',
    color: GeneralPalette.green,
    transition: 'color 2s',
  },
  labelwrong: {
    marginLeft: '-5px',
    transition: 'color 2s',
    textDecoration: 'line-through',
    color: GeneralPalette.buttonBackground,
  },
  iconstyle: {
    fill: GeneralPalette.darkPurple,
  },
  entirequiz: {
    marginTop: '10px',
    marginBottom: '10px',
  },
  bold: {
    fontSize: 'auto',
    fontFamily: 'sfns',
    fontWeight: 900
  },
  buttonGroup: {
    width: '100%',
    zIndex: '-10'
  },
  row: {
    fontSize: '13px',
    lineHeight: '21.8px',
    paddingTop: '2px'
  },
  result: {
    marginTop: '30px',
    textAlign: 'center',
  }
};

const QuizBlock = ({ block, setAnswerSelected, quizResult, challengeCompleted, quizAnswerSelected }) => {
    // console.log(quizResult);
    // console.log(block.question);
    // console.log(quizResult[block.question]);
  return (
       <span>
           {block.question ? <span style={style.quiz.question}>{block.question}</span> : null}
           {block.choices ?
               <div style={style.entirequiz}>
                   <RadioButtonGroup style={style.buttonGroup} name={block.question} onChange={setAnswerSelected}>
                       {block.choices.map((c) => {
                         return (
                               challengeCompleted ? (block.choices.indexOf(c) === block.answer - 1 ? <RadioButton key={block.choices.indexOf(c)} label={c} value={`${block.choices.indexOf(c).toString()}:${block.question}`} labelStyle={style.labelcorrect} iconStyle={style.iconcorrect} disabled />
                               : (quizResult[block.question] === 'WRONG' && block.choices.indexOf(c).toString() === quizAnswerSelected[block.question] ? <RadioButton key={block.choices.indexOf(c)} label={c} value={`${block.choices.indexOf(c).toString()}:${block.question}`} labelStyle={style.labelwrong} iconStyle={style.iconwrong} disabled />
                               : <RadioButton key={block.choices.indexOf(c)} label={c} value={`${block.choices.indexOf(c).toString()}:${block.question}`} labelStyle={style.label} iconStyle={style.iconstyle} disabled />))
                               : <RadioButton key={block.choices.indexOf(c)} label={c} value={`${block.choices.indexOf(c).toString()}:${block.question}`} labelStyle={style.label} iconStyle={style.iconstyle} />
                           );
                       })}
                   </RadioButtonGroup>
                   {(challengeCompleted) ? <div><div style={style.result} /><Explanation result={quizResult[block.question] == 'WRONG'} text={block.explanation} /></div> : null}
               </div>
               : null}
       </span>
   );
};

export default QuizBlock;

// {quizResult[block.question] === "WRONG" ? "WRONG" : "RIGHT"}
