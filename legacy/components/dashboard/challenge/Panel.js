import React from 'react';
import ChallengeContent from 'legacy_containers/dashboard/ChallengeContent';
// import ChallengeList from 'legacy_containers/dashboard/ChallengeList';
import ChallengeListComponent from 'containers/Dashboard/ChallengeListComponent';
import Documentation from 'legacy_components/documentation/Documentation';

class Panel extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
  }

  render() {
    const style = {
      root: {
        display: 'inline-block',
        width: '100%',
        height: '100%',
        overflowY: 'scroll',
        overflowX: 'auto',
      },
      header: {
        width: '24%',
        zIndex: 10
      },
      content: {
        zIndex: 2
      },
      list: {
        marginTop: '4em',
        position: 'absolute',
        zIndex: 2,
        width: '25%',
                // transitionProperty: 'margin-left',
                // transitionDuration: this.props.pageDelay ? '1s': '0s',
        marginLeft: this.props.showChallengeContent ? '-100%' : '0%'
      },
      cheat: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start',
      },
      docs: {
        position: 'absolute',
                // transitionProperty: 'margin-left',
                // transitionDuration: '1s',
        width: '25%',
        zIndex: 20,
        marginLeft: this.props.openDocs ? '0%' : '-130%'
      }
    };
    return (
            <div style={style.root}>
                <div style={style.docs}>
                    <Documentation />
                </div>
                <div style={style.cheat}>
                {
                    //this.props.showChallengeContent ? <ChallengeContent style={style.content} /> : <ChallengeListComponent />
                    <ChallengeContent style={style.content} />
                }
                </div>
            </div>
        );
  }

  componentDidUpdate() {
    this.props.immediatePageChange();

        // these delays are to address the bug of material ui radio buttons being
        // clickable even when under the challenge list
        // if (this.props.showChallengeContent) { //show the challenge list
        //     this.props.immediatePageChange();
        // } else {
        //     this.props.delayedPageChange();
        // }
  }

}

export default Panel;
