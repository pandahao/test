import React from 'react';
import ReactDOM from 'react-dom';
import { GeneralPalette } from 'legacy_styles/Colors';
import AceEditor from 'react-ace';
import HintContent from './HintContent';
import FlatButton from 'material-ui/FlatButton';
import { ToolboxArrow, ToolboxDownArrow } from 'legacy_assets/codepadIcons';
import TranslatedText from 'legacy_containers/TranslatedText';

const _style = {
  button: {
    // backgroundColor: 'white',
    // boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
    // border: 'none',
    // borderRadius: '20px',
    // color: GeneralPalette.buttonBackground,
    borderRadius:'20px',
    backgroundColor:'#fff',
    border:'2px solid #e7e9ed',
    color:'#868fac',
    boxShadow:'0px 0px 0px 0px #ccc',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    width: '120px',
    lineHeight: '32px',
    paddingTop: '2px',
    letterSpacing: '0.2em',
    marginLeft:'-40px',
  },
  hintContent: {
    paddingLeft: '20px',
    paddingBottom: '40px',

  }
};

const calcLine = (code) => {
  const count = (code.match(/\n/g) || []).length + 1;
  const height = count * 20;
  return `${height}px`;
};

class HintBlock extends React.Component {
  componentDidUpdate() {
    if (this.props.hintStatus) {
      ReactDOM.findDOMNode(this).scrollIntoView({ behavior: 'smooth', block: 'end' });
    }
  }
  render() {
    return (this.props.hint instanceof Object && Object.keys(this.props.hint).length > 0)?
            (<div style={_style.hintContent}>
                <FlatButton style={_style.button} onClick={() => this.props.toggleHint(this.props.hintStatus)}>
                    <span style={{ color: GeneralPalette.buttonBackground }}><TranslatedText text={'HINT'} /></span> &nbsp;
                    {!this.props.hintStatus ? <ToolboxDownArrow /> : <ToolboxArrow />}
                </FlatButton>
                {this.props.hintStatus ? <HintContent hint={this.props.hint} /> : null}
            </div>) : null;
  }
}

export default HintBlock;
