import React from 'react';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import AceEditor from 'react-ace';


const calcLine = (code) => {
  const count = (code.match(/\n/g) || []).length + 1;
  const height = count * 20;
  return `${height}px`;
};

const HintContent = ({ hint }) => {
  const _style = {
    text: {
      fontFamily: 'sfns',
      lineHeight: '25px',
      fontWeight: 'normal',
      paddingTop: '20px',
      display: hint.text ? 'block' : 'none'
    },
    code: {
      backgroundColor: ToolbeltPalette.backgroundDark,
      padding: '5px',
      width: '100%',
      borderRadius: '3px',
      marginTop: '3%',
      fontFamily: 'menlo',
      fontSize: '12px',
      color: GeneralPalette.white,
      display: hint.code ? 'block' : 'none',
      //pointerEvents: 'none',
    },
  };
  const hintCode = hint.code ? hint.code : '';
  return (
    <div>
      <div style={_style.text}>
        {hint.text}
      </div>
      <div style={_style.code}>
        
          <AceEditor
            name="hintCode"
            width="100%"
            height={calcLine(hintCode)}
            readOnly
            showGutter={false}
            theme="castle-rock"
            mode="c_cpp"
            fontSize={12}
            value={hint.code}
            wrapEnable={true}
            editorProps={{$backscrolling:false}}
           onLoad={(editor) => {
          editor.getSession().setUseWrapMode(false);
  }}
/>
        
      </div>
    </div>
    );
};

export default HintContent;
