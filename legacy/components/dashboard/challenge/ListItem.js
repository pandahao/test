import React, { PropTypes } from 'react';
import Radium from 'radium';
import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';
import { TypeBuild, TypeBuildLocked, TypeCode, TypeCodeLocked, TypeLearn, TypeLearnLocked, DoneStatusCircle} from 'legacy_assets';

class ListItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { level, title, type, challengeProgress, selected, onClick, challengeCompleted, currentChallengeSelected } = this.props;
    let status = 'LOCKED';
    if (challengeProgress === level) {
      status = 'UNLOCKED';
    }
    else if (challengeProgress > level) {
      status = 'COMPLETED';
    }

    const level_number = (level) < 10 ? (`0${level}`) : (level);
    const textColor = () => {
      switch (status) {
        case 'LOCKED':
          return ChallengePalette.listDecorator;
        case 'COMPLETED':
          return selected ? 'white' : CastleRockPalette.textDefault;
        case 'UNLOCKED':
          return selected ? 'white' : CastleRockPalette.textDefault;
        default:
          return CastleRockPalette.textDefault;
      }
    };

    const style = {
      row: {
        width: '50%',
        cursor: status != 'LOCKED' ? 'pointer' : 'default',
      },
      number: {
        width: '15%',
        textAlign: 'center',
        alignSelf: 'center',
        color: selected ? ChallengePalette.white : GeneralPalette.buttonBackground,
        borderBottomLeftRadius: '20px',
        borderTopLeftRadius: '20px',
        backgroundColor: selected ? GeneralPalette.brightPurple : GeneralPalette.menuBG,
        verticalAlign: 'top',
        paddingTop: '8px',
        paddingBottom: '3px',

      },
      title: {
        color: selected ? ChallengePalette.white : (status != 'LOCKED' ? GeneralPalette.darkPurple : GeneralPalette.sideMenuPurple),
        textAlign: 'left',
        backgroundColor: selected ? GeneralPalette.brightPurple : GeneralPalette.menuBG,
        width: '65%',
        verticalAlign: 'top',
        paddingTop: '8px',
        paddingBottom: '3px',
        paddingRight: '10px'
      },
      typeIcon: {
        color: GeneralPalette.darkPurple,
        fill: GeneralPalette.darkPurple,
        backgroundColor: selected ? GeneralPalette.brightPurple : GeneralPalette.menuBG,
        height: '100%',
        paddingRight: '20px',
        width: '5%',
        borderTopRightRadius: '20px',
        borderBottomRightRadius: '20px',
        verticalAlign: 'top',
        paddingTop: '8px',
        paddingBottom: '3px',
      },
      selected: selected && status != 'LOCKED' ? {
        display: 'block',
        position: 'absolute',
        marginTop: '-0.5em',
        marginLeft: '1em',
        paddingLeft: 0,
        height: '2em',
        width: '85%',
        background: ChallengePalette.primary,
        boxShadow: `0px 2px 1px ${ChallengePalette.squareShadow}`,
        zIndex: -10,
        borderRadius: '20px',
      } : {}
    };

    let typeIcon;
    const locked = (status === 'LOCKED');
    if (type === 'code') {
      typeIcon = locked ? <TypeCodeLocked /> : <TypeCode />;
    } else if (type === 'quiz') {
      typeIcon = locked ? <TypeLearnLocked /> : <TypeLearn />;
    } else if (type === 'build') {
      typeIcon = locked ? <TypeBuildLocked /> : <TypeBuild />;
    }
    // const clickable = locked ? null : onClick.bind(null, type, challengeCompleted, currentChallengeSelected);
    // const lockIcon = locked ? <Locked /> : null;

    return (
              <tr style={style.row} onClick={clickable}>
                <td style={style.number}> {level_number} </td>
                <td style={style.title}> {title} </td>
                <td style={style.typeIcon}> {typeIcon} </td>
              </tr>
        );
  }
}

export default Radium(ListItem);
