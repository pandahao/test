import React from 'react';
import ReactDOM from 'react-dom';
import { TypeCode, TypeLearn, TypeBuild, DoneStatusIcon } from 'legacy_assets';
import { GeneralPalette } from 'legacy_styles/Colors';
import TranslatedText from 'legacy_containers/TranslatedText';

const style = {
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: '80px 0px 20px 21px',
    fontSize: '12px',
    color: GeneralPalette.buttonBackground,
    letterSpacing: '1.5px',
    fontFamily: 'StaticBold',
    zIndex: 20,
  },
  hidden: {
    visibility: 'hidden'
  },
  challenge_title: {
    marginLeft: '20px',
    marginRight: '20px',
    marginTop: '-5px'
  },
  icon: {
    zoom: 1.4,
    marginRight: '3px'
  }

};

class ContentHeader extends React.Component {
  componentDidUpdate() {
    ReactDOM.findDOMNode(this).scrollIntoView(false);
  }
  render() {
    const { type, level, title, progress } = this.props;
// JASONTRANSLATE
    let typeIcon;
    let typeName;
    if (type === 'code') {
      typeIcon = <TypeCode />;
      typeName = '(CODE)';
    } else if (type === 'quiz') {
      typeIcon = <TypeLearn />;
      typeName = '(QUIZ)';
    } else if (type === 'build') {
      typeIcon = <TypeBuild />;
      typeName = '(BUILD)';
    }
    const statusIcon = (progress > level) ?
            <DoneStatusIcon /> : <span style={style.hidden}> <DoneStatusIcon /></span>;

    return (
            <div>
                <div style={style.root}>
                    <div style={style.icon}>{typeIcon}</div>
                    <div><TranslatedText text={'CHALLENGE'} />{level}</div>
                    {typeName}
                    <div style={{ width: `${25}%` }} />
                    {statusIcon}
                </div>
                <h1 style={style.challenge_title}>{title}</h1>
            </div>
        );
  }
}

export default ContentHeader;
