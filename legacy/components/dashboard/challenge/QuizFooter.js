import React from 'react';
import { ChallengePalette, CastleRockPalette, GeneralPalette,Design } from 'legacy_styles/Colors';
import FlatButton from 'material-ui/FlatButton';
import TranslatedText from 'legacy_containers/TranslatedText';

const style = {
  submitbutton: {
    backgroundColor: GeneralPalette.white,
    border:`2px solid${Design.buttonborder}`,
    color: Design.text,
    marginBottom: '25%',
    letterSpacing: '0.15em',
    fontSize: '15px',
    cursor: 'pointer',
    // backgroundColor: 'white',
    // boxShadow: `0px 1.5px 1px 1px${GeneralPalette.whiteShadow}`,
    borderRadius: '20px',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    width: '90%',
    minWidth: '10px',
    marginLeft: '25px'

  },
  donebutton: {
    color: 'white',
    marginBottom: '25%',
    letterSpacing: '0.15em',
    fontSize: '15px',
    cursor: 'pointer',
    backgroundColor: GeneralPalette.green,
    // boxShadow: `0px 3px 1px 0px${GeneralPalette.greenShadow}`,
    borderRadius: '20px',
    color: GeneralPalette.white,
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    width: '90%',
    minWidth: '10px',
    marginLeft: '25px'
  },
};


const QuizFooter = ({ lastChallenge, currentProjectID, checkAnswer, nextChallenge, completeProject, challengeCompleted }) => {
  return (
        !challengeCompleted ?
        <FlatButton style={style.submitbutton} onClick={checkAnswer}>
            <TranslatedText text={'SUBMIT ANSWER'} />
        </FlatButton>
        : lastChallenge ?
            <FlatButton style={style.donebutton} onClick={() => completeProject(currentProjectID)}>
                <TranslatedText text={'COMPLETE PROJECT'} />
            </FlatButton> :
            <FlatButton style={style.donebutton} onClick={nextChallenge}>
                <TranslatedText text={'NEXT CHALLENGE'} />
            </FlatButton>
    );
};

export default QuizFooter;
