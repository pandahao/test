import React from 'react';

class Blur extends React.Component {

  render() {
    const _style = {
      width: '100%',
      height: '100%',
      position: 'absolute',
      backgroundColor: '#696969',
      opacity: 0.7,
      zIndex: 10
    };
    return (
            <div style={_style} />
        );
  }

}

export default Blur;
