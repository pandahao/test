import React from 'react';
import ChallengeListOB from 'legacy_components/onBoarding/tutorialOnBoarding/onBoardingParts/ChallengeListOB';

class ChallengeTwoOnBoarding extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <ChallengeListOB language={this.props.language} currentStep={this.props.currentStep} firstStep={6} lastStep={7} previousOnBoarding={this.props.previousOnBoarding} nextOnBoarding={this.props.nextOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />;
  }
}

export default ChallengeTwoOnBoarding;
