import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';
import { onBoardChallenge } from 'legacy_mock/onBoardingData';
import { onBoardChallengeCN } from 'legacy_mock/onBoardingDataChinese';
import OnboardTitle from 'legacy_components/onBoarding/initialOnBoard/OnboardTitle';
import OnboardText from 'legacy_components/onBoarding/initialOnBoard/OnboardText';
import OnboardProgress from 'legacy_components/onBoarding/initialOnBoard/OnboardProgress';
import OnboardButtons from 'legacy_components/onBoarding/initialOnBoard/OnboardButtons';
import OnboardArrowRight from 'legacy_components/onBoarding/initialOnBoard/OnboardArrowRight';
import VirtualRobotContainer from 'legacy_containers/virtualrobot/VirtualRobotContainer';
import EventConsole from 'legacy_containers/virtualrobot/EventConsole';

class VirtualRobotOB extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const style = {
      arrow: {
        position: 'absolute',
        bottom: this.props.currentStep === 8 ? 600 : (this.props.currentStep == 9 ? 700 : 130),
        left: '62%'
      },
      root: {
        position: 'absolute',
        bottom: this.props.currentStep === 10 ? 50 : 200,
        right: '37%',
        margin: 'auto',
        padding: '30px',
        width: '280px',
        height: '470px',
        backgroundColor: GeneralPalette.darkPurple,
        borderRadius: 7,
        boxShadow: `0px 2px 3px 0px${GeneralPalette.boxShadow}`,
        zIndex: 500,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        textAlign: 'center',
      },
      virtualrobot: {
        position: 'fixed',
        width: '34%',
        top: 0,
        zIndex: 10,
        left: '66%'
      }
    };

    let data = onBoardChallenge;
    if (this.props.language === 'CHINESE') {
      data = onBoardChallengeCN;
    }

    return (
            <div>
            <div style={style.virtualrobot}>
                <VirtualRobotContainer />
            </div>
                <div style={style.arrow}>
                    <OnboardArrowRight />
                </div>
                <div style={style.root}>
                    <OnboardTitle content={data[this.props.currentStep].title} />
                    <OnboardText content={data[this.props.currentStep].text} />
                    <OnboardProgress progress={this.props.currentStep - 7} length={3} />
                    <OnboardButtons onBoarding={this.props.currentStep} firstStep={this.props.firstStep} lastStep={this.props.lastStep} previousOnBoarding={this.props.previousOnBoarding} nextOnBoarding={this.props.nextOnBoarding} exitOnBoarding={this.props.exitOnBoarding} content={data[this.props.currentStep].button} />
                </div>
            </div>
        );
  }
}

export default VirtualRobotOB;
