import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';
import { onBoardChallenge } from 'legacy_mock/onBoardingData';
import { onBoardChallengeCN } from 'legacy_mock/onBoardingDataChinese';
import OnboardTitle from 'legacy_components/onBoarding/initialOnBoard/OnboardTitle';
import OnboardText from 'legacy_components/onBoarding/initialOnBoard/OnboardText';
import OnboardProgress from 'legacy_components/onBoarding/initialOnBoard/OnboardProgress';
import OnboardButtons from 'legacy_components/onBoarding/initialOnBoard/OnboardButtons';
import OnboardArrowUp from 'legacy_components/onBoarding/initialOnBoard/OnboardArrowUp';
import ToolboxContainer from 'containers/codepad/ToolboxContainer';
import RunButtonContainer from 'containers/codepad/RunButtonContainer';

class ToolboxOB extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      arrow: {
        position: 'absolute',
        top: 45,
        left: this.props.currentStep == 2 ? '59%' : '53%',
      },
      root: {
        position: 'absolute',
        top: 75,
        left: '42%',
        margin: 'auto',
        padding: '30px',
        width: '300px',
        height: '300px',
        backgroundColor: GeneralPalette.darkPurple,
        borderRadius: 7,
        boxShadow: `0px 2px 3px 0px${GeneralPalette.boxShadow}`,
        zIndex: 500,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        textAlign: 'center',
      },
      dummyCodepad: {
        position: 'fixed',
        width: '33%',
        top: 0,
        left: '33%',
        zIndex: 10
      },
      runbutton: {
        float: 'right',
        marginRight: '2px',
        visibility: 'hidden',
      },
      toolbox: {
        float: 'right',
      }
    };
    let data = onBoardChallenge;
    if (this.props.language === 'CHINESE') {
      data = onBoardChallengeCN;
    }
    return (
            <div>
                <div style={style.dummyCodepad}>
                    <div style={style.runbutton}>
                        <RunButtonContainer />
                    </div>
                    <div style={style.toolbox}>
                        <ToolboxContainer />
                    </div>
                </div>
                <div style={style.arrow}>
                    <OnboardArrowUp />
                </div>
                <div style={style.root}>
                    <OnboardTitle content={data[this.props.currentStep].title} />
                    <OnboardText content={data[this.props.currentStep].text} />
                    <OnboardProgress progress={this.props.currentStep} length={5} />
                    <OnboardButtons onBoarding={this.props.currentStep} lastStep={this.props.lastStep} firstStep={this.props.firstStep} previousOnBoarding={this.props.previousOnBoarding} nextOnBoarding={this.props.nextOnBoarding} exitOnBoarding={this.props.exitOnBoarding} content={data[this.props.currentStep].button} />
                </div>
            </div>
        );
  }
}

export default ToolboxOB;
