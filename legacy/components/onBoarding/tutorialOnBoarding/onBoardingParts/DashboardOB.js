import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';
import { onBoardChallenge } from 'legacy_mock/onBoardingData';
import { onBoardChallengeCN } from 'legacy_mock/onBoardingDataChinese';
import OnboardTitle from 'legacy_components/onBoarding/initialOnBoard/OnboardTitle';
import OnboardText from 'legacy_components/onBoarding/initialOnBoard/OnboardText';
import OnboardProgress from 'legacy_components/onBoarding/initialOnBoard/OnboardProgress';
import OnboardButtons from 'legacy_components/onBoarding/initialOnBoard/OnboardButtons';
import OnboardArrow from 'legacy_components/onBoarding/initialOnBoard/OnboardArrow';
import ChallengeContent from 'legacy_containers/dashboard/ChallengeContent';
import ChallengeHeader from 'legacy_containers/dashboard/ChallengeHeader';

class DashboardOB extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        position: 'absolute',
        top: 10,
        left: '35%',
        margin: 'auto',
        padding: '30px',
        width: '250px',
        height: this.props.currentStep == '6' ? '250px' : '380px',
        backgroundColor: GeneralPalette.darkPurple,
        borderRadius: 7,
        boxShadow: `0px 2px 3px 0px${GeneralPalette.boxShadow}`,
        zIndex: 500,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
      },
      text: {
        display: 'flex',
        alignItems: 'flex-start',
        flexDirection: 'column',
        textAlign: 'left',
        justifyContent: 'space-between'
      },
      bottom: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        textAlign: 'left',
        justifyContent: 'space-between'
      },
      arrow: {
        position: 'absolute',
        top: 10,
        left: '33%',
        zIndex: 300
      },
      dashboard: {
        position: 'fixed',
        width: '28%',
        top: 0,
        zIndex: 1,
        left: '5%'
      }
    };
    let data = onBoardChallenge;
    if (this.props.language === 'CHINESE') {
      data = onBoardChallengeCN;
    }

    return (
            <div>
                <div style={style.dashboard}>
                    <ChallengeHeader />
                    <ChallengeContent style={style.content} />
                </div>
                <div style={style.arrow}><OnboardArrow /></div>
                <div style={style.root}>
                    <div style={style.text}>
                        <OnboardTitle content={data[this.props.currentStep].title} />
                        <OnboardText content={data[this.props.currentStep].text} />
                    </div>
                    <OnboardProgress progress={this.props.currentStep} length={5} />
                    <OnboardButtons onBoarding={this.props.currentStep} nextOnBoarding={this.props.nextOnBoarding} lastStep={this.props.lastStep} firstStep={this.props.firstStep} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} content={data[this.props.currentStep].button} />
                </div>
            </div>
        );
  }
}

export default DashboardOB;
