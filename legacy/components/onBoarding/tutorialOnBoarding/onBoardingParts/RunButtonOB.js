import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';
import { onBoardChallenge } from 'legacy_mock/onBoardingData';
import { onBoardChallengeCN } from 'legacy_mock/onBoardingDataChinese';
import OnboardTitle from 'legacy_components/onBoarding/initialOnBoard/OnboardTitle';
import OnboardText from 'legacy_components/onBoarding/initialOnBoard/OnboardText';
import OnboardProgress from 'legacy_components/onBoarding/initialOnBoard/OnboardProgress';
import OnboardButtons from 'legacy_components/onBoarding/initialOnBoard/OnboardButtons';
import OnboardArrow from 'legacy_components/onBoarding/initialOnBoard/OnboardArrow';
import RunButtonContainer from 'containers/codepad/RunButtonContainer';

class RunButtonOB extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      arrow: {
        position: 'absolute',
        top: 10,
        left: '65.5%',
      },
      root: {
        position: 'absolute',
        top: 8,
        left: '67%',
        margin: 'auto',
        padding: '30px',
        width: '300px',
        height: '300px',
        backgroundColor: GeneralPalette.darkPurple,
        borderRadius: 7,
        boxShadow: `0px 2px 3px 0px${GeneralPalette.boxShadow}`,
        zIndex: 500,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        textAlign: 'center',
      },
      dummyCodepad: {
        position: 'fixed',
        width: '33%',
        top: 0,
        zIndex: 10,
        left: '33%'
      },
      runbutton: {
        float: 'right',
        marginRight: '2px'
      }
    };
    let data = onBoardChallenge;
    if (this.props.language === 'CHINESE') {
      data = onBoardChallengeCN;
    }
    return (
            <div>
                <div style={style.dummyCodepad}>
                    <div style={style.runbutton}>
                        <RunButtonContainer />
                    </div>
                </div>
                <div style={style.arrow}>
                    <OnboardArrow />
                </div>
                <div style={style.root}>
                    <OnboardTitle content={data[this.props.currentStep].title} />
                    <OnboardText content={data[this.props.currentStep].text} />
                    <OnboardProgress progress={this.props.currentStep} length={5} />
                    <OnboardButtons onBoarding={this.props.currentStep} lastStep={this.props.lastStep} firstStep={this.props.firstStep} previousOnBoarding={this.props.previousOnBoarding} nextOnBoarding={this.props.nextOnBoarding} exitOnBoarding={this.props.exitOnBoarding} content={data[this.props.currentStep].button} />
                </div>
            </div>
        );
  }
}

export default RunButtonOB;
