import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';
import { onBoardChallenge } from 'legacy_mock/onBoardingData';
import { onBoardChallengeCN } from 'legacy_mock/onBoardingDataChinese';
import OnboardTitle from 'legacy_components/onBoarding/initialOnBoard/OnboardTitle';
import OnboardText from 'legacy_components/onBoarding/initialOnBoard/OnboardText';
import OnboardProgress from 'legacy_components/onBoarding/initialOnBoard/OnboardProgress';
import OnboardButtons from 'legacy_components/onBoarding/initialOnBoard/OnboardButtons';
import OnboardArrow from 'legacy_components/onBoarding/initialOnBoard/OnboardArrow';
import ChallengeList from 'legacy_containers/dashboard/ChallengeList';
import ChallengeHeader from 'legacy_containers/dashboard/ChallengeHeader';
import { TypeCode, TypeLearn, TypeBuild } from 'legacy_assets';


class ChallengeListOB extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        position: 'absolute',
        top: 10,
        left: '35%',
        margin: 'auto',
        padding: '30px',
        width: '250px',
        height: this.props.currentStep == '6' ? '250px' : '400px',
        backgroundColor: GeneralPalette.darkPurple,
        borderRadius: 7,
        boxShadow: `0px 2px 3px 0px${GeneralPalette.boxShadow}`,
        zIndex: 500,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
      },
      text: {
        display: 'flex',
        alignItems: 'flex-start',
        flexDirection: 'column',
        textAlign: 'left',
        justifyContent: 'space-between'
      },
      bottom: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        textAlign: 'left',
        justifyContent: 'space-between'
      },
      arrow: {
        position: 'absolute',
        top: this.props.currentStep === 7 ? 100 : 10,
        left: '33%',
        zIndex: 300
      },
      types: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-start',
        color: 'white',
        textAlign: 'left',
        fontSize: '15px',
        fontFamily: 'sfns'
      },
      typeicon: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-start',
        color: 'white',
        textAlign: 'left',
        fontSize: '15px',
        fontFamily: 'sfns',
        marginRight: '20px'
      },
      dashboard: {
        position: 'fixed',
        width: '100%',
        top: 0,
        zIndex: 1,
        left: '5%',
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif']
      }
    };

    let overlay = (
            <div style={style.dashboard}>
                <ChallengeHeader />
            </div>
        );
    let drawer = null;
    if (this.props.currentStep === 7) {
      drawer = (
            <div style={style.types}>
                 <div style={style.typeicon}><TypeBuild />&nbsp;BUILD</div>
                 <div style={style.typeicon}><TypeLearn />&nbsp;LEARN</div>
                 <div style={style.typeicon}><TypeCode />&nbsp;CODE</div>
            </div>);
      overlay = (
                <div style={style.dashboard}>
                    <ChallengeHeader />
                    <ChallengeList />
                </div>
            );
    }
    let data = onBoardChallenge;
    if (this.props.language === 'CHINESE') {
      data = onBoardChallengeCN;
    }

    return (
            <div>
                {overlay}
                <div style={style.arrow}><OnboardArrow /></div>
                <div style={style.root}>
                    <div style={style.text}>
                        <OnboardTitle content={data[this.props.currentStep].title} />
                        <OnboardText content={data[this.props.currentStep].text} />
                    </div>
                    {drawer}
                    <OnboardProgress progress={this.props.currentStep - 5} length={2} />
                    <OnboardButtons onBoarding={this.props.currentStep} nextOnBoarding={this.props.nextOnBoarding} lastStep={this.props.lastStep} firstStep={this.props.firstStep} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} content={data[this.props.currentStep].button} />
                </div>
            </div>

        );
  }
}

export default ChallengeListOB;
