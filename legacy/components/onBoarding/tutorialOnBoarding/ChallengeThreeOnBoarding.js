import React from 'react';
import VirtualRobotOB from 'legacy_components/onBoarding/tutorialOnBoarding/onBoardingParts/VirtualRobotOB';

class ChallengeThreeOnBoarding extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (8 < this.props.currentStep < 11) {
      return <VirtualRobotOB language={this.props.language} currentStep={this.props.currentStep} firstStep={8} lastStep={10} previousOnBoarding={this.props.previousOnBoarding} nextOnBoarding={this.props.nextOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />;
    }
  }
}

export default ChallengeThreeOnBoarding;
