import React from 'react';
import FirstChallengeOB from 'legacy_components/onBoarding/tutorialOnBoarding/onBoardingParts/FirstChallengeOB';
import EditorOB from 'legacy_components/onBoarding/tutorialOnBoarding/onBoardingParts/EditorOB';
import RunButtonOB from 'legacy_components/onBoarding/tutorialOnBoarding/onBoardingParts/RunButtonOB';
import ToolboxOB from 'legacy_components/onBoarding/tutorialOnBoarding/onBoardingParts/ToolboxOB';
import OpenToolboxOB from 'legacy_components/onBoarding/tutorialOnBoarding/onBoardingParts/OpenToolboxOB';
import DashboardOB from 'legacy_components/onBoarding/tutorialOnBoarding/onBoardingParts/DashboardOB';


class ChallengeOneOnBoarding extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.currentStep === 0) {
      return <FirstChallengeOB language={this.props.language} currentStep={this.props.currentStep} firstStep={0} lastStep={5} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />;
    }
    else if (this.props.currentStep === 1) {
      return <EditorOB language={this.props.language} currentStep={this.props.currentStep} firstStep={0} lastStep={5} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />;
    }
    else if (this.props.currentStep === 2) {
      return <RunButtonOB language={this.props.language} currentStep={this.props.currentStep} firstStep={0} lastStep={5} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />;
    }
    else if (this.props.currentStep === 3) {
      return <ToolboxOB language={this.props.language} currentStep={this.props.currentStep} firstStep={0} lastStep={5} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />;
    }
    else if (this.props.currentStep === 4) {
      return <OpenToolboxOB language={this.props.language} currentStep={this.props.currentStep} firstStep={0} lastStep={5} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />;
    }
    else if (this.props.currentStep === 5) {
      return <DashboardOB language={this.props.language} currentStep={this.props.currentStep} firstStep={0} lastStep={5} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />;
    }
  }
}

export default ChallengeOneOnBoarding;
