import React from 'react';
import ChallengeOneOnBoarding from 'legacy_components/onBoarding/tutorialOnBoarding/ChallengeOneOnBoarding';
import ChallengeTwoOnBoarding from 'legacy_components/onBoarding/tutorialOnBoarding/ChallengeTwoOnBoarding';
import ChallengeThreeOnBoarding from 'legacy_components/onBoarding/tutorialOnBoarding/ChallengeThreeOnBoarding';

class ChallengeOnBoarding extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (0 <= this.props.currentStep && this.props.currentStep <= 5 && this.props.currentChallenge === 0) {
      return (
                <div>
                    <ChallengeOneOnBoarding language={this.props.language} currentStep={this.props.currentStep} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />
                    <div style={this.props.blurCover} />
                </div>
            );
    }
    else if (6 <= this.props.currentStep && this.props.currentStep <= 7 && this.props.currentChallenge === 1) {
      return (
                <div>
                    <ChallengeTwoOnBoarding language={this.props.language} currentStep={this.props.currentStep} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />
                    <div style={this.props.blurCover} />
                </div>
            );
    }
    else if (8 <= this.props.currentStep && this.props.currentStep <= 10 && this.props.currentChallenge === 2) {
      return (
                <div>
                    <ChallengeThreeOnBoarding language={this.props.language} currentStep={this.props.currentStep} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />
                    <div style={this.props.blurCover} />
                </div>
            );
    }
    else {
      return null;
    }
  }
}

export default ChallengeOnBoarding;
