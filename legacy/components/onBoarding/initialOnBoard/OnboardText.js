import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';


class OnboardText extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        color: GeneralPalette.white,
        fontFamily: 'sfns',
        fontSize: '15px',
      }
    };

    return (
            <pre style={style.root}>
                {this.props.content}
            </pre>
        );
  }
}

export default OnboardText;
