import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import { onBoardingNav } from 'legacy_mock/onBoardingData';
import { onBoardingNavCN } from 'legacy_mock/onBoardingDataChinese';
import OnboardTitle from 'legacy_components/onBoarding/initialOnBoard/OnboardTitle';
import OnboardText from 'legacy_components/onBoarding/initialOnBoard/OnboardText';
import OnboardButtons from 'legacy_components/onBoarding/initialOnBoard/OnboardButtons';

class CastleRockOnBoard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        position: 'absolute',
        top: 0,
        bottom: 150,
        left: 0,
        right: 0,
        margin: 'auto',
        padding: '30px',
        width: '350px',
        height: '250px',
        backgroundColor: GeneralPalette.darkPurple,
        borderRadius: 7,
        boxShadow: `0px 2px 3px 0px${GeneralPalette.boxShadow}`,
        zIndex: 500,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        textAlign: 'center',
      }
    };

    let data = onBoardingNav;
    if (this.props.language === 'CHINESE') {
      data = onBoardingNavCN;
    }

    return (
            <div style={style.root}>
                <OnboardTitle content={data[this.props.onBoarding].title} />
                <OnboardText content={data[this.props.onBoarding].text} />
                <OnboardButtons onBoarding={this.props.onBoarding} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} firstStep={0} lastStep={5} content={data['0'].button} />
            </div>
        );
  }
}

export default CastleRockOnBoard;
