import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';
import TranslatedText from 'legacy_containers/TranslatedText';


class OnboardButtons extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        position: 'relative',
        bottom: 0
      },
      action: {
        backgroundColor: GeneralPalette.green,
        boxShadow: `0px 3px 1px 0px${GeneralPalette.greenShadow}`,
        border: 'none',
        borderRadius: '20px',
        color: GeneralPalette.white,
        fontSize: '15px',
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        textAlign: 'center',
        width: '200px',
        lineHeight: '35px',
        letterSpacing: '0.1em',
        marginBottom: '2px'
      },
      previous: {
        marginTop: '1em',
        borderRadius: '20px',
        color: 'white',
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        fontSize: '13px',
        backgroundColor: 'none'
      },
      skip: {
        borderRadius: '20px',
        color: 'white',
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        fontSize: '10px',
        lineHeight: '12px',
        backgroundColor: 'none',
        marginTop: '1em'
      }
    };

    let backButton;
    if (this.props.onBoarding === this.props.firstStep) {
      backButton = <div style={style.previous} />;
    }
    else {
      backButton = (
        <FlatButton style={style.previous} onClick={() => this.props.previousOnBoarding(this.props.onBoarding - 1)}>
          <TranslatedText text={'go back'} />
        </FlatButton>
            );
    }

    return (
      <div style={style.root}>
        <FlatButton style={style.action} onClick={() => this.props.nextOnBoarding(this.props.onBoarding + 1)}>
          {this.props.content}
        </FlatButton>
        {backButton}
        <FlatButton style={style.skip} onClick={() => this.props.exitOnBoarding(this.props.lastStep + 1)}>
          <TranslatedText text={'skip'} />
        </FlatButton>
      </div>
        );
  }
}

export default OnboardButtons;
