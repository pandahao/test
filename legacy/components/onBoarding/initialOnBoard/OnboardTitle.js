import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';


class OnboardTitle extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        color: GeneralPalette.white,
        fontFamily: 'staticbold',
        fontSize: '30px',
      }
    };

    return (
            <div style={style.root}>
                {this.props.content}
            </div>
        );
  }
}

export default OnboardTitle;
