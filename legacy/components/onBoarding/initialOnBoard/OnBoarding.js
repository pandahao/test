import React from 'react';
import CastleRockOnBoard from 'legacy_components/onBoarding/initialOnBoard/CastleRockOnBoard';
import NavbarOnBoard from 'legacy_components/onBoarding/initialOnBoard/NavbarOnBoard';
import { GeneralPalette } from 'legacy_styles/Colors';

class OnBoarding extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.onBoarding === 0) {
      return <CastleRockOnBoard onBoarding={this.props.onBoarding} language={this.props.language} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />;
    }
    else if (5 > this.props.onBoarding > 0) {
      return <NavbarOnBoard onBoarding={this.props.onBoarding} language={this.props.language} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} exitOnBoarding={this.props.exitOnBoarding} />;
    }
    else {
      return null;
    }
  }
}

export default OnBoarding;
