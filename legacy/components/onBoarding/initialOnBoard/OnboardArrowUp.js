import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';
import { TriangleUp } from 'legacy_assets/index';


class OnboardArrowUp extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        position: 'absolute',
        top: this.props.arrowtop,
        left: this.props.arrowleft,
        zIndex: 600,
      }
    };

    if (this.props.onBoarding != 1)
            { return (
                    <div style={style.root}>
                        <TriangleUp />
                    </div>
                );
    }

    else
            { return (
                    null
                );
    }
  }
}

export default OnboardArrowUp;
