import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';
import { TriangleRight } from 'legacy_assets/index';


class OnboardArrowRight extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        position: 'absolute',
        top: this.props.arrowtop,
        left: this.props.arrowleft,
        zIndex: 600,
      }
    };

    if (this.props.onBoarding != 1)
            { return (
                    <div style={style.root}>
                        <TriangleRight />
                    </div>
                );
    }

    else
            { return (
                    null
                );
    }
  }
}

export default OnboardArrowRight;
