import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import { onBoardingNav } from 'legacy_mock/onBoardingData';
import { onBoardingNavCN } from 'legacy_mock/onBoardingDataChinese';
import OnboardTitle from 'legacy_components/onBoarding/initialOnBoard/OnboardTitle';
import OnboardText from 'legacy_components/onBoarding/initialOnBoard/OnboardText';
import OnboardProgress from 'legacy_components/onBoarding/initialOnBoard/OnboardProgress';
import OnboardButtons from 'legacy_components/onBoarding/initialOnBoard/OnboardButtons';
import OnboardArrow from 'legacy_components/onBoarding/initialOnBoard/OnboardArrow';


class NavbarOnBoard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        position: 'absolute',
        top: 10,
        left: '7%',
        margin: 'auto',
        padding: '30px',
        width: '300px',
        height: '265px',
        backgroundColor: GeneralPalette.darkPurple,
        borderRadius: 7,
        boxShadow: `0px 2px 3px 0px${GeneralPalette.boxShadow}`,
        zIndex: 500,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
      },
      text: {
        display: 'flex',
        alignItems: 'flex-start',
        flexDirection: 'column',
        textAlign: 'left',
        justifyContent: 'space-between'
      },
      bottom: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        textAlign: 'left',
        justifyContent: 'space-between'
      },
      space: {
        marginBottom: 10
      }
    };

    let data = onBoardingNav;
    if (this.props.language === 'CHINESE') {
      data = onBoardingNavCN;
    }

    return (
            <div>
            <OnboardArrow onBoarding={this.props.onBoarding} arrowtop={data[this.props.onBoarding].arrowtop} arrowleft={data[this.props.onBoarding].arrowleft} />
                <div style={style.root}>
                    <div style={style.text}>
                        <OnboardTitle content={data[this.props.onBoarding].title} />
                        <OnboardText content={data[this.props.onBoarding].text} />
                    </div>
                    <div style={style.bottom}>
                        <OnboardProgress progress={this.props.onBoarding} length={4} />
                        <div style={style.space} />
                        <OnboardButtons onBoarding={this.props.onBoarding} nextOnBoarding={this.props.nextOnBoarding} previousOnBoarding={this.props.previousOnBoarding} firstStep={0} lastStep={5} exitOnBoarding={this.props.exitOnBoarding} content={data[this.props.onBoarding].button} />
                    </div>
                </div>
            </div>
        );
  }
}

export default NavbarOnBoard;
