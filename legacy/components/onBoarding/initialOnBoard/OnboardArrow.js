import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import { TriangleLeft } from 'legacy_assets/index';


class OnboardArrow extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        position: 'absolute',
        top: this.props.arrowtop,
        left: this.props.arrowleft,
        zIndex: 600,
      }
    };

    if (this.props.onBoarding != 1)
            { return (
                    <div style={style.root}>
                        <TriangleLeft />
                    </div>
                );
    }

    else
            { return (
                    null
                );
    }
  }
}

export default OnboardArrow;
