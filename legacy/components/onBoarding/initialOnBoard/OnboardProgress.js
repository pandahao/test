import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import { ProgressCircle, FullProgressCircle } from 'legacy_assets';

class OnboardProgress extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      root: {
        color: GeneralPalette.white,
        fontFamily: 'sfns',
        fontSize: '20px',
      },
      space: {
        paddingRight: 5
      }
    };

    const circles = [];
        // progress is 1-4.
    for (let i = 0; i < this.props.length; i++) {
      if (i < this.props.progress) {
        circles.push(<span key={i} style={style.space}><FullProgressCircle /></span>);
      }
      else {
        circles.push(<span key={i} style={style.space}><ProgressCircle /></span>);
      }
    }

    return (
            <div style={style.root}>
                {circles}
            </div>
        );
  }
}

export default OnboardProgress;
