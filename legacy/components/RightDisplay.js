import React from 'react';
import BuildViewContainer from 'legacy_containers/buildview/BuildViewContainer';
import VirtualRobotContainer from 'legacy_containers/virtualrobot/VirtualRobotContainer';
import CodepadContainer from 'containers/codepad/CodepadContainer';
import { GeneralPalette } from 'legacy_styles/Colors';


import { zIndex } from 'styles/zIndex';
import TutorialPopup from 'components/TutorialPopup';


class RightDisplay extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {
    const style = {
      panel: {
        height: '100%',
        display: 'inline-block',
        verticalAlign: 'top'
      },
      codepad: {
        width: '49.1%',
      },
      virtualrobot: {
        width: '50.9%',
      },
      buildView: {
        width: '66.67%'
      }
    };
    //console.log(this.props.tutorialMode);
    const tutorialMode = this.props.tutorialMode === 7;
    const codepadTutorial = this.props.tutorialMode === 8;
    const virtualTutorial = this.props.tutorialMode === 9;
    const robotTutorial = this.props.tutorialMode === 10;
    const eventTutorial = this.props.tutorialMode === 11;
    const quizTutorial = this.props.tutorialMode === 12;

    switch (this.props.challengeType) {
      case 'build':
        return (
          <div style={Object.assign({}, style.panel, style.buildView)}>
            <BuildViewContainer key={`${Math.random()}-${Math.random()}`} />
          </div>
          );
      case 'code' :
        return (
          <div>
            <div style={{
              position : 'absolute',
              top : '50%',
              left : '65%',
              // -1 for behind dashboard
              zIndex : zIndex.medium-1
            }}>
            {
            tutorialMode ? <TutorialPopup mode={'CENTER'}/> : null
            }
            </div>
            <div style={Object.assign({
              //+2 for above nav bar, -2 to stay behind previous tutorial
              zIndex : codepadTutorial ? zIndex.medium+2 : zIndex.medium-2,
              position : 'relative'
            }, style.panel, style.codepad)}>
              {
                codepadTutorial ? <TutorialPopup /> : null
              }

              <CodepadContainer />

            </div>
            <div style={Object.assign({
              //+2 for above nav bar, -2 to stay behind previous tutorial
              zIndex : (virtualTutorial || robotTutorial || eventTutorial) ? zIndex.medium+2 : zIndex.medium-2,
              position : 'relative'
            }, style.panel, style.virtualrobot)}>
              {
                virtualTutorial ? <TutorialPopup mode={'LEFT'}/> : null
              }
              <VirtualRobotContainer tutorialMode={robotTutorial||eventTutorial} />
              {
                robotTutorial? (
                  <div style={{
                  position : 'absolute',
                  zIndex : zIndex.medium+3,
                  top : '64px',
                  height : 'calc(75% - 64px)',
                  width : '100%'
                  }}>
                    <TutorialPopup mode={'LEFT'}/>
                  </div>) : null
              }
              {
                eventTutorial? (
                  <div style={{
                    position : 'absolute',
                    zIndex : zIndex.medium+3,
                    bottom : '0px',
                    height : '25%',
                    width : '100%'
                  }}>
                    <TutorialPopup mode={'LEFTUP'}/>
                  </div>) : null
              }
            </div>
          </div>
        )
      default :
        return (
          <div style={{
            width : 'calc(71% - 64px)',
            height: '100%',
            display: 'inline-block',
            verticalAlign: 'top',
            position : 'absolute',
            backgroundColor : '#f4f4f4',
            textAlign : 'center',
            right: '0px',
            fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
            fontSize : '24px',
            zIndex : zIndex.medium-1,
            color :GeneralPalette.darkPurple
          }}>
          {
            quizTutorial ? (
              <div style={{
                position : 'absolute',
                zIndex : zIndex.medium,
                width : '0px',
                right : '50%',
                top : '50%'
              }}>
                <TutorialPopup mode={'CENTER'}/>
              </div>) : null
          }
            <div style={{
              marginTop:'23%'
            }}>
              <img role="presentation" src="static/media/generic.png"/>
            </div>
          </div>
        )

    }
  }
}

export default RightDisplay;
