import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import ProjectsViewPanelContainer from 'legacy_containers/projects/ProjectsViewPanelContainer';
import ProjectsViewHeaderContainer from 'legacy_containers/projects/ProjectsViewHeaderContainer';

//import ProjectView from 'containers/ProjectView';

const style = {
  panel: {
    display: 'block',
    paddingTop: '20px',
    width: '106%',
    height: '100vh',
    overflow : 'scroll',
    fontFamily: 'Static',
    backgroundColor: GeneralPalette.white,
    textAlign: 'center',
    margin: 'auto',
  }
};

const ProjectsView = () => {
  return (
            <div style={style.panel}>
                <ProjectsViewHeaderContainer />
                <ProjectsViewPanelContainer />
            </div>
        );
};

export default ProjectsView;
