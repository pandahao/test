import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';


export default class ImageLink extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false
    };
  }
  render() {
    const divStyles = {
      position: 'relative',
      paddingBottom: '75%',
      marginBottom: 10,

    };
    const imageStyles = {
      position: 'absolute',
      top: 0, right: 0, bottom: 0, left: 0,
      backgroundSize: 'cover',
      backgroundPosition: 'center center',
      backgroundColor: GeneralPalette.hover,
      opacity: this.state.loaded ? 0 : 100,
      transition: this.props.transition || 'opacity 1.0s ease',
      zIndex: 11
    };
    const imgStyle = {
      position: 'absolute',
    };
    return (
            <div>
                <div style={divStyles}>
                    <img src={this.props.src} onLoad={this._imageOnload.bind(this)} style={Object.assign({}, this.props.style, imgStyle)} />
                    <div style={imageStyles} />
                </div>
            </div>
        );
  }
  _imageOnload() {
    this.setState({ loaded: true });
  }
}
