import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import ProjectItemContainer from 'legacy_containers/projects/ProjectItemContainer';
import TranslatedText from 'legacy_containers/TranslatedText';

const style = {
  space: {
    height: '20px',
  },
  table: {
    borderCollapse: 'collapse',
    textAlign: 'left',
    width: '45em'
  },
  tableHeader: {
    color: GeneralPalette.buttonBackground,
    fontSize: '1em',
    borderBottom: `0.001em solid${GeneralPalette.buttonBackground}`,
    paddingBottom: '20px',
    textTransform: 'uppercase'
  },
};

const ListView = ({ projects }) => {
  return (
        <div>
            <table style={style.table}>
                <tbody>
                <tr>
                    <th style={Object.assign({}, style.tableHeader, { width: '10%' })} />
                    <th style={Object.assign({}, style.tableHeader, { width: '40%' })}> <TranslatedText text={'Project Name'} /></th>
                    <th style={Object.assign({}, style.tableHeader, { width: '21%' })} />
                    <th style={Object.assign({}, style.tableHeader, { width: '21%' })} />
                    <th style={Object.assign({}, style.tableHeader, { width: '3%' })} />
                </tr>
                <tr style={style.space} />
                {projects.map((project) => {
                  return (<ProjectItemContainer key={project.projectID} {...project} />);
                })}
                </tbody>
            </table>
        </div>
    );
};


export default ListView;
