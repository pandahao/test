import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import ProjectItemContainer from 'legacy_containers/projects/ProjectItemContainer';
import ProjectGridComponent from 'containers/ProjectView/ProjectGridComponent';
// const style = {
//   space: {
//     height: '20px',
//   },
//   entiregrid: {
//     display: 'flex',
//     justifyContent: 'flex-start',
//     alignContent: 'flex-start',
//     margin: '10px',
//     width: '85%',
//     flexWrap: 'wrap',
//     height: 'auto',
//   },
// };
//
// const GridView = ({ projects }) => {
//   return (
//         <div style={style.entiregrid}>
//                 {projects.map((project) => {
//                   return (<ProjectItemContainer key={project.projectID} {...project} />);
//                 })}
//         <div style={style.space} />
//         </div>
//     );
// };


export default ProjectGridComponent;
