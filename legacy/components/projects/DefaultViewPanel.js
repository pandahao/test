import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import ListView from 'legacy_components/projects/ListView';
import GridView from 'legacy_components/projects/GridView';

class DefaultViewPanel extends React.Component {
  render() {
    if (this.props.gridView) {
      return (
                <GridView projects={this.props.projects} />
            );
    }
    else {
      return (
                <ListView projects={this.props.projects} />
            );
    }
  }

}


export default DefaultViewPanel;
