import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import TranslatedText from 'legacy_containers/TranslatedText';

import {ListViewSelectedIcon, GridViewSelectedIcon, BackArrow } from 'legacy_assets/listViewIcons.js';
import {GridViewIcon, ListViewIcon} from 'assets/Icons';

import {ChromeEnv} from 'config';
const style = {
  root: {
    display: 'flex',
    width: '80%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  backButton: {
    color: GeneralPalette.darkPurple,
    backgroundColor: 'white',
    borderRadius: '20px',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: 'auto',
    border: 'none',
    height: '80%',
    paddingLeft: '10px',
    paddingRight: '10px',
    hoverColor: GeneralPalette.hover,
    boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
  },
  text: {
    fontSize: 15,
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    marginLeft: 10,
    marginTop: 0
  },
  view: {
    display: 'flex',
    justifyContent: 'space-around',
    width: '200px',
    paddingBottom: '0px'
  },
  viewToggle: {
    cursor : 'pointer',
    zoom: 1.5,
    width : '45px',
    height : '45px',
    margin: 'none',
    padding: 'none',
    display : 'inline-block',
    backgroundColor: GeneralPalette.white
  },
  fakeroot: {
    display: 'flex',
    paddingTop: '24px',
    backgroundColor: GeneralPalette.white,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  title: {
    display : 'inline-block',
    padding: '50px',
    color: GeneralPalette.darkPurple,
    fontSize: '36px',
    paddingTop: '0px'
  }

};

const ProjectsViewHeader = ({ projectListView, gridView, goBack, listView, toggleGridView, blockView }) => {
  return (
        <div style={style.fakeroot}>
            <div style={style.root}>
                <div style={{  width: '180px'}}>
                </div>
                <div style={style.title}>
                    <TranslatedText text={'PROJECTS'} />
                </div>
                <div style={style.view}>
                  {(() => {
                    if(ChromeEnv){
                      return ''
                    }
                    if (!gridView) {
                      return (
                              <div>
                                  <div style={style.viewToggle} onClick={toggleGridView}>
                                      <GridViewIcon />
                                  </div>
                                  <div style={style.viewToggle} >
                                      <ListViewIcon color={GeneralPalette.brightPurple} />
                                  </div>
                              </div>
                          );
                    }
                    else if (gridView) {
                      return (
                              <div>
                                  <div style={style.viewToggle} >
                                      <GridViewIcon color={GeneralPalette.brightPurple} />
                                  </div>
                                  <div style={style.viewToggle} onClick={listView}>
                                      <ListViewIcon />
                                  </div>
                              </div>
                         );
                    }
                  })()}
                  </div>
            </div>
        </div>
    );

    // Render the robot view
    // else if (robotView) {
    //     return (
                // <div style = {style.root}>
                //     <div>
                //         <FlatButton style = {style.backButton}>
                //             <BackArrow/>
                //             <span style={style.text}>
                //                 BACK TO HOME
                //             </span>
                //         </FlatButton>
                //     </div>
                //     <div style = {style.view}>
                //         <IconButton style = {style.viewToggle}>
                //             <ListViewIcon/>
                //         </IconButton>
                //         <IconButton style = {style.viewToggle} disabled = {true}>
                //             <GridViewSelectedIcon/>
                //         </IconButton>
                //     </div>
                // </div>
    //     );
    // };
};


export default ProjectsViewHeader;
