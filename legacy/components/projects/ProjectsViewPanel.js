import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import DefaultViewPanelContainer from 'legacy_containers/projects/DefaultViewPanelContainer';
import CircularProgress from 'material-ui/CircularProgress';
import TranslatedText from 'legacy_containers/TranslatedText';

const style = {
  panel: {
    display: 'flex',
    backgroundColor: GeneralPalette.white,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: '100px',
    width: '100%'
  }
};

class ProjectsViewPanel extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this.props.loadProjects();
  }

  render() {
    if (this.props.projectStatus === 'loading' || this.props.userStatus === 'loading') {
      return (
                    <div style={style.panel}>
                        <div style={style.title}>
                            <CircularProgress color={GeneralPalette.brightPurple} />
                        </div>
                    </div>
                );
    }
    else {
      {
        return (
                <div style={style.panel}>
                  <DefaultViewPanelContainer />
                </div>
              );
      }
    }
  }

}

export default ProjectsViewPanel;
