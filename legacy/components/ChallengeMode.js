import React from 'react';
// import ProjectsView from 'legacy_components/projects/ProjectsView';
import ProjectView from 'containers/ProjectView';

import OverviewContainer from 'legacy_containers/overview/OverviewContainer';
import ChallengeViewContainer from 'legacy_containers/ChallengeViewContainer';
import { GeneralPalette } from 'legacy_styles/Colors';

class ChallengeMode extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      panel: {
        height: '100%',
        display: 'inline-block',
        verticalAlign: 'top'
      },
      projectView: {
        width: '96%'
      },
      overview: {
        width: '100%',
        overflow: 'scroll',
        backgroundColor: GeneralPalette.menuBG,
        height: '100vh'
      }
    };
    if (this.props.view === 0 || this.props.view === 1) {
      return (
               <div style={Object.assign({}, style.panel, style.projectView)}>
                   <ProjectView />
               </div>
           );
    }
    else if (this.props.view === 2) {
      return (
               <div style={Object.assign({}, style.panel, style.overview)}>
                   <OverviewContainer />
               </div>
           );
    }
    else if (this.props.view === 3) {
      return (
                <div style={Object.assign({}, style.panel, { width: '100%' })}>
                    <ChallengeViewContainer />
                </div>
           );
    }
  }
}

export default ChallengeMode;
