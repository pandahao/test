import React from 'react';
import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';

const Event = ({ arr }) => {
  const style = {
    root: {
      marginTop: '7px',
      marginBottom: '9px',
      padding: '20px 10px 10px 10px',
      backgroundColor: ChallengePalette.contentBackground,
      marginLeft: '-45px',
      width: '95%'
    },
    title: {
      fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
      marginBottom: '18px',
      color: GeneralPalette.brightPurple
    }
  };
  return (
        <div style={style.root}>
        {
            arr.map((event) => {
              return (
                    <div key={event.key}>
                        <div style={style.title}>{event.title}</div>
                        <ul style={{ marginBottom: '18px' }}>
                            <li><strong>Emitted When:</strong> {event.emitted}</li>
                            <li><strong>Event Source:</strong> {event.source}</li>
                            <li><strong>Event Type:</strong> {event.type}</li>
                            <li><strong>Event Data:</strong> <div dangerouslySetInnerHTML={{ __html: event.data.split('newline').join('<br/>') }} /></li>
                        </ul>
                    </div>
                );
            })
        }
        </div>
    );
};

export default Event;
