import React from 'react';
import { ChallengePalette, GeneralPalette } from 'legacy_styles/Colors';
import IconButton from 'material-ui/IconButton';
import ContentClear from 'material-ui/svg-icons/content/clear';
import TranslatedText from 'legacy_containers/TranslatedText';

const DocHeader = ({ toggleContent, freemode }) => {
  const style = {
    title: {
      marginLeft: '12px',
      color: GeneralPalette.darkPurple,
      letterSpacing: '4.2px',
      fontSize: '14px',
      fontFamily: 'staticbold'
    },
    root: {
      width: '27.33%',
      backgroundColor: ChallengePalette.topBar,
      position: 'absolute',
      boxShadow: `0px 0px 20px -2px${GeneralPalette.buttonBackground}`
    },
    bar: {
      display: 'flex',
      alignItems: 'center',
      height: '64px',
      marginLeft: '10px',
      cursor: 'pointer',
      closeIcon: {
        marginTop: '0px'
      },
    }
  };
  return (
    <div style={style.root}>
      <div style={style.bar}>
        <div style={style.bar.closeIcon}>
          <IconButton onClick={() => toggleContent(freemode)} iconStyle={{ fill: GeneralPalette.darkPurple }}>
            <ContentClear />
          </IconButton>
        </div>
        <span style={style.title}><TranslatedText text={'DOCUMENTATION'} /></span>
      </div>
    </div>
    );
};

export default DocHeader;
