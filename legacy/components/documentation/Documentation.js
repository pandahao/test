import React from 'react';
import { ChallengePalette, CastleRockPalette } from 'legacy_styles/Colors';

import DocumentationHeader from 'legacy_containers/documentation/DocumentationHeader';
import DocumentationPanel from 'legacy_containers/documentation/DocumentationPanel';

const Documentation = () => {
  const style = {
    panel: {
      display: 'inline-block',
      width: '100%',
      height: '100vh',
      verticalAlign: 'top',
      overflow: 'auto',
      backgroundColor: ChallengePalette.mainBackground,
      overflowY: 'auto',
      overflowX: 'hidden',
      zIndex: 300
    },
    footer: {
      height: '3%',
      width: '100%',
      bottom: '0%'
    }
  };
  return (
      <div style={style.panel}>
          <DocumentationHeader />
          <DocumentationPanel />
          <div style={style.footer} />
      </div>
    );
};

export default Documentation;
