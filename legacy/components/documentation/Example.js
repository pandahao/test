import React from 'react';
import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';

const Example = ({ arr }) => {
  const style = {
    root: {
      marginTop: '7px',
      marginBottom: '9px',
      padding: '20px 10px 10px 10px',
      backgroundColor: ChallengePalette.contentBackground,
      marginLeft: '-45px',
      width: '95%'
    },
    step: {
      marginBottom: '18px',
      fontWeight: 900
    },
    text: {
      marginBottom: '18px'
    },
    label: {
      fontFamily: 'sfns',
      marginBottom: '6px',
      marginLeft: '-18px'
    },
    code: {
      fontFamily: 'menlo',
      fontSize: '12px',
      marginBottom: '20px',
      color: GeneralPalette.brightPurple,
      marginLeft: '-18px'
    }
  };
  return (
        <div style={style.root}>
            {
                arr.map((example) => {
                  return (
                        <div key={example.key}>
                            <div style={style.step}>{example.step}</div>
                            <ul style={{ paddingLeft: '30px' }}>
                            {
                                example.content.map((content) => {
                                  return (
                                        <div key={content.key}>
                                            <li>
                                            <div style={style.text}>{content.text}</div>
                                            </li>
                                            <div style={style.label}>Sample Code:</div>
                                            <div style={style.code} dangerouslySetInnerHTML={{ __html: content.code.split('newline').join('<br/>').split('indent').join('&emsp;') }} />
                                        </div>
                                    );
                                })
                            }
                            </ul>
                        </div>
                    );
                })
            }
        </div>
    );
};

export default Example;
