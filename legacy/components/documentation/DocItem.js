import React from 'react';
import { ChallengePalette, CastleRockPalette } from 'legacy_styles/Colors';
import Event from './Event';
import Command from './Command';
import Example from './Example';
import { Cross, Minus } from 'legacy_assets/docViewIcons';

const DocItem = ({ num, title, content, onClick, opendocs, onExtraClick }) => {
  const myComponents = {
    Event,
    '事件' : Event,
    'Example Usage': Example,
    '示例' : Example,
    '指令': Command,
    'Command Functions': Command
  };
  const style = {
    item: {
      marginLeft: '3px',
      marginTop: '7px',
      fontFamily: 'sfns',
      fontSize: '14px'

    },
    click: {
      cursor: 'pointer'
    },
    sub: {
      marginLeft: '32px',
      marginTop: '5px',
      fontFamily: 'sfns',
      fontSize: '14px'
    },
    cross: {
      transitionDuration: '0.5s',
      transitionProperty: 'transform',
      transformOrigin: '50% 50%',
      transform: 'rotate(90deg)'
    },
    wrapper: {
      display: 'inline-block',
      marginRight: '20px'
    },
    innerwrapper: {
      display: 'inline-block',
      marginRight: '16px'
    },
    regular: {
      transitionDuration: '0.5s',
      transitionProperty: 'transform',
      transformOrigin: '50% 50%',
      transform: 'rotate(45deg)'
    }
  };
  return (
        <div style={style.item}>
            <div onClick={onClick.bind(null, num - 1)} style={style.click}>
            <div style={style.wrapper}>
                {!opendocs[num - 1].open ? <Cross /> : <Minus />}
            </div>
            <div style={{ display: 'inline-block' }}>{title}</div>
            </div>
            {
                content.map((section) => {
                  const Classname = myComponents[section.name];
                  return (
                        <div key={section.key}>
                            {opendocs[num - 1].open ? <div style={style.sub}>
                                <div style={style.click} onClick={onExtraClick.bind(null, num - 1, section.name)}>
                                <div style={style.innerwrapper}>
                                    {opendocs[num - 1].inner.indexOf(section.name) == -1 ? <Cross /> : <Minus />}
                                </div>
                                <div style={{ display: 'inline-block' }}>{section.name}</div>
                                {opendocs[num - 1].inner.indexOf(section.name) != -1 ? <Classname arr={section.content} /> : null}
                                </div>
                            </div> : null}
                        </div>
                    );
                })
            }

        </div>
    );
};

export default DocItem;
