import React from 'react';
import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';

const Command = ({ arr }) => {
  const style = {
    root: {
      marginTop: '7px',
      marginBottom: '9px',
      padding: '20px 10px 10px 10px',
      backgroundColor: ChallengePalette.contentBackground,
      marginLeft: '-45px',
      width: '95%'
    },
    title: {
      fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
      marginBottom: '18px',
      color: GeneralPalette.brightPurple,
    }
  };
  return (
        <div style={style.root}>
        {
            arr.map((func) => {
              return (
                    <div key={func.key}>
                        <div style={style.title}>{func.title}</div>
                        <ul style={{ marginBottom: '18px' }}>
                            <li>{func.content}</li>
                        </ul>
                    </div>
                );
            })
        }
        </div>
    );
};

export default Command;
