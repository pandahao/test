import React from 'react';
import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';
import DocumentationItem from 'legacy_containers/documentation/DocumentationItem';
import CircularProgress from 'material-ui/CircularProgress';


const style = {
  root: {
    display: 'inline-block',
    width: '100%',
    height: '100%',
    color: CastleRockPalette.textDefault,
    paddingTop: '4em',
  },
  title: {
    color: ChallengePalette.titleBar,
    letterSpacing: '4.0px',
    fontSize: '14px',
    marginTop: '45px'
  },
  progress: {
    padding: '38%',
    color: GeneralPalette.darkPurple,
    fontSize: '40px',
    paddingTop: '20px',
  },
  content: {
    width: '100%',
    padding: '20px 14px 20px 25px',
    background: ChallengePalette.mainBackground,
  }
};
const DocPanel = ({ status,docs,loadDocs }) => {
  if (status === 'loading') {
    loadDocs();
    return (
      <div style={style.root}>
        <div style={style.progress}>
          <CircularProgress color={GeneralPalette.brightPurple} />
        </div>
      </div>
        );
  }
  if (status === 'success') {
    return (
      <div style={style.root}>
        <div style={style.content}>
          {
                      docs.map((doc) => {
                        return (<DocumentationItem key={doc.num} {...doc} />);
                      })
                  }
        </div>
      </div>
        );
  }
};

export default DocPanel;
