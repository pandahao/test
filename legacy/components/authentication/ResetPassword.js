import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import LicenseAgreement from 'legacy_mock/LicenseAgreement';
import Checkbox from 'material-ui/Checkbox';
import { EmptyCheckBox, CheckBox } from 'legacy_assets';
import TextInput from './TextInput';
import TranslatedText from 'legacy_containers/TranslatedText';

const style = {
  center: {
    paddingTop: '80px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: '100%',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    position:'relative',
    top:'14%',
    left:'-11%',
    width:'80%',
    // left:'-5%',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  button: {
    backgroundColor: GeneralPalette.brightPurple,
    // boxShadow: `0px 1.5px 2px 1px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    paddingTop: '2px',
    width: '200px',
    marginTop: '20px',
    cursor: 'pointer'
  },
  cancel: {
    backgroundColor:'white',
    border:'1px solid #ccc',
    color: GeneralPalette.brightPurple,
    // boxShadow: `0px 1.5px 1px 1px${GeneralPalette.whiteShadow}`,
    // color: GeneralPalette.darkPurple,
    width: '200px',
    marginTop: '20px'
  },
  disable: {
    backgroundColor:'white',
    // boxShadow: '0px 1.5px 2px 1px grey',
    color: GeneralPalette.underline,
    cursor: 'default'
  },
  errorMessage: {
    fontFamily: 'sfns',
    color: ToolbeltPalette.compileError,
    fontWeight: 100,
    fontSize: '12px',
    marginTop: '10px',
    height: '20px'
  },
  title: {
    fontFamily: 'menlo',
    fontSize: '18px',
    marginBottom: '10px'
  },
  text: {
    boxShadow: `0px 0px 0px 1px${GeneralPalette.codepadBG}`,
    padding: '6px 6px 6px 6px',
    fontFamily: 'menlo',
    fontSize: '12px',
    width: '80vh',
    height: '35vh',
    overflowX: 'hidden',
    overflowY: 'auto',
    wordWrap: 'break-word'
  },
  check: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '500px',
    fontFamily: 'sfns',
  }
};


const ResetPassword = ({ newPassChange, confirmChange, errorMessage, resetPassword, resendCode, cancel }) => {
  return (
    <div style={style.center}>
      <div style={style.content}>
        {/* <img style={style.image} src="static/media/logo2.png" /> */}
        <TextInput text={<TranslatedText text={'Confirmation Code'} />} onChange={confirmChange} />
        <TextInput text={<TranslatedText text={'New Password'} />} onChange={newPassChange} />
        <div style={style.errorMessage}> {errorMessage} </div>
      </div>
      <FlatButton style={style.button} onClick={resetPassword}>
        <TranslatedText text={'RESET PASSWORD'} />
      </FlatButton>
      <FlatButton style={Object.assign({}, style.button, style.cancel)} onClick={resendCode}>
        <TranslatedText text={'RESEND CODE'} />
      </FlatButton>
      <FlatButton style={Object.assign({}, style.button, style.cancel, { width: '100px' })} onClick={cancel}>
        <TranslatedText text={'CANCEL'} />
      </FlatButton>
    </div>
    );
};


export default ResetPassword;
