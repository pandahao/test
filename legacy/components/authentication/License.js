import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import LicenseAgreement from 'legacy_mock/LicenseAgreement';
import Checkbox from 'material-ui/Checkbox';
import { CheckBox } from 'legacy_assets';
import TranslatedText from 'legacy_containers/TranslatedText';
import { t } from 'legacy_utils/i18n';

const style = {
  center: {
    // paddingTop: '150px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    // width:'80%',
    // marginBottom:'30%',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'left',
    position:'relative',
    // margin:'auto',
    // marginTop:'20%',
    left:'20%',
    width:'80%',
    // marginLeft:'28%'
  },
  button: {
    backgroundColor: GeneralPalette.brightPurple,
    // boxShadow: `0px 0px 0px 0px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    paddingTop: '2px',
    width: '180px',
    marginTop: '20px',
    cursor: 'pointer',
    marginTop:'-5%',
    marginLeft:'45%',
  },
  cancel: {
    backgroundColor: 'white',
    border:'1px solid #50c3ff',
    color: '#50c3ff',
    width: '180px',
    marginTop: '20px',
    marginLeft:'-15%',
  },
  disable: {
    backgroundColor: GeneralPalette.darkPurple,
    color: GeneralPalette.underline,
    cursor: 'default'
  },
  errorMessage: {
    fontFamily: 'sfns',
    color: ToolbeltPalette.compileError,
    fontWeight: 100,
    fontSize: '12px',
    marginTop: '10px',
    height: '20px'
  },
  title: {
    fontFamily: 'menlo',
    fontSize: '30px',
    marginBottom: '10px',
    color:'#686E84',
    // fontWeight:'bold',
    letterSpacing:3,
  },
  text: {
    backgroundColor:'#f5f5f5',
    padding: '6px 6px 6px 16px',
    fontFamily: 'menlo',
    fontSize: '12px',
    width: '80%',
    height: '20vh',
    overflowX: 'auto',
    overflowY: 'auto',
    wordWrap: 'break-word',
    borderRadius:10,
    // height:'10%',
    // wordWrap : 'normal',
  },
  check: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '600px',
    fontFamily: 'sfns',
    fontSize:12,
    letterSpacing:1,
    fontWeight:'bold',
    color:'#686E84',
  }

};

class License extends React.Component {
  render() {
    let buttonStyle = style.button;
    if (!this.props.isChecked) buttonStyle = Object.assign({}, style.button, style.disable);
    let buttonIcon = null;
    if (this.props.isChecked) buttonIcon = <CheckBox />;
    return (
      <div style={style.center}>
        <div style={style.content}>
          <div style={style.title}>
            <TranslatedText text="Agreement" />
          </div>
          <pre style={style.text}>
            {
              LicenseAgreement(this.props.lan)
            }
          </pre>
          <div style={style.check}>
            <Checkbox
              label={
                t(
                  'I have read and accept the ROBOTERRA Terms & Conditions',
                  localStorage.getItem('skylineDefaultLanguage')
                )
              }
              onCheck={this.props.setCheck}
              defaultChecked={this.props.isChecked}
            />
          </div>
          <div style={style.errorMessage}> {this.props.errorMessage} </div>
        </div>
        <FlatButton style={Object.assign({}, style.button, style.cancel)} onClick={this.props.cancel}>
          <TranslatedText text={'CANCEL'} />
        </FlatButton>
        <FlatButton style={buttonStyle} disabled={!this.props.isChecked} onClick={this.props.proceed}>
          <TranslatedText text={'NEXT'} />
        </FlatButton>
      </div>
        );
  }
}

export default License;
