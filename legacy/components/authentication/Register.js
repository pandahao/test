import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';

import ExtraRegisterContainer from 'legacy_containers/authentication/ExtraRegisterContainer';
import TextInput from './TextInput';
import TranslatedText from 'legacy_containers/TranslatedText';
import { debounce } from 'underscore';

import { REGISTER_ONE, LICENSE } from 'legacy_actions/AuthActions';

const style = {
  root: {
    //  margin: 'auto',
    //  width: '40%',
     height: '100%',
    //  marginTop:110,
    position:'relative',
    left:'18%',
    top:'8%',
    //  width:'80%',
   },
   roots: {
    //   margin: 'auto',
    //   width: '40%',
    //   height: '100%',
    //   marginTop:150,
     position:'relative',
     top:'15%',
    //  left:'-10%',
    //   width:'80%',
    },
   center: {
     paddingTop: '150px',
     display: 'flex',
     flexDirection: 'column',
     alignItems: 'center',
   },
  //  image: {
  //    width: '23%',
  //    marginRight: '30px',
  //    marginLeft: '30px',
  //  },
   content: {
     display: 'flex',
     flexDirection: 'row',
     alignItems: 'center'
   },
   label: {
     // fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
     // fontSize: '30px',
     // color: GeneralPalette.darkPurple,
     textAlign: 'left',
     marginBottom: '10px',
     color:GeneralPalette.darkPurple,
    fontSize:'32px',
    fontFamily:'.SF NS display',
    letterSpacing:3
   },
   fields: {
     display: 'flex',
     alignItems: 'flex-start',
     flexDirection: 'column'
   },
   // link: {
   //   display: 'inline-block',
   //   textDecoration: 'underline',
   //   fontSize: '12px',
   //   fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
   //   color: GeneralPalette.buttonBackground,
   // },
   // button: {
   //   backgroundColor: GeneralPalette.brightPurple,
   //   boxShadow: `0px 1.5px 2px 1px${GeneralPalette.purpleShadow}`,
   //   border: 'none',
   //   borderRadius: '20px',
   //   color: 'white',
   //   fontSize: '15px',
   //   fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
   //   textAlign: 'center',
   //   lineHeight: '35px',
   //   paddingTop: '2px',
   //   width: '200px',
   //   marginTop: '50px',
   //   cursor: 'pointer'
   // },
   cancel: {
     backgroundColor: GeneralPalette.menuBG,
     boxShadow: `0px 1.5px 1px 1px${GeneralPalette.whiteShadow}`,
     color: GeneralPalette.darkPurple,
     width: '100px',
     marginTop: '20px'
   },
   errorMessage: {
     fontFamily: 'sfns',
     color: ToolbeltPalette.compileError,
     fontWeight: 100,
     fontSize: '12px',
     marginTop: '10px',
     height: '20px'
   },
   puts:{
     display:'flex',
     flexDirection:'column'
    },
    input:{
     display:'flex',
     letterSpacing:3,
     // flexDirection:'row',
     // marginRight:'15px'
    },
    inputs:{
     display:'flex',
     flexDirection:'row',
     letterSpacing:3,
     // marginLeft:'-15px'
   },
   in:{
     width:'30px',
   },
  foot:{
    display:'flex',
    flexDirection:'row',
    marginTop:15
   },
   link: {
     display: 'inline-block',
     fontSize: '12px',
     fontFamily: 'sfns',
     cursor: 'pointer',
     color: GeneralPalette.darkPurple,
     opacity: '0.5',
     marginLeft:15,
     marginTop:10,
    //  letterSpacing:6,
   },
  button: {
    backgroundColor: GeneralPalette.brightPurple,
    boxShadow: `0px 0px 0px 0px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
   //  paddingTop: '2px',
    width: '150px',
    cursor: 'pointer',
    marginLeft:'-272px',
    letterSpacing:3,
  },
};

class Register extends React.Component {

  render() {
    let rootStyle = style.root;
    if (this.props.authLocation == LICENSE) { rootStyle = Object.assign({}, style.roots, { width: '60%' }); }
    return (

          <div style={rootStyle}>
              {this.props.authLocation == REGISTER_ONE ?
                  <div style={style.center}>
                   <div style={style.content}>
                        <div style={style.fields}>
                            <div style={style.label}><TranslatedText text='Please create an account' /></div>
                              {/* <div style={style.input}>
                               <TextInput text={<TranslatedText text={'FirstName'} />} />
                              <TextInput type={'LastName'} text={<TranslatedText text={'LastName'} />}  />
                              </div>
                              <div style={style.inputs}
                              <TextInput type={'password'} text={<TranslatedText text={'Password'} />} onChange={this.props.pass1Change} />
                              <TextInput type={'password'} text={<TranslatedText text={'Confirm Password'} />} onChange={this.props.pass2Change} />
                            </div> */}
                            <div style={style.puts}>
                <div style={style.input}>
                    <TextInput  text={<TranslatedText text={'FirstName'} />} value={this.props.firstName}  onChange={this.props.updateFirstname} />
                    <div style={style.in}></div>
                    <TextInput onChange={this.props.updateLastname} value={this.props.lastName} type={'LastName'} text={<TranslatedText text={'LastName'} style={style.in} />}  />
                </div>
                 <div style={style.inputs}>
                    <TextInput type={'Email'} text={<TranslatedText text={'Email'} />} value={this.props.email} onChange={this.props.emailChange}
                    />
                  <div style={style.in}></div>
                    <TextInput type={'PassWord'} text={<TranslatedText text={'Password'}  />}  value={this.props.password} onChange={this.props.updatePass} />
                 </div>
               </div>
                            <div style={style.errorMessage}> {this.props.errorMessage} </div>
                        </div>
                   </div>
                  <div style={style.foot}>
                       <FlatButton style={style.button} onClick={debounce(this.props.register, 1000, true)}>
                         <TranslatedText text={'CREATE'} />
                       </FlatButton>
                       <a  style={style.link} onClick={this.props.cancel} ><TranslatedText text={'I already have an account'}  /></a>
                 </div>
             </div> : <ExtraRegisterContainer cancel={this.props.cancel} />}
          </div>
        );
  }

}

export default Register;
