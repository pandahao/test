import { REGISTER_TWO } from 'legacy_actions/AuthActions';
import License from 'legacy_components/authentication/License';

import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import TextInput from './TextInput';

const style = {
  center: {
    paddingTop: '150px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  image: {
    width: '23%',
    marginRight: '30px',
    marginLeft: '30px'
  },
  content: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  label: {
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    fontSize: '30px',
    color: GeneralPalette.darkPurple,
    textAlign: 'left',
    marginBottom: '10px',
  },
  fields: {
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column'
  },
  button: {
    backgroundColor: GeneralPalette.brightPurple,
    boxShadow: `0px 1.5px 2px 1px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    paddingTop: '2px',
    width: '200px',
    marginTop: '50px',
    cursor: 'pointer'
  },
  cancel: {
    backgroundColor: GeneralPalette.menuBG,
    boxShadow: `0px 1.5px 1px 1px${GeneralPalette.whiteShadow}`,
    color: GeneralPalette.darkPurple,
    width: '100px',
    marginTop: '20px'
  },
  errorMessage: {
    fontFamily: 'sfns',
    color: ToolbeltPalette.compileError,
    fontWeight: 100,
    fontSize: '12px',
    marginTop: '10px',
    height: '20px'
  }

};

class ExtraRegister extends React.Component {

  render() {
    return (
      <div>
        {this.props.authLocation == REGISTER_TWO ?
          <div style={style.center}>
            <div style={style.content}>
              <img role="presentation" style={style.image} src="static/media/logo.png" />
              <div style={style.fields}>
                <div style={style.label}>Almost there!</div>
                <TextInput text={'First Name'} onChange={this.props.firstChange} />
                <TextInput text={'Last Name'} onChange={this.props.lastChange} />
                <TextInput text={'Date of Birth'} onChange={this.props.birthChange} />
                <TextInput text={'Preferred Language'} onChange={this.props.langChange} />
                <div style={style.errorMessage}> {this.props.errorMessage} </div>
              </div>
            </div>
            <FlatButton style={style.button} onClick={this.props.nextStep}>
                          NEXT STEP
            </FlatButton>
            <FlatButton style={Object.assign({}, style.button, style.cancel)} onClick={this.props.cancel}>
                          CANCEL
            </FlatButton>
          </div> : <License lan={this.props.lan} cancel={this.props.cancel} proceed={this.props.proceed} errorMessage={this.props.errorMessage} isChecked={this.props.isChecked} setCheck={this.props.setCheck} />
                }
      </div>
        );
  }
}

export default ExtraRegister;
