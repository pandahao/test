import React from 'react';
import TextField from 'material-ui/TextField';
import { GeneralPalette } from 'legacy_styles/Colors';

const TextInput = ({ text, type, onChange, defaultValue }) => {
  const _style = {
    input: {
      textAlign: 'center',
      color: GeneralPalette.buttonBackground,
      fontWeight: 200
    },
  };
  return (
        <div>
            <TextField style={_style.input}
              hintText={text}
              type={type}
              underlineFocusStyle={{ borderColor: GeneralPalette.brightPurple }}
              onChange={onChange}
              defaultValue={defaultValue}
            />
        </div>
    );
};

export default TextInput;
