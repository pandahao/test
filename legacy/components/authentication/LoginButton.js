import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import CircularProgress from 'material-ui/CircularProgress';
import TranslatedText from 'legacy_containers/TranslatedText';
import { LOGIN } from 'legacy_actions/AuthActions';
import { debounce } from 'underscore';

const style = {
  button: {
    backgroundColor: GeneralPalette.brightPurple,
    // boxShadow: `0px 1.5px 2px 1px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    paddingTop: '2px',
    width: '150px',
    cursor: 'pointer',
    marginLeft:'-8%',
  },
  load: {
  }
};
let button;

class LoginButton extends React.Component {

    // handleKeyDown (e) {
    //     if(!this.props.loggedIn){
    //         if(e.keyCode == 13) {
    //             this.props.login(this.props.username,this.props.password);
    //         };
    //     };
    // };
    //
    // componentDidMount() {
    //     if(!this.props.loggedIn){
    //         window.addEventListener("keydown", this.handleKeyDown.bind(this));
    //     }
    // };
    //
    // componentWillUnmount() {
    //     if(this.props.loggedIn){
    //         window.removeEventListener("keydown", this.handleKeyDown.bind(this));
    //     }
    // };

  render() {
    if (this.props.fetchStatus === 'fetching') {
      button = <div style={style.load}><CircularProgress color={GeneralPalette.brightPurple} /></div>;
    }
    else {
      button = (
        <FlatButton style={style.button} onClick={debounce(() => this.props.login(this.props.username, this.props.password), 200, true)}>
          <TranslatedText text={'LOG IN'} />
        </FlatButton>
            );
    }
    return (
      <div>
        {button}
      </div>
        );
  }

}

export default LoginButton;
