import React from 'react';
import Dashboard from 'containers/Dashboard';
import RightDisplayContainer from 'legacy_containers/RightDisplayContainer';
import Documentation from 'legacy_components/documentation/Documentation';
import { GeneralPalette } from 'legacy_styles/Colors';
import {zIndex} from '../../src/styles/zIndex';

class ChallengeView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      panel: {
        //position : 'absolute',
        height: '100%',
        display: 'inline-block',
        verticalAlign: 'top'
      },
      dashboard: {
        width: '29.33%',
      },
      rightSide: {
        width: '70.67%',
        WebkitFilter: this.props.blurContent ? 'blur(3px)' : ''
      },
      blurCover: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: zIndex.mediumHigh
      },
    };

    const dashboardContent = this.props.docView ? <Documentation /> : <Dashboard />;
    const hideToolbox = this.props.toolboxStatus ? this.props.closeToolbox : null;
    const blurCover = this.props.blurContent ? <div style={style.blurCover} /> : null;

    return (
      <div onClick={this.props.closeToolbox} >
        <div style={Object.assign({}, style.panel, style.dashboard)}>
          {dashboardContent}
        </div>
        <div style={Object.assign({}, style.panel, style.rightSide)}>
          <RightDisplayContainer />
        </div>
      </div>
       );
  }
}

export default ChallengeView;
