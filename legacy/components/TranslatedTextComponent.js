import React from 'react';
import { t } from 'legacy_utils/i18n';

class TranslatedTextComponent extends React.Component {
  render() {
    let translation = this.props.text;
    if (this.props.language !== 'ENGLISH') {
      translation = t(translation, 'CHINESE');
    }
    return (
      <span style={{ fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'] }}>{translation}</span>
    );
  }
}

export default TranslatedTextComponent;
