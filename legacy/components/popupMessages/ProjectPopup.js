import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import { XMark, CheckmarkGreen } from 'legacy_assets/codepadIcons';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import TranslatedText from 'legacy_containers/TranslatedText'; //* *

class ProjectPopup extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      panel: {
        position: 'absolute',
        top: 0,
        bottom: 150,
        left: 0,
        right: 0,
        margin: 'auto',
        width: '600px',
        height: this.props.moreUnlock ? '430px' : '330px',
        background:'url(./static/media/project_completed.gif)',
        borderRadius: 7,
        boxShadow: `0px 2px 30px 0px${GeneralPalette.boxShadow}`,
        zIndex: 500,
      },
      XMark: {
        width: 15,
        height: 15,
        left: '96%',
        top: '2%',
        zoom: 0.8,
        cursor: 'pointer',
        position: 'relative'
      },
      content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
      checkMark: {
        zoom: 2,
        flexDirection: 'row',
        alignItems: 'center',
        textAlign: 'center',
        marginTop: '1%'
      },
      congrats: {
        fontFamily: 'staticbold',
        color: GeneralPalette.menuBG,
        fontSize: 30,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center',
        padding: '10px 50px 10px 50px',
        lineHeight: '25px'
      },
      message: {
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        color: GeneralPalette.menuBG,
        fontSize: 25,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center',
        padding: '10px 50px 10px 50px',
        lineHeight: '25px'
      },
      subMessage: {
        fontFamily: 'sfns',
        fontWeight: 100,
        color: GeneralPalette.menuBG,
        fontSize: 17,
        textAlign: 'center',
        lineHeight: '20px',
        margin: '10px 0px 50px 0px'
      },
      buttonGroup: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '65%'
      },
      button: {
        outline: 'none',
        position: 'relative',
        cursor: 'pointer',
        backgroundColor: 'white',
        // boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
        border: 'none',
        borderRadius: '20px',
        fontSize: '15px',
        textAlign: 'center',
        lineHeight: '35px',
        padding: '0px 20px 0px 20px',
        marginBottom: '15px',
        color: GeneralPalette.darkPurple,
        width: '180px',
        fontFamily: 'staticbold'
      },
      greenButton: {
        outline: 'none',
        position: 'relative',
        cursor: 'pointer',
        backgroundColor: GeneralPalette.green,
        boxShadow: `0px 3px 1px 0px${GeneralPalette.greenShadow}`,
        border: 'none',
        borderRadius: '20px',
        fontSize: '17px',
        textAlign: 'center',
        lineHeight: '35px',
        padding: '0px 20px 0px 20px',
        marginBottom: '15px',
        color: GeneralPalette.white,
        width: '370px',
        fontFamily: 'staticbold'
      },
    };
    let content;
    if (this.props.moreUnlock) {
      content = (
                <div>
                    <div style={style.message}>
                        You finished {this.props.projectName} and unlocked a new project.
                    </div>
                    <div style={style.subMessage}>
                    <TranslatedText text={'Next step: build your own robot in free mode or start a new project.'} />   /*Next step: build your own robot in <br />free mode or start a new project.*/

                    </div>
                    <div style={style.content}>
                        <FlatButton style={style.greenButton} onClick={() => this.props.nextProject(this.props.next, this.props.nextSet, this.props.nextStatus, this.props.nextProgress)}>
                              <TranslatedText text={'NEXT PROJECT'} />
                        </FlatButton>
                    </div>
                </div>
            );
    }
    else {
      content = (
                <div>
                    <div style={style.message}>
                        <TranslatedText text={'You have completed this project!'} />
                    </div>
                    <div style={style.subMessage}>
                        <TranslatedText text={'Next step: build your own robot in free mode or look over other projects.'} />
                    </div>
                </div>
            );
    }
    return (
            <div style={style.panel}>
                <div style={style.XMark} onClick={this.props.closePopup}>
                    <XMark />
                </div>
                <div style={style.content}>
                    <div style={style.checkMark}>
                        <CheckmarkGreen />
                    </div>
                    <div style={style.congrats}><TranslatedText text={'Congratulations!'} /></div>
                    {content}
                    <div style={style.buttonGroup}>
                        <FlatButton style={style.button} onClick={this.props.goToFreemode}>
                            <TranslatedText text={'BUILD MY OWN'} />
                        </FlatButton>
                        <FlatButton style={style.button} onClick={this.props.viewProjects}>
                             <TranslatedText text={'VIEW PROJECT LIST'} />
                        </FlatButton>
                    </div>
                </div>
            </div>
        );
  }
}

export default ProjectPopup;
