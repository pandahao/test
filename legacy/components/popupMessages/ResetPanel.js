import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import FlatButton from 'material-ui/FlatButton';
import TranslatedText from 'legacy_containers/TranslatedText';

class ResetPanel extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      panel: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        margin: 'auto',
        width: '600px',
        height: '220px',
        backgroundColor: GeneralPalette.darkPurple,
        borderRadius: 7,
        boxShadow: `0px 2px 30px 0px${GeneralPalette.boxShadow}`,
        zIndex: 500
      },
      message: {
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        fontWeight: 100,
        color: GeneralPalette.white,
        fontSize: 25,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center',
        padding: '10px 50px 10px 50px',
        marginTop: '5%'
      },
      buttonGroup: {
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems: 'center',
        margin: 'auto',
        marginTop: '5%',
        fontFamily: 'staticbold',
        width: '70%'
      },
      button: {
        outline: 'none',
        position: 'relative',
        cursor: 'pointer',
        backgroundColor: 'white',
        boxShadow: `0px 2px 0px 0px${GeneralPalette.buttonBackground}`,
        border: 'none',
        borderRadius: '20px',
        fontSize: '15px',
        textAlign: 'center',
        lineHeight: '35px',
        padding: '0px 20px 0px 20px',
        marginBottom: '15px',
        color: GeneralPalette.darkPurple,
        width: '200px',
        fontFamily: 'staticbold'
      },
    };

    let message;
    if (this.props.resetWarning) {
      message = <TranslatedText text={'Are you sure you want to restart to the default template? All changes will be lost.'} />;
    }
    else {
      message = <TranslatedText text={'Are you sure you want to restart the project? All changes will be lost.'} />;
    }
    return (
            <div style={style.panel}>
                <div style={style.message}>
                    {message}
                </div>
                <div style={style.buttonGroup}>
                    <FlatButton style={style.button} onClick={this.props.resetWarning ? this.props.closeWarning : this.props.closeReset}>
                        <TranslatedText text={'NO, KEEP SAVED CHANGES'} />
                    </FlatButton>

                    <FlatButton style={style.button} onClick={this.props.resetWarning ? this.props.resetCode : () => this.props.resetProject(this.props.currentProjectIndex)}>
                        <TranslatedText text={'YES, RESTART TO DEFAULT'} />
                    </FlatButton>
                </div>
            </div>
        );
  }
}

export default ResetPanel;
