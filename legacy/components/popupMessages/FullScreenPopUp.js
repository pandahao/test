import React from 'react';

import ResetPanelContainer from 'legacy_containers/popupMessages/ResetPanelContainer';
import ProjectPopupContainer from 'legacy_containers/popupMessages/ProjectPopupContainer';
import OnBoardingContainer from 'legacy_containers/onBoarding/OnBoardingContainer';
import ChallengeOnBoardingContainer from 'legacy_containers/onBoarding/ChallengeOnBoardingContainer';

import TutorialPopup from '../../../src/components/TutorialPopup';
import { zIndex } from '../../../src/styles/zIndex';


class FullScreenPopUp extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      blurCover: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 300
      },
    };
    if (this.props.resetWarning || this.props.resetProject) {
      return (
        <div>
          <ResetPanelContainer />
          {
            // <div style={style.blurCover} />
          }
        </div>);
    }
    else if (this.props.completionPopup) {
      return (
        <div>
          <ProjectPopupContainer moreUnlock={this.props.moreUnlock} />
          {
            // <div style={style.blurCover} />
          }
        </div>
      );
    }
    else if (this.props.userOnBoard) {
      return (
        <div style={{
          position : 'absolute',
          width : '0px',
          top : '50%',
          left : '50%',
          height : '0px',
          zIndex : zIndex.mediumHigh
        }}>
          <TutorialPopup mode="CENTER"/>
        </div>
      )

    }
    // else if (this.props.index === 0) {
    //   return <ChallengeOnBoardingContainer blurCover={style.blurCover} />;
    // }
    else {
      return null;
    }
  }
}

export default FullScreenPopUp;
