import React from 'react';
import { ErraSidebar, ProjectsIcon, ProjectsSelectedIcon, FreeModeIcon, FreeModeSelectedIcon, SettingsIcon,
  SettingsSelectedIcon } from 'legacy_assets/navbarIcons';
import {UpdateIcon} from 'assets/Icons';
import { GeneralPalette } from 'legacy_styles/Colors';
import IconButton from 'material-ui/IconButton';
// import ActionHome from 'material-ui/svg-icons/action/home';

import { LearnIcon,LearnsIcon,HomeIcon,HomesIcon } from '../../src/assets/Icons';
import { t } from 'legacy_utils/i18n';

import { zIndex } from 'styles/zIndex';
import TutorialPopup from 'components/TutorialPopup';



const style = {
  root: {
    display: 'block',
    textAlign: 'center',
    width: '64px',
    height: '100vh',
    background: GeneralPalette.menuBG,
    position: 'fixed',
    // boxShadow: `0px 0px 15px 1px${GeneralPalette.whiteShadow}`,
    zIndex: zIndex.medium+1,
  },
  square: {
    width: '100%',
    height: '64px',
    // backgroundColor: '#50C3FF',
    background : GeneralPalette.brightPurple,
    // opacity : this.state.showHelp ? 1 : 0
    zIndex: zIndex.medium,
    cursor: 'pointer',
  },
  icona: {
    marginTop: '1.3em',
    fontFamily:'.AppleSystemUIFont',
    fontSize:12,
    color:'#50C3FF',
    textAlign:'center',
    marginLeft:'0.6em',
  },
  icons: {
    marginTop: '-0.1em',
    fontFamily:'.AppleSystemUIFont',
    fontSize:12,
    color:'#50C3FF',
    textAlign:'center',
    marginLeft:'0.3em',
  },
  iconss: {
    marginTop: '0.6em',
    fontFamily:'.AppleSystemUIFont',
    fontSize:12,
    color:'#50C3FF',
    textAlign:'center',
    marginLeft:'0.3em',
  },
  iconb: {
    marginTop: '0.3em',
    position : 'relative',
    fontFamily:'.AppleSystemUIFont',
    fontSize:12,
    zIndex : zIndex.medium,
    color:'#50C3FF',
    textAlign:'center',
    //marginLeft:'0.0em',
  },
  settingIcon: {
    position:'absolute',
    left:'15%',
    bottom:'1em',
    fontFamily:'.AppleSystemUIFont',
    fontSize:12,
    color:'#50C3FF',
    textAlign:'center',
  },
  logoIcon: {
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  image: {
    paddingTop: '2px',
    width: 'auto',
    height: '56px',
    cursor: 'pointer'
  },
};

class Navbar extends React.Component {
  constructor(props) {
    super(props);
  }
  // handleUpdate(){
  //   if(window.confirm('Quit and update now?')){
  //     window.SkylineUpdater.quitAndInstall();
  //   }
  // }
  // renderUpdateIcon(){
  //   return (
  //     <div onClick={this.handleUpdate}>
  //       <UpdateIcon/>
  //     <div>
  //   );
  // }
  render(){
    const tutorialMode = this.props.tutorialMode === 2;
    const learnTutorial = this.props.tutorialMode === 3;

    const home = (this.props.view === 'HOME_VIEW') ?
            (<IconButton  disabled color={GeneralPalette.brightPurple} >
              <HomeIcon />
            </IconButton>) :
            (<IconButton  disableTouchRipple color={GeneralPalette.darkPurple}  onClick={this.props.goToWelcome}>
              <HomesIcon />
            </IconButton>);
    const learn = (this.props.view === 0 && !this.props.erraMode) ?
        <IconButton  disabled color={GeneralPalette.brightPurple} >
          <LearnIcon />
        </IconButton> :
        <IconButton  disableTouchRipple  color={GeneralPalette.darkPurple}  onClick={this.props.openProjectListView}>
            <LearnsIcon />
        </IconButton>;

    const freemode = this.props.view === 2 ?
      <IconButton  disabled color={GeneralPalette.brightPurple} >
        <FreeModeSelectedIcon />
      </IconButton> :
        <IconButton  disableTouchRipple color={GeneralPalette.darkPurple}  onClick={this.props.gotoFreemode}>
          <FreeModeIcon />
        </IconButton>;

    const erra = (this.props.view === 0 && this.props.erraMode) ?
      <IconButton disabled color={GeneralPalette.brightPurple} >
        <ErraSidebar selected />
      </IconButton> :
        <IconButton  disableTouchRipple color={GeneralPalette.darkPurple}
          onClick={this.props.gotoErraProjects}>
          <ErraSidebar />
        </IconButton>;

    const settings = (this.props.view === 1) ?
      <IconButton  disabled color={GeneralPalette.brightPurple}>
        <SettingsSelectedIcon />
      </IconButton> :
        <IconButton  disableTouchRipple color={GeneralPalette.darkPurple} onClick={this.props.goToSettings}>
          <SettingsIcon />
        </IconButton>;

    const showUpdateIcon = this.props.showUpdateIcon;

    return (
      <div style={Object.assign({},style.root, tutorialMode ? {
        zIndex: zIndex.mediumHigh
      } : {})}>
        {
          tutorialMode ? <TutorialPopup/> : null
        }
        <div style={style.square} onClick={this.props.goToHome}>
          <div style={style.logoIcon}>
            <img style={style.image} src="static/media/logo.png" />
          </div>
        </div>
        <div style={style.icona}>
          {home}
        </div>
        <div style={style.iconb}>
          {
            learnTutorial ? <TutorialPopup optionalNextAction={this.props.openProjectListView}/> : null
          }
          {learn}
        </div>

        <div style={style.icons}>
          {erra}
        </div>

        <div style={style.icons}>
          {freemode}
        </div>

        <div style={style.icons}>
          {
            showUpdateIcon ? this.renderUpdateIcon() : null
          }
        </div>

        <div style={style.settingIcon}>
          {settings}
        </div>
      </div>
        );
  }
}

export default Navbar;
