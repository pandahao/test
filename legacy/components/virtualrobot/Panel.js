import React from 'react';
import Dot from './Dot';
const Panel = ({ panel, dot, content, style }) => {
  const genContent = content ? content : () => {};

  const _style = {
    root: {
      margin: 5,
      display: 'inline-block',
      position: 'relative',
      minWidth: '5.8%',
      borderRadius: '5px',
    }
  };
  return (
        <div style={Object.assign(_style.root, panel, style)}>
            <Dot style={dot} top={false} left />
            <Dot style={dot} top={false} left={false} />
            <Dot style={dot} top left />
            <Dot style={dot} top left={false} />
            {
                genContent()
            }
        </div>
    );
};

export default Panel;
