/*eslint-disable*/
import React from 'react';
import ReactGridLayout, { WidthProvider } from 'react-grid-layout';

const Grid = WidthProvider(ReactGridLayout);

import RobotPart from 'legacy_containers/virtualrobot/RobotPart';
import { EditorPalette } from 'legacy_styles/Colors';

class Robot extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    console.log('Mounting VirtualRobot');
    this.props.connect();
  }
  componentWillUnmount() {
    console.log('Unmounting VirtualRobot');
    this.props.disconnect();
  }
  componentDidUpdate() {
    window.dispatchEvent(new Event('resize'));
    // if (!this.props.uploading && !this.props.connected) {
    //   this.props.connect();
    // }
  }

  render() {
    const _style = {
      height: 'calc(100% - 200px)',
      paddingLeft: '0.2em',
      paddingRight: '0.2em',
      width: '96%'
      // marginLeft: freemode ? (docView ? '0%' : '12%') : '0%',
    };

    return (
      <Grid
        style={_style}
        measureBeforeMount
        layout={this.props.layout}
        cols={this.props.freemode ? this.props.docView ? 6 : 8 : 6}
        rowHeight={72}
      >
        <RobotPart key="PORT_1" port="PORT_1" />
        <RobotPart key="PORT_2" port="PORT_2" />
        <RobotPart key="PORT_3" port="PORT_3" />
        <RobotPart key="PORT_4" port="PORT_4" />
        <RobotPart key="PORT_5" port="PORT_5" />
        <RobotPart key="PORT_6" port="PORT_6" />
        <RobotPart key="PORT_7" port="PORT_7" />
        <RobotPart key="DIO_1" port="DIO_1" />
        <RobotPart key="DIO_2" port="DIO_2" />
        <RobotPart key="DIO_3" port="DIO_3" />
        <RobotPart key="DIO_4" port="DIO_4" />
        <RobotPart key="DIO_5" port="DIO_5" />
        <RobotPart key="DIO_6" port="DIO_6" />
        <RobotPart key="DIO_7" port="DIO_7" />
        <RobotPart key="DIO_8" port="DIO_8" />
        <RobotPart key="DIO_9" port="DIO_9" />
        <RobotPart key="IR_TRAN" port="IR_TRAN" />
        <RobotPart key="AI_1" port="AI_1" />
        <RobotPart key="AI_2" port="AI_2" />
        <RobotPart key="SERVO_A" port="SERVO_A" />
        <RobotPart key="SERVO_B" port="SERVO_B" />
        <RobotPart key="SERVO_C" port="SERVO_C" />
        <RobotPart key="SERVO_D" port="SERVO_D" />
        <RobotPart key="MOTOR_A" port="MOTOR_A" />
        <RobotPart key="MOTOR_B" port="MOTOR_B" />
      </Grid>
    );
  }
}

export default Robot;
