import React, { Component }  from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import {zIndex} from '../../../src/styles/zIndex';
import SnapshotConsole from './SnapshotConsole';
import {VelocityTransitionGroup} from 'velocity-react';

const rootStyle = {
  width : '100%',
  height : '100%',
  position : 'absolute',
  backgroundColor : 'rgba(0,0,0,0)',
  zIndex : zIndex.medium
}
class Snapshot extends Component{
  constructor(props){
    super(props);
    this.state = {
      showShot : true,
      showConsole : false
    }
  }
  shouldComponentUpdate(){
    return true;
  }
  componentDidMount(){
    const $ = this;
    //$.forceUpdate();
    window.setTimeout(()=>{
      $.forceUpdate();
      $.setState({
        showShot : false
      })
      $.forceUpdate();
      window.setTimeout(()=>{
        $.setState({showConsole : true})
      },1100)
    },50)
    // $.forceUpdate();
  }
  renderConsole(){
    const consoleStyle = {
      width : '100%',
      height : 'calc(100% - 64px)',
      //marginTop : '64px',
      backgroundColor : GeneralPalette.white,
      zIndex : zIndex.summit,
      overflowY : 'scroll',
      overflowX : 'hidden'
    }
    return (<div style={consoleStyle}>
      <SnapshotConsole {...this.props}/>
    </div>)
  }
  render(){
    const shotStyle = {
      width : '100%',
      height : '100%',
      position : 'absolute',
      backgroundColor : GeneralPalette.white,
      zIndex : zIndex.medium,
      transition : 'opacity 1s',
      opacity : this.state.showShot ? 1 : 0,
    }
    return(<div style={rootStyle}>
      <VelocityTransitionGroup
      enter={{animation:"slideDown"}}
      leave={{animation:"slideUp"}}
      >
      {
        this.state.showConsole ? this.renderConsole() : undefined
      }
      </VelocityTransitionGroup>
      <div style={shotStyle}/>
    </div>)
  }
}

export default Snapshot;
