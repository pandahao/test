import React from 'react';


const Dot = ({ style, top, left }) => {
  const _style = {
    root: {
      width: 5,
      height: 5,
      display: 'inline-block',
      position: 'absolute',
      borderRadius: 5,
      margin: 4,
      bottom: top ? undefined : 0,
      right: left ? undefined : 0
    }
  };
  return (
        <div style={Object.assign(_style.root, style)} />
    );
};

export default Dot;
