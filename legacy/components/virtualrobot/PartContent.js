import React from 'react';

import Button from './parts/Button';
import Led from './parts/Led';
import Servo from './parts/Servo';
import Tape from './parts/Tape';
import Light from './parts/Light';
import Sound from './parts/Sound';
import Motor from './parts/Motor';
import Irr from './parts/Irr';
import Irt from './parts/Irt';
import Joystick from './parts/Joystick';

import { RobotPalette } from 'legacy_styles/Colors';

const PartContent = ({ device, style }) => {
  const _style = {
    root: {
      width: '100%',
      height: '100%',
      position: 'relative',
      fontFamily: 'Static'
    },
    center: {
      position: 'relative',
      top: '50%',
      transform: 'translateY(-50%)'
    },
    block: {
      display: 'inline-block',
      width: `calc( 80% / ${device.size})`,
      height: '100%',
      position: 'relative',
      verticalAlign: 'bottom'
    },
    align: {
      display: 'inline-block',
      width: `calc( 80% / ${device.size})`,
      position: 'relative',
      verticalAlign: 'bottom'
    },
    icon: {
      position: 'relative',
      display: 'inline-block',
      left: '5%',
      height: 'wrap-content',
      marginLeft: '-12px'
    },
    message: {
      title: {
        color: device.event_name === 'DEACTIVATE' ? RobotPalette.deactivateText : '#FFFFFF',
        display: 'block',
        marginTop: '50%',
        margin: 'auto',
        letterSpacing: '2px',
        fontSize: '.84vw'
      },
      content: {
        color: device.event_name === 'DEACTIVATE' ? RobotPalette.deactivateText : '#FFFFFF',
        display: 'block',
        margin: 'auto',
        zoom: 1.5,
        fontSize: '17px',
        fontSize: '0.8vw'
      }
    },
    label: {
      position: 'absolute',
      textAlign: 'center',
      top: '80%',
      left: 'calc(100% - 12px)',
      // marginLeft : 202,
      color: device.event_name === 'DEACTIVATE' ? RobotPalette.deactivateText : '#FFFFFF',
      fontSize: '8px',
      transformOrigin: '0 0',
      transform: 'rotate(-90deg)'
    }
  };

  const PartIcon = () => {
    switch (device.name) {
      case 'button':
        return <Button style={_style.icon} device={device} />;
      case 'led':
        return <Led style={_style.icon} device={device} />;
      case 'tape':
        return <Tape style={_style.icon} device={device} />;
      case 'light':
        return <Light style={_style.icon} device={device} />;
      case 'sound':
        return <Sound style={_style.icon} device={device} />;
      case 'servo':
        return <Servo style={_style.icon} device={device} />;
      case 'motor':
        return <Motor style={_style.icon} device={device} />;
      case 'irr':
        return <Irr style={_style.icon} device={device} />;
      case 'joy':
        return <Joystick style={_style.icon} device={device} />;
      case 'irt':
        return <Irt style={_style.icon} device={device} />;
      default:
        return <div style={_style.icon}>{`Sorry, ${device.name} icon is missing`}</div>;
    }
  };
  return (
    <div style={Object.assign({}, _style.root, style)}>
      <div style={_style.center}>
        <div style={_style.align}>
          <PartIcon />
        </div>
        {device.msg.map((m, index) => {
          // console.log(device);
          return (
            <div key={index} style={_style.align}>
              <div style={_style.message.title}>
                <div>{m.label.toUpperCase()}</div>
              </div>
              <div style={_style.message.content}>
                {m.label.toUpperCase() === device.name.toUpperCase() ? 'N/A' : m.value}
              </div>
            </div>
          );
        })}
        <div style={_style.label}>
          <div>{device.port}</div>
        </div>
      </div>
    </div>
  );
};

export default PartContent;
