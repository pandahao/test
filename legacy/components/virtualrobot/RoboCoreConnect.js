import React from 'react';
import Radium from 'radium';
import Panel from './Panel';
import TranslatedText from 'legacy_containers/TranslatedText';

import { StatePalette, CastleRockPalette } from 'legacy_styles/Colors';


const RoboCoreConnect = ({ freemode, docView, blurFromListDisplay, virtualrobotblur }) => {
  const panel = {
    position: 'absolute',
    marginLeft: freemode ? (docView ? '10%' : '20%') : '10%',
    marginTop: '15px',
    width: freemode ? (docView ? '80%' : '60%') : '80%',
    height: '100px',
    zIndex: 20,
    backgroundColor: StatePalette.background,
  };
  const dot = {
    backgroundColor: StatePalette.text,
  };

  const RoboCoreConnectContent = () => {
    const root = {
      position: 'relative',
      textAlign: 'center',
      width: '100%',
      height: '100%',
      color: StatePalette.text,

    };

    const block = {
      textAlign: 'center',
      verticalAlign: 'middle',
      margin: 'auto',
      position: 'relative',
      top: '50%',
      transform: 'translateY(-50%)',
      fontSize: '20px',
      fontFamily: 'Static'
    };

    return (
            <div style={root}>
                <div style={block}>
                    <TranslatedText text={'Your RoboCore is not connected'} />.
                </div>
            </div>
        );
  };

  const _style = {
    maxWidth: '100%',
    WebkitFilter: virtualrobotblur ? 'blur(3px)' : '',
    WebkitFilter: blurFromListDisplay ? 'blur(3px)' : ''
  };

  return (
        <Panel style={_style} panel={panel} dot={dot} content={RoboCoreConnectContent} />
    );
};

export default Radium(RoboCoreConnect);
