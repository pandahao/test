import React, { Component } from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import TranslatedText from 'legacy_containers/TranslatedText';
import ReactList from 'legacy_lib/ReactList';
import { CastleRockPalette, ConsolePalette, GeneralPalette } from 'legacy_styles/Colors';
import { DotsIcon } from 'legacy_assets';
import {VelocityTransitionGroup} from 'velocity-react';

import Part from './Part';

import _ from 'underscore';


class SnapshotConsole extends Component{
  constructor(props){
    super(props);
    this.state = {
      tabIndex: 0,
    };
  }

  renderDevice(){
    const {snapshotRobot} = this.props
    const robot = Object.keys(snapshotRobot).map(k=>{
      return snapshotRobot[k]
    }).filter(ob=>{
      return ob.hasDevice;
    })

    const deviceRootStyle={
      padding : '1em',
      display : 'flex',
      flexDirection : 'column',
      justifyContent : 'space-around'
    }
    const partStyle={
      margin: 'auto',
      width : `90%`,
    }
    const deviceStyle=(size)=>{
      return {
        width : `100%`,
        height : '85px'
      }
    }

    return (
      <div style={deviceRootStyle}>
      {
          robot.map((part,index)=>{
            return (
              <div style={partStyle} key={index}>
                <Part
                  port={part.device.port}
                  device={part.device}
                  hasDevice={true}
                  style={deviceStyle(part.device.size)}
                />
              </div>
            )
          })
      }
      </div>
    )
  }



  renderList(){
    const history = this.props.snapshotEvent;
    const variableName = this.props.snapshotVariableName;
    const {freemode,docView} = this.props;
    const _style = {
      tableProps: {
        width : '100%',
        borderCollapse: 'collapse',
        tableLayout: 'fixed',
        textAlign:'center',
      },
      list: {
        height: '100%',
        display: 'block',
        marginTop: 30,
        overflow: 'scroll',
        zIndex: -1
      },
      listItem: {
        display: 'table-row-group',
        marginTop: 5,
      },
      padding: {
        paddingLeft: freemode ? (docView ? '0em' : '1.9em') : '0em',
        paddingBottom: '0.4em',
        minWidth: freemode ? (docView ? '150px' : '210px') : '150px',
        // textAlign:'center',
      },
      indexPadding: {
        color: CastleRockPalette.border,
        borderRight: '1px solid',
        paddingBottom: '0.4em'
      },
      index: {
        color: ConsolePalette.index,
        display: 'table-cell',
        width: 35,
        paddingLeft: '1.5em',
        fontSize: 12
      },
      port: {
        color: ConsolePalette.port,
        background: ConsolePalette.portBackground,
        display: 'table-cell',
        paddingLeft: '3px',
        paddingRight: '3px',
        fontSize: 12
      },
      events: {
        color: ConsolePalette.events,
        background: ConsolePalette.eventsBackground,
        display: 'table-cell',
        paddingLeft: '3px',
        paddingRight: '3px',
        fontSize: 12
      },
      data: {
        color: ConsolePalette.data,
        background: ConsolePalette.dataBackground,
        display: 'table-cell',
        paddingLeft: '3px',
        paddingRight: '3px',
        fontSize: 12,
        // marginLeft:'10%',
        // display:'inlineBlock',
      }
    };
    function portInfo(varName, history, index, port) {
      if (!variableName[history[index].port]) {
        return `${history[index].port}`;
      }
      if (variableName[history[index].port].name === '') {
        return `${history[index].port}`;
      }
      else {
        return `${history[index].port}` + ` (${shortenVariableName(variableName[history[index].port].name, freemode)}):`;
      }
    }
        // Function to shorten variable name if too long
    function shortenVariableName(variableName, freemode) {
      if (!freemode) {
        if (variableName.length > 10) {
          const result = `${variableName.slice(0, 8)}...`;
          return result;
        }
        return variableName;
      }
      else {
        if (variableName.length > 15) {
          const result = `${variableName.slice(0, 13)}...`;
          return result;
        }
        return variableName;
      }
    }
    const eventRootStyle={
      marginLeft: '0em',
      position : "absolute",
      top : '50px'
    }
    const eventHeaderStyle={
      root: {
        marginTop: '0.5em',
        marginBottom :'0.5em',
        paddingTop: '0.5em',
        paddingBottom : '0.5em',
        width:'100%',
        display: 'table-caption',
        display: 'flex',
        justifyContent : 'space-around',
        'boxShadow': `0 5px 3px 0px ${GeneralPalette.whiteShadow}`
      }
    }
    return (
      <div style={eventRootStyle}>
      <table style={_style.tableProps}>
        <div style={eventHeaderStyle.root}>
          <div>SOURCE</div>
          <div>EVENT</div>
          <div>DATA</div>
        </div>
        <tbody>
          {
             history.map((hist, index) => {
               return (
                 <tr key={index} style={_style.listItem}>
                   <td style={_style.indexPadding}>
                     <span style={_style.index}>{`${history.length - index}`}</span>
                   </td>
                   <td style={_style.padding} >
                     <span style={_style.port}>
                       {portInfo(variableName, history, index)}
                     </span>
                   </td>
                   <td style={_style.padding}>
                     <span style={_style.events}>{`${hist.event_name}`}</span>
                   </td>
                   <td>
                     <span style={_style.data}>{`${hist.msg ? hist.msg[0].value : ''}`}</span>
                   </td>
                   <td>
                     <span style={_style.data}>{`${hist.msg ? hist.msg[1] ? hist.msg[1].value : '' : ''}`}</span>
                   </td>
                 </tr>
                 );
             })
         }
        </tbody>
      </table>
      </div>
    )
  }
  switchTab(value){
    this.setState((pre,props)=>{
      return {
        tabIndex : value
      }
    })
  }
  render(){

    const rootStyle = {
      width : '100%',
      height : '100%',
      position : 'relative',
      color : GeneralPalette.darkPurple,
      backgroundColor : GeneralPalette.white
    }
    const headerStyles={
      root :{
        height : '50px',
        width : '100%',
        backgroundColor : GeneralPalette.white,
        display : 'flex',
        justifyContent : 'space-around'
      },
      tab : {
        width : '6em',
        textAlign : 'center',
        backgroundColor : GeneralPalette.white,
        height : '100%',
        cursor : 'pointer',
        //marginLeft : '1em',
        //marginRight : '1em',
        display : 'inline-block'
      },
      tabText:{
        marginTop : '1em'
      }
    };
    return (
      <div style={rootStyle}>
        <div style={headerStyles.root}>
          <div>
            <div
              style={Object.assign({},headerStyles.tab,{
                borderBottom: this.state.tabIndex===0 ? '3px solid' : '',
              })}
              onClick={this.switchTab.bind(this, 0)}
            >
              <div style={headerStyles.tabText}><TranslatedText text={'MODULES'}/></div>
            </div>
            <div
              style={Object.assign({},headerStyles.tab,{
                borderBottom: this.state.tabIndex===1 ? '3px solid' : '',
              })}
              onClick={this.switchTab.bind(this, 1)}
            >
              <div style={headerStyles.tabText}><TranslatedText text={'EVENTS'}/></div>
            </div>
          </div>
        </div>
        {
          // <VelocityTransitionGroup
          // enter={{animation : 'slideDown'}}
          // leave={{animation : 'slideUp'}}
          // >
          // {
          //   this.state.tabIndex === 0 ?
          //   this.renderDevice() :  this.renderList()
          // }
          // </VelocityTransitionGroup>

        }
        {
          this.state.tabIndex === 0 ?
          this.renderDevice() :  this.renderList()
        }

      </div>
    )
  }
}

export default SnapshotConsole;
