import React from 'react';

import Panel from './Panel';
import PartContent from './PartContent';
import { CastleRockPalette, StatePalette, RobotPalette } from 'legacy_styles/Colors';

const Part = ({ port, style, hasDevice, device }) => {
  const deactivated = hasDevice ? device.event_name === 'DEACTIVATE' : false;
  const _style = {
    panel: hasDevice
      ? {
          backgroundColor: deactivated ? RobotPalette.deactivate : RobotPalette[device.name],
          boxShadow: deactivated ? 'none' : `0px 3px 3px ${RobotPalette[`${device.name}Shadow`]}`,
          border: deactivated ? `1px solid${StatePalette.text}` : 'none'
        }
      : {
          backgroundColor: CastleRockPalette.white,
          color: StatePalette.text,
          border: '1px solid '
        },
    dot: hasDevice
      ? {
          backgroundColor: CastleRockPalette.white,
          color: deactivated ? StatePalette.text : 'none',
          border: deactivated ? '1px solid ' : 'none'
        }
      : {
          backgroundColor: CastleRockPalette.white,
          color: StatePalette.text,
          border: '1px solid ',
          width: 5,
          height: 5,
          borderRadius: 5,
          margin: 4
        },
    content: {
      textAlign: 'center',
      position: 'relative',
      width: '100%',
      height: '100%'
    },
    empty: {
      top: '40%',
      textAlign: 'center',
      margin: 'auto',
      fontSize: '.87vw'
    }
  };

  const content = () => {
    if (!hasDevice) {
      return (
        <div style={Object.assign(_style.empty, _style.content)}>
          <div>{port}</div>
        </div>
      );
    }
    return (
      <div style={_style.content}>
        <PartContent device={device} />
      </div>
    );
  };

  return <Panel style={style} panel={_style.panel} dot={_style.dot} content={content} />;
};

export default Part;
