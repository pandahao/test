import React from 'react';

import { CastleRockPalette, ConsolePalette, GeneralPalette } from 'legacy_styles/Colors';
import { DotsIcon } from 'legacy_assets';

import ReactList from 'legacy_lib/ReactList';
import _ from 'underscore';

class Console extends React.Component {
  constructor(props) {
    super(props);
    const $ = this;
    $.state = {
      showAll: true,
      isDragging: false,
      consoleHeight: 200,
      lastClientY: null
    };
    $.switchToLittle = _.debounce(() => {
      $.setState(() => {
        return {
          showAll: false
        };
      });
    }, 1000, true);
    $.switchToAll = _.debounce(() => {
      $.setState(() => {
        return {
          showAll: true
        };
      });
    }, 500);
    $.drag = $.drag.bind(this);
    $.dragStart = $.dragStart.bind(this);
    $.dragEnd = $.dragEnd.bind(this);
  }

  componentWillReceiveProps() {
    // this.switchToLittle();
  }
  componentDidUpdate() {
    // this.switchToAll();
  }
  dragStart(e) {
    this.setState({
      isDragging: true,
      lastClientY: e.clientY
    });
  }
  drag(e) {
    if(this.state.isDragging) {
      let newHeight = this.state.consoleHeight + this.state.lastClientY - e.clientY;
      this.setState({
        consoleHeight: newHeight,
        lastClientY: e.clientY
      });
    }
  }
  dragEnd() {
    this.setState({isDragging: false});
  }
  render() {
    const { history, show, toggleConsole, freemode, variableName, docView } = this.props;
    const _style = {
      root: {
        background: CastleRockPalette.background,
        margin: 0,
        padding: 0,
        width: '100%',
        height: show ? this.state.consoleHeight : '25px',
        display: 'block',
        position: 'fixed',
        zIndex: 0,
        bottom: 0,
        overflowY: 'auto',
        overflowX: 'auto',
        fontFamily: 'menlo',
        fontSize: 15
      },
      header: {
        color: CastleRockPalette.border,
        background: CastleRockPalette.background,
        position: 'fixed',
        width: '100%', //freemode ? (docView ? '22%' : '27%') : '22%',
        paddingLeft: freemode ? (docView ? '17%' : '25%') : '17%',
        height: 25,
        borderTop: '1px solid',
        zIndex: 1,
        cursor: 'n-resize',
        boxShadow: `0 2px 6px ${GeneralPalette.whiteShadow}`
      },
      tableProps: {
        borderCollapse: 'collapse',
        tableLayout: 'fixed'
      },
      list: {
        height: '100%',
        display: show ? 'block' : 'none',
        paddingTop: 30,
        overflow: 'auto',
        zIndex: -1
      },
      listItem: {
        display: 'table-row-group',
        marginTop: 5
      },
      padding: {
        paddingLeft: freemode ? (docView ? '2em' : '4em') : '2em',
        paddingBottom: '0.4em',
        maxWidth: freemode ? (docView ? '190px' : '400px') : '190px'
      },
      indexPadding: {
        color: CastleRockPalette.border,
        borderRight: '1px solid',
        paddingBottom: '0.4em'
      },
      index: {
        color: ConsolePalette.index,
        display: 'table-cell',
        width: 35,
        paddingLeft: '1.5em',
        fontSize: 12
      },
      port: {
        color: ConsolePalette.port,
        background: ConsolePalette.portBackground,
        display: 'table-cell',
        paddingLeft: '3px',
        paddingRight: '3px',
        fontSize: 12
      },
      events: {
        color: ConsolePalette.events,
        background: ConsolePalette.eventsBackground,
        display: 'table-cell',
        paddingLeft: '3px',
        paddingRight: '3px',
        fontSize: 12
      },
      data: {
        color: ConsolePalette.data,
        background: ConsolePalette.dataBackground,
        display: 'table-cell',
        paddingLeft: '3px',
        paddingRight: '3px',
        fontSize: 12
      }
    };

    function portInfo(varName, history, index, port) {
      if (!variableName[history[index].port]) {
        return `${history[index].port}`;
      }
      if (variableName[history[index].port].name === '') {
        return `${history[index].port}`;
      }
      else {
        return `${history[index].port}` + ` (${shortenVariableName(variableName[history[index].port].name, freemode)}):`;
      }
    }

        // Function to shorten variable name if too long
    function shortenVariableName(variableName, freemode) {
      if (!freemode) {
        if (variableName.length > 10) {
          const result = `${variableName.slice(0, 8)}...`;
          return result;
        }
        return variableName;
      }
      else {
        if (variableName.length > 15) {
          const result = `${variableName.slice(0, 13)}...`;
          return result;
        }
        return variableName;
      }
    }

    let consoleHeader;
    if (this.props.virtualrobotblur) {
      consoleHeader = (
        <div style={_style.header}>
          <DotsIcon fill={ConsolePalette.index} />
        </div>
            );
    }
    else {
      consoleHeader = (
        <div style={_style.header} onMouseLeave={this.dragEnd} onMouseDown={this.dragStart} onMouseUp={this.dragEnd} onMouseMove={this.drag} onDoubleClick={_.debounce(toggleConsole, 500, true)}>
          <DotsIcon fill={ConsolePalette.index} />
        </div>
      );
    }

    return (
      <div style={_style.root} ref="root" onScroll={ this.onScroll }>
        {consoleHeader}
        <div style={_style.list}>
          {this.state.showAll ?
            <ReactList
              itemRenderer={
                  (index, key) => {
                    return (
                      <tr key={key} style={_style.listItem}>
                        <td style={_style.indexPadding}>
                          <span style={_style.index}>{`${history.length - index}`}</span>
                        </td>
                        <td style={_style.padding}>
                          <span style={_style.port}>
                            {portInfo(variableName, history, index)}
                          </span>
                        </td>
                        <td style={_style.padding}>
                          <span style={_style.events}>{`${history[index].event_name}`}</span>
                        </td>
                        <td style={_style.padding}>
                          <span style={_style.data}>{`${history[index].msg ? history[index].msg[0].value : ''}`}</span>
                        </td>
                        <td style={_style.padding}>
                          <span style={_style.data}>{`${history[index].msg ? history[index].msg[1] ? history[index].msg[1].value : '' : ''}`}</span>
                        </td>
                      </tr>
                    );
                  }
              }
              itemsRenderer={
                (items, ref) => {
                  return (
                    <table style={_style.tableProps}>
                      <tbody ref={ref}>
                        {items}
                      </tbody>
                    </table>
                  );
                }
              }
              length={history.length}
            /> : <table style={_style.tableProps}>
              <tbody>
                {
                  history.slice(0, 10).map((hist, index) => {
                    return (
                      <tr key={index} style={_style.listItem}>
                        <td style={_style.indexPadding}>
                          <span style={_style.index}>{`${history.length - index}`}</span>
                        </td>
                        <td style={_style.padding}>
                          <span style={_style.port}>
                            {portInfo(variableName, history, index)}
                          </span>
                        </td>
                        <td style={_style.padding}>
                          <span style={_style.events}>{`${hist.event_name}`}</span>
                        </td>
                        <td style={_style.padding}>
                          <span style={_style.data}>{`${hist.msg ? hist.msg[0].value : ''}`}</span>
                        </td>
                        <td style={_style.padding}>
                          <span style={_style.data}>{`${hist.msg ? hist.msg[1] ? hist.msg[1].value : '' : ''}`}</span>
                        </td>
                      </tr>
                    );
                  })
               }
              </tbody>
            </table>}
        </div>
      </div>
    );
  }
}

export default Console;
