import React from 'react';
import Radium from 'radium';
import { IconStyle, IconDeactivateStyle } from 'legacy_styles/Icons';


// Props:
// style , device
class Motor extends React.Component {
  render() {
    const $ = this;
    const animation = () => {
      const rotateFrames = Radium.keyframes({
        '0%': {
          transform: 'rotate( 0deg )',
          transformOrigin: '50% 50% 0',
        },
        '100%': {
          transform: `rotate( ${$.props.device.msg[1].value === 0 ? '' : '-'}360deg )`,
          transformOrigin: '50% 50% 0',
        }
      }, 'rotate');

      if ($.props.device.state === 2) {
        return {
          animationDuration: `${5 / $.props.device.msg[0].value}s`,
          animationIterationCount: 'infinite',
          animationTimingFunction: 'linear',
          animationName: rotateFrames
        };
      } else {
        return {};
      }
    };
    const iconStyle = $.props.device.event_name === 'DEACTIVATE' ? IconDeactivateStyle : IconStyle;
    return (
            <div style={Object.assign({}, iconStyle, this.props.style, { top: '50%' }, { left: '0%' }, { width: '60%' })}>
            <svg x="0px" y="0px" viewBox="0 0 60 60" width="35" height="35">
            <path style={animation()} d="M30-0.062C13.397-0.062-0.062,13.397-0.062,30S13.397,60.062,30,60.062c16.601,0,30.06-13.459,30.06-30.062
            	S46.602-0.062,30-0.062z M45,41.736c2.091,0,3.786,1.695,3.786,3.786S47.091,49.309,45,49.309c-2.092,0-3.787-1.695-3.787-3.786
            	S42.908,41.736,45,41.736z M15.001,41.736c2.09,0,3.785,1.695,3.785,3.786s-1.695,3.786-3.785,3.786
            	c-2.092,0-3.787-1.695-3.787-3.786S12.909,41.736,15.001,41.736z M45,11.736c2.091,0,3.786,1.695,3.786,3.788
            	c0,2.09-1.695,3.785-3.786,3.785s-3.786-1.695-3.786-3.785C41.214,13.431,42.909,11.736,45,11.736z M15,11.735
            	c2.091,0,3.787,1.695,3.787,3.787c0,2.091-1.695,3.786-3.787,3.786s-3.787-1.695-3.787-3.786C11.214,13.431,12.909,11.735,15,11.735
            	z M30.002,23.339c3.677,0,6.659,2.983,6.659,6.662c0,3.677-2.982,6.662-6.659,6.662c-3.68,0-6.663-2.984-6.663-6.662
            	C23.339,26.322,26.322,23.339,30.002,23.339z M38.494,29.999c0,4.691-3.804,8.495-8.494,8.495c-4.691,0-8.494-3.804-8.494-8.495
            	c0-4.691,3.803-8.494,8.494-8.494C34.69,21.505,38.494,25.308,38.494,29.999z" />
            </svg>
            </div>
        );
  }
}


export default Radium(Motor);
