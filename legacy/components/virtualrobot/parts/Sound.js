import React from 'react';

import { RobotPalette } from 'legacy_styles/Colors';
import { IconStyle, IconDeactivateStyle } from 'legacy_styles/Icons';


const Sound = ({ style, device }) => {
  const iconStyle = device.event_name === 'DEACTIVATE' ? IconDeactivateStyle : IconStyle;
  if (device.state != 1) {
    return (
            <div style={Object.assign({}, iconStyle, style)}>
            <svg id="Layer_1" x="0px" y="0px"
              viewBox="0 0 60 60" width="35" height="35"
            >
                <g id="Quiet" className="st0" >
                    <path className="st2" d="M25.4,44.5L18,38h-8c-1.7,0-3-1.3-3-3V25c0-1.7,1.3-3,3-3h8l7.4-6.5c0.4-0.3,0.8-0.5,1.3-0.5H27
                        c1.1,0,2,0.9,2,2v26c0,1.1-0.9,2-2,2h-0.2C26.3,45,25.8,44.8,25.4,44.5z" />
                    <g className="st2">
                        <path d="M51.5,39.5c-0.5,0-1-0.2-1.4-0.6l-15-15c-0.8-0.8-0.8-2,0-2.8c0.8-0.8,2-0.8,2.8,0l15,15c0.8,0.8,0.8,2,0,2.8
                            C52.5,39.3,52,39.5,51.5,39.5z" />
                    </g>
                    <g className="st2">
                        <path d="M36.5,39.5c-0.5,0-1-0.2-1.4-0.6c-0.8-0.8-0.8-2,0-2.8l15-15c0.8-0.8,2-0.8,2.8,0c0.8,0.8,0.8,2,0,2.8l-15,15
                            C37.5,39.3,37,39.5,36.5,39.5z" />
                    </g>
                </g>
            </svg>
        </div>);
  } else {
    return (
            <div style={Object.assign({}, iconStyle, style)}>
            <svg id="Layer_1" x="0px" y="0px"
              viewBox="0 0 60 60" width="35" height="35"
            >
                <g id="Loud" className="st0">
                    <path className="st2" d="M45.4,48.7c-0.6,0-1.1-0.2-1.5-0.7c-0.7-0.8-0.6-2.1,0.2-2.8c3.9-4.1,6.1-9.5,6.1-15.2c0-5.6-2.1-10.9-6-15.1
                        c-0.6-0.7-0.9-1.6-0.4-2.4c0.7-1.3,2.3-1.4,3.2-0.4c4.7,4.9,7.2,11.3,7.2,17.9c0,6.8-2.6,13.3-7.4,18.2
                        C46.4,48.5,45.9,48.7,45.4,48.7z" />
                    <path className="st2" d="M25.4,44.5L18,38h-8c-1.7,0-3-1.3-3-3V25c0-1.7,1.3-3,3-3h8l7.4-6.5c0.4-0.3,0.8-0.5,1.3-0.5H27
                        c1.1,0,2,0.9,2,2v26c0,1.1-0.9,2-2,2h-0.2C26.3,45,25.8,44.8,25.4,44.5z" />
                    <path className="st2" d="M37.5,41.3c-0.5,0-0.9-0.2-1.3-0.5c-0.9-0.8-0.8-2.3,0.1-3.1c2.2-2,3.2-4.5,3.2-7.8c0-3.5-1.2-6.5-3.2-8.4
                        c-0.8-0.7-1-1.9-0.4-2.7c0.7-1,2.1-1.1,3-0.4c3,2.7,4.7,6.7,4.7,11.5c0,4.4-1.6,8-4.7,10.8C38.4,41.2,38,41.3,37.5,41.3z" />
                </g>
            </svg>
            </div>);
  }
};


export default Sound;
