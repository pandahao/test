import React from 'react';
import Radium from 'radium';
import { RobotPalette } from 'legacy_styles/Colors';
import { IconStyle } from 'legacy_styles/Icons';

class Irt extends React.Component {
  render() {
    const { style, device } = this.props;

    const show = () => {
      const blinkyFrames = Radium.keyframes({
        '0%': {
          opacity: 0
        },
        '50%': {
          opacity: 1
        },
        '100%': {
          opacity: 0,
        }
      }, 'blinky');
      return {
        opacity: 0,
        animationDuration: '50ms',
        animationIterationCount: '5',
        animationTimingFunction: 'linear',
        animationName: blinkyFrames
      };
    };

    return device.event_name === 'DEACTIVATE' ? (
            <div style={Object.assign({}, IconStyle, style)}>
            <svg version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 60 60" width="35" height="35">
            		<g id="Base" transform="translate(12.500000, 30.000000) rotate(-270.000000) translate(-12.500000, -30.000000) translate(-9.500000, 19.500000)">
            			<path id="Oval-1" fill={RobotPalette.deactivateText} d="M31.857,9.861C31.857,4.415,27.436,0,21.982,0c-5.454,0-9.875,4.415-9.875,9.861" />
            			<path id="Rectangle-1" fill={RobotPalette.deactivateText} d="M1.822,10.883h40.355c1.007,0,1.822,0.816,1.822,1.823v6.472C44,20.184,43.184,21,42.178,21H1.822C0.816,21,0,20.184,0,19.178v-6.472C0,11.699,0.816,10.883,1.822,10.883z" />
            		</g>
                <g style={show()}>
                <path fill={RobotPalette.deactivateText} stroke={RobotPalette.deactivateText} d="M47.059,53.897c-0.747,0-1.369-0.247-1.865-0.87c-0.871-0.995-0.747-2.611,0.248-3.482c4.849-5.097,7.584-11.812,7.584-18.899c0-6.964-2.609-13.554-7.461-18.776c-0.745-0.871-1.118-1.99-0.495-2.985c0.871-1.616,2.86-1.741,3.978-0.497C54.891,14.48,58,22.438,58,30.646c0,8.456-3.232,16.538-9.199,22.631C48.301,53.65,47.679,53.897,47.059,53.897z" />
                <path fill={RobotPalette.deactivateText} stroke={RobotPalette.deactivateText} d="M37.233,44.697c-0.619,0-1.118-0.249-1.616-0.621c-1.118-0.995-0.994-2.859,0.126-3.856c2.735-2.486,3.978-5.596,3.978-9.698c0-4.353-1.49-8.083-3.978-10.445c-0.995-0.871-1.244-2.362-0.499-3.357c0.871-1.244,2.611-1.368,3.73-0.499c3.729,3.357,5.845,8.332,5.845,14.301c0,5.471-1.991,9.948-5.845,13.429C38.354,44.571,37.855,44.697,37.233,44.697z" />
                </g>
            </svg>
            </div>
        ) : (
            <div style={Object.assign({}, IconStyle, style)}>
            <svg version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 60 60" width="35" height="35">
            		<g id="Base" transform="translate(12.500000, 30.000000) rotate(-270.000000) translate(-12.500000, -30.000000) translate(-9.500000, 19.500000)">
            			<path id="Oval-1" fill="#FFFFFF" d="M31.857,9.861C31.857,4.415,27.436,0,21.982,0c-5.454,0-9.875,4.415-9.875,9.861" />
            			<path id="Rectangle-1" fill="#FFFFFF" d="M1.822,10.883h40.355c1.007,0,1.822,0.816,1.822,1.823v6.472C44,20.184,43.184,21,42.178,21H1.822C0.816,21,0,20.184,0,19.178v-6.472C0,11.699,0.816,10.883,1.822,10.883z" />
            		</g>
                <g style={show()}>
            		<path fill="#FFFFFF" stroke="#FFFFFF" d="M47.059,53.897c-0.747,0-1.369-0.247-1.865-0.87c-0.871-0.995-0.747-2.611,0.248-3.482c4.849-5.097,7.584-11.812,7.584-18.899c0-6.964-2.609-13.554-7.461-18.776c-0.745-0.871-1.118-1.99-0.495-2.985c0.871-1.616,2.86-1.741,3.978-0.497C54.891,14.48,58,22.438,58,30.646c0,8.456-3.232,16.538-9.199,22.631C48.301,53.65,47.679,53.897,47.059,53.897z" />
            		<path fill="#FFFFFF" stroke="#FFFFFF" d="M37.233,44.697c-0.619,0-1.118-0.249-1.616-0.621c-1.118-0.995-0.994-2.859,0.126-3.856c2.735-2.486,3.978-5.596,3.978-9.698c0-4.353-1.49-8.083-3.978-10.445c-0.995-0.871-1.244-2.362-0.499-3.357c0.871-1.244,2.611-1.368,3.73-0.499c3.729,3.357,5.845,8.332,5.845,14.301c0,5.471-1.991,9.948-5.845,13.429C38.354,44.571,37.855,44.697,37.233,44.697z" />
                </g>
            </svg>
            </div>
        );
  }
}

export default Radium(Irt);
