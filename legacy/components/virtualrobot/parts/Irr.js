import React from 'react';
import Radium from 'radium';
import { RobotPalette } from 'legacy_styles/Colors';
import { IconStyle } from 'legacy_styles/Icons';

class Irr extends React.Component {
  render() {
    const { style, device } = this.props;

    const show = () => {
      const blinkyFrames = Radium.keyframes({
        '0%': {
          opacity: 0
        },
        '50%': {
          opacity: 1
        },
        '100%': {
          opacity: 0,
        }
      }, 'blinky');
      if (device.state > 1) {
        return {
          opacity: 0,
          animationDuration: '200ms',
          animationIterationCount: '1',
          animationTimingFunction: 'linear',
          animationName: blinkyFrames
        };
      } else {
        return {
          opacity: 0
        };
      }
    };

    return device.event_name === 'DEACTIVATE' ? (
            <div style={Object.assign({}, IconStyle, style, { transform: 'translate(-50%,0%)' })}>
            <svg viewBox="0 0 60 60" version="1.1" width="35" height="35">
            <g id="Page-1">
                <g id="Receiver-Small">
                    <g id="Base" transform="translate(47.500000, 30.000000) rotate(-90.000000) translate(-47.500000, -30.000000) translate(25.500000, 19.500000)">
                    <path id="Oval-1" fill={RobotPalette.deactivateText} d="M31.857,9.861C31.857,4.415,27.436,0,21.982,0c-5.455,0-9.875,4.415-9.875,9.861H31.857z" />
                    <path id="Rectangle-1" fill={RobotPalette.deactivateText} d="M1.822,10.883h40.354c1.006,0,1.822,0.816,1.822,1.823v6.472
                        C44,20.184,43.184,21,42.178,21H1.822C0.816,21,0,20.184,0,19.178v-6.472C0,11.699,0.816,10.883,1.822,10.883z" />
                    </g>
                    <g style={show()}>
                        <g id="Arrow-Head" transform="translate(-5,0) translate(31.500000, 30.000000) rotate(-270.000000) translate(-31.500000, -30.000000) translate(22.000000, 24.000000)">
                            <path id="Rectangle-21" fill={RobotPalette.deactivateText} d="M10.751,1.564L10.751,1.564c0.769,0.768,0.237,2.545-1.186,3.969L5.874,9.224
                                c-1.423,1.424-3.2,1.955-3.969,1.186l0,0C1.136,9.642,1.667,7.864,3.09,6.441L6.782,2.75C8.205,1.326,9.982,0.795,10.751,1.564z" />
                            <path id="Rectangle-21_1_" fill={RobotPalette.deactivateText} d="M8.226,1.564L8.226,1.564c0.769-0.771,2.545-0.238,3.969,1.186l3.69,3.691
                                c1.423,1.422,1.954,3.2,1.187,3.969l0,0c-0.771,0.769-2.547,0.238-3.969-1.186L9.412,5.533C7.988,4.109,7.457,2.332,8.226,1.564z
                                " />
                        </g>
                        <path transform="translate(-6,0)" fill={RobotPalette.deactivateText} d="M11.476,29.988c0,0.892-0.723,1.616-1.615,1.616H6.633c-0.892,0-1.615-0.725-1.615-1.616l0,0
                            c0-0.892,0.723-1.615,1.615-1.615h3.229C10.753,28.374,11.476,29.097,11.476,29.988L11.476,29.988z" />
                        <path transform="translate(-6,0)" fill={RobotPalette.deactivateText} d="M18.477,29.988c0,0.892-0.724,1.616-1.615,1.616h-3.229c-0.892,0-1.615-0.725-1.615-1.616l0,0
                            c0-0.892,0.723-1.615,1.615-1.615h3.229C17.753,28.373,18.477,29.097,18.477,29.988L18.477,29.988z" />
                        <path transform="translate(-6,0)" fill={RobotPalette.deactivateText} d="M32.477,29.988c0,0.892-0.724,1.616-1.615,1.616h-3.229c-0.892,0-1.615-0.725-1.615-1.616l0,0
                            c0-0.892,0.723-1.615,1.615-1.615h3.229C31.753,28.373,32.477,29.097,32.477,29.988L32.477,29.988z" />
                        <path transform="translate(-6,0)" fill={RobotPalette.deactivateText} d="M25.477,29.988c0,0.892-0.724,1.616-1.615,1.616h-3.229c-0.892,0-1.615-0.725-1.615-1.616l0,0
                            c0-0.892,0.723-1.615,1.615-1.615h3.229C24.753,28.373,25.477,29.097,25.477,29.988L25.477,29.988z" />
                    </g>
                </g>
            </g>
            </svg>
            </div>

        ) : (
            <div style={Object.assign({}, IconStyle, style, { transform: 'translate(-50%,0%)' })}>
            <svg viewBox="0 0 60 60" version="1.1" width="35" height="35">
            <g id="Page-1">
                <g id="Receiver-Small">
                    <g id="Base" transform="translate(47.500000, 30.000000) rotate(-90.000000) translate(-47.500000, -30.000000) translate(25.500000, 19.500000)">
                    <path id="Oval-1" fill="#FFFFFF" d="M31.857,9.861C31.857,4.415,27.436,0,21.982,0c-5.455,0-9.875,4.415-9.875,9.861H31.857z" />
                    <path id="Rectangle-1" fill="#FFFFFF" d="M1.822,10.883h40.354c1.006,0,1.822,0.816,1.822,1.823v6.472
                        C44,20.184,43.184,21,42.178,21H1.822C0.816,21,0,20.184,0,19.178v-6.472C0,11.699,0.816,10.883,1.822,10.883z" />
                    </g>
                    <g style={show()}>
                        <g id="Arrow-Head" transform="translate(-5,0) translate(31.500000, 30.000000) rotate(-270.000000) translate(-31.500000, -30.000000) translate(22.000000, 24.000000)">
                            <path id="Rectangle-21" fill="#FFFFFF" d="M10.751,1.564L10.751,1.564c0.769,0.768,0.237,2.545-1.186,3.969L5.874,9.224
                                c-1.423,1.424-3.2,1.955-3.969,1.186l0,0C1.136,9.642,1.667,7.864,3.09,6.441L6.782,2.75C8.205,1.326,9.982,0.795,10.751,1.564z" />
                            <path id="Rectangle-21_1_" fill="#FFFFFF" d="M8.226,1.564L8.226,1.564c0.769-0.771,2.545-0.238,3.969,1.186l3.69,3.691
                                c1.423,1.422,1.954,3.2,1.187,3.969l0,0c-0.771,0.769-2.547,0.238-3.969-1.186L9.412,5.533C7.988,4.109,7.457,2.332,8.226,1.564z
                                " />
                        </g>
                        <path transform="translate(-6,0)" fill="#FFFFFF" d="M11.476,29.988c0,0.892-0.723,1.616-1.615,1.616H6.633c-0.892,0-1.615-0.725-1.615-1.616l0,0
                            c0-0.892,0.723-1.615,1.615-1.615h3.229C10.753,28.374,11.476,29.097,11.476,29.988L11.476,29.988z" />
                        <path transform="translate(-6,0)" fill="#FFFFFF" d="M18.477,29.988c0,0.892-0.724,1.616-1.615,1.616h-3.229c-0.892,0-1.615-0.725-1.615-1.616l0,0
                            c0-0.892,0.723-1.615,1.615-1.615h3.229C17.753,28.373,18.477,29.097,18.477,29.988L18.477,29.988z" />
                        <path transform="translate(-6,0)" fill="#FFFFFF" d="M32.477,29.988c0,0.892-0.724,1.616-1.615,1.616h-3.229c-0.892,0-1.615-0.725-1.615-1.616l0,0
                            c0-0.892,0.723-1.615,1.615-1.615h3.229C31.753,28.373,32.477,29.097,32.477,29.988L32.477,29.988z" />
                        <path transform="translate(-6,0)" fill="#FFFFFF" d="M25.477,29.988c0,0.892-0.724,1.616-1.615,1.616h-3.229c-0.892,0-1.615-0.725-1.615-1.616l0,0
                            c0-0.892,0.723-1.615,1.615-1.615h3.229C24.753,28.373,25.477,29.097,25.477,29.988L25.477,29.988z" />
                    </g>
                </g>
            </g>
            </svg>
            </div>
        );
  }
}

export default Radium(Irr);
