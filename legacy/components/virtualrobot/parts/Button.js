import React from 'react';
import { IconStyle, IconDeactivateStyle } from 'legacy_styles/Icons';


const Button = ({ style, device }) => {
  const transform = device.state === 2 ? 'translate(0,2)' : 'translate(0,-2)';
  const extraspace = {
    position: 'relative',
    top: '50%',
    width: '60%'
  };
  const iconStyle = device.event_name === 'DEACTIVATE' ? IconDeactivateStyle : IconStyle;
  return (
        <div style={Object.assign({}, iconStyle, style, extraspace)}>
            <svg version="1.1" id="Layer_1" x="0px" y="0px"
              viewBox="0 0 60 60" width="35" height="35"
            >
                <g id="Button" >
                	<path transform={transform} d="M49,32.5H11v-12c0-1.7,1.3-3,3-3h32c1.7,0,3,1.3,3,3V32.5z" />
                	<rect x="8" y="39" width="44" height="4" />
                	<rect x="17" y="36.5" width="26" height="3" />
                </g>
            </svg>
        </div>
    );
};


export default Button;
