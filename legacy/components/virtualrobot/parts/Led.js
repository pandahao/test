import React from 'react';
import Radium from 'radium';
import { IconStyle, IconDeactivateStyle } from 'legacy_styles/Icons';


// Props:
// style , device
class Led extends React.Component {
  render() {
    const blinkFrames = Radium.keyframes({
      '0%': { opacity: 0 },
      '50%': { opacity: 0 },
      '60%': { opacity: 1 },
      '100%': { opacity: 1 }
    }, 'blink');

    const haloStyle = () => {
      switch (this.props.device.state) {
        case 1:
          return { display: 'none' };
        case 3:
          return {
            animationDuration: this.props.device.event_name === 'SLOWBLINK_BEGIN' ? '1s' : '0.25s',
            animationIterationCount: 'infinite',
            animationName: blinkFrames
          };
        default:
          return {
            display: 'block'
          };
      }
    };

    const iconStyle = this.props.device.event_name === 'DEACTIVATE' ? IconDeactivateStyle : IconStyle;

    return (
            <div style={Object.assign({}, iconStyle, this.props.style)}>
                <svg version="1.1" id="Layer_1" x="0px" y="0px"
                  viewBox="0 0 60 60" width="35" height="35"
                >
                        <g id="LED-Halo" style={haloStyle()}>
                            <path d="M46.2,14.7l-1.4,1.4c-0.8,0.8-2.1,0.8-2.9,0c-0.8-0.8-0.8-2.1,0-2.9l1.4-1.4c0.8-0.8,2.1-0.8,2.9,0
                             C46.9,12.6,46.9,13.9,46.2,14.7z" />
                            <path d="M16.7,11.8l1.4,1.4c0.8,0.8,0.8,2.1,0,2.9c-0.8,0.8-2.1,0.8-2.9,0l-1.4-1.4c-0.8-0.8-0.8-2.1,0-2.9
                             C14.6,11.1,15.9,11.1,16.7,11.8z" />
                            <path d="M8,27h2c1.1,0,2,0.9,2,2s-0.9,2-2,2H8c-1.1,0-2-0.9-2-2S6.9,27,8,27z" />
                            <path d="M50,27h2c1.1,0,2,0.9,2,2s-0.9,2-2,2h-2c-1.1,0-2-0.9-2-2S48.9,27,50,27z" />
                            <path d="M30,6c1.1,0,2,0.9,2,2v2c0,1.1-0.9,2-2,2s-2-0.9-2-2V8C28,6.9,28.9,6,30,6z" />
                        </g>
                        <rect x="14" y="50" width="32" height="4" />
                        <path d="M30,19.6c-5.5,0-10,4.5-10,10V50h20V29.6C40,24,35.5,19.6,30,19.6z M30.1,26.3c-1.8,0-3.1,1.5-3.1,3.3v4
                            c0,0.8-0.7,1.5-1.5,1.5S24,34.4,24,33.6v-4c0-3.4,2.7-6.3,6.1-6.3c0.8,0,1.5,0.7,1.5,1.5S31,26.3,30.1,26.3z" />
                </svg>
            </div>
        );
  }
}


export default Radium(Led);
