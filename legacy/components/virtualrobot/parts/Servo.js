import React from 'react';
import Radium from 'radium';
import { IconStyle, IconDeactivateStyle } from 'legacy_styles/Icons';

function calcSpeed(lvl) {
  const chart = {
    1: 20,
    2: 5.5,
    3: 3.2,
    4: 2.2,
    5: 1.75,
    6: 1.35,
    7: 1.2,
    8: 1.0,
    9: 0.85,
    10: 0.75
  };
  return chart[lvl];
}
function calcTime(sp, dg) {
    // console.log(sp,dg);
  return dg / (180 / calcSpeed(sp));
}


// Redux will mount and unmount this component each time it needs update
class Servo extends React.Component {
    /*
    componentDidMount(){
        console.log('componentDidMount ---> ' + this.props.device.port);
    }
    componentWillUnmount(){
        console.log('componentWillUnmount ---> '+ this.props.device.port);
    }
    componentWillUpdate(){
        console.log('componentWillUpdate ---> '+ this.props.device.port);
    }
    componentDidUpdate(){
        console.log('componentDidUpdate ---> '+ this.props.device.port);
    }
    componentWillMount(){
        console.log('componentWillMount ---> '+ this.props.device.port);
    }
    */
  render() {
    const $ = this;
        // console.log($.props.device);
    const __style = {
      position: 'relative',
      display: 'inline-block',
      top: '50%',
      left: '0%',
      marginLeft: '-18px',
      width: '60%'
    };
        // console.log("Rendering: ",$.props.device.event_name);
    const rotate = () => {
      const current = 135 - $.props.device.angle;

      if ($.props.device.state === 2) {
                // console.log("starting animation, move to ",$.props.device.angle);

        const move_to = 135 - $.props.device.msg[0].value;
        const delta_angle = Math.abs(current - move_to);
        const time = calcTime($.props.device.msg[1].value, delta_angle);
        const rotateFrames = Radium.keyframes({
          '0%': {

            transform: `rotate( ${current}deg )`,
            transformOrigin: '50% 50% 0',
          },
          '100%': {

            transform: `rotate( ${move_to}deg )`,
            transformOrigin: '50% 50% 0',
          }
        }, 'rotate');

        const animation = {
          transformOrigin: '50% 50% 0',
          transform: `rotate( ${current}deg )`,

          animationDuration: `${time}s`,
          animationIterationCount: '1',
          animationTimingFunction: 'linear',
          animationName: rotateFrames
        };

        return animation;
      } else {
                // console.log("no animation, stays at ",$.props.device.angle);
        const move_to = 135 - $.props.device.msg[0].value;
        return {
          transformOrigin: '50% 50% 0',
          transform: `rotate( ${move_to}deg )`
        };
      }
    };
    const iconStyle = $.props.device.event_name === 'DEACTIVATE' ? IconDeactivateStyle : IconStyle;
    return (
            <div style={Object.assign({}, iconStyle, $.props.style, __style)}>
                <svg x="0px" y="0px" viewBox="0 0 60 60" width="40" height="40" version="1.1" id="Layer_1">
                    <g id="Servo" >
                        <path style={rotate()} d="M30,5C16.2,5,5,16.2,5,30c0,13.8,11.2,25,25,25s25-11.2,25-25C55,16.2,43.8,5,30,5z M31.8,28.2c1,1,1,2.6,0,3.5
                                c-1,1-2.6,1-3.5,0L15.2,18.7c-1-1-1-2.6,0-3.5c1-1,2.6-1,3.5,0L31.8,28.2z" />
                    </g>
                </svg>
            </div>
        );
  }
}

export default Radium(Servo);
