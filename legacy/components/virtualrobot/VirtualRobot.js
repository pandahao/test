/*eslint-disable*/
import React, {Component} from 'react';

import RobotPanel from 'legacy_containers/virtualrobot/RobotPanel';
import EventConsole from 'legacy_containers/virtualrobot/EventConsole';
import Blur from '../Blur';

import FlatButton from 'material-ui/FlatButton';
import TranslatedText from 'legacy_containers/TranslatedText';
import { zIndex } from 'styles/zIndex';

import { CastleRockPalette, StatePalette,ToolbeltPalette } from 'legacy_styles/Colors';

import RoboCoreConnect from './RoboCoreConnect';

import Snapshot from './Snapshot';

const headerStyle = {
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    height: 64,
    width: '100%',
    backgroundColor:'#9598a4',
    zIndex : zIndex.medium
  },
  right: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  square: {
    position: 'relative',
    width: 48,
    height: 64,
    background: 'transparent',
  },
  cc:{
    color:'#FFFFFF',
    fontSize:'13px',
    fontFamily:'Static',
    marginTop:'25px',
    line:16,
    opacity:1,
    letterSpacing : '6px',
    character:'6px',
    marginLeft:'30px'
  }
};
const buttonStyle = {
  errorIcon: {
    display: 'fixed',
    transitionDuration: '0.5s',
    transitionProperty: 'opacity',
    bottom: 72,
    right: 3
  },
  loadIcon: {
    display: 'fixed',
    transitionDuration: '0.5s',
    transitionProperty: 'opacity',
    bottom: 35,
    right: 3
  },
  runIcon: {
    display: 'fixed',
    transitionDuration: '0.8s',
    transitionProperty: 'transform',
    zIndex: zIndex.mediumLow,
    width: '100%',
    backgroundColor:'#676d83',
    borderRadius:'45px',
    marginLeft:'-8px',
    marginTop:'-5px',
    color:'#FFFFFF',
    fontSize:'13px',
    fontFamily:'StaticBold',
    line:16
  },
  opac: {
    transitionDuration: '1s',
    transitionProperty: 'opacity',
  },
  buttons: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '100%',
    alignItems: 'center',
    marginTop : '1em',
    marginLeft : '1em',
    marginRight : '1em'
  }
};


class VirtualRobot extends Component{
  constructor(props){
    super(props);
    this.state = {
      isMonitoring : false,
      snapShotMode : false
    }
  }
  handleButtonClick(){
    const $ = this;
    const {monitor,takeSnapShot,uploading} = this.props;

    if(uploading){
      return;
    }

    if($.state.isMonitoring){
      if($.state.snapShotMode){
        $.setState({
          snapShotMode : false
        })
      }else{
        takeSnapShot();
        $.setState({
          snapShotMode : true
        })
        $.forceUpdate();
      }
    }else {
      //Start Monitoring
      $.setState({
        isMonitoring : true
      })
      monitor();
    }
  }
  componentWillReceiveProps(){
    console.log('CODE COMPILED');
    const {codeCompiled,virtualrobotblur,uploading} = this.props;
    if(uploading){
      this.setState({
        isMonitoring : true
      })
    }else if(virtualrobotblur && !codeCompiled){
      this.setState({
        isMonitoring : false
      })
    }
  }
  renderHeader(){
    const buttonText = ()=>{
      if(this.state.isMonitoring){
        return this.state.snapShotMode ? 'CLOSE' : 'SNAPSHOT'
      }
      return 'MONITOR';
    }
    return(
      <div style={headerStyle.root}>
        <div style={headerStyle.cc}><TranslatedText text={'CONSOLE'} /></div>
          <div style={headerStyle.square} />
          <div style={headerStyle.right}>
            <div style={buttonStyle.buttons}>
              <FlatButton
                onClick={this.handleButtonClick.bind(this)}
                style={buttonStyle.runIcon}
              >
                <TranslatedText text={buttonText()}/>
              </FlatButton>
            </div>
          </div>
      </div>
    )
  }
  render(){
    const {
      freemode,
      docView,
      virtualrobotblur,
      blurFromListDisplay,
      uploading,
      codeCompiled,
      snapshotRobot,
      snapshotEvent,
      snapshotVariableName,
      tutorialMode
    } = this.props

    const _style = {
      root: {
        display: 'block',
        position: 'relative',
        background: CastleRockPalette.white,
        color: 'white',
        height: '100vh',
        width: '100%',
        marginTop: 0,
        overflow: 'hidden',
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        zIndex : zIndex.mediumLow
      },
      blurCover: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: zIndex.mediumHigh
      },
      under: {
        zIndex : zIndex.low,
        position : 'relative',
        height : '100%',
        WebkitFilter: !tutorialMode && virtualrobotblur && !uploading && !codeCompiled ? 'blur(3px)' : ''
      }
    };
    let blurCover;
    if (!tutorialMode && (virtualrobotblur || blurFromListDisplay)) {
      blurCover = <div style={_style.blurCover} />;
    }
    else {
      blurCover = null;
    }
    return (
        <div style={_style.root}>
            {
              blurCover
            }
            {
              virtualrobotblur && !uploading && !codeCompiled ?
              <div>
                <Blur />
                <RoboCoreConnect
                  freemode={freemode}
                  docView={docView}
                  blurFromListDisplay={blurFromListDisplay}
                  virtualrobotblur={virtualrobotblur} />
              </div> : null
            }
            <div style={_style.under}>
              {this.renderHeader()}
              {
                this.state.snapShotMode ?
                <Snapshot
                freemode={freemode}
                docView={docView}
                snapshotRobot={snapshotRobot}
                snapshotEvent={snapshotEvent}
                snapshotVariableName={snapshotVariableName}
                /> : null
              }
              <RobotPanel />
              <EventConsole />
            </div>
        </div>
      );
  }

};

export default VirtualRobot;
