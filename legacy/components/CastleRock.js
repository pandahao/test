import React from 'react';
import NavbarContainer from 'legacy_containers/NavbarContainer';
import PanelContainer from 'legacy_containers/PanelContainer';
import FullScreenPopUp from 'legacy_components/popupMessages/FullScreenPopUp';
import GlobalMessage from 'legacy_containers/common/GlobalMessage';
import {zIndex} from 'styles/zIndex';
import ConfirmDialog from 'legacy_components/common/ConfirmDialog';

class CastleRock extends React.Component {

  constructor(props){
    super(props);
    this.state={
      showUpdateIcon : false
    }
  }
  componentWillMount() {
    this.props.loadProjects();
  }

  // componentDidMount(){
  //   let $ = this;
  //   console.log('checking updates');
  //   if(window.SkylineUpdater){
  //     window.SkylineUpdater.checkUpdates();
  //     window.SkylineUpdater.messenger.on('update-downloaded',()=>{
  //       let confirm = new ConfirmDialog('Allo allo!');
  //       confirm.show(()=>{
  //         console.log('AGREED')
  //         window.SkylineUpdater.quitAndInstall();
  //       },()=>{
  //         console.log('DECLINED');
  //         $.setState({showUpdateIcon : true})
  //       });
  //     })
  //   }
  // }

  componentDidMount(){
    // console.log('checking updates');
    // window.SkylineUpdater.checkUpdates();
    let confirm = new ConfirmDialog('Allo allo!');
    // confirm.show(()=>{
    //   console.log('AGREED')
    // },()=>{
    //   console.log('DECLINED');
    // })
  }

  render() {
    const style = {
      root: {
        overflow: 'hidden',
        //WebkitUserSelect: 'none',
        display: 'flex',
        flexDirection: 'row',
        //WebkitFilter: this.props.resetWarning || (this.props.completionPopup && this.props.moreUnlock) || this.props.resetProject || this.props.userOnBoard == '0' || this.props.blur ? 'blur(3px)' : ''
      },
      panel: {
        height: '100%',
        width: 'calc(100% - 64px)',
        //WebkitFilter: this.props.userOnBoard > 0 && this.props.userOnBoard < 5 ? 'blur(3px)' : ''
      },
      navbar: {
        width: '64px',
        //zIndex:zIndex.medium+1
      }
    };

    const tutorialMode = this.props.tutorialMode === 1;
    //console.log(tutorialMode,this.props.tutorialMode);
    return (
      <div>
        {
          //<div style={style.mask}/>
        }
          <FullScreenPopUp
            resetWarning={this.props.resetWarning}
            completionPopup={this.props.completionPopup}
            moreUnlock={this.props.moreUnlock}
            resetProject={this.props.resetProject}
            userOnBoard={tutorialMode}
            index={this.props.index} />
        <div style={style.root}>
          <div style={style.navbar}>
            <NavbarContainer showUpdateIcon={this.state.showUpdateIcon} />
          </div>
          <div style={style.panel}>
            <PanelContainer />
          </div>
        </div>
        <GlobalMessage />

      </div>
    );
  }

}

export default CastleRock;
