import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

class DialogWrapper {
  constructor(title, container) {
    this._title = title;
    this._container = container || this.defaultContainer();
    this.close = this.close.bind(this);
  }

  defaultContainer() {
    const container = document.createElement('div');
    document.body.appendChild(container);
    return container;
  }

  getComponent(callback) {
    throw Error('not implemented');
  }

  show(callback) {
    this.component = ReactDOM.render(this.getComponent(callback), this._container);
  }

  close() {
    setTimeout(() => {
      ReactDOM.unmountComponentAtNode(this._container);
      this._container.remove();
    }, 300);
  }
}

export default DialogWrapper;
