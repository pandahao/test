import React from 'react';
import TranslatedText from 'legacy_containers/TranslatedText';

export default function ControlGroup(props) {
  const style = {
    root: {
      display: 'inline-block',
      marginRight: '30px',
    },
    title: {
      position: 'relative',
      top: '10px'
    },
    input: {}
  };
  const { label, children } = props;
  return (
        <div style={style.root}>
            <div style={style.title}>
                <TranslatedText text={label} />
            </div>
            {children}
        </div>
    );
}
