import React, { Component, PropTypes } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { t } from 'legacy_utils/i18n';
import DialogWrapper from 'legacy_components/common/DialogWrapper';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { GeneralPalette } from 'legacy_styles/Colors';

const skylineTheme = getMuiTheme({
    palette: {
        primary1Color: GeneralPalette.brightPurple,
    }
});

class ConfirmDialogComponent extends React.Component {
    state = {
        open: true
    };

    handleClose = () => {
        this.setState({ open: false });
        this.props.closeSelf();
    };

    handleOK = ()=> {
        this.props.onOkClick();
    };

    render() {
        const style = {
            dialog: {
                width: '400px',
                borderRadius:'5px',
            },
            button:{
              width:100,
              height:30,
              backgroundColor:'#50C3FF',
              fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
              // border:`1px solid${Design.settingborder}`,
              color:'white',
              borderRadius:30,
              textAlign:'center',
              lineHeight:'30px',
              marginLeft:'10px',
              letterSpacing:10,
            }
        };
        const actions = [
            <FlatButton
                label={t("Cancel")}
                primary={true}
                onClick={this.handleClose}
                style={style.button}
            />,
            <FlatButton
                label={t("Ok")}
                primary={true}
                onClick={this.handleOK}
                style={style.button}
            />,
        ];

        return (
            <Dialog
                contentStyle={style.dialog}
                ip title={t(this.props.title)}
                actions={actions}
                modal={true}
                open={this.state.open}
            >
            </Dialog>
        );
    }
}

ConfirmDialogComponent.propTypes = {
    title: PropTypes.string.isRequired,
    closeSelf: PropTypes.func.isRequired,
    onOkClick: PropTypes.func.isRequired,
};

class ConfirmDialog extends DialogWrapper {
    getComponent(callback) {
        return (
            <MuiThemeProvider muiTheme={skylineTheme}>
            <ConfirmDialogComponent
                title={this._title}
                closeSelf={this.close}
                onOkClick={callback}
            />
            </MuiThemeProvider>
          );
    }
}

export default ConfirmDialog;
