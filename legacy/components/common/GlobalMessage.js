import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import {t} from 'legacy_utils/i18n';

export default class GlobalMessage extends React.Component {

  constructor(props) {
    super(props);
  }

  handleClick = () => {
    this.handleRequestClose();
  };

  handleRequestClose = () => {
    this.props.onClose();
  };

  render() {
    const lan = localStorage.getItem('skylineDefaultLanguage');
    let backgroundColor = '#21D325';
    switch (this.props.level) {
      case 'error':
        backgroundColor = 'red';
      case 'warn':
        backgroundColor = 'yellow';
      case 'info':
      default:
    }

    const style = {
      container: {
        fontFamily: 'staticbold',
        fontSize: '35px',
      },
      bar: {
        margin: 'auto',
        width: '50%',
        backgroundColor,
        textAlign: 'center'
      }
    };
    let msg = t(this.props.message, lan) || ''
    return (
      <div style = {style.container}>
        <Snackbar bodyStyle = {style.bar} open = {this.props.open} message = {msg} autoHideDuration = {3000}
          onClick = {() => {this.handleClick();}}
          onRequestClose = {() => {this.handleRequestClose();}}/>
      </div >
    );
  }
}