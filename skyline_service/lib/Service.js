import UserService from './UserService';
import ProjectService from './ProjectService';
// import CodeService from './CodeService';
// import AnswerService from './AnswerService';
import ProjectChallengeService from './ProjectChallengeService';

class SkylineService {
  constructor() {
    this.projectChallenge = new ProjectChallengeService();
    this.project = new ProjectService();
    this.user = new UserService();
  }
}

SkylineService.createService = () => {
  return new SkylineService();
};

module.exports = SkylineService;
