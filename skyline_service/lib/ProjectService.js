import { getApi, postHeader } from './utils/api';

class ProjectService {
  constructor() {
    this.projectlist = [];
  }

  listAll(params, callback) {
    const myInit = postHeader({
      ctype: 'all',
      language: 'zh_CN'
    });
    return fetch(getApi('project_all'), myInit)
      .then((response) => { 
        if(response.ok) {
          return response.json();
        } else {
          alert('network not avaliable');
        }
      })
      .then((response) => {
        if (response.errcode) {
          alert(response.errmsg);
          callback(response.errmsg);
        } else {
          response.data.projectlist.map((project) => {
            const p = project;
            p.projectID = p.id;
            p.imageLink = `${getApi()}${project.url_image}`;
            p.videoLink = `${getApi()}${project.url_video}`;
            p.overview = {
              learn: p.knowledge ? p.knowledge.split('\r') : [],
              parts: JSON.parse(p.part_used),
              description: p.overview,
              videoLink: `${getApi()}${project.url_video}`
            };
            p.projectName = p.title || 'Title';
            p.stageID = p.stage;
            p.type = p.ctype;
            p.numChallenges = p.cnt_challenge;
            return p;
          });
          this.projectlist = response.data.projectlist;
          callback(null, response.data.projectlist);
        }
      });
  }

  findRobot(pid, callback) {
    const product = this.projectlist.find((item) => { return item.id === pid; });
    if (product) {
      const robotUrl = product.url_robot;
      fetch(`${getApi()}${robotUrl}`)
      // fetch(`http://127.0.0.1:8081/NOC_P1.json`)
        .then((response) => {
          if(response.ok) {
            return response.json();
          } else {
            alert('network not avaliable');
          }
        })
        .then((response) => {
          if(response.errcode) {
            alert(response.errmsg);
          }
          if (response) {
            callback(null, response);
          }
        }).catch((err) => {
          callback(err);
        });
    }
  }
}

module.exports = ProjectService;
