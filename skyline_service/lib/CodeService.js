import { getApi, postHeader } from './utils/api';

class CodeService {
  constructor() {
    this.code = {
      codelist: [],
      cid_code: null,
      content: null,
    };
  }
  allCode(callback) {
    return fetch(getApi('code_all'), postHeader())
        .then((response) => { return response.json(); })
        .then((response) => {
          if (response.errcode) {
            callback(response.errmsg);
          } else {
            response.data.challengelist.map((challenge) => {
              const ch = challenge;
              return ch;
            });
            this.code = response.data.codelist;
            callback(null, response.data.codelist);
          }
        });
  }
  createCode(keyname, tech, callback) {
    return fetch(getApi('code_create'), postHeader({ keyname, tech }))
        .then((response) => { return response.json(); })
        .then((response) => {
          if (response.errcode) {
            callback(response.errmsg);
          } else {
            this.code.cid_code = response.data.cid_code;
            callback(null, response.data.cid_code);
          }
        });
  }
  commitCode(cidCode, content, callback) {
    return fetch(getApi('code_commit'), postHeader({ cid_code: cidCode, content }))
        .then((response) => { return response.json(); })
        .then((response) => {
          if (response.errcode) {
            callback(response.errmsg);
          } else {
            this.code.content = content;
            this.code.cid_code = cidCode;
            callback(null, response.data.cid_code);
          }
        });
  }
  deleteCode(cidCode, callback) {
    return fetch(getApi('code_delete'), postHeader({ cid_code: cidCode }))
        .then((response) => { return response.json(); })
        .then((response) => {
          if (response.errcode) {
            callback(response.errmsg);
          } else {
            delete this.code.cid_code;
            delete this.code.content;
            callback(null, this.code);
          }
        });
  }
}

module.exports = CodeService;
