import { required } from './utils/lang';
import { getApi, postHeader } from './utils/api';

class ProjectChallengeService {
  constructor() {
    this.challenge = {};
  }
  create(content = required(), callback) {
    const con = content;
    con.projectID = content.projectID || 'timeBasedUUID';
    super.create(con, callback);
  }
  findById(projectID, callback) {
    return fetch(getApi('challenge_all'), postHeader({ pid_project: projectID }))
        .then((response) => {
          if(response.ok) {
            return response.json();
          } else {
            alert('network not avaliable');
          }
        })
        .then((response) => {
          if (response.errcode) {
            callback(response.errmsg);
            alert(response.errmsg);
          } else {
            response.data.challengelist.map((challenge) => {
              const ch = challenge;
              ch.content = JSON.parse(challenge.content);
              ch.level = ch.order;
              ch.type = ch.ctype;
              return ch;
            });
            this.challenge = response.data.challengelist;
            callback(null, response.data.challengelist);
          }
        });
  }
}

module.exports = ProjectChallengeService;
