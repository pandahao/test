const settings = require('electron').remote.require('electron-settings');
const GBS_HOST = settings.get('GBS_HOST') || 'http://gbscn1.roboterra.com.cn';
settings.set('GBS_HOST', GBS_HOST);
const GBS_PORT = settings.get('GBS_PORT') || '9000';
settings.set('GBS_PORT', GBS_PORT);

const CONF = {
  server: {
    address: GBS_HOST,
    port: GBS_PORT
  },
  API: {
    login: '/api/login',
    register: '/api/register',
    reset_password: '/api/reset_password',
    confirm_code: '/api/confirm_code',
    account_update: '/api/account_update',

    project_all: '/api/project_all',

    challenge_all: '/api/challenge_all',
    challenge_all_kn: '/api/challenge_all',

    progress_all: '/api/progress_all',
    progress_set: '/api/progress_set',
    progress_delete: '/api/progress_delete',

    code_all: '/api/code_all',
    code_create: '/api/code_create',
    code_commit: '/api/code_commit',
    code_delete: '/api/code_delete',
  }
};

const getApi = (apiName) => {
  if (apiName) {
    return `${CONF.server.address}:${CONF.server.port}${CONF.API[apiName]}`;
  }
  return `${CONF.server.address}:${CONF.server.port}`;
};

const API = {
  getApi,
  postHeader: (params) => {
    const formData = new FormData();
    if (!params) {
      formData.append('null', 'null');
    } else {
      Object.entries(params).map((entry) => {
        return formData.append(entry[0], entry[1]);
      });
    }
    const myHeaders = new Headers();
    const searchParams = new URLSearchParams();
    searchParams.append('username', localStorage.getItem('username'));
    searchParams.append('userid', localStorage.getItem('userid'));
    searchParams.append('dt_login', localStorage.getItem('dt_login'));
    searchParams.append('sign', localStorage.getItem('sign'));
    myHeaders.append('X-RT-Session', searchParams.toString());
    return {
      method: 'POST',
      headers: myHeaders,
      mode: 'cors',
      cache: 'default',
      body: formData
    };
  }
};

module.exports = API;
