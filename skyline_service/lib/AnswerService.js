import { getApi, postHeader } from './utils/api';

class AnswerService {
  constructor() {
    this.answer = {
      ctype: null,
      cid_challenge: null,
      result: null,
      aid_answer: null
    };
  }

  getAnswer(ctype, cidChallenge, callback) {
    return fetch(getApi('answer_get'), postHeader({ ctype, cid_challenge: cidChallenge }))
        .then((response) => { return response.json(); })
        .then((response) => {
          if (response.errcode) {
            callback(response.errmsg);
          } else {
            Object.assign(this.answer, response.data);
            callback(null, response.data.challengelist);
          }
        });
  }

  setAnswer(ctype, cidChallenge, result, callback) {
    return fetch(getApi('answer_set'), postHeader({ ctype, cid_challenge: cidChallenge, result }))
      .then((response) => { return response.json(); })
      .then((response) => {
        if (response.errcode) {
          callback(response.errmsg);
        } else {
          this.answer.aid_answer = response.data.aid_answer;
          callback(null, response.data.challengelist);
        }
      });
  }
}

module.exports = AnswerService;
