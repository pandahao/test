import sha1 from 'sha1';

import { required } from './utils/lang';
import { getApi, postHeader } from './utils/api';

class UserService {
  constructor() {
    this.user = {
      profile: {},
      username: localStorage.getItem('username'),
      userid: localStorage.getItem('userid'),
      dt_login: localStorage.getItem('dt_login'),
      sign: localStorage.getItem('sign'),
      signOut: this.signOut.bind(this),
      getUserProfileAsync: this.getUserProfileAsync.bind(this),
      updateUserProfileAsync: this.updateUserProfileAsync.bind(this),
      getUserProgressAsync: this.getUserProgressAsync.bind(this),
      createUserProgressAsync: this.createUserProgressAsync.bind(this),
      getUserCodesAsync: this.getUserCodesAsync.bind(this),
      createUserCodeAsync: this.createUserCodeAsync.bind(this),
      updateUserCodeAsync: this.updateUserCodeAsync.bind(this),
      deleteUserCodeAsync: this.deleteUserCodeAsync.bind(this)
    };
    this.codelist = [];
  }

  /**
   * get the current signin user
   * must have a static function for bluebird
   * @param callback
   */
  static getCurrent(callback) {
    if (this.user.userid) {
      callback(null, this.user);
    } else {
      callback(new Error('can not find current user'));
    }
  }
  getCurrentAsync() {
    return new Promise((resolve, reject) => {
      if (this.user.userid) {
        resolve(this.user);
      } else {
        reject();
      }
    });
  }

  /**
   * signup a new user
   *
   * @param email
   * @param password
   * @param nicnickname
   * @param userProfile
   * @param callback
   */
  signUp(email = required(), password = required(),
        nickname = required(), userProfile = {}, callback) {
    const myInit = postHeader({
      email,
      spassword: sha1(password),
      nickname,
      given_name: userProfile.given_name || 'null',
      family_name: userProfile.family_name || 'null',
      region: userProfile.region || 'null',
      language: userProfile.language || 'zh_CN'
    });
    fetch(getApi('register'), myInit)
    .then((response) => {
      if(response.ok) {
        return response.json();
      } else {
        alert('network not avaliable');
      }
    })
    .then((response) => {
      if (response.errcode) {
        alert(response.errmsg);
        callback(response.errmsg);
      } else {
        Object.assign(this.user, {
          profile: {
            email,
            spassword: sha1(password),
            nickname,
            given_name: '',
            family_name: '',
            region: '',
            language: ''
          }
        });
        callback(null, this.user);
      }
    });
  }

  /**
   *
   * @param userName
   * @param password
   * @param callback
   */
  signInAsync(email = required(), password = required()) {
    const myInit = postHeader({
      email,
      spassword: sha1(password),
      password: sha1(password)
    });
    return fetch(getApi('login'), myInit)
      .then((response) => { return response.json(); })
      .then((response) => {
        return new Promise((resolve, reject) => {
          if (response.errcode) {
            alert(response.errmsg);
            reject(response.errmsg);
          } else {
            Object.assign(this.user, response.data);
            localStorage.setItem('username', response.data.username);
            localStorage.setItem('userid', response.data.userid);
            localStorage.setItem('dt_login', response.data.dt_login);
            localStorage.setItem('sign', response.data.sign);
            resolve(this.user);
          }
        });
      });
  }

  signOut() {
    localStorage.clear();
    this.user.userid = '';
    this.user.sign = '';
    this.user.username = '';
    this.user.dt_login = '';
  }

  /**
   * confirm newly created user by confirmation code
   * @param userName
   * @param code
   * @param callback
   */
  sendConfirmationCode(email = required(), code = required(), callback) {
    const myInit = postHeader({ email, code });
    return fetch(getApi('confirm_code'), myInit)
      .then((response) => {
        if(response.ok) {
          return response.json();
        } else {
          alert('network not avaliable');
        }
      })
      .then((response) => {
        if (response.errcode) {
          alert(response.errmsg);
          callback(response.errmsg);
        } else {
          callback(null, 'SUCCESS');
        }
        return this;
      });
  }

  confirm(email = required(), callback) {
    const myInit = postHeader({
      email
    });
    return fetch(getApi('reset_password'), myInit)
    .then((response) => {
      if(response.ok) {
        return response.json();
      } else {
        alert('network not avaliable');
      }
    })
    .then((response) => {
      if (response.errcode) {
        alert(response.errmsg);
        callback(response.errmsg);
      } else {
        callback(null, 'SUCCESS');
      }
      return this;
    });
  }

  /**
   * trigger forget password flow, an confirmation code will be sent to user
   * @param email
   * @param callback
   */
  forgetPasswordAsync(email = required()) {
    const myInit = postHeader({
      email
    });
    return fetch(getApi('confirm_code'), myInit)
    .then((response) => {
      if(response.ok) {
        return response.json();
      } else {
        alert('network not avaliable');
      }
    })
    .then((response) => {
      if (!response.errcode) {
        return 'SUCCESS';
      } else {
        alert(response.errmsg);
      }
      return this;
    });
  }

  getUserProfileAsync() {
    return new Promise((resolve, reject) => {
      resolve({ email: this.user.username });
    });
  }

  updateUserProfileAsync(userProfile) {
    if (Object.is(this.user.profile, userProfile)) {
      return new Promise();
    }
    const params = userProfile;
    Object.entries(userProfile).map((entry) => {
      if (!entry[1]) {
        delete params[entry[0]];
      }
      return entry;
    });
    const myInit = postHeader(params);
    return fetch(getApi('account_update'), myInit)
    .then((response) => {
      if(response.ok) {
        return response.json();
      } else {
        alert('network not avaliable');
      }
    })
    .then((response) => {
      if (response.errcode) {
        alert(response.errmsg);
        throw new Error(response.errmsg);
      } else {
        Object.assign(this.user, { userProfile });
      }
    });
  }

  /**
   * set new password to a user from forget password flow
   * @param userName
   * @param code
   * @param newPassword
   * @param callback
   */
  confirmPassword(email = required(), sign = required(), password = required(), callback) {
    const myInit = postHeader({
      email,
      sign,
      spassword1: sha1(password),
      spassword2: sha1(password)
    });
    return fetch(getApi('reset_password'), myInit)
      .then((response) => {
        if(response.ok) {
          return response.json();
        } else {
          alert('network not avaliable');
        }
      })
      .then((response) => {
        if (response.errcode) {
          alert(response.errmsg);
          callback(response.errmsg);
        } else {
          callback(null, response.data);
        }
        return this;
      });
  }

  getUserProgressAsync() {
    if (this.user.progress) {
      return new Promise((resolve) => {
        resolve(this.user.progress);
      });
    }
    const myInit = postHeader();
    const codelist = this.codelist;
    return fetch(getApi('progress_all'), myInit)
      .then((response) => {
        if(response.ok) {
          return response.json();
        } else {
          alert('network not avaliable');
        }
      })
      .then((response) => {
        if(response.errcode) {
          alert(response.errmsg);
        }
        const progresslist = response.data.progresslist;
        progresslist.map((progress) => {
          const p = progress;
          p.getCodeContent = (cidChallenge, callback) => {
            let code;
            if (codelist.length) {
              code = codelist.find((item) => {
                return item.cid_challenge === cidChallenge;
              });
              if (code) {
                callback(null, code);
              } else {
                callback('local code not found');
              }
            } else {
              fetch(getApi('code_all'), postHeader())
              .then((response) => {
                if(response.ok) {
                  return response.json();
                } else {
                  alert('network not avaliable');
                }
              })
              .then((res) => {
                if(res.errcode) {
                  alert(res.errmsg);
                }
                code = res.data.codelist.find((item) => {
                  return item.cid_challenge === cidChallenge;
                });
                if (code) {
                  callback(null, code);
                } else {
                  callback('remote code not found');
                }
              });
            }
          };
          return p;
        });
        this.user.progress = progresslist;
        return response.data.progresslist;
      });
  }

  createUserProgressAsync(projectID, challengeType, level, codeContent, codetype, challengeID) {
    if (challengeType === 'code') {
      fetch(getApi('code_create'), postHeader({
        keyname: `Challenge-${challengeID}`,
        tech: codetype,
        ctype: 'project',
        cid_challenge: challengeID
      }))
        .then((response) => {
          if(response.ok) {
            return response.json();
          } else {
            alert('network not avaliable');
          }
        })
        .then((response) => {
          if(response.errcode) {
            alert(response.errmsg);
          }
          fetch(getApi('code_commit'), postHeader({
            cid_code: response.data.cid_code,
            content: codeContent
          }));
        });
    }

    const myInit = postHeader({
      pid_project: projectID,
      cid_challenge: challengeID
    });
    return fetch(getApi('progress_set'), myInit)
      .then((response) => {
        if(response.ok) {
          return response.json();
        } else {
          alert('network not avaliable');
        }
      })
      .then((res) => {
        if(res.errcode) {
          alert(res.errmsg);
        }
        return this.user.progress;
      });
  }
  getUserCodesAsync() {
    return fetch(getApi('code_all'), postHeader())
      .then((response) => {
        if(response.ok) {
          return response.json();
        } else {
          alert('network not avaliable');
        }
      })
      .then((response) => {
        if(response.errcode) {
          alert(response.errmsg);          
        }
        response.data.codelist.map((code) => {
          const co = code;
          co.getCodeContent = (callback) => {
            return callback(null, co.content || '');
          };
          return co;
        });
        this.codelist = response.data.codelist;
        return response.data.codelist;
      });
  }
  createUserCodeAsync(keyname, content, tech, ctype, cid_challenge) {
    return fetch(getApi('code_create'), postHeader({ keyname, tech, ctype, cid_challenge }))
        .then((response) => {
          if(response.ok) {
            return response.json();
          } else {
            alert('network not avaliable');
          }
        })
        .then((response) => {
          if (!response.errcode) {
            return {
              id: response.data.cid_code,
              keyname,
              tech,
              dt_create: Math.floor(Date.now() / 1000),
              dt_update: Math.floor(Date.now() / 1000),
              getCodeContent: (callback) => {
                callback(null, content);
              }
            };
          } else {
            alert(response.errmsg);
          }
          return this;
        });
  }
  updateUserCodeAsync(cidCode, codeName, content, codeLanguage) {
    return fetch(getApi('code_commit'), postHeader({ cid_code: cidCode, content }))
        .then((response) => {
          if(response.ok) {
            return response.json();
          } else {
            alert('network not avaliable');
          }
        })
        .then((response) => {
          if (!response.errcode) {
            const res = response.data;
            res.id = cidCode;
            res.keyname = codeName;
            res.tech = codeLanguage;
            res.dt_create = Math.floor(Date.now() / 1000);
            res.dt_update = Math.floor(Date.now() / 1000);
            res.getCodeContent = (callback) => {
              callback(null, content);
            };
            return res;
          } else {
            alert(response.errmsg);
          }
          return this;
        });
  }
  deleteUserCodeAsync(cidCode) {
    return fetch(getApi('code_delete'), postHeader({ cid_code: cidCode }))
        .then((response) => {
          if(response.ok) {
            return response.json();
          } else {
            alert('network not avaliable');
          }
        })
        .then((response) => {
          if (!response.errcode) {
            return 'SUCCESS';
          } else {
            alert(response.errmsg);
          }
          return this;
        });
  }
}

module.exports = UserService;
