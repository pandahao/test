const fs = require('fs-extra');

// Dependencies
fs.copySync('./package.json','./dist/package.json');

// Static resource
fs.copySync('./static', './dist/static');

fs.copySync('./LICENSE', './dist/LICENSE');

// Webpack
fs.copySync('./index.html', './dist/index.html');
fs.copySync('./loading.html', './dist/loading.html');
fs.move('./dist/bundle.js', './dist/static/bundle.js', (err) => {
  if (err) return console.error(err)
  console.log("build success!");
})
