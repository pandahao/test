# RoboTerra CastleRock Skyline - ( Skyline App) #

The SPA of Skyline, deploy on GBS CDN.

## Development Environment Setup ##

*Dependencies:*

- node.js 7.X
- git

*Installation Steps: (Make sure you have the permissions by setting bitbucket SSH key on your machine)*

```$ git clone git@bitbucket.org:JiangShang/castle_rock.git ```

```$ cd PATH/TO/castal_rock```

```$ npm install```


## Run Dev Version

*Run dev version server for electron:*

```$ npm run electron```

or

```$ npm start```

*Run dev version server for browser:*

```$ npm run browser```

Then go to the address below

```http://localhost:8080```

If there is lib updates, remove the updated libs from ```node_mdules``` folder, and then run:

```$ npm install```

## Attention

If you don't know what is going on, or you don't know what you are doing, stop immediately, and check with ```Jiang Shang developer3@roboterra.com``` and ask him about the answer to the meaning of life, the universe, and everything.
