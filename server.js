/* eslint-disable*/
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

const config = (process.argv[2] === 'browser') ?  require('./webpack/webpack.browser') : require('./webpack/webpack.electron');


new WebpackDevServer(webpack(config), {
  contentBase : './',
  publicPath: config.output.publicPath,
  hot: true,
  stats: { colors: true },
  historyApiFallback: true
}).listen(8080, 'localhost', (err, result) => {
  if (err) {
    return console.log(err);
  }
  console.log('Listening at http://localhost:8080/');
});
