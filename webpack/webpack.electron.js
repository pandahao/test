const webpack = require('webpack');
const config = require('./webpack.browser.js');
const env = require('../build/env.js');

config.plugins = [
  new webpack.HotModuleReplacementPlugin(),
  new webpack.DefinePlugin({
    __DEVELOPMENT__: env.dev,
    __ELECTRON__: true,
    __SERIAL__: true,
    __CHROME__: false,
    // Indicate browser environment
    __BROWSER__: false,
    __DEVTOOLS__: true  // <-------- DISABLE redux-devtools HERE
  }),
];

config.target = 'electron';

module.exports = config;
