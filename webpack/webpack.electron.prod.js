const path = require('path');
const webpack = require('webpack');
const base = require('./webpack.browser.js');
const env = require('../build/env.js');

base.entry = [
  './src/index'
];
delete base.devtool;
base.output = {
  path: path.resolve('./dist'),
  filename: 'bundle.js',
  publicPath: '/static/'
};

base.plugins = [
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production'),
    },
    __DEVELOPMENT__: env.dev,
    __ELECTRON__: true,
    __SERIAL__: true,
    __CHROME__: false,
    // Indicate browser environment
    __BROWSER__: false,
    __DEVTOOLS__: false  // <-------- DISABLE redux-devtools HERE
  }),
  new webpack.optimize.OccurenceOrderPlugin(true),
  new webpack.optimize.DedupePlugin()
];

base.module.loaders[0].loaders.splice(0, 1);

base.target = 'electron';

module.exports = base;
