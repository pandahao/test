const path = require('path');
const webpack = require('webpack');
const base = require('./webpack.browser.js');
const env = require('../build/env.js');

base.entry = [
  './src/index'
];
base.devtool = 'source-map';
base.output = {
  path: path.resolve('./dist'),
  filename: 'bundle.js',
  publicPath: '/static/'
};

base.plugins = [
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production'),
    },
    __DEVELOPMENT__: env.dev,
    __SERIAL__: true,
    __ELECTRON__: false,
    __CHROME__: true,
    // Indicate browser environment
    __BROWSER__: false,
    __DEVTOOLS__: false  // <-------- DISABLE redux-devtools HERE
  }),
  // new webpack.optimize.OccurenceOrderPlugin(true),
  // new webpack.optimize.DedupePlugin(),
    // new webpack.optimize.UglifyJsPlugin({
    //     exclude:/\.min\.js$/,
    //     output: {
    //         comments: false
    //     },
    //     comments: false,
    //     compress: {
    //         warnings: false,
    //         screw_ie8: true
    //     }
    // })
];

base.module.loaders[0].loaders.splice(0, 1);

// base.node = {
//     __dirname: false,
//     __filename: false
//   }

module.exports = base;
