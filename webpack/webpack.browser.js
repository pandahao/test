const path = require('path');
const webpack = require('webpack');

module.exports = {
    // Enable source map in dev
  devtool: 'eval',
  entry: [
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    './src/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      __DEVELOPMENT__: true,
      __ELECTRON__: false,
      __SERIAL__: false,
      __CHROME__: false,
      // Indicate browser environment
      __BROWSER__: true,
      __DEVTOOLS__: true  // <-------- DISABLE redux-devtools HERE
    }),
  ],
  module: {
    noParse: [/skyline_core/],
    // Add more loaders here if want to use less instead of inline style
    // Also can use JSON loader to directly load json
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loaders: ['react-hot', 'babel'],
      include: [
        path.resolve('src'),
        path.resolve('legacy'),
        path.resolve('skyline_service')
      ]
    }, {
      test: /\.json$/,
      loader: 'json'
    }]
  },
  progress: true,
  resolve: {
    root: [
      path.resolve('./src')
    ],
    extension: ['js', 'jsx'],
    alias: {
      skyline_core: path.resolve('./skyline_core'),
      skyline_service: path.resolve('./skyline_service'),
      legacy_containers: path.resolve('./legacy/containers'),
      legacy_components: path.resolve('./legacy/components'),
      legacy_actions: path.resolve('./legacy/actions'),
      legacy_reducers: path.resolve('./legacy/reducers'),
      legacy_styles: path.resolve('./legacy/styles'),
      legacy_mock: path.resolve('./legacy/mock_data'),
      // Use src config
      config: path.resolve('./src/config'),
      legacy_lib: path.resolve('./legacy/lib'),
      legacy_assets: path.resolve('./legacy/assets'),
      legacy_utils: path.resolve('./legacy/utils'),
      components: path.resolve('./src/components'),
      containers: path.resolve('./src/containers'),
      assets: path.resolve('./src/assets'),
    }
  }
};
