import React, { Component } from 'react';
import Radium from 'radium';

const transationssframes = Radium.keyframes({
  '0%': {
    transform: 'translateY(-100%)'
  },
  '100%': {
    transform: 'translateY(0)'
  }
}, 'transationss');
const style = {
  root: {
    position: 'absolute',
    left: '0',
    top: '0',
    width: '100px',
    height: '100px',
    margin: '0 auto',
    backgroundColor: '#50c3ff',
    marginTop: '40px',
    animationDuration: '2s',
    animationIterationCount: '1',
    animationTimingFunction: 'linear',
    animationName: transationssframes,
  },
};
class Transitions extends React.Component {
  render() {
    return (
      <div style={style.root} />
    );
  }
}
export default Radium(Transitions);
