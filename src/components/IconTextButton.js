import React from 'react';
import { CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';


const IconTextButton = ({ text, children, isActive, onClick, invert, color }) => {
  const style = {
    iconStyle: {
      marginTop: '-6px',
      color: GeneralPalette.darkPurple,
    },
    textStyle: {
      fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
      fontSize: '10px',
      color: invert ? GeneralPalette.white : color,
      textAlign: 'center',
      lineHeight: '20px',
      marginLeft: '-0.8em',
      marginTop: '-23px'
    },
    root: {
      fontSize: '0px',
      display: 'inline-block',
      fontFamily: 'Roboto',
      border: '10px',
      cursor: 'pointer',
      textDecoration: 'none',
      margin: '0px',
      padding: '12px',
      outline: ' none',
      boxSizing: 'border-box',
      fontWeight: 'inherit',
      transform: 'translate(0px, 0px)',
      position: 'relative',
      overflow: 'visible',
      transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
      width: '48px',
      height: '48px',
      lineHeight: '48px',
      background: 'none',
    },
  };
  return (<button
    style={style.root}
    onClick={onClick}
    color={GeneralPalette.brightPurple}
  >
    <div style={style.iconStyle}>
      {React.cloneElement(children, { color: GeneralPalette.darkPurple })}
    </div>
    <div style={style.textStyle}>
      {text}
    </div>
  </button>);
};

export default IconTextButton;
