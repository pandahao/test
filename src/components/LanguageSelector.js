import React from 'react';
import { changeLanguage } from 'legacy_actions/AppActions';
import { connect } from 'react-redux';
import { SUPPORTED_LANGUAGES, CHINESE, ENGLISH } from 'legacy_actions/AppActions';
import { LangIcon } from 'legacy_assets';
import IconsTextButton from './IconsTextButton';

const getCurrentLanguage = () => {
  switch (localStorage.getItem('skylineDefaultLanguage')) {
    case 'CHINESE':
      return 'CHINESE';
    case 'ENGLISH':
      return 'ENGLISH';
    default:
      return 'CHINESE';
  }
};

const nextLanguage = (language) => {
  return (language === 'ENGLISH' ? 'CHINESE' : 'ENGLISH');
};

const languageLabel = (language) => {
  switch (language) {
    case 'ENGLISH':
      return 'EN';
    case 'CHINESE':
      return '中文';
    default:
      return 'Unknown Language';
  }
};


const LanguageSelectorComponent = ({ nextLanguageLabel, onClick }) => {
  return (
    <div>
      <IconsTextButton invert text={nextLanguageLabel} name="Change Language" onClick={onClick}>
        <LangIcon />
      </IconsTextButton>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    nextLanguageLabel: languageLabel(nextLanguage(state.shared.app.language))
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onClick: () => {
      dispatch(changeLanguage(nextLanguage(getCurrentLanguage())));
    }
  };
};

const LanguageSelector = connect(
  mapStateToProps,
  mapDispatchToProps
)(LanguageSelectorComponent);

export default LanguageSelector;
