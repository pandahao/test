import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';


export default class ImageLink extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      error: false
    };
  }
  render() {
    const rootStyle = {
      position: 'relative',
      paddingBottom: '75%',
      marginBottom: 10,

    };
    const coverStyle = {

      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      backgroundSize: 'cover',
      backgroundPosition: 'center center',
      // backgroundColor: GeneralPalette.hover,
      borderRadius: '8px',
      opacity: this.state.loaded ? 0 : 1,
      transition: this.props.transition || 'opacity 1.0s ease',
      zIndex: 11
    };
    const imageStyle = {
      position: 'absolute'
    };
    return (

      <div style={rootStyle}>
        {
          !this.state.error ? (
            <img
              src={this.props.src}
              onLoad={this._imageOnload.bind(this)}
              onError={this._imageOnError.bind(this)}
              style={Object.assign({}, this.props.style, imageStyle)}
            />
          ) : null
        }

        <div style={coverStyle}>
          <img
            src="./static/media/missing_image.png"
            style={Object.assign({}, this.props.style, imageStyle)}
          />
        </div>
        {this.props.children}
      </div>

    );
  }
  _imageOnload() {
    this.setState({ loaded: true });
  }
  _imageOnError() {
    this.setState({ error: true });
  }
}
