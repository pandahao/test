import React from 'react';
import { GeneralPalette, Design } from 'legacy_styles/Colors';
import TranslatedText from 'legacy_containers/TranslatedText';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import { Down } from 'legacy_assets/codepadIcons';

import { ProjectErra, ProjectLearn } from '../../assets/Icons';

const style = {
  root: {
    margin: 'auto',
  },
  space: {
    height: '20px',
    lineHeight: '20px',
  },
  table: {
    marginTop: '50px',
    position: 'relative',
    right: '-15%',
    borderCollapse: 'collapse',
    textAlign: 'left',
    width: '65%',
    color: '#7885A9',
  },
  tableHeader: {
    color: GeneralPalette.buttonBackground,
    fontSize: '15px',
    borderBottom: `0.001em solid${GeneralPalette.buttonBackground}`,
    paddingBottom: '20px',
    textTransform: 'uppercase'
  },
  row: {
    height: '30px',
  },
  button: {
    fontSize: '11px',
    // textAlign: 'center',
    height: '25px',
    lineHeight: '25px',
    borderRadius: '20px',
    backgroundColor: GeneralPalette.white,
    border: `1px solid${Design.buttonborder}`,
    color: Design.text,
      // Top:'5px',
    minWidth: '60px',

  },
  robotNameCell: {
    display: 'flex',
    alignItems: 'center'
  }
};

const formatTime = (time) => {
  let d = new Date(time),
    month = `${d.getMonth() + 1}`,
    day = `${d.getDate()}`,
    year = d.getFullYear();

  if (month.length < 2) {
    month = `0${month}`;
  }
  if (day.length < 2) {
    day = `0${day}`;
  }

  return [year, month, day].join('-');
};

const LoadButton = ({ robot, onClick }) => {
  return (<FlatButton style={style.button} onClick={() => { onClick(robot); }} >
    <TranslatedText text="LOAD" />
  </FlatButton>);
};

const DeleteButton = ({ robot, onClick }) => {
  return (<IconButton style={style.buttons} onClick={() => { onClick(robot); }} >
    <Down />
  </IconButton>);
};

const RobotList = ({ items, onRobotDelete, onRobotLoad }) => {
  const rows = items.filter((robot) => { return robot.raw.ctype === 'freeedit'; }).map((robot, index) =>
    <tr style={style.row} key={index}>
      <td>
        <div style={style.robotNameCell}>
          {robot.raw.tech === 'dnd' ?
            <ProjectErra /> : <ProjectLearn />
          }
          <span style={{ marginLeft: 12 }}>{robot.name}</span>
        </div>
      </td>
      <td>{robot.raw.tech}</td>
      <td>{formatTime(robot.created)}</td>
      <td>{formatTime(robot.updated)}</td>
      <td><LoadButton robot={robot} onClick={onRobotLoad} /></td>
      <td><DeleteButton robot={robot} onClick={onRobotDelete} /></td>
    </tr>
    );
  return (
    <div style={style.root}>
      <table style={style.table}>
        <tbody>
          <tr>
            <th style={Object.assign({}, style.tableHeader, { width: '40%' })}>
              <TranslatedText text={'Name'} />
            </th>
            <th style={Object.assign({}, style.tableHeader, { width: '20%' })}>
              <TranslatedText text={'Type'} />
            </th>
            <th style={Object.assign({}, style.tableHeader, { width: '20%' })}>
              <TranslatedText text={'Created'} />
            </th>
            <th style={Object.assign({}, style.tableHeader, { width: '20%' })}>
              <TranslatedText text={'Updated'} />
            </th>
            <th style={Object.assign({}, style.tableHeader, { width: '10%' })} />
            <th style={Object.assign({}, style.tableHeader, { width: '10%' })} />
          </tr>
          <tr style={style.space} />
          {rows}
        </tbody>
      </table>
    </div>
  );
};

export default RobotList;
