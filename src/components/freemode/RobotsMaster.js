import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import RobotsHeader from 'components/freemode/RobotsHeader';
import RobotGrid from 'components/freemode/RobotGrid';
import RobotList from 'components/freemode/RobotList';
import TranslatedText from 'legacy_containers/TranslatedText';

const style = {
  panel: {
    display: 'block',
    paddingTop: '20px',
    width: '106%',
    height: '100vh',
    overflow: 'auto',
    fontFamily: 'Static',
    backgroundColor: '#fff',
    textAlign: 'center',
    margin: 'auto',
  },
  title: {
    padding: '50px',
    marginTop: '-50px',
    color: GeneralPalette.darkPurple,
    fontSize: '40px',
    paddingTop: '0px',
  },
};

const RobotsMaster = ({ view, items, onViewChange, onRobotDelete, onRobotLoad }) => {
  const robots = items || [];
  const View = view === 'grid' ?
    <RobotGrid items={robots} onRobotDelete={onRobotDelete} onRobotLoad={onRobotLoad} /> :
    <RobotList items={robots} onRobotDelete={onRobotDelete} onRobotLoad={onRobotLoad} />;
  return (
    <div style={style.panel}>
      <RobotsHeader activeView={view} onViewChange={onViewChange} />
      <div style={style.title}>
        <TranslatedText text={'CODE VAULT'} />
      </div>
      {View}
    </div>
  );
};

export default RobotsMaster;
