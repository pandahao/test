import React, { Component, PropTypes } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { t } from 'legacy_utils/i18n';

class SaveRobotDialogComponent extends React.Component {
    state = {
        language: 'cpp',
        open: true,
        error: ''
    };

    error = (error)=> {
        this.setState({ error });
    };

    handleClose = () => {
        this.setState({ open: false });
        this.props.closeSelf();
    };
    handleSave = ()=> {
        this.setState({ error: '' }); // clear error message
        if (this.refs.name.getValue()) {
            this.props.onSaveClick(this.refs.name.getValue(), this.state.language);
        }
        else {
            this.error(t("请输入项目名称1～15个字符"));
        }
    };

    setLanguage = (language) => {
        this.setState({language});
    };

    render() {
        const style = {
            dialog: {
                width: 560
            },
            button:{
              width:110,
              height:38,
              backgroundColor:'#50C3FF',
              // border:`1px solid${Design.settingborder}`,
              color:'white',
              borderRadius:30,
              textAlign:'center',
              lineHeight:'30px',
              marginRight:'26px',
              marginBottom:'39px',
              letterSpacing:10,
            },
            body: {
                display: 'flex',
                alignItems: 'center'
            },
            languageSelector: {
                alignItems: 'center',
                display: 'flex',
                marginLeft: 24
            },
            languageSelectorButton: {
                marginRight: 6
            }
        };

        const {language} = this.state;

        const actions = [
            <FlatButton
                label={t('取消')}
                primary={true}
                onClick={this.handleClose}
                style={style.button}
            />,
            <FlatButton
                label={t("确认新建")}
                primary={true}
                onClick={this.handleSave}
                style={style.button}
            />,
        ];

        return (
            <Dialog contentStyle={style.dialog}
                    ip title={t(this.props.title)}
                    actions={actions}
                    modal={true}
                    open={this.state.open}
            >
                <div style={style.body}>
                    <TextField
                        hintText={t("项目名称")}
                        errorText={this.state.error}
                        ref="name"/>

                    <div style={style.languageSelector}>
                        <RaisedButton label="C++" primary={language === 'cpp'}
                            onClick={() => this.setLanguage('cpp')}
                            style={style.languageSelectorButton} />
                        <RaisedButton label="拖拽" primary={language === 'dnd'}
                            onClick={() => this.setLanguage('dnd')}
                            style={style.languageSelectorButton} />
                    </div>
                </div>
            </Dialog>
        );
    }
}

SaveRobotDialogComponent.propTypes = {
    closeSelf: PropTypes.func.isRequired,
    onSaveClick: PropTypes.func.isRequired,
};

export default SaveRobotDialogComponent;
