import React from 'react';
import { connect } from 'react-redux';
import { GeneralPalette } from 'legacy_styles/Colors';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import TranslatedText from 'legacy_containers/TranslatedText';
import {
    ListViewIcon,
    ListViewSelectedIcon,
    GridViewIcon,
    GridViewSelectedIcon,
    BackArrow
} from 'legacy_assets/listViewIcons.js';

import { createRobot } from 'legacy_actions/FreeModeActions';

const style = {
  root: {
    display: 'flex',
    width: '80%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  backButton: {
    color: '#50C3FF',
    backgroundColor: 'white',
    borderRadius: '20px',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: 'auto',
    border: '2px #50C3FF solid',
    height: '80%',
    paddingLeft: '10px',
    paddingRight: '10px',
    // hoverColor: GeneralPalette.hover,
    // boxShadow: `0px 0px 0px 1px${GeneralPalette.whiteShadow}`,
    width: '150px',
    marginLeft: '50px',
  },
  text: {
    fontSize: 15,
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    marginLeft: 0,
    marginTop: 0
  },
  view: {
    display: 'flex',
    justifyContent: 'space-around',
    width: '100px',
    paddingRight: '100px'
  },
  viewToggle: {
    zoom: 1.5,
    margin: 'none',
    padding: 'none'
  },
  fakeroot: {
    display: 'flex',
    paddingTop: '24px',
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  }

};

const RobotsHeaderComponent = ({ activeView, onViewChange, onNewRobotClick }) => {
  const disableGrid = activeView === 'grid';
  const disableList = activeView === 'list';
  const gridClick = (e) => {
    e.preventDefault();
    !disableGrid && onViewChange('grid');
  };
  const listClick = (e) => {
    e.preventDefault();
    !disableList && onViewChange('list');
  };
  const backClick = (e) => {
    e.preventDefault();
    onViewChange('editor');
  };

  const GridIcon = disableGrid ? <GridViewSelectedIcon /> : <GridViewIcon />;
  const ListIcon = disableList ? <ListViewSelectedIcon /> : <ListViewIcon />;
  return (
    <div style={style.fakeroot}>
      <div style={style.root}>
        <div>
          <FlatButton style={style.backButton} onClick={onNewRobotClick}>
            <span style={style.text}>
              <TranslatedText text={'NEW PROGRAM'} />
            </span>
          </FlatButton>
        </div>
        <div style={style.view}>
          {/* <IconButton style={style.viewToggle} disabled={disableGrid} onClick={gridClick}>
            {GridIcon}
          </IconButton>
          <IconButton style={style.viewToggle} disabled={disableList} onClick={listClick}>
            {ListIcon}
          </IconButton> */}
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    onNewRobotClick: () => {
      dispatch(createRobot());
    },
  };
};

const RobotsHeader = connect(null, mapDispatchToProps)(RobotsHeaderComponent);
export default RobotsHeader;
