import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Dialog from 'material-ui/Dialog';
import FlatTextButton from 'components/FlatTextButton';
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import { openSaveExistingFreeModeSessionDialog, closeSaveExistingFreeModeSessionDialog } from 'legacy_actions/FreeModeActions';
import { GeneralPalette } from 'legacy_styles/Colors';
import { t } from 'legacy_utils/i18n';
/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 */
const buttonStyle = {
  display: 'inline-block',
  margin: '1em'
};

class SaveExistingFreeModeSessionDialogComponent extends React.Component {

  render() {
    const actions = [
      (<div style={buttonStyle}>
        <FlatTextButton
          text="Cancel"
          width="5em"
          invert
          color={GeneralPalette.darkPurple}
          onClick={this.props.handleClose}
        />
      </div>),
      (<div style={buttonStyle}>
        <FlatTextButton
          text="Don't save"
          width="5em"
          color={GeneralPalette.brightPurple}
          onClick={this.props.onDecline}
        />
      </div>),
      (<div style={buttonStyle}>
        <FlatTextButton
          text="Save"
          invert
          width="5em"
          color={GeneralPalette.brightPurple}
          onClick={this.props.onAccept}
        />
      </div>)
    ];

    return (
      <MuiThemeProvider>
        <div>
          <Dialog
            title={t('You have unsaved changes!')}
            actions={actions}
            modal
            open={this.props.open}
            onRequestClose={this.props.handleClose}
          >
            {
            t('All changes will be lost.')
          }
          </Dialog>
        </div>
      </MuiThemeProvider>
    );
  }
}

// SaveExistingFreeModeSessionDialogComponent.propTypes = {
//   open: PropTypes.boolean,
//   handleOpen: PropTypes.function,
//   handleClose: PropTypes.function
// }


const mapStateToProps = (state) => {
  const root = state.freeMode;
  return {
    open: root.showSaveExistingFreeModeSessionDialog
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleOpen: () => {
      dispatch(openSaveExistingFreeModeSessionDialog());
    },
    handleClose: () => {
      dispatch(closeSaveExistingFreeModeSessionDialog());
    }
  };
};

const SaveExistingFreeModeSessionDialog = connect(mapStateToProps, mapDispatchToProps)(SaveExistingFreeModeSessionDialogComponent);

export default SaveExistingFreeModeSessionDialog;
