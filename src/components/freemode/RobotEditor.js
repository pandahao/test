import React, { Component } from 'react';
import Documentation from 'legacy_components/documentation/Documentation';
import CodepadContainer from 'containers/codepad/CodepadContainer';
import VirtualRobotContainer from 'legacy_containers/virtualrobot/VirtualRobotContainer';

class RobotEditor extends Component {
  componentDidMount() {
    console.log('Mounting Robot Editor');
    this.props.setup();
  }
  render() {
    const { docView } = this.props;
    const style = {
      root: {
        height: '100%',
        display: 'inline-block',
        verticalAlign: 'top'
      },
      documentation: {
        // freemode boolean that toggles this from 0 -> 1/3
        width: docView ? '27.90%' : '0%',
      },
      codepad: {
        // freemode boolean that toggles this from 1/2 -> 1/3
        width: docView ? '33.04%' : '45.79%',

      },
      vr: {
        // freemode boolean that toggles this from 1/2 -> 1/3
        overflow: 'hidden',
        width: docView ? '39.06%' : '54.21%'

      }
    };
    const documentationContent = docView ? <Documentation /> : null;
    return (
      <div>
        <div style={Object.assign({}, style.root, style.documentation)}>
          {documentationContent}
        </div>
        <div style={Object.assign({}, style.root, style.codepad)}>
          <CodepadContainer />
        </div>
        <div style={Object.assign({}, style.root, style.vr)}>
          <VirtualRobotContainer />
        </div>
      </div>
    );
  }
}

export default RobotEditor;
