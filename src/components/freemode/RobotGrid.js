import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import IconButton from 'material-ui/IconButton';
import { RobotIcon, DeleteRobotIcon, LoadRobotIcon } from 'legacy_assets/robotsIcons';
import { t } from 'legacy_utils/i18n';
const style = {
  space: {
    height: '20px',
  },
  entiregrid: {
    margin: 'auto',
    minHeight: '0px',
    display: 'flex',
    justifyContent: 'flex-start',
    alignContent: 'flex-start',
    width: '85%',
    flexWrap: 'wrap',
    height: 'auto',
  },
  header: {
    display: 'flex',
    justifyContent: 'center',
    fontSize: '20px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    backgroundColor: 'white',
    color: GeneralPalette.darkPurple,
    height: '1%',
    padding: '10px',
    paddingBottom: '10px',
    textTransform: 'uppercase',
  },
  buttongroup: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingBottom: '10px',
  },
  box: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
    width: '20%',
    height: '20%',
    backgroundColor: 'white',
    margin: '0 1.5% 2% 1.5%',
    boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
  },
  img: {
    margin: 'auto',
    width: '45px',
  },
  icon: {
    width: 'auto',
    height: 'auto'
  }
};

const GridItem = ({ robot, onRobotDelete, onRobotLoad }) => {
  return (
    <div style={style.box}>
      <div style={style.header}>
        {robot.name}
      </div>
      <div style={style.content}>
        <div style={style.img}>
          <RobotIcon />
        </div>
        <div style={style.buttongroup}>
          <IconButton
            style={style.icon}
            onClick={() => onRobotDelete(robot)}
            tooltip={t('Delete Robot')}
            tooltipPosition="top-center"
          >
            <DeleteRobotIcon />
          </IconButton>
          <IconButton
            style={style.icon}
            onClick={() => onRobotLoad(robot)}
            tooltip={t('Load Robot')}
            tooltipPosition="top-center"
          >
            <LoadRobotIcon />
          </IconButton>
        </div>
      </div>
    </div>
  );
};

const GridView = ({ items, onRobotDelete, onRobotLoad }) => {
  return (
    <div style={style.entiregrid}>
      {items.map((robot, index) => {
        return (<GridItem robot={robot} key={index} onRobotDelete={onRobotDelete} onRobotLoad={onRobotLoad} />);
      })}
      <div style={style.space} />
    </div>
  );
};

export default GridView;
