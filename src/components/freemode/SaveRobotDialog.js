import React, { Component } from 'react';
import SaveRobotDialogComponent from 'components/freemode/SaveRobotDialogComponent';
import DialogWrapper from 'legacy_components/common/DialogWrapper';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { GeneralPalette } from 'legacy_styles/Colors';

const skylineTheme = getMuiTheme({
  palette: {
    primary1Color: GeneralPalette.brightPurple,
  }
});

class SaveRobotDialog extends DialogWrapper {
  error(error) {
    if (this.component) {
      this.component.error(error);
    }
  }

  getComponent(callback) {
    return (
      <MuiThemeProvider muiTheme={skylineTheme}>
        <SaveRobotDialogComponent
          title={this._title}
          closeSelf={this.close}
          onSaveClick={callback}
        />
      </MuiThemeProvider>
    );
  }
}

export default SaveRobotDialog;
