import React from 'react';
import { ToolbeltPalette } from 'legacy_styles/Colors';
import IconButton from 'material-ui/IconButton';
import { XMark } from 'legacy_assets/codepadIcons';


function trimError(s) {
    // console.log('Trimming :', s);
  if (s.typeof !== 'string') {
    s = s.toString();
  }

  if (s.indexOf('receiveData timeout after') >= 0) {
    return 'Upload timeout. Please try uploading again.';
  }

  try {
    const news = s.split('^');
    let ss = '';
    if (news.length <= 2) {
          // no line
      s = news[0];
    } else {
          // with line
      s = news[1];
      ss = 'Line';
    }


    const rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    const etrim = /[\/]|.build|uno|usr|share|arduino|libraries|src|sketch.ino:|sketch.cpp:[0-9]+.[0-9]+:|sketch.d/g;

    s = s.replace(rtrim, '').replace(etrim, '');

    if (s.indexOf('note:') >= 0) {
          // find error line number
          // console.log(s.match(/^[0-9]*:[0-9]*:/));
      const line = s.match(/^[0-9]*:[0-9]*:/)[0];
      s = `${line} RoboTerra API Function Specification Not Match!`;
    }
    const lineNum = s.match(/^[0-9]*:[0-9]*:/)[0];
      // console.log(ss,s)
    return [s, lineNum];
  } catch (error) {
    console.log(s);
    return s;
  }
}

class ErrorBanner extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const _style = {
      root: {
        height: 'auto',
        width: '100%',
        position: 'absolute',
        padding: '10px 0px 10px 0px',
        bottom: '0%',
        zIndex: 10,
        background: this.props.codeCompiled ? ToolbeltPalette.challengeError : ToolbeltPalette.compileError,
        display: 'flex',
        flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'flex-start'
      },
      icon: {
        marginLeft: 3,
        backgroundColor: this.props.codeCompiled ? ToolbeltPalette.challengeError : ToolbeltPalette.compileError,
        hoverColor: this.props.codeCompiled ? ToolbeltPalette.challengeError : ToolbeltPalette.compileError
      },
      text: {
        fontSize: 14,
        fontFamily: 'menlo',
      }
    };

    let errorMessage,
      trimmedError;
    if (this.props.error !== undefined) {
      if (this.props.error.stack === undefined) {
        errorMessage = this.props.error;
        if (typeof errorMessage === 'string') {
          errorMessage = errorMessage.split('').map((character) => {
            return (
              character === ' ' ? <span>&nbsp;</span> :
              character === '\n' ? <br /> : <span>{character}</span>
            );
          });
        }
      } else {
        errorMessage = this.props.error.message.toString();
      }

      if ((this.props.codeCompiled) && (typeof (errorMessage) === 'string')) {
        trimmedError = trimError(errorMessage);
        if (typeof (trimmedError) !== 'string') {
          trimmedError = (
            <span style={_style.text}>
              <br />
              COMPILATION ERROR :
              <br /><br />
              LINE : {
                trimmedError[1].slice(0, -1)
              }
              <br /><br />
              REPORT : <br /><br />{trimmedError[0]}
              <br /><br />
            </span>
          );
        } else {
          trimmedError = (
            <span style={_style.text}>
              {trimmedError}
              <br />
            </span>
          );
        }
      }
    }
    return this.props.showBanner ? (
      <div style={_style.root}>
        <IconButton style={_style.icon} onClick={this.props.closeBanner}>
          <XMark />
        </IconButton>
        {
                  ((this.props.codeCompiled) && (typeof (errorMessage) === 'string')) ? (
                    trimmedError
                  ) : (<span
                    style={{
                      marginRight: '1em',
                      display: 'inline-block',
                      fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
                      paddingRight: '1em'
                    }}
                  >
                    {
                      this.props.error
                      // errorMessage
                    }
                  </span>)
                }
      </div>
        ) : (
          <div />
        );
  }
}

export default ErrorBanner;
