import React from 'react';
import { ToolbeltPalette } from 'legacy_styles/Colors';
import IconButton from 'material-ui/IconButton';
import { XMark, Checkmark } from 'legacy_assets/codepadIcons';
import NextChallengePanel from 'containers/codepad/NextChallengePanel';
import TranslatedText from 'legacy_containers/TranslatedText'; //* *
import { zIndex } from 'styles/zIndex';
const HeaderBanner = ({ toggleBanner }) => {
  const _style = {
    root: {
      zIndex: zIndex.medium,
      color: 'white',
      display: 'block',
      position: 'absolute',
      height: 64,
      left: '0.5em',
      width: '100%',
      background: ToolbeltPalette.actionIcon,
      top: '0px'
    },
    icon: {
      width: 25,
      height: 25,
      display: 'inline-block',
      position: 'absolute',
      marginTop: 23,
      marginLeft: 17,
    },
    exit: {
      width: 15,
      height: 15,
      display: 'inline-block',
      position: 'relative',
      float: 'right',
      marginTop: -5,
      marginRight: -185,
      hoverColor: ToolbeltPalette.text,
      zoom: 0.8
    },
    text: {
      fontSize: 14,
      fontFamily: 'menlo',
      display: 'inline-block',
      position: 'absolute',
      verticalAlign: 'top',
      marginLeft: 50,
      marginTop: 18
    }
  };
  return (
    <div style={_style.root}>
      <div style={_style.icon}>
        <Checkmark />
      </div>
      <span style={_style.text}>
        <TranslatedText text={'Congratulations!'} /><br />
        <TranslatedText text={'You did it!'} />
      </span>
      <NextChallengePanel />
      <IconButton style={_style.exit} onClick={toggleBanner}>
        <XMark />
      </IconButton>
    </div>
  );
};

export default HeaderBanner;
