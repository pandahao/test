import React from 'react';
import { ToolbeltPalette } from 'legacy_styles/Colors';
import ToolboxPanel from 'containers/codepad/ToolboxContainer';
import RunButtonPanel from 'containers/codepad/RunButtonContainer';
import TranslatedText from 'legacy_containers/TranslatedText';
const _style = {
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    height: 64,
    width: '100%',
    background: ToolbeltPalette.header,
  },
  right: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  square: {
    position: 'relative',
    width: 48,
    height: 64,
    background: 'transparent',
  },
  cc: {
    color: '#FFFFFF',
    fontSize: '13px',
    fontFamily: 'Static',
    letterSpacing: '6px',
    marginTop: '25px',
    line: 16,
    character: '1px',
    marginLeft: '30px'
  }
};

class ChallengeViewHeader extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div style={_style.root}>
        <div style={_style.cc}><TranslatedText text={'CODEPAD'} /></div>
        <div style={_style.square} />
        <div style={_style.right}>
          <ToolboxPanel />
          <RunButtonPanel />
        </div>
      </div>
    );
  }
}

export default ChallengeViewHeader;
