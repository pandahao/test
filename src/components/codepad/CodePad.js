import React from 'react';
import HeaderContainer from 'containers/codepad/HeaderContainer';
import EditorContainer from 'containers/codepad/EditorContainer';
import ErrorContainer from 'containers/codepad/ErrorContainer';

import DndEditorContainer from '../../containers/dnd-editor/DndEditorContainer';

import { zIndex } from 'styles/zIndex';

export default class Codepad extends React.Component {
  render() {
    const { blurFromListDisplay, isDnd } = this.props;

    const _style = {
      root: {
        background: 'black',
        color: 'white',
        width: '100%',
        WebkitFilter: blurFromListDisplay ? 'blur(3px)' : '',
        zIndex: zIndex.mediumLow,
        position: 'relative',
        height: '100vh',
        display: 'flex',
        flexDirection: 'column'
      },
      blurCover: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: zIndex.mediumHigh
      },
      container: {
        zIndex: zIndex.medium - 2,
        overflow: 'hidden',
        position: 'relative',
        width: '100%',
        height: '100%'
      }
    };

    return (
      <div style={_style.container}>
        {blurFromListDisplay && <div style={_style.blurCover} />}

        <div style={_style.root} id="Editor">
          <HeaderContainer />

          {isDnd ? <DndEditorContainer /> : <EditorContainer />}

          <ErrorContainer />
        </div>
      </div>
    );
  }
}
