/*eslint-disable*/
import React from 'react';
import { ToolbeltPalette, GeneralPalette } from 'legacy_styles/Colors';
import DebugButton from 'components/codepad/DebugButton';
import ResetButton from 'components/codepad/ResetButton';
import ToolboxContentItem from 'components/codepad/ToolboxContentItem';
import {
    ToolboxSave,
    ToolboxSaveAs,
    ToolboxNew,
    ToolboxHamburger,
    ToolboxDoc
} from 'legacy_assets/codepadIcons';
import SaveExistingFreeModeSessionDialog from 'components/freemode/SaveExistingFreeModeSessionDialog';

const ToolboxContent = ({
    challengeType,
    resetCode,
    showToolbox,
    freemode,
    uploading,
    docView,
    robot,
    onRobotSaveClick,
    onRobotSaveAsClick,
    onNewRobotClick,
    onListRobotClick,
    startDebug,
    checkingAnswers,
    onAccept,
    onDecline
}) => {
  const open_style = {
    root: {
      backgroundColor: ToolbeltPalette.button,
      marginTop: freemode ? '0' : '1.3em',
      marginRight: '1em',
      width: 150,
      height: freemode ? 146 : 146, // should be 146 once
      borderRadius: showToolbox ? '0px 0px 5px 5px' : '5px 5px 5px 5px',
      color: ToolbeltPalette.text,
      zIndex: 100,
      transitionProperty: 'transform',
      transitionDuration: '0.5s',
    },
    listItem: {
      height: 37,
      // lineHeight:37
    },
    list: {
      textAlign: 'left',
      height: freemode ? '16.66%' : 'auto',
      width: '100%',
      border: `0.5px solid ${ToolbeltPalette.text}`,
      borderStyle: 'solid none none none'
    },
    text: {
      fontSize: 14,
      display: 'inline-block',
      verticalAlign: 'top',
      marginLeft: 14,
      marginTop: -2,
      fontFamily: 'staticbold',
      color: GeneralPalette.darkPurple
    },
    icon: {
      width: 10,
      height: 10,
      display: 'inline-block',
      marginTop: 0,
      marginLeft: 10,
      zoom: 1
    }
  };
    // TRANSFORM for freemode should be as below:
    // {transform: showToolbox ? 'scale(1,1) translate(0px,83px)' : 'scale(0.95,0.1) translate(0px,0px)'}   'scale(1,1) translate(0px,19px)'
  return freemode ? (
    <div style={Object.assign({}, open_style.root, { transform: showToolbox ? 'scaleY(1) translate(0px,83px)' : 'scale(1,0.1) translate(0px,0px)' }, { zIndex: showToolbox ? 10 : 0 })}>
      <div style={open_style.listItem}>
        {
          <ToolboxContentItem open_style={open_style} text={'DOCUMENTATION'} icon={
            <ToolboxDoc />} click={() => docView(freemode)}
          />
        }
        <ToolboxContentItem open_style={open_style} text={'SAVE'} icon={
          <ToolboxSave />} click={onRobotSaveClick}
        />
        <ToolboxContentItem open_style={open_style} text={'SAVE AS'} icon={
          <ToolboxSaveAs />} click={onRobotSaveAsClick}
        />
      <ToolboxContentItem open_style={open_style} text={'CODE VAULT'} icon={
          <ToolboxHamburger />} click={onListRobotClick}
        />

        {
          <SaveExistingFreeModeSessionDialog onAccept={onAccept} onDecline={onDecline}/>
        }
      </div>
    </div>
    ) : (
      <div style={Object.assign({}, open_style.root, { transform: showToolbox ? 'scaleY(1) translate(0px,60px)' : 'scaleY(0) translate(0px,0px)' }, { zIndex: showToolbox ? 10 : 0 })}>
        <DebugButton open_style={open_style} startDebug={startDebug} checkingAnswers={checkingAnswers} uploading={uploading} />
        <div style={open_style.listItem}>
          <ToolboxContentItem open_style={open_style} text={'DOCUMENTATION'} icon={
            <ToolboxDoc />} click={() => docView(freemode)}
          />
        </div>
        <div style={open_style.listItem}>
          <ToolboxContentItem open_style={open_style} text={'SAVE AS'} icon={
            <ToolboxSaveAs />} click={onRobotSaveAsClick}
          />
        </div>
        <ResetButton open_style={open_style} resetCode={resetCode} uploading={uploading} challengeType={challengeType} />
      </div>
    );
};

export default ToolboxContent;
