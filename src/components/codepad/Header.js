import React from 'react';
import { ToolbeltPalette } from 'legacy_styles/Colors';
import FreemodeHeader from 'containers/freemode/FreemodeHeaderContainer';
import ChallengeViewHeader from 'components/codepad/ChallengeViewHeader';
import SuccessBanner from 'containers/codepad/SuccessBannerContainer';

const _style = {
  rootbanner: {
    display: 'block',
    position: 'relative',
    height: 64,
    width: '100%',
    background: ToolbeltPalette.backgroundDark
  },
  square: {
    position: 'relative',
    width: 48,
    height: 64,
    background: 'transparent'
  }
};

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return false ? (
      <div style={_style.rootbanner}>
        <div style={_style.square} />
        <SuccessBanner />
      </div>
        ) : this.props.freemode ? (
          <FreemodeHeader />
        ) : (
          <ChallengeViewHeader />
        );
  }
}

export default Header;
