import React from 'react';

import IconButton from 'material-ui/IconButton';
import AvPlayArrow from 'material-ui/svg-icons/av/play-arrow';
import { WarningIcon, LoadingIcon, RunIcon, DisabledRunIcon } from 'legacy_assets/codepadIcons';

class FreemodeRunButton extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.connect();
  }
  // componentWillUnmount() {
  //   console.log('disconnecting');
  //   this.props.disconnect();
  // }

  componentDidUpdate() {
    console.log('componentDidUpdate ---> CodePad');
    if (!this.props.uploading && !this.props.connected) {
      this.props.connect();
    }
  }

  uploadCode() {
    const { checkCode, dndEditor: { code }, sketchChange, erraMode, upload } = this.props;

    if (erraMode) {
      sketchChange(generateCode(code));
    }

    upload(checkCode);
  }

  render() {
    const _style = {
      errorIcon: {
        display: 'fixed',
        transitionDuration: '0.5s',
        transitionProperty: 'opacity',
        bottom: 96,
        right: 3
      },
      loadIcon: {
        display: 'fixed',
        transitionDuration: '0.5s',
        transitionProperty: 'opacity',
        bottom: 48,
        right: 3
      },
      runIcon: {
        display: 'fixed',
        transitionDuration: '0.8s',
        transitionProperty: 'transform',
        zIndex: 100,
        width: '100%',
        marginTop: 2
      },
      opac: {
        transitionDuration: '1s',
        transitionProperty: 'opacity'
      },
      buttons: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        width: '90%',
        alignItems: 'center'
      }
    };
    const runButton = !this.props.connected && !this.props.uploading
      ? (<IconButton style={_style.runIcon} disabled>
        <DisabledRunIcon />
      </IconButton>)
      : (<div
        style={Object.assign({}, _style.opac, {
          opacity: !this.props.uploading && this.props.showRun ? 1 : 0
        })}
      >
        <IconButton
          style={Object.assign({}, _style.runIcon, {
            transform: !this.props.uploading && this.props.showRun
                ? 'scale(1,1)'
                : 'scale(0.2,0.2) translate(25px,15px)'
          })}
          onClick={() => this.upload()}
          disableTouchRipple
        >
          <RunIcon />
        </IconButton>
      </div>);

    return (
      <div style={_style.buttons} id="freemode-run-button">
        {runButton}
        <IconButton
          disabled
          style={Object.assign({}, _style.loadIcon, { opacity: this.props.uploading ? 1 : 0 })}
        >
          <LoadingIcon />
        </IconButton>
        <IconButton
          disabled
          style={Object.assign({}, _style.errorIcon, {
            opacity: !this.props.uploading && !this.props.showRun ? 1 : 0
          })}
        >
          <WarningIcon />
        </IconButton>
      </div>
    );
  }
}

export default FreemodeRunButton;
