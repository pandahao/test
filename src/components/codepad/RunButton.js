/* eslint-disable*/

import React from 'react';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import AvPlayArrow from 'material-ui/svg-icons/av/play-arrow';
import { WarningIcon, LoadingIcon, RunIcon, DisabledRunIcon } from 'legacy_assets/codepadIcons';
import TranslatedText from 'legacy_containers/TranslatedText';

import generateCode from '../../dnd-editor/utils/code-gen';

class RunButton extends React.Component {
  // componentWillMount() {
  //   console.log('Mounting Run Button');
  //   this.props.connect();
  // }
  // componentWillUnmount() {
  //   console.log('Unmounting Run Button');
  //   this.props.disconnect();
  // }
  // componentDidUpdate() {
  //   if (!this.props.uploading && !this.props.connected) {
  //     this.props.connect();
  //   }
  // }

  uploadCode() {
    const { checkCode, dndEditor: { code }, sketchChange, isDnd, upload } = this.props;

    if (isDnd) {
      sketchChange(generateCode(code));
    }

    upload(checkCode);
  }

  render() {
    const _style = {
      errorIcon: {
        display: 'fixed',
        transitionDuration: '0.5s',
        transitionProperty: 'opacity'
      },
      loadIcon: {
        display: 'fixed',
        transitionDuration: '0.5s',
        transitionProperty: 'opacity'
      },
      runIcon: {
        display: 'fixed',
        transitionDuration: '0.8s',
        transitionProperty: 'transform',
        zIndex: 100,
        width: '100%',
        backgroundColor: '#676d83',
        borderRadius: '45px',
        marginLeft: '-8px',
        marginTop: '-5px',
        color: '#FFFFFF',
        fontSize: '13px',
        fontFamily: 'StaticBold',
        line: 16
      },
      opac: {
        transitionDuration: '1s',
        transitionProperty: 'opacity'
      },
      buttons: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        width: '100%',
        alignItems: 'center',
        marginLeft: '1em',
        marginRight: '1em',
        marginTop: '1.2em'
      }
    };

    const { connected, showRun, uploading } = this.props;
    let runButton = null;

    if (!connected && !uploading) {
      runButton = (
        <FlatButton style={_style.runIcon} disabled>
          <TranslatedText text={'UPLOAD'} />
        </FlatButton>
      );
    } else {
      runButton = !uploading &&
        showRun &&
        <div style={_style.opac}>
          <FlatButton
            style={Object.assign({}, _style.runIcon, {
              transform: !uploading && showRun
                ? 'scale(1,1)'
                : 'scale(0.2,0.2) translate(25px,15px)'
            })}
            onClick={() => this.uploadCode()}
            disableTouchRipple
          >
            <TranslatedText text={'UPLOAD'} />
          </FlatButton>
        </div>;
    }

    return (
      <div style={_style.buttons}>
        {runButton}
        {uploading &&
          <FlatButton disabled style={_style.loadIcon}>
            <LoadingIcon />
          </FlatButton>}

        {!uploading &&
          !showRun &&
          <FlatButton disabled style={_style.errorIcon}>
            <WarningIcon />
          </FlatButton>}
      </div>
    );
  }
}

export default RunButton;
