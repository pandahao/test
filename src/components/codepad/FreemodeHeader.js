import React, { Component } from 'react';
import { PropTypes } from 'react';
import { ToolbeltPalette } from 'legacy_styles/Colors';
import ToolboxPanel from 'containers/codepad/ToolboxContainer';
import FreemodeRunButtonPanel from 'containers/codepad/FreemodeRunButtonContainer';
import TranslatedText from 'legacy_containers/TranslatedText';
import IconButton from 'material-ui/IconButton';
import CloudUploadIcon from 'material-ui/svg-icons/file/cloud-upload';
import { saveCurrentRobot } from 'legacy_actions/FreeModeActions';
import { GeneralPalette } from 'legacy_styles/Colors';
import SaveExistingFreeModeSessionDialog
  from 'components/freemode/SaveExistingFreeModeSessionDialog';
import { t } from 'legacy_utils/i18n';

import ConfirmDialog from 'legacy_components/common/ConfirmDialog';

const _style = {
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'baseline',
    flexDirection: 'row',
    height: 64,
    width: '100%',
    background: ToolbeltPalette.header
  },
  right: {
    display: 'flex',
    fontFamily: 'staticbold',
    alignItems: 'center'
  },
  left: {
    marginLeft: '1.5em'
  },
  saves: {
    borderRadius: '10px',
    backgroundColor: '#50c3ff',
    height: 10,
    width: 10,
    border: 'none',
    cursor: 'pointer',
    outline: 'none',
    marginLeft: '-130px',
    ':hover': {
      backgroundColor: '#f00'
    }
  }
};

class FreeModeHeader extends Component {
  componentWillReceiveProps(nextProp) {
    const { title, dirty } = nextProp;
    if (dirty) {
      window.onbeforeunload = (e) => {
        if (window.SkylineReadyToClose) {
        } else {
          e.returnValue = false;
          const confirm = new ConfirmDialog(
            `${t('You have unsaved changes in free mode!')} ${t('If quit now, all changes will be lost.')}`
          );
          confirm.show(
            () => {
              window.SkylineReadyToClose = true;
              window.close();
            },
            () => {}
          );
        }
      };
    } else {
      window.SkylineReadyToClose = false;
      window.onbeforeunload = null;
    }
  }
  render() {
    const { robot, title, dirty, onRobotSaveClick } = this.props;
    const indicatorStyle = {
      width: '0.7em',
      height: '0.7em',
      marginRight: '1em',
      borderRadius: '1em',
      display: dirty ? 'inline-block' : 'none',
      backgroundColor: GeneralPalette.brightPurple
    };
    return (
      <div style={_style.root}>
        <div style={_style.left}>
          <div style={indicatorStyle} />
        </div>
        <div
          style={{
            fontSize: '18px',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            width: '30em'
          }}
        >
          <TranslatedText text={title} />
        </div>
        <div>
          {<SaveExistingFreeModeSessionDialog />}
        </div>
        <div style={_style.right}>
          <ToolboxPanel />
          <FreemodeRunButtonPanel />
        </div>
      </div>
    );
  }
}

export default FreeModeHeader;
