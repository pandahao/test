import React from 'react';
import { ToolbeltPalette } from 'legacy_styles/Colors';
import FlatButton from 'material-ui/FlatButton';
import TranslatedText from 'legacy_containers/TranslatedText';

class NextButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const _style = {
      text: {
        fontSize: 14,
        fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
        display: 'inline-block',
        verticalAlign: 'top',
        marginLeft: 12,
        marginTop: 0,
      },
      root: {
        display: 'inline-block',
        verticalAlign: 'top',
        textAlign: 'left',
        float: 'right',
        backgroundColor: ToolbeltPalette.text,
        marginTop: 18,
        marginRight: 25,
        paddingLeft: 8,
        width: 135,
        height: 35,
        borderRadius: 20,
        color: ToolbeltPalette.button
      }
    };

    return (
      <FlatButton style={_style.root} onClick={this.props.nextChallenge}>
        <span style={_style.text}>
          <TranslatedText text={'NEXT CHALLENGE'} />
        </span>
      </FlatButton>
    );
  }
}


export default NextButton;
