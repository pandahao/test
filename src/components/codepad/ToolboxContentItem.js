import React from 'react';
import { ToolbeltPalette, GeneralPalette } from 'legacy_styles/Colors';
import TranslatedText from 'legacy_containers/TranslatedText';
import FlatButton from 'material-ui/FlatButton';
const ToolboxContentItem = ({ click, icon, text }) => {
  const open_style = {
    list: {
      textAlign: 'left',
      height: '100%',
      width: '100%',
      border: `0.5px solid ${ToolbeltPalette.text}`,
      borderStyle: 'solid none none none'
    },
    text: {
      fontSize: 14,
      display: 'inline-block',
      verticalAlign: 'top',
      marginLeft: 14,
      marginTop: -2,
      fontFamily: 'staticbold',
      color: GeneralPalette.darkPurple
    },
    icon: {
      width: 10,
      height: 10,
      display: 'inline-block',
      marginTop: 0,
      marginLeft: 10,
      zoom: 1
    }
  };

  return (
    <FlatButton style={open_style.list} onClick={click}>
      <span style={open_style.icon}>
        {icon}
      </span>
      <span style={open_style.text}>
        <TranslatedText text={text} />
      </span>
    </FlatButton>
  );
};

export default ToolboxContentItem;
