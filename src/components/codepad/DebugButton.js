import React from 'react';

import FlatButton from 'material-ui/FlatButton';

import { ToolboxDebug } from 'legacy_assets/codepadIcons';
import TranslatedText from 'legacy_containers/TranslatedText';

class DebugButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let disabled;
    let iconStyle;
    let textStyle;
    if (this.props.checkingAnswers && !this.props.uploading) {
      disabled = false;
      iconStyle = Object.assign({}, this.props.open_style.icon, { opacity: 1 });
      textStyle = Object.assign({}, this.props.open_style.text, { opacity: 1 });
    } else {
      disabled = true;
      iconStyle = Object.assign({}, this.props.open_style.icon, { opacity: 0.2 });
      textStyle = Object.assign({}, this.props.open_style.text, { opacity: 0.2 });
    }

    return (
      <FlatButton style={this.props.open_style.list} disabled={disabled} onClick={this.props.startDebug}>
        <span style={iconStyle}>
          <ToolboxDebug />
        </span>
        <span style={textStyle}>
          <TranslatedText text={'DEBUG'} />
        </span>
      </FlatButton>
    );
  }
}

export default DebugButton;
