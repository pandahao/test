import React from 'react';
import { ToolbeltPalette, GeneralPalette } from 'legacy_styles/Colors';
import ToolboxContentContainer from 'containers/codepad/ToolboxContentContainer';
import FlatButton from 'material-ui/FlatButton';
import { ToolboxWrench, ToolboxDoc, ToolboxDownArrow } from 'legacy_assets/codepadIcons';
import TranslatedText from 'legacy_containers/TranslatedText'; //* *

const Toolbox = ({ showToolbox, toolbox, openProjectListView, opendocs }) => {
  const _style = {
    toolbox: {
      display: 'inline-block',
      textAlign: 'left',
      backgroundColor: ToolbeltPalette.button,
      marginTop: '1em',
      marginRight: '1em',
      width: 150,
      height: 30,
      borderRadius: showToolbox ? '5px 5px 0px 0px' : '5px 5px 5px 5px',
      color: ToolbeltPalette.text,
      zIndex: 50,
      opacity: 1
    },
    icon: {
      width: 10,
      height: 10,
      display: 'inline-block',
      marginTop: -2,
      marginLeft: 9,
      zoom: 1,
      transitionProperty: 'opacity',
      transitionDuration: '1.5s'
    },
    text: {
      fontSize: 14,
      bottom: 2,
      display: 'inline-block',
      position: 'relative',
      marginLeft: 14,
      fontFamily: 'staticbold',
      transitionProperty: 'opacity',
      transitionDuration: '1s'
    },
    toolboxbutton: {
      display: 'fixed',
      position: 'relative',
      transitionProperty: 'z-index',
      transitionDuration: '0.5s'
    },
    toolboxcontent: {
      display: 'fixed',
      position: 'absolute',
      zIndex: 10,
      marginTop: '-85px'
    },
    iconArrow: {
      width: 10,
      height: 10,
      display: 'inline-block',
      float: 'right',
      marginTop: -4,
      marginRight: 4,
      zoom: 1.3,
      transitionProperty: 'opacity',
      transitionDuration: '1.5s',
      transform: {
        transitionProperty: 'transform',
        transitionDuration: '0.5s'
      }
    },
    tempIcon: {
      backgroundColor: ToolbeltPalette.button,
      marginTop: 14,
      width: 100,
      height: 32,
      color: 'black',
      borderRadius: 25,
      fontSize: 14,
      fontFamily: 'staticbold',
      boxShadow: `0px 1px 1px 0px${GeneralPalette.menuBG}`
    },
    tempPic: {
      marginRight: '6px',
      marginTop: '10px',
      paddingTop: '10px'
    }
  };

  return (
    <div>
      <div
        style={Object.assign({}, _style.toolboxbutton, { zIndex: showToolbox ? 1 : 100 })}
        onClick={toolbox}
      >
        <FlatButton style={_style.toolbox}>
          <div style={Object.assign({}, _style.icon, { opacity: showToolbox ? 0.2 : 1 })}>
            {/* <ToolboxWrench /> */}
            <img src="./static/media/icon_toolbox.png" />
          </div>
          <div style={Object.assign({}, _style.text, { opacity: showToolbox ? 0.2 : 1 })}>
            <TranslatedText text={'TOOLBOX'} />
          </div>
          <span style={Object.assign({}, _style.iconArrow, { opacity: showToolbox ? 0.2 : 1 })}>
            <div
              style={Object.assign({}, _style.iconArrow.transform, {
                transform: showToolbox ? 'translate(0px,3px) rotateX(180deg)' : 'rotate(0deg)'
              })}
            >
              <ToolboxDownArrow />
            </div>
          </span>
        </FlatButton>
      </div>
      <div style={_style.toolboxcontent}>
        <ToolboxContentContainer />
      </div>
    </div>
  );
};

export default Toolbox;
