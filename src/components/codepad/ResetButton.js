/*eslint-disable*/
import React from 'react';

import FlatButton from 'material-ui/FlatButton';

import { ResetIcon } from 'legacy_assets/codepadIcons';
import TranslatedText from 'legacy_containers/TranslatedText'; //* *

class ResetButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let disabled;
    let iconStyle;
    let textStyle;
    if (this.props.uploading || this.props.challengeType !== 'code') {
      disabled = true;
      iconStyle = Object.assign({}, this.props.open_style.icon, { opacity: 0.2 });
      textStyle = Object.assign({}, this.props.open_style.text, { opacity: 0.2 });
    } else {
      disabled = false;
      iconStyle = Object.assign({}, this.props.open_style.icon, { opacity: 1 });
      textStyle = Object.assign({}, this.props.open_style.text, { opacity: 1 });
    }
    //console.log(this.props.uploading,this.props.challengeType,disabled)

    return (
      <FlatButton style={this.props.open_style.list} disabled={disabled} onClick={this.props.resetCode}>
        <span style={iconStyle}>
          <ResetIcon />
        </span>
        <span style={textStyle}>
          <TranslatedText text={'RESET CODE'} />
        </span>
      </FlatButton>
        );
  }
}

export default ResetButton;
