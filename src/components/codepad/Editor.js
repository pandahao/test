import _ from 'underscore';

import React from 'react';
import ReactDOM from 'react-dom';
import AceEditor from 'react-ace';
import brace from 'brace';

import 'legacy_styles/c_cpp';
import 'legacy_styles/editor/castlerock_theme';

import { ToolbeltPalette, GeneralPalette } from 'legacy_styles/Colors';

const Editor = ({ makeChange, sketch, blurFromListDisplay }) => {
  const style = {
    filler: {
      display: 'block',
      position: 'relative',
      height: 30,
      width: '100%',
      background: ToolbeltPalette.backgroundDark,
      marginBottom: -15
    },
    square: {
      display: 'inline-block',
      position: 'relative',
      width: 48,
      height: 30,
      background: ToolbeltPalette.backgroundLight,

    },
    bottomFiller: {
      position: 'fixed',
      height: 30,
      width: '100%',
      background: ToolbeltPalette.backgroundDark,
    }
  };

  return (
    <div>
      <div style={style.filler}>
        <div style={style.square} />
      </div>
      <div>
        <AceEditor
          name="mainCodepad"
          mode="c_cpp"
          onChange={_.debounce(makeChange, 500)}
          theme="castle-rock"
          width="100%"
          height="91vh"
          fontSize={12} // This line raises warning but works.
          value={sketch}
          readOnly={blurFromListDisplay}
          highlightActiveLine={!blurFromListDisplay}
        />
      </div>
      <div style={style.bottomFiller}>
        <div style={style.square} />
      </div>
    </div>
  );
};

export default Editor;
