import React, { Component, PropTypes } from 'react';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import DialogWrapper from 'legacy_components/common/DialogWrapper';
import { t } from 'legacy_utils/i18n';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';

const skylineTheme = getMuiTheme({
    palette: {
        primary1Color: GeneralPalette.brightPurple,
    }
});

const style = {
    dialog: {
        width: '400px',
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    },
    btnGroup: {
    },
    cancelBtn: {
        marginRight: '30px',
        textAlign: 'center',
        borderRadius: '20px',
        backgroundColor: '#BBBBBB',
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        color: GeneralPalette.white,
        boxShadow: 'rgb(226, 231, 245) 0px 1.5px 2px 1px'
    },
    applyBtn: {
        marginRight: '30px',
        textAlign: 'center',
        borderRadius: '20px',
        backgroundColor: '#50C3FF',
        fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
        color: GeneralPalette.white,
        boxShadow: 'rgb(226, 231, 245) 0px 1.5px 2px 1px'
    }
}

class ChangePwdDialogComponent extends React.Component {
    state = {
        open: true,
        oldError: '',
        newError: ''
    };

    error = (newError)=> {
        this.setState({ newError });
    };

    oldError = (oldError)=> {
        this.setState({ oldError });
    };

    handleClose = () => {
        this.setState({ open: false });
        this.props.closeSelf();
    };
    handleSave = ()=> {
        const lan = localStorage.getItem('skylineDefaultLanguage');
        this.setState({ oldError: '', newError: '' }); // clear error message
        let oldPassword = this.refs.oldPassword.getValue();
        let newPassword = this.refs.newPassword.getValue();
        if (oldPassword && newPassword) {
            this.props.onSaveClick({ oldPassword, newPassword });
        }
        else if (!oldPassword) {
            this.oldError(t("Please enter old password",lan));
        }
        else if (!newPassword) {
            this.error(t("Please enter new password",lan));
        }
    };

    render() {
        const actions =
            <div style={style.btnGroup}>
            <FlatButton
                style={style.cancelBtn}
                label={t("cancel",lan)}
                onClick={this.handleClose}
            />
            <FlatButton
                style={style.applyBtn}
                label={t("change",lan)}
                onClick={this.handleSave}
            />
        </div>;
        return (
            <Dialog contentStyle={style.dialog}
                    ip title={t(this.props.title)}
                    actions={actions}
                    modal={true}
                    open={this.state.open}
            >
                <TextField
                    type="password"
                    hintText={t("Old Password",lan)}
                    errorText={this.state.oldError}
                    ref="oldPassword"/>
                <br/>
                <TextField
                    type="password"
                    hintText={t("New Password",lan)}
                    errorText={this.state.newError}
                    ref="newPassword"/>
            </Dialog>
        );
    }
}

ChangePwdDialogComponent.propTypes = {
    closeSelf: PropTypes.func.isRequired,
    onSaveClick: PropTypes.func.isRequired,
};

class ChangePwdDialog extends DialogWrapper {
    error(error) {
        if (this.component) {
            this.component.error(error);
        }
    }

    getComponent(callback) {
        return (
            <MuiThemeProvider muiTheme={skylineTheme}>
                <ChangePwdDialogComponent
                    title={this._title}
                    closeSelf={this.close}
                    onSaveClick={callback}
                />
            </MuiThemeProvider>);
    }
}

export default ChangePwdDialog;
