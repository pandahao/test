import React from 'react';
import TranslatedText from 'legacy_containers/TranslatedText';

import { ChallengePalette, GeneralPalette } from 'legacy_styles/Colors';

const SettingsHeader = () => {
  const style = {
    title: {
      marginLeft: '30px',
      color: GeneralPalette.darkPurple,
      letterSpacing: '4.2px',
      fontSize: '14px',
      fontFamily: 'staticbold'
    },
    root: {
      width: '100%',
      background: GeneralPalette.menuBG,
      position: 'absolute',
    },
    bar: {
      display: 'flex',
      alignItems: 'center',
      height: '64px',
      zIndex: 20,
      marginLeft: '10px',
    }
  };

  return (
    <div style={style.root}>
      <div style={style.bar}>
        <span style={style.title}>
          <TranslatedText text={'SETTINGS'} />
        </span>
      </div>
    </div>
  );
};

export default SettingsHeader;
