import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import SettingsPanelContainer from 'containers/Settings/SettingsPanelContainer';
import SettingsContentContainer from 'containers/Settings/SettingsContentContainer';
import SettingsHeader from 'components/Settings/SettingsHeader';
// material-ui theme support
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SkylineTheme from 'styles/Theme';

const style = {
  root: {
    height: '100vh',
    overflow: 'auto',
    fontFamily: 'Static',
    margin: 'auto',
    verticalAlign: 'top',
    width: '100%'
  },
  leftpart: {
    display: 'inline-block',
    height: '100%',
    position: 'absolute',
    width: '26.33%',
    marginLeft: '-5px',
    verticalAlign: 'top',
    backgroundColor: GeneralPalette.menuBG,
    borderRight: 'solid 1px #Cdd1d1',
  },
  rightpart: {
    height: '100%',
    position: 'absolute',
    width: '66.66%',
    marginLeft: '28.33%',
    backgroundColor: 'white',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    marginTop: '3%',
    overflow: 'scroll'
  },

};


class SettingsView extends React.Component {
  render() {
    return (
      <MuiThemeProvider muiTheme={SkylineTheme}>
        <div style={style.root}>
          <div style={style.leftpart}>
            <SettingsHeader />
            {
              <SettingsPanelContainer />
                      }
          </div>
          <div style={style.rightpart}>
            {
              <SettingsContentContainer />
                    }

          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}


export default SettingsView;
