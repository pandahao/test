import React from 'react';
import Radium from 'radium';
import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';

const SettingsItem = ({ item, title, clicked, selected }) => {
  const style = {
    row: {
      width: '50%',
      height: '2em',
      cursor: 'pointer'
    },
    title: {
      color: item == selected ? ChallengePalette.white : ChallengePalette.dark,
      textAlign: 'left',
      width: '67%',
      backgroundColor: item == selected ? GeneralPalette.brightPurple : GeneralPalette.white,
      paddingRight: '3px',
      borderRadius: '20px',
      paddingLeft: '20px',
    }
  };

  return (
    <tr style={style.row} onClick={clicked}>
      <td style={style.title}> {title} </td>
    </tr>

  );
};

export default Radium(SettingsItem);
