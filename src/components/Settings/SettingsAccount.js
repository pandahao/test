import React, { PropTypes } from 'react';
import { GeneralPalette, ToolbeltPalette ,Design} from 'legacy_styles/Colors';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import ControlGroup from 'legacy_components/common/ControlGroup';
import TranslatedText from 'legacy_containers/TranslatedText';

const style = {
    rightpart: {
        height: '100%',
        position: 'absolute',
        marginLeft: '10%',
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'flex-start',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        overflow: 'hidden'
    },
    header: {
        fontFamily: 'staticbold',
        fontSize: '35px',
        marginTop: '0px',
        textTransform: 'uppercase',
        color: GeneralPalette.darkPurple
    },
    body: {
      fontFamily:'Museo-700',
      color:Design.text,
    },
    buttonGroup: {
        position: 'fixed',
        bottom: '50px',
    },
    text: {
        fontFamily: 'sfns',
        color: GeneralPalette.darkPurple,
        marginLeft: '20px',
    },
    table: {
        marginTop: '0px',
        borderCollapse: 'collapse',
    },
    cancelButton: {
        marginRight: '30px',
        textAlign: 'center',
        borderRadius: '20px',
        backgroundColor: Design.grounds,
        color: GeneralPalette.white,
        marginTop:'30px',
    },
    applyButton: {
        marginRight: '30px',
        textAlign: 'center',
        borderRadius: '20px',
        backgroundColor: GeneralPalette.brightPurple,
        color: GeneralPalette.white,
        marginLeft:450,
    },
    editButton: {
        marginRight: '30px',
        textAlign: 'center',
        borderRadius: '20px',
        backgroundColor:GeneralPalette.white,
        border:`1px solid${Design.settingborder}`,
        color:Design.settingcolor,
      position:'relative',
    top:10,
    },
    errorMessage: {
      fontFamily: 'sfns',
      color: ToolbeltPalette.compileError,
      fontWeight: 100,
      fontSize: '12px',
      marginBottom: '10px',
      left: '-50%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      position: 'relative',
      width: '260px',
      zIndex: 1
    },
    tempLogout: {
      marginRight: '30px',
      textAlign: 'center',
      borderRadius: '20px',
      backgroundColor:GeneralPalette.white,
      border:`1px solid${Design.buttonborder}`,
      color:Design.settingcolor,
      float:'right',
      marginTop:-45,
      marginRight:-400,
      lineHeight:'10px',
    },
    restPassword: {
        width: '100px',
        marginRight: '30px',
        textAlign: 'center',
        borderRadius: '20px',
        backgroundColor: GeneralPalette.white,
        border:`2px solid${Design.settingborder}`,
        color:Design.settingcolor,
        position:'fixed',
         marginLeft:430,
        bottom:540,

    }
};

class SettingsAccount extends React.Component {
    state = {
        edit: false
    };

    handleApply() {
        const profiles = {
            'given_name': this.refs.firstName.getValue(),
            'family_name': this.refs.lastName.getValue(),
            'email': this.refs.email.getValue(),
            'custom:school': this.refs['custom:school'].getValue(),
        };
        this.setState({ edit: false });
        this.props.onApplyClick(profiles);
    }

    handleCancel() {
        //reset values
        // [this.refs.firstName, this.refs.lastName, this.refs.email, this.refs.address].forEach((control)=> {
        //     control.setValue(control.props.defaultValue);
        // });
        this.setState({ edit: false });
    }

    handleEdit() {
        this.setState({ edit: true });
    }

    handleResetPassword() {
        this.props.onResetPasswordClick();
    }

    render() {
        const Buttons = this.state.edit ?
            <div style={style.buttonGroup}>
                <FlatButton style={style.cancelButton} onClick={()=> {this.handleCancel()}}>
                    <TranslatedText text={"Cancel"}/>
                </FlatButton>
                <FlatButton style={style.applyButton} onClick={()=> {this.handleApply()}}>
                    <TranslatedText text={"Apply"}/>
                </FlatButton>
            </div> :
            <div style={style.buttonGroup}>
                <FlatButton style={style.editButton} onClick={()=> {this.handleEdit()}}>
                    <TranslatedText text={"Edit"}/>
                </FlatButton>
                <div style={style.errorMessage}> {this.props.errorMessage} </div>
            </div>;
        return (
            <div style={style.rightpart}>
                <div style={style.header}><TranslatedText text={"Account"}/></div>
                <div style={style.body}>
                    <ControlGroup label={'First Name'}>
                        <TextField disabled={this.state.edit ? false : true} id= "firstName" ref="firstName" defaultValue={this.props.profile['given_name']}/>
                    </ControlGroup>
                    <ControlGroup label={'Last Name'}>
                        <TextField disabled={this.state.edit ? false : true} id= "lastName" ref="lastName" defaultValue={this.props.profile['family_name']} />
                    </ControlGroup>
                    <br/>
                    <ControlGroup label={'Email'}>
                        <TextField disabled={this.state.edit ? false : true} id= "email" ref="email" defaultValue={this.props.profile['email']} disabled={true}/>
                    </ControlGroup>
                    <ControlGroup label={'School'}>
                        <TextField disabled={this.state.edit ? false : true} id= "custom:school" ref="custom:school" defaultValue={this.props.profile['custom:school']||''}/>
                    </ControlGroup>
                </div>
                    {Buttons}
            </div>
        );
    }
}

SettingsAccount.propTypes = {
    profile: PropTypes.object.isRequired,
    onApplyClick: PropTypes.func.isRequired,
    onResetPasswordClick: PropTypes.func.isRequired,
};
export default SettingsAccount;
