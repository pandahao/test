import React, { Component, PropTypes } from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import TranslatedText from 'legacy_containers/TranslatedText';
import FlatTextButton from 'components/FlatTextButton';
import LinearProgress from 'material-ui/LinearProgress';


const style = {
  rightpart: {
    height: '100%',
    position: 'absolute',
    marginLeft: '10%',
    backgroundColor: 'white',
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    overflow: 'hidden'
  },
  header: {
    fontFamily: 'staticbold',
    fontSize: '35px',
    marginTop: '0px',
    textTransform: 'uppercase',
    color: GeneralPalette.darkPurple
  },
  body: {

  },
  versions: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '30px',
  },
  txts: {
    color: '#c8cee2',
    display: 'flex',
    flexDirection: 'column'
  },
  txt: {
    marginLeft: '80px',
    color: '#c8cee2',
    display: 'flex',
    flexDirection: 'column'
  },
  num: {
    display: 'flex',
    flexDirection: 'row'
  },
  roots: {
    color: '#777c90',
    marginTop: '10px'
  },
  rot: {
    color: '#777c90',
    marginTop: '10px'
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '40px',
  },
  cont: {
    color: '#a5a9b6'
  }
};

class SettingsVersion extends Component {
  GPUInfo() {
    window.open('chrome://gpu', 'GPU Info');
  }
  render() {
    return (
      <div style={style.rightpart}>

        <div style={style.header}><TranslatedText text={'VERSION'} /></div>
        <div style={style.body}>
          <div style={style.versions}>
            <span style={style.txts}><TranslatedText text={'NUMBER'} />
              <span style={style.roots}>2.2.0</span>
            </span>
          </div>
          <div style={style.content}>
            <span style={style.txts}><TranslatedText text={'RELEASE NOTES'} /></span>
            <p style={style.cont}>
              <TranslatedText text={'Global Backend Server(GBS), our custom backend server running on domestic cloud service. No more dependency on AWS. CastleRock now loads its runtime as well as most static resources from GBS once and cache them locally, vastly improving responsiveness on subsequent runs. Updates are now available as soon as they are uploaded to GBS. Added Drag & Drop programming mode(for Erra users).'} />
            </p>
          </div>
        </div>
        <div
          style={{
            position: 'fixed',
            bottom: '2em'
          }}
        >
          <FlatTextButton text={'GPU INFO'} onClick={this.GPUInfo} color={GeneralPalette.brightPurple} />
        </div>

      </div>
    );
  }
}

export default SettingsVersion;
