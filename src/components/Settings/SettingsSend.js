import React, { PropTypes } from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import TranslatedText from 'legacy_containers/TranslatedText';
import FlatButton from 'material-ui/FlatButton';
const style = {
  rightpart: {
    height: '100%',
    position: 'absolute',
    marginLeft: '10%',
    backgroundColor: 'white',
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    overflow: 'hidden'
  },
  header: {
    fontFamily: 'staticbold',
    fontSize: '35px',
    marginTop: '0px',
    textTransform: 'uppercase',
    color: GeneralPalette.darkPurple
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '40px'
  },
  spans: {
    color: '#c8cee2',
  },
  area: {
    marginTop: '10px',
    outline: 'none',
    borderColor: '#c0c5d3'
  },
  submit: {
    width: '80px',
    height: '30px',
    borderRadius: '20px',
    backgroundColor: '#fff',
    border: '2px solid #e7e9ed',
    color: '#868fac',
    boxShadow: '0px 0px 0px 0px #ccc',
    marginLeft: '350px',
    marginTop: '20px',
    outline: 'none',
    textAlign: 'center',
    lineHeight: '27px',
  },
};

class SettingsSends extends React.Component {

  render() {
    return (
      <div style={style.rightpart}>
        <div style={style.header}><TranslatedText text={'SEND FREEMODE'} /></div>
        <div style={style.body}>
          <span style={style.spans}><TranslatedText text={'YOUR COMMENTS'} /></span>
          <textarea rows="20" cols="70" style={style.area} />
          <FlatButton style={style.submit}>
            <TranslatedText text={'SUBMIT'} />
          </FlatButton>
        </div>
      </div>
    );
  }
}

export default SettingsSends;
