import React from 'react';
import Radium from 'radium';

import FlatButton from 'material-ui/FlatButton';
import { CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';
import TranslatedText from 'legacy_containers/TranslatedText';
import { zIndex } from 'styles/zIndex';

const FlatTextButton = ({ text, children, disabled, color, invert, width, onClick, hoverOutline }) => {
  const style = {
    root: {
      backgroundColor: invert ? color : 'rgba(1,1,1,0)',
      fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
      width: '100%',
      fontSize: '16px',
      letterSpacing: '0.06em',
      color: invert ? GeneralPalette.white : color,
      border: `${invert || hoverOutline ? 0 : 1}px solid ${color}`,
      borderRadius: '40px'
    },
    outter: {
      marginTop: '12px',
      width: width || '150px',
      border: '1px solid rgba(0,0,0,0)',
      transition: 'border 0.3s',
      borderRadius: '40px',
      ':hover': {
        border: `1px solid ${hoverOutline ? color : 'rgba(0,0,0,0)'}`,
      }
    }
  };

  return (
    <div style={style.outter}>
      <FlatButton
        onClick={onClick}
        style={style.root}
        disabled={disabled}
        rippleColor={invert ? GeneralPalette.white : color}
      >
        <span>
          <TranslatedText text={text} />
        </span>
      </FlatButton>
    </div>
  );
};

export default Radium(FlatTextButton);
