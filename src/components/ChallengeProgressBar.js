import React from 'react';
import { GeneralPalette } from 'legacy_styles/Colors';
import TranslatedText from 'legacy_containers/TranslatedText';

const ChallengeProgressBar = ({ current, step }) => {
  const style = {
    root: {
      display: 'inline-block',
      margin: '1em',
      width: '90%',
      color: GeneralPalette.hover,
      letterSpacing: '5px'
    },
    title: {
      display: 'flex',
      fontSize: '16px',
      justifyContent: 'space-between'
    },
    bar: {
      height: '3px',
      marginTop: '3px',
      width: '100%',
      borderRadius: '4px',
      backgroundColor: GeneralPalette.hover
    },
    progress: {
      height: '100%',
      borderRadius: '4px',
      width: `${(current / step) * 100}%`,
      backgroundColor: GeneralPalette.brightPurple
    }
  };
  return (<div style={style.root}>
    <div style={style.title}>
      <span><TranslatedText text={'INSTRUCTIONS'} /></span>
      <span>{current}/{step}</span>
    </div>
    <div style={style.bar}>
      <div style={style.progress} />
    </div>
  </div>);
};

export default ChallengeProgressBar;
