import TranslatedText from 'legacy_containers/TranslatedText';
import TextInput from 'legacy_components/authentication/TextInput';
import React, { Component } from 'react';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import { ChromeEnv } from 'config';
import RegisterContainer from 'legacy_containers/authentication/RegisterContainer';
import Password from 'containers/Authentication/PassWord';
import LogInButtonContainer from 'legacy_containers/authentication/LoginButtonContainer';
import ConfirmContainer from 'legacy_containers/authentication/ConfirmContainer';
import ResetPasswordContainer from 'legacy_containers/authentication/ResetPasswordContainer';
import { LOGIN, PASSWORD, LICENSE, CONFIRM, RESET } from 'legacy_actions/AuthActions';
import LanguageSelector from 'components/LanguageSelector';
import { shell } from 'electron';

const style = {
  root: {
    display: 'flex',
    width: '100%',
    height: '1280px',
    flexDirection: 'row',
  },
  left: {
    display: 'flex',
    width: '300px',
    height: '1280px',
    backgroundColor: '#50c3ff',
    position: 'fixed',
  },
  rom: {
    margin: '50px auto 0',
    width: '80%',
    height: '0',
    paddingTop: '80%',
    borderRadius: '100%',
    backgroundColor: '#fff',
    position: 'relative',
    bottom: '-15%',
    left: '49%',
  },
  img: {
    width: '250%',
    height: '250%',
    position: 'absolute',
    top: '-70%',
    left: '-70%',
  },
  right: {
    display: 'flex',
    width: '80%',
    height: '800px',
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginTop: '2%',
    marginLeft: '20%',
  },
  center: {
    width: '100%',
    transform: 'translateY(20%)',
    display: 'flex',
    flexDirection: 'column',
    // marginLeft:'200px',
    marginTop: '90px'
  },
  p1: {
    color: '#686e84',
    fontSize: '32px',
    fontFamily: '.SF NS display',
    marginLeft: -15,
    letterSpacing: 3,
  },
  input: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: '-15px'
  },
  corner: {
    width: '155px',
    backgroundColor: 'fff',
    marginTop: 70,
    marginLeft: 40
  },

  foot: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 15
  },
  link: {
    display: 'inline-block',
    fontSize: '12px',
    fontFamily: 'sfns',
    cursor: 'pointer',
    color: GeneralPalette.darkPurple,
    opacity: '0.5',
    marginLeft: 15,
    marginTop: 10
  },
  button: {
    backgroundColor: GeneralPalette.brightPurple,
    boxShadow: `0px 0px 0px 0px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    paddingTop: '2px',
    width: '150px',
    cursor: 'pointer',
    marginLeft: -15
  },
  lang: {
    width: '197px',
    height: '197px',
    borderRadius: '200px',
    backgroundColor: '#50c3ff',
    position: 'absolute',
    top: -110,
    right: '-6.8em',
    cursor: 'pointer',
  },
  tra: {
    position: 'absolute',
    top: '40px',
    right: '63%'
  },
  smalltext: {
    fontSize: '14px',
    fontFamily: 'sfns',
    color: 'white',
    position: 'absolute',
    width: '40px',
  },
  corners: {
    width: '100%',
    marginLeft: '20%',
    paddingTop: '70px',
    cursor: 'pointer'
  },
  errorMessage: {
    marginTop: '1em',
    color: ToolbeltPalette.compileError,
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif']
  }
};

let content;

class Login extends Component {
  componentWillMount() {
    this.props.checkLogin();
  }
  gotoRegister() {
    // this.props.setAuthLocation(LICENSE);
    shell.openExternal('http://castlerock.roboterra.com.cn/account/register');
  }
  chooseContent() {
    if (this.props.authLocation === RESET) {
      content = <ResetPasswordContainer />;
    } else if (this.props.authLocation === CONFIRM) {
      content = <ConfirmContainer />;
    } else if (this.props.authLocation === LICENSE) {
      content = <RegisterContainer />;
    } else if (this.props.authLocation === PASSWORD) {
      content = <Password />;
    } else if (this.props.authLocation === LOGIN) {
      content =
            (
              <div style={style.right}>
                <div style={style.center}>
                  <div>
                    <span style={style.p1} > <TranslatedText text={'Please log in'} />
                    </span>
                  </div>
                  <div style={style.input}>
                    <TextInput text={<TranslatedText text={'Email'} />} onChange={this.props.userChange} defaultValue={this.props.username} />
                    <TextInput type={'password'} text={<TranslatedText text={'Password'} />} onChange={this.props.passChange} />
                  </div>

                  <div style={style.foot}>
                    <LogInButtonContainer />
                    <a onClick={() => this.props.setAuthLocation(PASSWORD)} style={style.link}><TranslatedText text={'I forgot my PassWord'} />?</a>
                    {/*<a onClick={this.gotoRegister} style={style.link}><TranslatedText text={'Register'} /></a>*/}
                    <a onClick={() => this.props.setAuthLocation(LICENSE)} style={style.link}><TranslatedText text={'Register'} /></a>
                  </div>
                  <div style={style.errorMessage}> {this.props.errorMessage}</div>
                </div>
              </div>
          );
    }
  }

  render() {
    this.chooseContent();
    return (
      <div style={style.root}>
        <div style={style.left}>
          <div style={style.rom}>
            <img style={style.img} src="./static/media/logo.svg" alt="" />
          </div>
        </div>
        <div style={{ marginTop: '15px' }}>
          <div style={style.corner}>
            {
            ChromeEnv ? '' :
            <div style={style.lang}>
              <div style={style.tra}>
                <div onClick={this.props.changeLanguage} style={style.corners} />
                {
                  <LanguageSelector />
                }
              </div>
            </div>
          }
          </div>
        </div>
        {content}
      </div>
    );
  }

}
export default Login;
