import React, { Component } from 'react';
// import FlatButton from 'material-ui/FlatButton';
const style = {
  root: {
    width: '25%',
    height: 'auto',
    backgroundColor: '#686d85',
    margin: 'auto',
    position: 'relative',
    lineHeight: 'auto',
    borderRadius: '8px',
  },
  center: {
    width: '340px',
    margin: 'auto',
    textAlign: 'center',
  },
  left: {
    width: '0px',
    height: '0px',
    position: 'absolute',
    overflow: 'hidden',
    borderWidth: '10px',
    borderStyle: 'solid solid dashed dashed',
    marginLeft: '-20px',
    top: '20px',
    borderColor: ' transparent #686d85 transparent transparent'
  },
  right: {
    width: '0px',
    height: '0px',
    position: 'absolute',
    overflow: 'hidden',
    borderWidth: '10px',
    borderStyle: 'solid solid dashed dashed',
    right: -20,
    top: 30,
    borderColor: ' transparent  transparent transparent #686d85'
  },
  top: {
    width: '0px',
    height: '0px',
    position: 'absolute',
    left: '135px',
    top: '-20px',
    overflow: 'hidden',
    borderWidth: '10px',
    borderStyle: 'solid solid dashed dashed',
    borderColor: ' transparent  transparent #686d85 transparent'
  },
  bottom: {
    width: '0px',
    height: '0px',
    position: 'absolute',
    overflow: 'hidden',
    borderWidth: '10px',
    borderStyle: 'solid solid dashed dashed',
    marginLeft: '-20px',
    top: '20px',
    borderColor: ' transparent #686d85 transparent transparent'
  },
  title: {
    textAlign: 'center',
    lineHeight: '80px',
    color: '#fff',
    fontFamily: '.SF NS Display',
    fontSize: 32
  },
  p1: {
    width: '240px',
    height: '68px',
    textAlign: 'center',
    lineHeight: '20px',
    marginTop: '-20px',
    color: '#fff',
    line: 17,
    fontSize: 15,
    fontFamily: '.Helvetica Neue Deskinterface',
    marginLeft: '40px'
  },
  button: {
    margin: 'auto',
    marginTop: '18%',
    marginLeft: '25%',
      //  backgroundColor: GeneralPalette.brightPurple,
      //  boxShadow: `0px 0px 0px 0px${GeneralPalette.purpleShadow}`,
    border: 'none',
    width: '50%',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    cursor: 'pointer',
  }
};
class Pop extends Component {
  render() {
    return (
      <div>
        <div style={style.root}>
          <div style={style.center}>
            <h1 style={style.title}>This is a PROJECT</h1>
            <p style={style.p1}>While completing projects, you will learn how to construct robots and program them. Finishing a project will unlock new projects for you to explore. Click the project image to continue. </p>
          </div>
          <div style={style.left} />
          <div style={style.top} />
          <div style={style.right} />
          <div style={style.bottom} />
          <button style={style.button}>按钮</button>
        </div>
      </div>
    );
  }
}
export default Pop;
