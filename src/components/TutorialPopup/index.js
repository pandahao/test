import { connect } from 'react-redux';
import React, { Component } from 'react';
import { zIndex } from 'styles/zIndex';
import { GeneralPalette } from 'legacy_styles/Colors';

import FlatTextButton from 'components/FlatTextButton';

import { getTutorialContent } from './TutorialContent';

// Tutorial Popup component
class TutorialPopupComponent extends Component {

  renderTriangle(mode) {
    const style = {
      CENTER: {
        width: '340px',
        margin: 'auto',
        textAlign: 'center',
      },
      RIGHT: {
        width: '0px',
        height: '0px',
        position: 'absolute',
        overflow: 'hidden',
        borderWidth: '10px',
        borderStyle: 'solid solid dashed dashed',
        marginLeft: '-20px',
        top: '20px',
        borderColor: ` transparent ${GeneralPalette.darkPurple} transparent transparent`
      },
      LEFT: {
        width: '0px',
        height: '0px',
        position: 'absolute',
        overflow: 'hidden',
        borderWidth: '10px',
        borderStyle: 'solid solid dashed dashed',
        right: -20,
        top: 30,
        borderColor: ` transparent  transparent transparent ${GeneralPalette.darkPurple}`
      },
      LEFTUP: {
        width: '0px',
        height: '0px',
        position: 'absolute',
        overflow: 'hidden',
        borderWidth: '10px',
        borderStyle: 'solid solid dashed dashed',
        right: -20,
        bottom: 30,
        borderColor: ` transparent  transparent transparent ${GeneralPalette.darkPurple}`
      },
      BOTTOM: {
        width: '0px',
        height: '0px',
        position: 'absolute',
        left: '135px',
        top: '-20px',
        overflow: 'hidden',
        borderWidth: '10px',
        borderStyle: 'solid solid dashed dashed',
        borderColor: ` transparent  transparent ${GeneralPalette.darkPurple} transparent`
      },
      TOP: {
        width: '0px',
        height: '0px',
        position: 'absolute',
        overflow: 'hidden',
        borderWidth: '10px',
        borderStyle: 'solid solid dashed dashed',
        marginLeft: '-20px',
        top: '20px',
        borderColor: ` transparent ${GeneralPalette.darkPurple} transparent transparent`
      }
    };
    // style.LEFTUP = style.LEFT

    return (
      <div style={style[mode]} />
    );
  }
  handleNext() {
    if (this.props.optionalNextAction) {
      this.props.optionalNextAction();
    }
    this.props.next();
  }
  render() {
    // Black mask style
    const maskStyle = {
      zIndex: zIndex.low,
      position: 'absolute',
      height: '100%',
      width: '100%',
      backgroundColor: 'rgba(0,0,0,0)',
      boxShadow: '0 0 0 9999px rgba(0, 0, 0, 0.84)'
    };

    // Style object for dialog panel - need add the little triangle for all modes
    const dialogStyle = {
      width: '350px',
      position: 'absolute',
      zIndex: zIndex.summit,
      backgroundColor: GeneralPalette.darkPurple,
      top: '10%',
      borderRadius: '15px',
    };
    // Style object for dialog content
    const dialogContentStyle = {
      margin: '1em',
      color: GeneralPalette.white,
      fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
      textAlign: 'center'
    };
    // Attributes of this component - all passed in by its parent components
    const {
      padding,
      top,
      left,
      borderRadius,
      mode,
      tutorialMode,
      language
    } = this.props;

    // Alter dialog panel style object and position according to mode
    switch (mode) {
      // dialog will appear beneath the target component
      case 'BOTTOM':
        Object.assign(dialogStyle, {
          top: 'calc(3em + 100%)',
          left: '0px'
        });
        break;
      case 'LEFT' :
        Object.assign(dialogStyle, {
          top: '150px',
          left: '-398px'
        });
        break;
      case 'CENTER':
        Object.assign(dialogStyle, {
          top: '-150px',
          left: '-150px'
        });
        break;
      case 'LEFTUP':
        Object.assign(dialogStyle, {
          top: '-5em',
          left: '-398px'
        });
        break;
      // default mode, dialog will appear on the right side of the target component
      default:
        Object.assign(dialogStyle, {
          top: '10%',
          left: 'calc(3em + 100%)'
        });
    }

    const content = getTutorialContent(tutorialMode, language);
    console.log(tutorialMode);
    return (
    <div style={maskStyle} id="TutorialPopupShadow">
      <div style={dialogStyle} id="TutorialPopup">
        { this.renderTriangle(this.props.mode || 'RIGHT') }
        <div style={dialogContentStyle}>
          <div style={{ display: 'block', fontSize: '26px', marginTop: '1em'}}>
            {content.title}
          </div>
          <div style={{ margin: '1em', fontSize: '16px' }}>
            {content.content}
          </div>
          <div style={{ display: 'flex', justifyContent: 'space-around'}}>
            <FlatTextButton
              invert="true"
              text={tutorialMode === 12 ? 'COMPLETE' : 'NEXT STEP'}
              color={GeneralPalette.green}
              onClick={this.handleNext.bind(this)} />
          </div>
        </div>
      </div>
    </div>);
  }
}

const mapStateToProps = (state) => {
  return {
    tutorialMode: state.shared.app.tutorialMode,
    language: state.shared.app.language
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    next: () => {
      dispatch({
        type: 'NEXT_TUTORIAL_STEP'
      });
    },
    previous: () => {
      dispatch({
        type: 'PREVIOUS_TUTORIAL_STEP'
      });
    },
    stop: () => {
      dispatch({
        type: 'TURN_OFF_TURTORIAL'
      });
    }
  };
};

const TutorialPopup = connect(
  mapStateToProps,
  mapDispatchToProps,
)(TutorialPopupComponent);


export default TutorialPopup;
