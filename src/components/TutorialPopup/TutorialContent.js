export const getTutorialContent = (mode, language) => {
  if (language === 'ENGLISH') {
    return contentData_en_US[mode];
  }
  return contentData_zh_CN[mode];
};

const contentData_en_US = {
  1: {
    title: 'Welcome to CastleRock!',
    content: 'This introduction will help you learn your way around CastleRock. Let’s get started!'
  },
  2: {
    title: 'This is the MENU',
    content: 'Use the icons here to navigate around CastleRock. You are currently viewing your HOME page.'
  },
  3: {
    title: 'This is the LEARN mode',
    content: 'You will find all your CastleRock projects here.'
  },
  4: {
    title: 'This is a PROJECT',
    content: 'While completing projects, you will learn how to construct robots and program them. Finishing a project will unlock new projects for you to explore. Click the project image to continue.'
  },
  5: {
    title: 'This is the GUIDE',
    content: 'Each project consists of several.Challenges. Here, you’ll be given instructions to complete until you finish your robot project.'
  },
  6: {
    title: 'This is the VIEWPAD',
    content: 'The ViewPad displays an interactive 3D model of the robot you’ll be building, starting with the parts needed and how to assemble them step-by-step. Feel free to explore! Once you get the hang of the ViewPad, click the button below to read the instructions in the Guide and follow along.'
  },
  7: {
    title: 'Welcome to the CODE section of the project',
    content: 'Here you’ll learn how to talk to your robot and force it to do your bidding! Note that the Guide panel contains a Checklist that explains the tasks and goals to be completed for this project.'
  },
  8: {
    title: 'This is the CODEPAD',
    content: 'This is where you’ll do your actual coding. You’ll learn more about this later.'
  },
  9: {
    title: 'This is the CONSOLE',
    content: 'The Console displays live feedback from your robot. You’ll use this to monitor your robot’s behavior. If you own a Roboterra robotics kit, connect your RoboCore Microcontroller to your computer via the USB cable now, then follow the instructions in the guide. If you DON’T own a RoboCore, click the button below to continue.'
  },
  10: {
    title: 'This is the MODULE MONITOR',
    content: 'This watches for changes in your robot’s sensors and actuators.'
  },
  11: {
    title: 'This is the EVENT MONITOR',
    content: 'This the types of events that are being triggered while your code is running.'
  },
  12: {
    title: 'You’ve reacted the final phase of the project : the REVIEW',
    content: 'Here, you will answer questions about what you created and what you learned along the way. Review the infomation in the guide panel and answer the question to complete the project'
  },
  // 12 : {
  //   title : 'Mission Accomplished!',
  //   content : 'You’ve finished the tutorial and hopefully you have a general understanding of what you can do in CastleRock. Now click the LEARN icon to return to the Projects Index and begin your journey into the world of robotics!'
  // }
};

const contentData_zh_CN = {
  1: {
    title: '欢迎登录萝卜云平台!',
    content: '本说明旨在帮你探索熟悉萝卜云平台，让我们现在开始吧！'
  },
  2: {
    title: '左侧为导航栏',
    content: '使用导航栏进行功能区的切换。您现所在的位置是平台主页。'
  },
  3: {
    title: '这是学习模式按钮',
    content: '你可以在学习模式中找到所有项目。'
  },
  4: {
    title: '上面即为一个项目',
    content: '你将在完成项目的同时学会机器人的搭建与编程。一个项目结束，另一个项目则被解锁。'
  },
  5: {
    title: '左侧即为项目教程版面',
    content: '每个项目均由若干挑战组成。该版面将给你通关提示，直至机器人项目完成。'
  },
  6: {
    title: '机器人仿真模拟版面/3D视图版面',
    content: '在机器人仿真模拟界面/3D视图版面，您要搭建的机器人将会以3D可交互的模型显示出来，从搭建需要用到的元器件到每一步的组装都会有3D呈现，欢迎随心探索！\n当您理解使用的窍门后，点击教程版面底部的按钮，请阅读指导并按步骤进行。' },
  7: {
    title: '欢迎进入项目的编程环节',
    content: '在这里，你将学会如何同你的机器人交流，并让他们服从你的吩咐 \n请注意项目教程面板中的过关条件，它描述了项目需要完成的任务和目标'
  },
  8: {
    title: '中间面板为程序编辑器',
    content: '这里是代码编程区域，稍后你将学习了解到更多的程序编程器的介绍和知识。'
  },
  9: {
    title: '右侧面板为机器人仪表盘/控制台',
    content: '这个控制台显示着来自你机器人的实时反馈，你将可以用它来监控你机器人的行动 \n如果你已经拥有了萝卜太辣机器人套件，请用USB线将萝卜芯主控板连接至你的电脑，之后请继续了解本教程。\n如果您手上还没有萝卜芯主控板也没关系，请点击按钮继续'
  },
  10: {
    title: '面板这里是模块监视器',
    content: '它监视着机器人传感器和执行器的状态变化。'
  },
  11: {
    title: '面板该区域是事件监视器',
    content: '显示程序运行后所触发的事件信息'
  },
  12: {
    title: '您已进入本项目的最后阶段回顾部分。',
    content: '在这里，请根据你刚才了解到的学习模式和创建模式的内容回答一些问题'
  }
  // 12 : {
  //   title : '任务完成！',
  //   content : 'You’ve finished the tutorial and hopefully you have a general understanding of what you can do in CastleRock. Now click the LEARN icon to return to the Projects Index and begin your journey into the world of robotics!'
  // }
};
