import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import CircularProgress from 'material-ui/CircularProgress';
import TranslatedText from 'legacy_containers/TranslatedText';
import { LangIcon } from 'legacy_assets/index';
import TextInput from 'legacy_components/authentication/TextInput';

const style = {
  root: {
    display: 'flex',
    width: '100%',
    height: '1280px',
    backgroundColor: '#50c3ff',
    flexDirection: 'row'
  },
  left: {
    display: 'flex',
    width: '18%',
    height: '1280px',
    backgroundColor: '#50c3ff'
  },
  right: {
    display: 'flex',
    width: '82%',
    height: '1280px',
    flexDirection: 'row',
    backgroundColor: '#fff'
  },
  center: {
    width: '90%',
    transform: 'translateY(20%)',
    display: 'flex',
    flexDirection: 'column',
    marginLeft: '15rem',
    marginTop: '70px'
  },
  p1: {
    color: '#686e84',
    fontSize: '48px',
    fontFamily: '.SF NS display'
  },
  puts: {
    display: 'flex',
    flexDirection: 'column'
  },
  input: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: '-15px'
  },
  inputs: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: '-15px'
  },
  lang: {
    position: 'fixed',
    left: '93%',
    bottom: '82%',
  },
  tra: {
    width: '197px',
    height: '197px',
    backgroundColor: '#50c3ff',
    borderRadius: '100px',
    lineHeight: '100px',
  },
  smalltext: {
    fontSize: '14px',
    fontFamily: 'sfns',
    color: '#fff',
    marginLeft: 40,
    marginTop: -75
  },
  corner: {
    width: '100%',
    marginLeft: '20%',
    paddingTop: '70px',
    //  marginTop:'50px'
  },
  rom: {
    width: '300px',
    height: '300px',
    borderRadius: '200px',
    backgroundColor: '#fff',
    position: 'absolute',
    left: '10%',
    top: '33%',
  },
  img: {
    width: '700px',
    height: '700px',
    position: 'absolute',
    top: '-65%',
    left: '-68%'
  },
  foot: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 15
  },
  link: {
    display: 'inline-block',
    fontSize: '12px',
    fontFamily: 'sfns',
    cursor: 'pointer',
    color: GeneralPalette.darkPurple,
    opacity: '0.5',
    marginLeft: 15,
    marginTop: 10
  },
  //  button: {
  //    backgroundColor:'#50c3ff',
  //    border: 'none',
  //    borderRadius: '20px',
  //    color: 'white',
  //    fontSize: '15px',
  //    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
  //    textAlign: 'center',
  //    lineHeight: '35px',
  //    paddingTop: '2px',
  //    width: '150px',
  //    cursor: 'pointer'
  //  },
  button: {
    backgroundColor: GeneralPalette.brightPurple,
    boxShadow: `0px 0px 0px 0px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    paddingTop: '2px',
    width: '150px',
    cursor: 'pointer'
  },
};
const Registration = () => {
  <div style={style.root}>
    <div style={style.left}>
      <div style={style.rom}>
        <img style={style.img} src="static/media/logo2.svg" />
      </div>
    </div>
    <div style={style.right}>
      <div style={style.center}>
        <div>
          <span style={style.p1} > <TranslatedText text={'Welcome,builder!'} /><br />
            <TranslatedText text={'Please create an account'} />
          </span>
        </div>
        <div style={style.puts}>
          <div style={style.input}>
            <TextInput text={<TranslatedText text={'FirstName'} />} />
            <TextInput type={'LastName'} text={<TranslatedText text={'LastName'} />} />
          </div>
          <div style={style.inputs}>
            <TextInput type={'Email'} text={<TranslatedText text={'Email'} />} />
            <TextInput type={'PassWord'} text={<TranslatedText text={'PassWord'} />} />
          </div>
        </div>
        <div style={style.foot}>
          <FlatButton style={style.button}>
            <TranslatedText text={'CREATE'} />
          </FlatButton>
          <a style={style.link}><TranslatedText text={'I already have an account?'} /></a>
        </div>
      </div>
      <div style={style.lang}>
        <div style={style.tra}>
          <div style={style.corner}>
            <LangIcon />
          </div>
          <div style={style.smalltext}><TranslatedText text={'ENGLISH'} /></div>
        </div>
      </div>
    </div>
  </div>;
};
export default Registration;
