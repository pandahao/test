import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

// Store Configuration
import configureStore from 'store/configureStore';

// Root container
import CastleRockApp from 'legacy_containers/CastleRockContainer';
import LoginContainer from 'containers/Authentication/LoginContainer';

import SettingsView from 'components/Settings/SettingsView';

// The MOTHER SHIP
import App from './App';

const store = configureStore({});
const history = syncHistoryWithStore(hashHistory, store);

class AppRoute extends Component {
  shouldComponentUpdate() {
    return false;
  }
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Route path="/" component={App}>
            <Route path="home" component={CastleRockApp} />
            <IndexRoute component={LoginContainer} />
          </Route>
          <Route path="settings" component={SettingsView} />
        </Router>
      </Provider>
    );
  }
}

// <IndexRoute component={ChallengeModeContainer} />
// <Route path="freemode" component={FreemodeContainer} />
// </Route>
export default AppRoute;
