/* eslint global-require: "off", no-console: 0*/

import React from 'react';
import { render } from 'react-dom';

import AppRoute from 'router';


if (!__DEVELOPMENT__) {
  console.log = () => {};
  console.warn = () => {};
  console.error = () => {};
}


const run = () => {
  const root = document.getElementById('root');
  if (__DEVELOPMENT__) {
    const RedBox = require('redbox-react').default;
    try {
      render(<AppRoute />, root);
    } catch (e) {
      render(<RedBox error={e} />, root);
    }
  } else {
    render(<AppRoute />, root);
  }
};

// Start the app
run();
