import { connect } from 'react-redux';
import { selectSetting } from 'legacy_actions/SettingsActions';


import React from 'react';
import { GeneralPalette, ChallengePalette } from 'legacy_styles/Colors';
import SettingsAccountContainer from 'containers/Settings/SettingsAccountContainer';
import SettingsVersion from 'components/Settings/SettingsVersion';

import SettingsSendContainer from 'containers/Settings/SettingsSendContainer';

import {
    ACCOUNT, PROFILE, LANGUAGE, SEND, VERSION
} from 'legacy_actions/SettingsActions';


const style = {
  root: {
    display: 'inline-block',
    width: '100%',
    backgroundColor: 'white',
    height: '100vh',
    overflow: 'auto'
  }
};

const SettingsContent = ({ selected }) => {
  return (
    <div style={style.root}>
      {
                  (() => {
                    switch (selected) {
                      case ACCOUNT:
                        return <SettingsAccountContainer />;
                      case PROFILE:
                        return <SettingsAccountsContainer />;
                      case LANGUAGE:
                        return <SettingsLanguageContainer />;
                      case VERSION:
                        return (<SettingsVersion />);// <SettingsVersionContainer />;
                      case SEND:
                        return <SettingsSendContainer />;
                      default:
                        console.log(`unknown selected setting: ${selected}`);
                        return <SettingsAccountsContainer />;
                    }
                  })()
                  }
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    selected: state.setting.currentSettingSelected,
    // editProfile: state.editProfile
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {

  };
};
const SettingsContentContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingsContent);

export default SettingsContentContainer;
