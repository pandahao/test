import { connect } from 'react-redux';
import SettingsAccount from 'components/Settings/SettingsAccount';
import { updateUserProfile, changePassword } from 'legacy_actions/shared/UserActions';

const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.projectMode.project.userProgress.info
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onApplyClick: (profile) => {
      dispatch(updateUserProfile(profile));
    },
    onResetPasswordClick: () => {
      dispatch(changePassword());
    }
  };
};
const SettingsAccountContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingsAccount);

export default SettingsAccountContainer;
