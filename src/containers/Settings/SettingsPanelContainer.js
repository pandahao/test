import { connect } from 'react-redux';
import { logOut } from 'legacy_actions/AuthActions';
// import {openSettingsView} from '../../actions/SettingsActions';
import React from 'react';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import Drawer from 'material-ui/Drawer';
import SettingsItemContainer from 'containers/Settings/SettingsItemContainer';

import FlatButton from 'material-ui/FlatButton';
import TranslatedText from 'legacy_containers/TranslatedText';
import {
    ACCOUNT, PROFILE, LANGUAGE, SEND, VERSION
} from 'legacy_actions/SettingsActions';

import SaveExistingFreeModeSessionDialog from 'components/freemode/SaveExistingFreeModeSessionDialog';

import { toggleDocumentation, saveEditingRobotThen, changeFreeModeView, saveCurrentRobot, saveAsCurrentRobot, newRobot, promptSaveEditingRobot, promptSaveEditingRobotBeforeCodeVault, closeSaveExistingFreeModeSessionDialog, doBeforeClose, doAfterClose, skipSave, openSaveExistingFreeModeSessionDialog } from 'legacy_actions/FreeModeActions';

const style = {
  root: {
    display: 'inline-block',
    width: '100%',
    backgroundColor: GeneralPalette.menuBG,
    height: '100vh',
    overflow: 'auto'
  },
  table: {
    marginTop: '5.5em',
    textAlign: 'center',
    borderCollapse: 'collapse',
    marginLeft: '11%',
    width: '60%',
    position: 'absolute',
    zIndex: 200,
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
  },
  background: {
    position: 'fixed',
    bottom: '0%',
    top: '0%',
    height: '100%',
    marginTop: '4em',
    backgroundColor: '#fff',
    width: '26.3%',
    zIndex: 1,
  },
  tempLogout: {
    backgroundColor: 'white',
    // boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
    border: '1px solid #9099b2',
    borderRadius: '20px',
    color: '#9099b2',
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    width: 140,
    lineHeight: '30px',
    height: '35px',
    paddingTop: '2px',
    letterSpacing: '0.2em',
    position: 'relative',
    flexDirection: 'column',
    alignItems: 'center',
    left: '-25%',
    zIndex: 2,
    marginTop: '3px',
  },
  center: {
    position: 'absolute',
    bottom: 50,
    left: '50%',
  },
  errorMessage: {
    fontFamily: 'sfns',
    color: ToolbeltPalette.compileError,
    fontWeight: 100,
    fontSize: '12px',
    marginBottom: '10px',
    left: '-50%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'relative',
    width: '260px',
    zIndex: 1
  }
};

const SettingsPanel = ({ logOut, errorMessage, onAccept, onDecline }) => {
  return (
    <div style={style.root}>
      <div style={style.background} />
      <table style={style.table}>
        <tbody>
          <SettingsItemContainer title={<TranslatedText text={'Account'} />} item={ACCOUNT} />
          <SettingsItemContainer title={<TranslatedText text={'Version'} />} item={VERSION} />
          {/* <SettingsItemContainer title={"Contact"} item={6}/>
                        <SettingsItemContainer title={"Logout"} item={7}/> */}
        </tbody>
      </table>
      <div style={style.center}>
        <div style={style.errorMessage}> {errorMessage} </div>
        <FlatButton style={style.tempLogout} onClick={logOut}>
          {<TranslatedText text={'Logout'} />}
        </FlatButton>
        {
          <SaveExistingFreeModeSessionDialog onAccept={onAccept} onDecline={onDecline} />
                    }
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
        /* challenges: state.currentChallengeSet,
        showChallengeContent : state.showChallengeContent,
        freemode : state.rootLocation === 2,*/
    errorMessage: state.auth.loggedIn.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logOut: () => {
      dispatch(promptSaveEditingRobot(logOut()));
    },
    onAccept: () => {
      dispatch(closeSaveExistingFreeModeSessionDialog());
      dispatch(saveEditingRobotThen(logOut()));
    },
    onDecline: () => {
      dispatch(closeSaveExistingFreeModeSessionDialog());
      dispatch(skipSave());
      dispatch(logOut());
    }
        /* openSettingsView : ()=> {
            dispatch(openSettingsView());
        }*/
  };
};
const SettingsPanelContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingsPanel);

export default SettingsPanelContainer;
