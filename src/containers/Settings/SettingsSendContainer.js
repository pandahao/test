import { connect } from 'react-redux';
import SettingsSend from 'components/Settings/SettingsSend';

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};
const SettingsSendContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingsSend);

export default SettingsSendContainer;
