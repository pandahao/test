import { connect } from 'react-redux';
import SettingsItem from 'components/Settings/SettingsItem';
import { selectSetting } from 'legacy_actions/SettingsActions';

const mapStateToProps = (state) => {
  return {
    selected: state.setting.currentSettingSelected,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    clicked: () => {
      dispatch(selectSetting(ownProps.item));
    }
  };
};
const SettingsItemContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingsItem);

export default SettingsItemContainer;
