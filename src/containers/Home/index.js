import React, { Component } from 'react';
import { connect } from 'react-redux';

import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import CircularProgress from 'material-ui/CircularProgress';
import TranslatedText from 'legacy_containers/TranslatedText';
import { LangIcon } from 'legacy_assets/index';
import LanguageSelector from 'components/LanguageSelector';
import SaveExistingFreeModeSessionDialog from 'components/freemode/SaveExistingFreeModeSessionDialog';

const style = {
  root: {
    display: 'flex',
    width: '100%',
    height: '1280px',
    backgroundColor: '#50c3ff',
    flexDirection: 'row',
  },
  left: {
    display: 'flex',
    width: '25%',
    height: '1280px',
    backgroundColor: '#50c3ff'
  },
  rom: {
    margin: '50px auto 0',
    width: '80%',
    height: '0',
    paddingTop: '80%',
    borderRadius: '100%',
    backgroundColor: '#fff',
    position: 'relative',
    top: '18%',
    left: '49%',
  },
  img: {
    width: '250%',
    height: '250%',
    position: 'absolute',
    top: '-70%',
    left: '-70%',
  },
  right: {
    display: 'flex',
    width: '80%',
    height: '1280px',
    flexDirection: 'row',
    backgroundColor: '#fff'
  },
  center: {
    width: '100%',
    transform: 'translateY(20%)',
    display: 'flex',
    flexDirection: 'column',
    marginLeft: '15%',
    marginTop: '7%',
  },
  p1: {
    color: '#686E84',
    fontSize: '48px',
    fontFamily: '.SF NS display'
  },
  input: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: '8px',
    color: '#c2c9df',
    fontSize: 14,
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    line: 17
  },
  foot: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 15
  },
  button: {
    backgroundColor: GeneralPalette.brightPurple,
    boxShadow: `0px 0px 0px 0px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    paddingTop: '2px',
    width: '150px',
    cursor: 'pointer'
  },
  lang: {
    width: '197px',
    height: '197px',
    borderRadius: '200px',
    backgroundColor: '#50c3ff',
    position: 'absolute',
    top: -110,
    right: '-6.8em',
    cursor: 'pointer',
  },
  tra: {
    position: 'absolute',
    bottom: '20%',
    right: '63%'

  },
  smalltext: {
    fontSize: '14px',
    fontFamily: 'sfns',
    color: 'white',
    position: 'absolute',
    width: '40px',
    left: '5px'
  },
  corner: {
    width: '100%',
    marginLeft: '20%',
    paddingTop: '70px',
  },
};
const HomePageComponent = ({ projectsCompleted, lastProject, startTutorial, userName }) => {
  return (
    <div style={style.root}>
      <div style={style.left}>
        <div style={style.rom}>
          <img style={style.img} src="./static/media/logo.svg" />
        </div>
      </div>
      <div style={style.right}>
        <div style={style.center}>
          <div>
            <span style={style.p1} > <TranslatedText text={'Welcome : '} />
            </span>
            <span style={style.p1} > <TranslatedText text={userName || 'Builder'} />
            </span>
          </div>
          <div style={style.input}>
            <p><TranslatedText text={'Projects Completed'} />: {projectsCompleted}</p>
            { lastProject === -1 ? null : <p><TranslatedText text={'Last Project'} />: {lastProject}</p> }
          </div>
          <div style={style.foot}>
            <FlatButton
              onClick={startTutorial}
              style={style.button}
            >
              <TranslatedText text={'TUTORIAL'} />
            </FlatButton>
          </div>
          {
            <SaveExistingFreeModeSessionDialog />
          }
        </div>
        <div style={style.lang}>
          <div style={style.tra}>
          {
            <LanguageSelector />
          }
          </div>
        </div>
      </div>
    </div>
  );
};

const numCompleted = (projects) => {
  return projects.filter((x) => {
    return x.status === 'COMPLETED';
  }).length;
};

const mapStateToProps = (state) => {
  return {
    projectsCompleted: numCompleted(state.projectMode.project.userProgress.info.projectsOwned),
    lastProject: state.projectMode.project.currentProjectID,
    userName: state.projectMode.project.userProgress.info.email
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    startTutorial: () => {
      dispatch({
        type: 'TURN_ON_TUTORIAL'
      });
    }
  };
};

const Home = connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePageComponent);

export default Home;
