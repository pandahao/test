import { connect } from 'react-redux';
import CodePad from 'components/codepad/CodePad';

const mapStateToProps = ({ freeMode, projectMode, shared }) => {
  const { app } = shared;
  const robot = freeMode.editingRobot.item;

  // project view is 0
  const isDnd = app.rootLocation ? robot.raw.tech === 'dnd' : app.erraMode;

  return {
    blurFromListDisplay: projectMode.challenge.challengelistBlur,
    isDnd,
    tutorialMode: shared.app.tutorialMode
  };
};

const CodepadContainer = connect(mapStateToProps)(CodePad);

export default CodepadContainer;
