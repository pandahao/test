import { connect } from 'react-redux';
import Header from 'components/codepad/Header';

const mapStateToProps = (state, ownProps) => {
  return {
    freemode: state.shared.app.rootLocation === 2,
    showBanner: state.shared.codepad.showNextChallengeBanner,
    checkingAnswers: state.shared.codepad.checkingAnswers
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  };
};


const HeaderContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Header);

export default HeaderContainer;
