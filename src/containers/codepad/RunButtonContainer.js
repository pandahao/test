import { connect } from 'react-redux';
import RunButton from 'components/codepad/RunButton';
import {
  uploadSketch,
  closeErrorBanner,
  checkPortConnections,
  monitor,
  sketchChange
} from 'legacy_actions/CodepadActions';

const mapStateToProps = (state, ownProps) => {
  const { dndEditor, freeMode, projectMode, shared: { app, codepad } } = state;

  let challengeStatus;
  const proState = { ...projectMode.project, ...projectMode.challenge };
  if (
    proState.currentChallengeSelected ===
    proState.userProgress.info.projectsOwned.find((p) => {
      return p.projectId === proState.currentProjectID;
    }).progress -
      1
  ) {
    challengeStatus = 'UNLOCKED';
  }

  const checkCode = proState.currentChallengeSelected !== -1 &&
    proState.currentChallengeSet[proState.currentChallengeSelected].type === 'code' &&
    challengeStatus === 'UNLOCKED';

  return {
    connected: codepad.roboCoreStatus.connected,
    dndEditor,
    uploading: codepad.roboCoreStatus.uploading,
    showRun: codepad.roboCoreStatus.showingRun,
    showErrorBanner: codepad.showErrorBanner,
    checkCode,
    isDnd: app.erraMode
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    sketchChange: value => dispatch(sketchChange(value)),
    upload: (checkCode) => {
      dispatch(closeErrorBanner());
      dispatch(uploadSketch(checkCode));
    }
  };
};

const RunButtonPanel = connect(mapStateToProps, mapDispatchToProps)(RunButton);

export default RunButtonPanel;
