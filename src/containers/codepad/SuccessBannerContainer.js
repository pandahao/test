import { connect } from 'react-redux';
import HeaderBanner from 'components/codepad/HeaderBanner';
import { toggleChallengeBanner } from 'legacy_actions/CodepadActions';

const mapStateToProps = (state, ownProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    toggleBanner: () => {
      dispatch(toggleChallengeBanner());
    }
  };
};


const SuccessBanner = connect(
    mapStateToProps,
    mapDispatchToProps,
)(HeaderBanner);

export default SuccessBanner;
