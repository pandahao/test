import { connect } from 'react-redux';
import ErrorBanner from 'components/codepad/ErrorBanner';
import { closeErrorBanner } from 'legacy_actions/CodepadActions';

const mapStateToProps = (state, ownProps) => {
  return {
    codeCompiled: state.shared.codepad.roboCoreStatus.codeCompiled,
    error: state.shared.codepad.roboCoreStatus.error,
    showBanner: state.shared.codepad.showErrorBanner
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    closeBanner: () => {
      dispatch(closeErrorBanner());
    },
    calcLine: (msg) => {
      return 1; // HERE: CALCULATE BANNER HEIGHT BASED ON MSG LENGTH
    }
  };
};


const ErrorContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ErrorBanner);

export default ErrorContainer;
