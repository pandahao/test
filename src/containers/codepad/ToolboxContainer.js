import { connect } from 'react-redux';
import Toolbox from 'components/codepad/Toolbox';

import { openProjectListView, closeFreemode, toggleDocView } from 'legacy_actions/AppActions';
import { toggleToolbox } from 'legacy_actions/CodepadActions';
import { layoutResize } from 'legacy_actions/RobotActions';

const mapStateToProps = (state) => {
  return {
    showToolbox: state.shared.codepad.showToolbox,
    opendocs: state.shared.app.docView
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
        // Need debounce
    toolbox: () => {
      setTimeout(() => {
        dispatch(toggleToolbox());
      }, 150);
    },
    openProjectListView: () => {
      dispatch(openProjectListView());
      dispatch(closeFreemode());
    }
  };
};


const ToolboxPanel = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Toolbox);

export default ToolboxPanel;
