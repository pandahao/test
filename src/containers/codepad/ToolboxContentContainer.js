import { connect } from 'react-redux';
import ToolboxContent from 'components/codepad/ToolboxContent';

import { toggleDocView } from 'legacy_actions/AppActions';
import { startDebug, showWarning } from 'legacy_actions/CodepadActions';
import { toggleDocumentation, changeFreeModeView, saveCurrentRobot, saveAsCurrentRobot, newRobot, promptSaveEditingRobot, promptSaveEditingRobotBeforeCodeVault, openSaveExistingFreeModeSessionDialog, closeSaveExistingFreeModeSessionDialog, saveEditingRobotThen, doBeforeClose, doAfterClose, skipSave } from 'legacy_actions/FreeModeActions';
import { layoutResize } from 'legacy_actions/RobotActions';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  let challengeType = 'undefined';

  const currentChallenge = proState.currentChallengeSet[proState.currentChallengeSelected];

  if (currentChallenge !== undefined) {
    challengeType = currentChallenge.type;
  }

  return {
    freemode: state.shared.app.rootLocation === 2,
    uploading: state.shared.codepad.roboCoreStatus.uploading,
    checkingAnswers: state.shared.codepad.checkingAnswers,
    showToolbox: state.shared.codepad.showToolbox,
    challengeType,
    robot: state.freeMode.editingRobot
  };
};

const listRobot = () => changeFreeModeView('list');

const mapDispatchToProps = (dispatch) => {
  return {
        // Need debounce
    docView: (freemode) => {
      dispatch(toggleDocumentation());
      dispatch(toggleDocView());
      // if (freemode) {
      //   dispatch(layoutResize());
      // }
    },
    startDebug: () => {
      dispatch(startDebug());
    },
    resetCode: () => {
      dispatch(showWarning());
    },
    onListRobotClick: () => {
      dispatch(promptSaveEditingRobot(listRobot()));
      // dispatch(changeFreeModeView('list'));
    },
    onRobotSaveClick: () => {
      dispatch(saveCurrentRobot());
    },
    onRobotSaveAsClick: () => {
      dispatch(saveAsCurrentRobot());
    },
    onNewRobotClick: () => {
      dispatch(newRobot());
    },
    onAccept: () => {
      // dispatch(saveCurrentRobot())
      dispatch(closeSaveExistingFreeModeSessionDialog());
      dispatch(saveEditingRobotThen(listRobot()));
    },
    onDecline: () => {
      dispatch(closeSaveExistingFreeModeSessionDialog());
      dispatch(skipSave());
      dispatch(listRobot());
    }
  };
};


const ToolboxContentContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ToolboxContent);

export default ToolboxContentContainer;
