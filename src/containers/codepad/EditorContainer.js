import { connect } from 'react-redux';
import Editor from 'components/codepad/Editor';
import { sketchChange, markDirty } from 'legacy_actions/CodepadActions';

const empty_sketch = '#include "ROBOTERRA.h"\n\n/*--------------------- STEP 1 ---------------------*/\n// Name your RoboCore and all electronics attached it.\n// These names are considered as eventSource of EVENT. \n\nRoboTerraRoboCore tom;\n\n/*--------------------- STEP 2 ---------------------*/\n// Attach electronics to physical ports on RoboCore.   \n\nvoid attachRoboTerraElectronics() {\n\n}\n\n/*--------------------- STEP 3 ---------------------*/\n/* Design your robot algorithm by handling each EVENT. \n * Based on the source, type and data of each EVENT, \n * you can organize your loigc and form program flow.\n * Call Command Functions of instances you defined in\n * STEP 1 for your robot to take physical actions. \n * Calling Command Functions can result in new EVENT. \n */\n\nvoid handleRoboTerraEvent() {\n    if (EVENT.isType(ROBOCORE_LAUNCH)) {\n        // Initialize robot by calling Comand Function  \n    \n    }\n    \n    // Your robot code goes below\n\n\n}';


const mapStateToProps = (state, ownProps) => {
  return {
    sketch: state.shared.codepad.sketch,
    errorBanner: state.shared.codepad.showErrorBanner,
    blurFromListDisplay: state.projectMode.challenge.challengelistBlur
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    makeChange: (val) => {
      dispatch(sketchChange(val));
      dispatch(markDirty());
    }
  };
};

const EditorContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Editor);

export default EditorContainer;
