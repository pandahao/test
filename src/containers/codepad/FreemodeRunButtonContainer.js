import { connect } from 'react-redux';
import FreemodeRunButton from 'components/codepad/RunButton';
import {
  uploadSketch,
  closeErrorBanner,
  monitor,
  sketchChange
} from 'legacy_actions/CodepadActions';

const mapStateToProps = ({ dndEditor, freeMode, shared }) => {
  const robot = freeMode.editingRobot.item;

  return {
    connected: shared.codepad.roboCoreStatus.connected,
    dndEditor,
    // Let component run button decide whether to connect
    uploading: shared.codepad.roboCoreStatus.uploading,
    showRun: shared.codepad.roboCoreStatus.showingRun,
    showErrorBanner: shared.codepad.showErrorBanner,
    isDnd: robot.raw.tech === 'dnd'
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    connect: () => {
      // dispatch(setupRoboCore());
    },
    sketchChange: value => dispatch(sketchChange(value)),
    upload: () => {
      dispatch(closeErrorBanner());
      dispatch(uploadSketch(false));
    },
    disconnect: () => {
      // dispatch(unmountRoboCore());
    }
  };
};

const FreemodeRunButtonPanel = connect(mapStateToProps, mapDispatchToProps)(FreemodeRunButton);

export default FreemodeRunButtonPanel;
