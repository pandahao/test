import { connect } from 'react-redux';
import NextButton from 'components/codepad/NextButton';
import { nextChallenge } from 'legacy_actions/ChallengeActions';
import { toggleChallengeBanner } from 'legacy_actions/CodepadActions';

const mapStateToProps = (state, ownProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    nextChallenge: () => {
      dispatch(nextChallenge());
      dispatch(toggleChallengeBanner());
    }
  };
};


const NextChallengePanel = connect(
    mapStateToProps,
    mapDispatchToProps,
)(NextButton);

export default NextChallengePanel;
