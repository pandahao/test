// Infrastructure
import { connect } from 'react-redux';
import React, { Component } from 'react';
import _ from 'underscore';

// Action Creators
import { nextChallenge, updateProjects, resetBuildProgress, incrementChallengesCompleted } from 'legacy_actions/ChallengeActions';
import { closeDoc, projectView, settingsView, freemode, gridView, homeView } from 'legacy_actions/AppActions';
// Assets and Style
import { CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';
import { zIndex } from 'styles/zIndex';


// Components
import IconButton from 'material-ui/IconButton';
import { layoutResize } from 'legacy_actions/RobotActions';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import NavigationArrowDropUp from 'material-ui/svg-icons/navigation/arrow-drop-up';
import NavigationArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import IconTextButton from 'components/IconTextButton';
import TutorialPopup from 'components/TutorialPopup';

// React Transition Components
import { VelocityTransitionGroup } from 'velocity-react';

// Container like Components
import ChallengeListComponent from './ChallengeListComponent';
import ChallengeContentComponent from './ChallengeContentComponent.js';
// Legacy Containers/Components
import ChallengePanel from 'legacy_containers/dashboard/ChallengePanel';


const rootStyle = {
  position: 'absolute',
  width: '29%',
  height: '100%',
  zIndex: zIndex.medium

};
const headerStyle = {
  root: {
    width: '100%',
    background: GeneralPalette.menuBG,
    position: 'absolute',
    zIndex: zIndex.mediumHigh,
  },
  bar: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '64px',
    marginLeft: '1%',
    zIndex: zIndex.medium,
    cursor: 'pointer',
  },
  infoZone: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'flex-start',
  },

  projectTitle: {
    marginTop: '1.2em',
    // marginLeft: '1.0%',
    color: GeneralPalette.darkPurple,
    letterSpacing: '.39vw',
    fontSize: '14px',
    fontFamily: 'staticbold',
    textTransform: 'uppercase',
  },
  indexIcon: {
    marginRight: '1em'
  }
};


class DashboardComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showList: false
    };
  }

  toggleList() {
    const $ = this;
    return _.debounce(() => {
      $.setState((pre, props) => {
        return {
          showList: !pre.showList
        };
      });
    }, 1000, true);
  }

  goToNextChallenge() {
    const { nextChallenge, currentChallenge, currentChallengeSet, currentProjectIndex, currentProjectID } = this.props;
    // return ()=>{
    nextChallenge(currentChallenge, currentChallengeSet, currentProjectIndex, currentProjectID);
    // }
  }

  render() {
    const { title, challenge } = this.props;

    const tutorialMode = this.props.tutorialMode === 5;
    return (
      <div style={rootStyle}>
        { tutorialMode ? <TutorialPopup /> : null }
        <div id="header" style={headerStyle.root}>
          <div style={headerStyle.bar}>
            <div style={headerStyle.infoZone}>

              <IconButton onClick={this.toggleList().bind(this)}>
                {this.state.showList ? <NavigationArrowDropUp color={CastleRockPalette.textDefault} /> : <NavigationMenu color={CastleRockPalette.textDefault} />}
              </IconButton>

              <span style={headerStyle.projectTitle}>
                {title}
              </span>
            </div>
            <div style={headerStyle.indexIcon} />
            <IconButton onClick={this.props.goToHome}>
              <NavigationArrowBack color={CastleRockPalette.textDefault} />
            </IconButton>
          </div>
        </div>

        <VelocityTransitionGroup
          enter={{ animation: 'slideDown' }}
          leave={{ animation: 'slideUp' }}
        >
          {
          this.state.showList ? <ChallengeListComponent toggle={this.toggleList().bind(this)} /> : undefined
        }
        </VelocityTransitionGroup>
        {
          challenge.type === 'quiz' ? (<ChallengePanel />) : (
            <ChallengeContentComponent
              showBanner={this.props.showBanner}
              nextChallenge={this.goToNextChallenge.bind(this)}
              challenge={challenge}
              currentEventOrder={this.props.currentEventOrder}
              completed={this.props.completed}
            />)
        }
      </div>);
  }

}
const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };
  const currentChallenge = proState.currentChallengeSelected;
  const challengeSet = proState.currentChallengeSet;
  const projectsData = proState.projects.projectsData;

  return {
    showBanner: state.shared.codepad.showNextChallengeBanner,
    challenge: challengeSet[currentChallenge],
    currentChallenge,
    title: projectsData.find((p) => { return p.projectID === proState.currentProjectID; }).projectName,
    currentChallengeSet: challengeSet,
    currentProjectIndex: state.projectMode.project.currentProjectIndex,
    currentProjectID: state.projectMode.project.currentProjectID,
    tutorialMode: state.shared.app.tutorialMode,
    // Checklist related
    currentEventOrder: proState.currentEventOrder,
    completed: proState.currentChallengeSelected < proState.userProgress.info.projectsOwned.find((p) => { return p.projectId === proState.currentProjectID; }).progress - 1,

  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    nextChallenge: (currentChallenge, currentChallengeSet, currentProjectIndex, currentProjectID) => {
      if (currentChallenge === currentChallengeSet.length - 1) {
        dispatch(updateProjects(currentProjectID));
      } else {
        // START WARNING AREA: These two functions must be called in this order --- reason unknown
        console.log(`currentChallenge: ${JSON.stringify(currentChallenge)}`);
        dispatch(incrementChallengesCompleted(currentProjectID, currentChallenge)); // TODO: specify currentChallenge in other places incrementChallengesCompleted is called
        // NOTE: currentChallenge is 0-indexed
        dispatch(nextChallenge());
       // STOP WARNING AREA
      }
      dispatch(resetBuildProgress());
    },
    goToHome: () => {
      dispatch(projectView());
      dispatch(gridView());
      dispatch(closeDoc());
      dispatch(layoutResize());
            // dispatch(setBlur(false));
    },
  };
};


const Dashboard = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashboardComponent);

export default Dashboard;
