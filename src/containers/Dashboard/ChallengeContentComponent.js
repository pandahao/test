import React, { Component } from 'react';
import FlatTextButton from 'components/FlatTextButton';
import _ from 'underscore';

import { TypeCode, TypeLearn, TypeBuild, DoneStatusIcon } from 'legacy_assets';
import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';
import TranslatedText from 'legacy_containers/TranslatedText';
import { zIndex } from 'styles/zIndex';
import { t } from 'legacy_utils/i18n';

import ChallengeProgressBar from 'components/ChallengeProgressBar';
import ChallengeCheckListComponent from './ChallengeCheckListComponent';

// Success Banner
import SuccessBanner from 'containers/codepad/SuccessBannerContainer';
import InlineCodeText from 'legacy_components/dashboard/challenge/InlineCodeText';
import CodeBlock from 'legacy_components/dashboard/challenge/CodeBlock';
import HintBlockContainer from 'legacy_containers/dashboard/challenge/HintBlockContainer';

const rootStyle = {
  height: '100%'
};
const headerStyle = {
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: '80px 0px 20px 8px',
    fontSize: '12px',
    color: GeneralPalette.buttonBackground,
    letterSpacing: '1.5px',
    zIndex: zIndex.medium,
  },
  hidden: {
    visibility: 'hidden'
  },
  challenge_title: {
    marginLeft: '20px',
    marginRight: '20px',
    marginTop: '-5px'
  },
  icon: {
    zoom: 1.4,
    marginRight: '3px'
  }
};

const contentStyle = {
  root: {
    background: ChallengePalette.mainBackground,
    color: CastleRockPalette.textDefault,
    fontFamily: 'sfns',
    paddingRight: '30px',
    fontSize: '15px',
    overflowX: 'hidden',
    height: 'calc(100% - 80px)',
    zIndex: zIndex.medium - 1
  },
  head: {
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    paddingLeft: '8px',
    fontSize: '20px',
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: '30px',
    marginRight: '10px',
    marginTop: '80px',
    marginBottom: '20px',
    fontSize: '10px',
    overflow: 'hidden',
    color: ChallengePalette.headerBar,
    letterSpacing: '3px',
    fontFamily: 'staticbold',
    zIndex: zIndex.medium
  },
  introduction: {
    backgroundColor: ChallengePalette.introductionBackground,
    padding: '0px 0px 30px 30px',
    lineHeight: '25px',
  },
  contentheader: {
    width: '100%',
    padding: '30px 20px 0px 30px',
    color: GeneralPalette.buttonBackground,
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    letterSpacing: '0.15em',
    fontSize: '15px',
    borderTop: `2px solid${GeneralPalette.menuBG}`,
    marginBottom: '20px',
  },
  content: {
    width: '93%',
    marginRight: '0.5em',
    marginTop: 'none',
    lineHeight: '25px',
    padding: '0px 80px 20px 30px',
    zIndex: zIndex.medium - 1
  },
  footer: {
    display: 'flex',
    justifyContent: 'space-around',
    width: '29%',
    left: '64px',
    height: '64px',
    position: 'fixed',
    bottom: '0px',
    boxShadow: `inset 0 5px 9px -5px ${GeneralPalette.whiteShadow}`,
    border: 'none',
    right: '0.5em',
    backgroundColor: GeneralPalette.white,
    zIndex: zIndex.medium
  },
  codes: {
    height: '15px',

  },
  editor: {
    backgroundColor: GeneralPalette.backgroundDark,
    padding: '10px',
    width: '90%',
    borderRadius: '3px',
    marginTop: '3%',
    fontFamily: 'menlo',
    fontSize: '12px',
    color: GeneralPalette.white,
    zIndex: 0
  }
};

const ChallengeBlockComponent = ({ block, type }) => {
  // console.log(block,type);
  if (type === 'build') {
    return (<InlineCodeText text={block} />);
    // return (<div>{block}</div>)
  } else if (block.code) {
    return <CodeBlock block={block} num={''} />;
  }
  return (
    <div>
      <div>
        <InlineCodeText text={block.text} />
      </div>
      <div style={contentStyle.codes}>
        {
    block.code
   }
      </div>
    </div>);
};


class ChallengeContentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adminMode: false,
      current: 1,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.challenge !== nextProps.challenge) {
      this.setState({
        current: 1
      });
    }
  }
  componentWillMount() {
    const $ = this;

    $.nextStepHandler = _.throttle(() => {
      const { nextChallenge, challenge, completed } = $.props;
      const limit = $.props.challenge.content.blocks.length;

      // Build Animation Control
      if (challenge.type === 'build' && window.SkylineHandlers) {
        window.SkylineHandlers.forward();
      }

      if ($.state.current < limit) {
        $.setState((pre, props) => {
          return {
            current: pre.current + 1
          };
        });
      } else if (challenge.type === 'code' && !completed) {
          // Pass challenge
          // nextChallenge();
          // $.setState({current:1});
          // $.forceUpdate();
      } else {
        if (challenge.type === 'build' && window.SkylineHandlers) {
          window.SkylineHandlers.onUnmont();
        }
        nextChallenge();
        $.setState({ current: 1 });
        $.forceUpdate();
      }
    }, 1500, true);
    $.backStepHandler = _.throttle(() => {
      const { nextChallenge, challenge, completed } = $.props;
      // Build Animation Control
      if (window.SkylineHandlers && challenge.type === 'build') {
        window.SkylineHandlers.backward();
      }
      if ($.state.current > 1) {
        $.setState((pre, props) => {
          return {
            current: pre.current - 1
          };
        });
      }
    }, 1500, true);
  }
  render() {
    const { challenge, completed } = this.props;

    // const nextStepHandler = _.debounce(this.nextStep(),1000,true);

    const limit = challenge.content.blocks.length;

    let buttonColor,
      buttonText;
    if (this.state.current < limit) {
      buttonText = t('NEXT STEP');
      buttonColor = GeneralPalette.brightPurple;
    } else if (challenge.type === 'code' && !completed) {
      buttonText = t('NEXT STEP');
      buttonColor = GeneralPalette.darkPurple;
    } else {
      buttonText = t('COMPLETE CHALLENGE');
      buttonColor = GeneralPalette.green;
    }


    let titleWording;
    if (challenge.type === 'quiz') {
      titleWording = <TranslatedText text={'KNOWLEDGE CHECK'} />;
    } else {
      titleWording = <TranslatedText text={'INSTRUCTIONS'} />;
    }


    return (
      <div style={rootStyle}>
        <div style={contentStyle.root} id="content-root">
          <div style={contentStyle.head}>
            <div style={headerStyle.root}>
              <ChallengeProgressBar current={this.state.current} step={challenge.content.blocks.length} />
              {completed ? <DoneStatusIcon /> : null}
            </div>

            <h3 style={headerStyle.challenge_title}>{challenge.title}</h3>
          </div>
          <div style={contentStyle.contentheader}>
            STEP {this.state.current}
          </div>
          <div style={contentStyle.content}>
            {
              this.state.current === 1 ? <div>{challenge.content.introduction}<br /><br /></div> : null
            }
            <ChallengeBlockComponent type={challenge.type} block={challenge.content.blocks[this.state.current - 1]} />
            {
              challenge.type === 'code' ?
                <div>
                  <HintBlockContainer />
                </div> : null
            }
          </div>
        </div>

        <div style={contentStyle.footer}>
          {
            this.props.showBanner ? (<SuccessBanner />) : undefined
          }
          {
            challenge.type === 'code' ? (
              <ChallengeCheckListComponent
                checklist={challenge.content.checklist}
                currentEventOrder={this.props.currentEventOrder}
                completed={this.props.completed}
              />) : undefined
          }
          <FlatTextButton
            onClick={this.backStepHandler.bind(this)}
            text={`< ${t('GO BACK')}`}
            color={this.state.current === 1 ? GeneralPalette.darkPurple : GeneralPalette.brightPurple}
          />
          <FlatTextButton
            onClick={this.nextStepHandler.bind(this)}
            invert
            text={buttonText}
            color={buttonColor}
          />
        </div>
      </div>
    );
  }
}

export default ChallengeContentComponent;
