import React, { Component } from 'react';
import Radium from 'radium';
import { GeneralPalette } from 'legacy_styles/Colors';
import zIndex from 'styles/zIndex';
import TranslatedText from 'legacy_containers/TranslatedText';
import { VelocityTransitionGroup } from 'velocity-react';

import { CheckBox, EmptyCheckBox, Donut } from 'legacy_assets/index';


const rootStyle = {
  width: '100%',
  backgroundColor: GeneralPalette.buttonBackground,
  color: GeneralPalette.white,
  position: 'absolute',
  textAlign: 'center',
  fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
  left: '0px',
  bottom: '64px',
};
const titleStyle = {
  cursor: 'pointer',
  marginTop: '0.4em',
  marginBottom: '0.4em',
  fontSize: '12px',
  letterSpacing: '2px'
};
const listStyle = {
  fontSize: '12px',
  padding: '1em 1em 1em 3em',
  textAlign: 'left',
  color: GeneralPalette.buttonBackground,
  backgroundColor: GeneralPalette.whiteShadow
};
// const showList = true;
class ChallengeCheckListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showList: false
    };
  }
  toggleList() {
    this.setState((pre, props) => {
      return {
        showList: !pre.showList
      };
    });
  }
  render() {
    const { checklist } = this.props;

    return (<div style={rootStyle}>
      <div style={titleStyle} onClick={this.toggleList.bind(this)}>
        <TranslatedText text={'CHECKLIST'} />
      </div>
      <VelocityTransitionGroup
        enter={{ animation: 'slideDown' }}
        leave={{ animation: 'slideUp' }}
      >
        {
        this.state.showList ?
          <div style={listStyle}>
            {
            checklist.map((block, index) => {
              return (<div key={index}>
                <span
                  style={{
                    marginRight: '0.5em'
                  }}
                >
                  {
                  (index < this.props.currentOrder || this.props.completed) ? (<CheckBox />) : <Donut />
                }
                </span>
                {
                  block.text
                }
              </div>);
            })
          }
          </div> : undefined
      }
      </VelocityTransitionGroup>
    </div>);
  }
}

export default Radium(ChallengeCheckListComponent);
