import { connect } from 'react-redux';
import React, { PropTypes } from 'react';
import Radium from 'radium';
import FlatButton from 'material-ui/FlatButton';

import TranslatedText from 'legacy_containers/TranslatedText';


import { showReset } from 'legacy_actions/ListViewActions';
import { gridView } from 'legacy_actions/AppActions';
import { initializeQuizResult, initializeAnswersSelected, selectChallenge, toggleChallengeView, setBlur, toggleBuildView, setHint, goToPoint } from 'legacy_actions/ChallengeActions';
import { closeChallengeBanner } from 'legacy_actions/CodepadActions';

import { ChallengePalette, CastleRockPalette, GeneralPalette } from 'legacy_styles/Colors';
import { TypeBuild, TypeBuildLocked, TypeCode, TypeCodeLocked, TypeLearn, TypeLearnLocked, DoneStatusCircle, Locked } from 'legacy_assets';
import { zIndex } from 'styles/zIndex';

class ListItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { level, title, type, challengeProgress, selected, onClick, challengeCompleted, currentChallengeSelected } = this.props;
    let status = 'LOCKED';
    if (challengeProgress === level) {
      status = 'UNLOCKED';
    } else if (challengeProgress > level) {
      status = 'COMPLETED';
    }

    const level_number = (level) < 10 ? (`0${level}`) : (level);
    const textColor = () => {
      switch (status) {
        case 'LOCKED':
          return ChallengePalette.listDecorator;
        case 'COMPLETED':
          return selected ? 'white' : CastleRockPalette.textDefault;
        case 'UNLOCKED':
          return selected ? 'white' : CastleRockPalette.textDefault;
        default:
          return CastleRockPalette.textDefault;
      }
    };

    const style = {
      row: {
        width: '50%',
        cursor: status != 'LOCKED' ? 'pointer' : 'default'
      },
      number: {
        width: '15%',
        textAlign: 'center',
        alignSelf: 'center',
        color: selected ? ChallengePalette.white : GeneralPalette.buttonBackground,
        borderBottomLeftRadius: '20px',
        borderTopLeftRadius: '20px',
        backgroundColor: selected ? GeneralPalette.brightPurple : null,
        verticalAlign: 'top',
        paddingTop: '8px',
        paddingBottom: '3px',

      },
      title: {
        color: selected ? ChallengePalette.white : (status != 'LOCKED' ? GeneralPalette.darkPurple : GeneralPalette.sideMenuPurple),
        textAlign: 'left',
        backgroundColor: selected ? GeneralPalette.brightPurple : null,
        width: '65%',
        verticalAlign: 'top',
        paddingTop: '8px',
        paddingBottom: '3px',
        paddingRight: '10px'
      },
      typeIcon: {
        color: GeneralPalette.darkPurple,
        fill: GeneralPalette.darkPurple,
        backgroundColor: selected ? GeneralPalette.brightPurple : null,
        height: '100%',
        paddingRight: '20px',
        width: '5%',
        borderTopRightRadius: '20px',
        borderBottomRightRadius: '20px',
        verticalAlign: 'top',
        paddingTop: '8px',
        paddingBottom: '3px',
      },
      lock: {
        position: 'relative',
        paddingTop: '5px',
        right: '18px',
        verticalAlign: 'top',
        paddingTop: '8px',
        paddingBottom: '3px',
        display: 'fixed'
      },
      selected: selected && status != 'LOCKED' ? {
        display: 'block',
        position: 'absolute',
        marginTop: '-0.5em',
        marginLeft: '1em',
        paddingLeft: 0,
        height: '2em',
        width: '85%',
        background: ChallengePalette.primary,
        boxShadow: `0px 2px 1px ${ChallengePalette.squareShadow}`,
        zIndex: -10,
        borderRadius: '20px',
      } : {}
    };

    let typeIcon;
    const locked = (status === 'LOCKED');
    if (type === 'code') {
      typeIcon = locked ? <TypeCodeLocked /> : <TypeCode />;
    } else if (type === 'quiz') {
      typeIcon = locked ? <TypeLearnLocked /> : <TypeLearn />;
    } else if (type === 'build') {
      typeIcon = locked ? <TypeBuildLocked /> : <TypeBuild />;
    }
    const clickable = locked ? null : onClick.bind(null, type, challengeCompleted, currentChallengeSelected);
    const lockIcon = locked ? <Locked /> : null;

    return (
      <tr style={style.row} onClick={clickable}>
        <td style={style.number}> {level_number} </td>
        <td style={style.title}> {title} </td>
        <td style={style.typeIcon}> {typeIcon} </td>
        {/* <td style={style.lock}>
                  {lockIcon}
                </td> */}
      </tr>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  const currentProject = proState.projects.projectsData.find((p) => {
    return p.projectID === proState.currentProjectID;
  });
  const thisProject = proState.userProgress.info.projectsOwned.find((p) => {
    return p.projectId === proState.currentProjectID;
  });
  return {
    currentChallengeSelected: proState.currentChallengeSelected,
    selected: proState.currentChallengeSelected === ownProps.level - 1,
    challengeCompleted: thisProject.progress > currentProject.challengeData[proState.currentChallengeSelected].level,
    challengeProgress: thisProject.progress,
    ...ownProps
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: (type, challengeCompleted, currentChallengeSelected) => {
      if (type === 'quiz') {
        if (!challengeCompleted) {
          dispatch(initializeQuizResult());
          dispatch(initializeAnswersSelected());
        }
      }
      if (ownProps.level - 1 === currentChallengeSelected) {
        dispatch(toggleChallengeView());
      } else {
        if (type === 'build') {
          dispatch(goToPoint(1));
        }
        dispatch(selectChallenge(ownProps.level));
        dispatch(toggleChallengeView());
        dispatch(setHint(false));
        dispatch(closeChallengeBanner());
      }
      // toggle is being passed all the way from index down to here
      ownProps.toggle();
    }
  };
};

const ChallengeListItem = Radium(connect(
    mapStateToProps,
    mapDispatchToProps
)(ListItem));


// import Radium from 'radium';
const style = {
  root: {
    zIndex: zIndex.medium + 1,
    position: 'fixed',
    bottom: '0%',
    top: '0%',
    height: '100%',
    marginTop: '4em',
    backgroundColor: GeneralPalette.white,
    width: '29%',
    display: 'flex',
    flexDirection: 'column',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    overflow: 'scroll',
    // Blur and Shadow Effect
    // boxShadow: '0 0 0 0px rgba(0, 0, 0, 0.84)',
    transition: 'box-shadow 0.6s ease-in-out'
  },
  table: {
    marginTop: '2em',
    textAlign: 'center',
    width: '100%',
    borderCollapse: 'collapse',
    marginLeft: '2%',
    zIndex: zIndex.medium,
    marginBottom: '100%',
    overflowY: 'scroll',
  },
  buttongroup: {
    display: 'flex',
    position: 'fixed',
    bottom: '0%',
    marginLeft: '0px',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '28%',
    zIndex: zIndex.medium,
    paddingBottom: '1%',
    paddingTop: '1%',
    color: GeneralPalette.buttonBackground,
    backgroundColor: GeneralPalette.menuBG,
    letterSpacing: '0.1em',
    boxShadow: `-0.5px 0px 15px 0px${GeneralPalette.buttonBackground}`,
  },
  button: {
    outline: 'none',
    cursor: 'pointer',
    backgroundColor: 'white',
    boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
    border: 'none',
    borderRadius: '20px',
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    padding: '0px 35px 0px 35px',
    marginBottom: '15px',
    color: GeneralPalette.darkPurple,
    letterSpacing: '0.1em',
    width: '250px'
  },
  restart: {
    outline: 'none',
    position: 'relative',
    cursor: 'pointer',
    backgroundColor: GeneralPalette.white,
    boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
    border: 'none',
    borderRadius: '20px',
    fontSize: '10px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    lineHeight: '20px',
    padding: '0px 5px 0px 5px',
    marginBottom: '0px',
    color: GeneralPalette.buttonBackground,
    letterSpacing: '0.1em',
    width: '150px'
  },
  tableonly: {
    overflowX: 'hidden',
    overflowY: 'scroll',
  }

};

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shadow: false
    };
  }
  componentDidMount() {
    window.setTimeout(() => {
      this.setState({
        shadow: true
      });
    }, 360);
  }
  render() {
    const { challenges, openProjectListView, restartProject, toggle } = this.props;

    return (
      <div
        style={Object.assign({}, style.root, {
          boxShadow: `0 0 0 ${this.state.shadow ? 9999 : 0}px rgba(0, 0, 0, 0.84)`,
        })}
      >
        <div style={style.tableonly}>
          <table style={style.table}>
            <tbody>
              {this.props.challenges.map((ch) => {
                return (<ChallengeListItem key={ch.level}{...ch} toggle={toggle} />);
              })}
            </tbody>
          </table>
        </div>
      </div>

    );
  }
}

const listMapStateToProps = (state, ownProps) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  return {
    challenges: proState.currentChallengeSet,
    ...ownProps
  };
};
const listMapDispatchToProps = (dispatch) => {
  return {
    openProjectListView: () => {
      dispatch(gridView());
      dispatch(setBlur(false));
    },
    restartProject: () => {
      dispatch(showReset());
    }
  };
};

const ChallengeListComponent = connect(
    listMapStateToProps,
    listMapDispatchToProps
)(List);

export default ChallengeListComponent;
