import React, { Component } from 'react';
import Carousel from 'components/Carousel';

import ClipCorners from './clip-corners';
import ProjectCardComponent from './ProjectCardComponent';

import { GeneralPalette } from 'legacy_styles/Colors';
import { StagePicker } from 'styles/StagePalette';

import { zIndex } from 'styles/zIndex';
import { t } from 'legacy_utils/i18n';
import TranslatedText from 'legacy_containers/TranslatedText';


const style = {
  root: {
    position: 'relative',
    display: 'flex',
    backgroundColor: 'gold',
    width: '100%',
    height: '280px',
    margin: '1em',
    borderRadius: '12px'
  },
  badge(erraMode) {
    return {
      display: 'inline-block',
      float: 'left',
      margin: '2em',
      backgroundColor: 'rgba(0,0,0,0.15)',
      width: '120px',
      borderRadius: erraMode ? 0 : 400,
      height: '120px',
      position: 'relative'
    };
  },
  stageTitle: {
    font: 'static',
    color: GeneralPalette.white,
    opacity: 1,
    letterSpacing: 4,
    marginLeft: '0.25em',
  },
  stageIndex: {
    color: GeneralPalette.white,
    font: 'static-bold',
    fontSize: '48px',
    height: 60,
    marginTop: 12
  },
  button: {
    marginTop: '10%',
    cursor: 'pointer',
    fill: GeneralPalette.white
  },
  showCaseRoot: {
    width: '58vw',
    position: 'relative'
  },
  dummy: {
    position: 'relative',
    marginTop: '3em',
    width: '200px',
    textAlign: 'center'
  },
  ratings: {
    color: 'white',
    fontSize: 14
  }
};

const stages = {
  1: {
    age: 10,
    displayNumber: 1,
    label: '新手任务',
    rating: 1
  },
  2: {
    age: 10,
    displayNumber: 2,
    label: '基础任务',
    rating: 2
  },
  3: {
    age: 12,
    displayNumber: 3,
    label: '进阶任务',
    rating: 3
  },
  4: {
    age: 14,
    displayNumber: 4,
    label: '大师挑战',
    rating: 4
  },
  5: {
    label: 'NOC竞赛'
  },
  9: {
    label: '导师营地'
  },
  11: {
    age: 8,
    displayNumber: 1,
    label: '菜鸟任务',
    rating: 0
  },
  12: {
    age: 8,
    displayNumber: 2,
    label: '入门挑战',
    rating: 1
  },
  13: {
    label: '导师营地'
  }
};

function Ratings({ rating }) {
  const stars = Array(5).fill().map((_, i) => {
    return <span key={i}>{ i >= rating ? '☆' : '★' }</span>;
  });
  return (
    <div>{stars}</div>
  );
}

class ProjectStageComponent extends Component {
  constructor(props) {
    super(props);
    const $ = this;
    $.resizeHandler = $.handleResize.bind($);
    $.mql = window.matchMedia('(min-width:1600px)');

    $.mql.addListener($.resizeHandler);
    $.state = {
      column: $.mql.matches ? 4 : 3
    };
  }
  componentWillUnmount() {
    const $ = this;
    $.mql.removeListener($.resizeHandler);
  }
  handleResize(mediaQuery) {
    if (mediaQuery.matches) {
      this.setState({ column: 4 });
    } else {
      this.setState({ column: 3 });
    }
  }

  render() {
    const { rawStage, progress, goToProject, projectOverview, erraMode, tutorialMode } = this.props;
    let stage;
    const mod = rawStage.length % this.state.column;
    if (mod !== 0) {
      stage = rawStage.concat([...new Array(this.state.column - mod)].fill({ isDummy: true }));
    } else {
      stage = rawStage;
    }

    const stageID = stage[0].stageID;
    const stageConfig = stages[stageID] || {};

    const tutorialStage = tutorialMode && stageID == 1;

    const sliderSettings = {
      slidesToShow: this.state.column,
      frameOverflow: tutorialStage ? 'visiable' : 'hidden'
    };
    if (tutorialStage) {
      stage.splice(1, stage.length);
    }
    const backgroundColor = StagePicker(parseInt(stageID));
    return (
      <div
        style={{ ...style.root,
          ...{
            backgroundColor,
            zIndex: tutorialStage ? zIndex.mediumHigh : zIndex.medium,
          } }}
      >
        {erraMode && <ClipCorners size={48} />}

        <div>
          <div style={style.badge(erraMode)}>
            {erraMode && <ClipCorners color={backgroundColor} size={40} />}
            <div style={style.stageIndex}>{stageConfig.displayNumber}</div>
            <div style={style.stageTitle}>{stageConfig.label}</div>
          </div>
          {Number.isInteger(stageConfig.rating) &&
            <div style={style.ratings}>
              <Ratings rating={stageConfig.rating} />
              <div>推荐年龄: {stageConfig.age}+</div>
            </div>
          }
        </div>

        <Carousel style={style.showCaseRoot} {...sliderSettings}>
          {
            stage.map((proj, index) => {
              return (
                <ProjectCardComponent
                  key={index}
                  progress={progress}
                  proj={proj}
                  goToProject={goToProject}
                  projectOverview={projectOverview}
                  highLight={tutorialStage && proj.id === 5}
                />);
            })
        }
        </Carousel>
      </div>
    );
  }
}
export default ProjectStageComponent;
