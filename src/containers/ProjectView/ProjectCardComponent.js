import React, { Component } from 'react';
import _ from 'underscore';

import ImageLink from 'components/ImageLink';
import IconButton from 'material-ui/IconButton';
import FlatTextButton from 'components/FlatTextButton';
import TutorialPopup from 'components/TutorialPopup';

import { GeneralPalette } from 'legacy_styles/Colors';

// import NavigationMoreHoriz from 'material-ui/svg-icons/navigation/more-horiz';
import ActionLock from 'material-ui/svg-icons/action/lock';
import { ProjectRibbon, MoreInfo } from 'assets/Icons';

import { zIndex } from 'styles/zIndex';
import { t } from 'legacy_utils/i18n';

import { COMPLETED, LOCKED, ONGOING } from 'legacy_actions/ChallengeActions';

const style = {
  showCase: {
    card: {
      position: 'relative',
      marginTop: '3em',
          // backgroundColor : 'blue',
      width: '200px',
      // maxWidth : 'calc(17vw - 100px)',
      textAlign: 'center'
    },
    loader: {
      position: 'absolute',
      zIndex: zIndex.high,
      top: '18%',
      left: '33%',
      opacity: 1,
      transition: 'visibility 0.4s'
    },
    img: {
      height: '100%',
      width: '100%',
      position: 'relative',
      top: '0%',
      left: '0%',
      borderRadius: '8px',
      cursor: 'pointer'
    },
    title: {
      marginTop: '1em'
    },
    bar: {
      position: 'relative',
      width: '95%',
      margin: '0 auto 0.6em auto',
      borderRadius: '500px',
      height: '2px',
      backgroundColor: 'rgba(104,110,132,0.3)'
    },
    progress: {
      position: 'absolute',
      height: '2px',
      backgroundColor: GeneralPalette.white,
    },
    placeholder: {
      borderRadius: '8px',
      opacity: 0.2,
      backgroundColor: GeneralPalette.black,
      position: 'relative',
      paddingBottom: '75%',
      marginBottom: 10,
    }
  },
  overview: {
    zIndex: zIndex.high,
    position: 'absolute',
    bottom: '0%',
    right: '0%',
    //backgroundColor : 'rgba(104,110,132,0.2)',
    //borderRadius : '60px'
  },
  lock: {
    zIndex: zIndex.high,
    position: 'absolute',
    margin: '0.5em',
    bottom: '0%',
    left: '0%'
  },
  badge: {
    zIndex: zIndex.high,
    position: 'absolute',
    margin: '0.5em',
    // zoom : '%',
    top: '-1.5em',
    right: '-1.5em'
  }
};

class ProjectCardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaderID: null
    };
  }
  toProject(proj, prog) {
    const $ = this;
    const { goToProject } = $.props;
    return _.debounce(() => {
      $.setState({
        loaderID: proj.projectID
      });
      goToProject(proj.projectID, prog.status, proj.challengeData, prog.progress);
    }, 5000, true);
  }
  openOverview(projectID, e) {
    e.stopPropagation();
    const { projectOverview } = this.props;
    projectOverview(projectID);
  }
  render() {
    const { progress, proj, highLight } = this.props;
    if (proj.isDummy) {
      return (
        <div style={style.showCase.card}>
          <ImageLink style={style.showCase.img} src="./static/media/empty_project.png" />
        </div>
      );
    }

    const projProgress = progress.find((p) => {
      return p.projectId === proj.projectID;
    });

    const loaderStyle = {
      display: this.state.loaderID === proj.projectID ? 'block' : 'none'
    };

    if (proj.placeHolder) {
      return (<div style={style.showCase.card} >
        <div style={style.showCase.img}>
          <div style={style.showCase.placeholder} />
        </div>
      </div>);
    }
    let challengeProgress;
    if (projProgress.progress - 1 > proj.numChallenges) {
      challengeProgress = proj.numChallenges + 1;
    } else {
      challengeProgress = projProgress.progress;
    }
    return (
      <div style={style.showCase.card} onClick={this.toProject(proj, projProgress)}>
        {
        highLight ? <TutorialPopup
          padding="2em 2em 1em 2em"
          top="-2em"
          left="-2em"
          borderRadius="1em"
          mode="BOTTOM"
          optionalNextAction={this.toProject(proj, projProgress)}
        /> : null
      }
        <div style={Object.assign({}, style.showCase.loader, loaderStyle)}>
          <img src="./static/media/project_loading.gif" />
        </div>
        <ImageLink style={style.showCase.img} src={proj.imageLink}>
          <div style={style.overview}>
            <div
              style={{
                zoom: '70%',
                cursor: 'pointer'
              }} onClick={this.openOverview.bind(this, proj.projectID)}
            >
              <MoreInfo color={GeneralPalette.buttonBackground} />
            </div>
          </div>
          {
            projProgress.status === LOCKED ? (<div style={style.lock}>
              <ActionLock color="rgba(104,110,132,0.2)" />
            </div>) : null
          }{
             projProgress.status === COMPLETED ? (<div style={style.badge}>
               {
                 <img src="./static/media/project_completed.png" />
                // <ProjectRibbon />
              }
             </div>) : null
          }
        </ImageLink>
        <div
          style={
              Object.assign(
                  {},
                  style.showCase.bar,
                  { opacity: projProgress.status === 'ONGOING' ? 1 : 0 }
              )
          }
        >
          <div
            style={Object.assign({}, style.showCase.progress,
              {
                width: `${((challengeProgress - 1) / proj.numChallenges) * 100}%`
              }
              )}
          />
        </div>
        <div style={style.showCase.title}>
          <FlatTextButton
            onClick={this.toProject(proj, projProgress)}
            width={'100%'}
            hoverOutline
            color={GeneralPalette.white}
            text={proj.projectName.toUpperCase()}
          />
        </div>
      </div>);
  }
}
export default ProjectCardComponent;
