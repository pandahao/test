import React, { Component } from 'react';

const size = 60;

const styles = {
  style: {
    overflow: 'hidden',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  corner(color, size) {
    return {
      backgroundColor: color,
      position: 'absolute',
      width: size,
      height: size,
      transform: 'rotate(45deg)'
    };
  },
  topLeftCorner(color, size) {
    return {
      ...styles.corner(color, size),
      left: -size / 2,
      top: -size / 2
    };
  },
  topRightCorner(color, size) {
    return {
      ...styles.corner(color, size),
      right: -size / 2,
      top: -size / 2
    };
  },
  bottomLeftCorner(color, size) {
    return {
      ...styles.corner(color, size),
      left: -size / 2,
      bottom: -size / 2
    };
  },
  bottomRightCorner(color, size) {
    return {
      ...styles.corner(color, size),
      right: -size / 2,
      bottom: -size / 2
    };
  }
};

export default function ClipCorners({ color = 'white', size }) {
  return (
    <div style={styles.style}>
      <div style={styles.topLeftCorner(color, size)} />
      <div style={styles.topRightCorner(color, size)} />
      <div style={styles.bottomLeftCorner(color, size)} />
      <div style={styles.bottomRightCorner(color, size)} />
    </div>
  );
}
