import { connect } from 'react-redux';
import React from 'react';


import ProjectStageComponent from './ProjectStageComponent';


const style = {
  space: {
    height: '20px',
  },
  entiregrid: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignContent: 'flex-start',
    margin: '10px',
    width: '85%',
    flexWrap: 'wrap',
    height: 'auto',
  },
};

export default function ProjectGridComponent({
  stages, progress, goToProject, projectOverview, tutorialMode, erraMode
}) {
  return (
    <div style={style.entiregrid}>
      {stages.map((stage, index) =>
        <ProjectStageComponent
          key={index} rawStage={stage}
          {...{ progress, goToProject, projectOverview, erraMode, tutorialMode }}
        />
      )}
      <div style={style.space} />
    </div>
  );
}
