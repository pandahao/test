import { connect } from 'react-redux';
import React, { Component } from 'react';

// Actions
import { gridView, listView } from 'legacy_actions/AppActions';
import { loadProjects, loadSingleProject } from 'legacy_actions/ListViewActions';
import { overview } from 'legacy_actions/AppActions';
import { selectProject, beginProject, setLoading } from 'legacy_actions/ListViewActions';
import { layoutResize } from 'legacy_actions/RobotActions';
import { showChallengeView, initializeChallenges, initialize_challenge_set, goToPoint } from 'legacy_actions/ChallengeActions';
import { loadModel } from 'legacy_lib/build/ModelManager';

// Style & Assets
import { GeneralPalette } from 'legacy_styles/Colors';

// Components
import CircularProgress from 'material-ui/CircularProgress';
import TranslatedText from 'legacy_containers/TranslatedText';

import ProjectHeaderComponent from './ProjectHeaderComponent';
import ProjectGridComponent from './ProjectGridComponent';
import ProjectListComponent from './ProjectListComponent';


const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };
  const { challengeLocation, erraMode, tutorialMode } = state.shared.app;

  const stages = state.projectMode.project.projects.projectsData.filter((p) => {
    if (erraMode) {
      return p.ctype === 'erra';
    }
    return p.ctype !== 'erra';
  }).reduce((pre, cur) => {
    if (pre[cur.stageID]) {
      pre[cur.stageID].push(cur);
    } else {
      pre[cur.stageID] = [cur];
    }
    return pre;
  }, []);

  const progress = state.projectMode.project.userProgress.info.projectsOwned;

  return {
    projectStatus: proState.projects.status,
    userStatus: proState.userProgress.status,
    projects: proState.projects.projectsData,
    projectListView: proState.challengeLocation === 1,
    gridView: challengeLocation === 0,
    progress,
    stages,
    erraMode,
    tutorialMode
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadProjects: () => {
      dispatch(loadProjects());
    },
    listView: () => {
      dispatch(listView());
    },
    toggleGridView: () => {
      dispatch(gridView());
    },
    projectOverview: (projectID) => {
      dispatch(selectProject(projectID));
      dispatch(overview());
    },
    goToProject: (projectID, status, challengeData, challengeProgress) => {
      dispatch(setLoading(projectID));
      dispatch(loadSingleProject(projectID));
      loadModel(projectID).then(() => {
        dispatch(setLoading(-1));
        dispatch(selectProject(projectID));
        dispatch(initialize_challenge_set(challengeData));
        dispatch(initializeChallenges(status, challengeProgress));
        dispatch(goToPoint(1));
        dispatch(showChallengeView(status));
        dispatch(layoutResize());
        if (status === 'UNLOCKED') {
          dispatch(beginProject(projectID));
        }
      });
    }
  };
};

const style = {
  root: {
    display: 'block',
    paddingTop: '20px',
    width: '106%',
    height: '100vh',
    overflow: 'scroll',
    fontFamily: 'Static',
    backgroundColor: GeneralPalette.white,
    textAlign: 'center',
    margin: 'auto'
  },
  panel: {
    display: 'flex',
    backgroundColor: GeneralPalette.white,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: '100px',
    width: '100%'
  }
};

class ProjectViewContainer extends Component {
  componentWillMount() {
    // this.props.loadProjects();
  }
  render() {
    const {
      stages,
      progress,
      goToProject,
      projectOverview,
      projects,
      userStatus,
      projectStatus,
      projectListView,
      gridView,
      listView,
      toggleGridView,
      erraMode
    } = this.props;

    // Hard Coded For Now
    const tutorialMode = this.props.tutorialMode === 4;

    return (
      <div style={style.root}>
        <ProjectHeaderComponent
          projectListView={projectListView}
          gridView={gridView}
          listView={listView}
          toggleGridView={toggleGridView}
        />
        <div style={style.panel}>
          {
          (projectStatus === 'loading' || userStatus === 'loading') ?
          (
            <CircularProgress color={GeneralPalette.brightPurple} />
          ) : (
            gridView ? (
              <ProjectGridComponent
                stages={stages}
                progress={progress}
                goToProject={goToProject}
                projectOverview={projectOverview}
                erraMode={erraMode}
                tutorialMode={tutorialMode}
              />
            ) : (
              <ProjectListComponent
                projects={projects}
                gridView={gridView}
                userStatus={progress}
                projectOverview={projectOverview}
                goToProject={goToProject}
                erraMode={erraMode}
              />
            )
          )
        }
        </div>
      </div>
    );
  }
}

const ProjectView = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProjectViewContainer);

export default ProjectView;
