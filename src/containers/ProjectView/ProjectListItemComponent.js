import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette } from 'legacy_styles/Colors';
import { DoneStatusIcon, Locked, Donut } from 'legacy_assets/index';
import TranslatedText from 'legacy_containers/TranslatedText';
import CircularProgress from 'material-ui/CircularProgress';

import { DevEnv } from 'config';

const style = {
  data: {
    color: GeneralPalette.darkPurple,
    fontSize: '1em',
    fontFamily: 'sfns',
    paddingTop: '10px',
  },
  header: {
    display: 'flex',
    justifyContent: 'center',
    fontSize: '20px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    backgroundColor: 'white',
    color: GeneralPalette.darkPurple,
    height: '1%',
    padding: '10px',
    paddingBottom: '10px',
    textTransform: 'uppercase',
  },
  buttongroup: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingBottom: '10px',
  },
  box: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
    width: '30%',
    height: '20%',
    backgroundColor: 'white',
    margin: '0 1.5% 2% 1.5%',
    boxShadow: `0px 1.5px 2px 1px${GeneralPalette.whiteShadow}`,
  },
  statusIcon: {
    marginLeft: '80%',
    position: 'absolute',
    top: '20%',
    zIndex: '10',
    left: '10%',
  },
  lockedicon: {
    zoom: 2,
  },
  overview: {
    backgroundColor: GeneralPalette.menuBG,
    // boxShadow: `0px 1.5px 1px 1px${GeneralPalette.whiteShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: GeneralPalette.darkPurple,
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    width: '120px',
    lineHeight: '35px',
    paddingTop: '2px'
  },
  continue: {
    backgroundColor: GeneralPalette.brightPurple,
    // boxShadow: `0px 1.5px 2px 1px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    width: '120px',
    lineHeight: '35px',
    paddingTop: '2px'
  },
  unclickable: {
    backgroundColor: GeneralPalette.darkPurple,
    boxShadow: '0px 2px 0px 0px grey',
    borderRadius: '20px',
    color: GeneralPalette.underline,
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    width: '120px',
    lineHeight: '35px',
    paddingTop: '2px',
    cursor: 'default'
  },
  img: {
    height: 'auto',
    width: '100%',
    position: 'relative',
    top: '0%',
    left: '0%'
  }

};

function buttonText(status) {
  if (status.toUpperCase() === 'ONGOING') {
    return 'CONTINUE';
  }
  if (status.toUpperCase() === 'UNLOCKED') {
    return 'START';
  }
  if (status.toUpperCase() === 'COMPLETED') {
    return 'VIEW';
  }
  return '';
}


function startButton(status, goToProject, projectID, challengeData, challengeProgress, projectOverview) {
  if (status === 'ONGOING' || status === 'COMPLETED') {
    return (
      <FlatButton style={style.continue} onClick={() => goToProject(projectID, status, challengeData, challengeProgress)}>
        <TranslatedText text={buttonText(status)} />
      </FlatButton>
    );
  } else if (status === 'UNLOCKED') {
    return (
      <FlatButton style={style.continue} onClick={() => projectOverview(projectID)}>
        <TranslatedText text={buttonText(status)} />
      </FlatButton>
    );
  } else if (status === 'LOCKED') {
    return (
      <div style={style.unclickable}> <TranslatedText text={'LOCKED'} /> </div>
    );
  }
}

function overviewButton(status, projectOverview, projectID) {
  if (status === 'LOCKED' || status === 'UNLOCKED') {
    return null;
  }

  return (
    <FlatButton style={style.overview} onClick={() => projectOverview(projectID)}>
      <TranslatedText text={'OVERVIEW'} />
    </FlatButton>
  );
}

function statusIcon(status) {
  if (status === 'COMPLETED') {
    return (
      <DoneStatusIcon />
    );
  }
  if (status === 'LOCKED') {
    return (
      <span style={style.lockedicon}>
        <Locked />
      </span>
    );
  }
  if (status === 'ONGOING') {
    return (
      <span style={style.lockedicon}>
                &nbsp;
      </span>
    );
  }
  return null;
}

function lockedStyle(status) {
  if (status === 'LOCKED') {
    return (
            Object.assign({}, style.data, { color: GeneralPalette.buttonBackground })
    );
  }

  return (
            Object.assign({}, style.data, { color: GeneralPalette.darkPurple })
  );
}


const ProjectListItemComponent = ({ projectID, projectName, goToProject, userStatus, projectOverview, challengeData }) => {
  const thisProject = userStatus.find((p) => { return p.projectId === projectID; });

  return (
    <tr>
      <td style={thisProject.status === 'LOCKED' ? Object.assign({}, style.data, { color: GeneralPalette.buttonBackground }) : Object.assign({}, style.data, { color: GeneralPalette.darkPurple })} >
        {// projectID
              DevEnv ? projectID : ''
          }
      </td>

      <td style={thisProject.status === 'LOCKED' ? Object.assign({}, style.data, { color: GeneralPalette.buttonBackground }) : Object.assign({}, style.data, { color: GeneralPalette.darkPurple })}>
        {projectName}
      </td>
      <td style={style.data}>
        {overviewButton(thisProject.status, projectOverview, projectID)}
      </td>
      <td style={style.data}>
        {startButton(thisProject.status, goToProject, projectID, challengeData, thisProject.progress, projectOverview)}
      </td>
      <td style={style.data}>
        {statusIcon(thisProject.status)}
      </td>

    </tr>);
};

export default ProjectListItemComponent;
