import { connect } from 'react-redux';
import FreemodeHeader from 'components/codepad/FreemodeHeader';
import { saveCurrentRobot } from 'legacy_actions/FreeModeActions';

const mapStateToProps = (state) => {
  const proState = { ...state.projectMode.project, ...state.projectMode.challenge };

  let challengeType = 'undefined';

  const currentChallenge = proState.currentChallengeSet[proState.currentChallengeSelected];

  if (currentChallenge !== undefined) {
    challengeType = currentChallenge.type;
  }

  return {
    // freemode: state.shared.app.rootLocation === 2,
    // uploading: state.shared.codepad.roboCoreStatus.uploading,
    // checkingAnswers: state.shared.codepad.checkingAnswers,
    // showToolbox: state.shared.codepad.showToolbox,
    // challengeType,
    robot: state.freeMode.editingRobot,
    title: state.freeMode.editingRobot.item.name,
    dirty: state.shared.codepad.dirty
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onRobotSaveClick: () => {
      dispatch(saveCurrentRobot());
    },
  };
};
const FreeModeHeader = connect(
    mapStateToProps,
    mapDispatchToProps,
)(FreemodeHeader);

export default FreeModeHeader;
