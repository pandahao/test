import React from 'react';
import { connect } from 'react-redux';
import RobotEditor from 'components/freemode/RobotEditor';
import RobotsMaster from 'components/freemode/RobotsMaster';
import { hideToolbox } from 'legacy_actions/CodepadActions';
import { changeFreeModeView, loadRobot, deleteRobot } from 'legacy_actions/FreeModeActions';

import { deviceSetup, layoutSetup } from 'legacy_actions/RobotActions';

const mapStateToProps = (state) => {
  return {
    view: state.freeMode.view,
    docView: state.freeMode.docView,
    editingRobot: state.freeMode.editingRobot,
    items: state.freeMode.robots.items,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    setup: () => {
      dispatch(deviceSetup());
      dispatch(layoutSetup());
    },
    closeToolbox: () => {
      dispatch(hideToolbox());
    },
    onViewChange: (newView) => {
      dispatch(changeFreeModeView(newView));
    },
    onRobotLoad: (robot) => {
      dispatch(loadRobot(robot));
    },
    onRobotDelete: (robot) => {
      dispatch(deleteRobot(robot));
    },
  };
};

class FreemodeContainerComponent extends React.Component {
  // componentDidMount(){
  //   this.props.setup();
  // }
  render() {
    const {
      setup,
      view,
      docView,
      editingRobot,
      items,
      onViewChange,
      onRobotDelete,
      onRobotLoad,
      closeToolbox
    } = this.props;

    return (
      <div onClick={closeToolbox}>
        {view === 'editor' ?
          <RobotEditor
            setup={setup}
            docView={docView}
            editingRobot={editingRobot}
          /> :
          <RobotsMaster
            view={view}
            items={items}
            onViewChange={onViewChange}
            onRobotDelete={onRobotDelete}
            onRobotLoad={onRobotLoad}
          />
              }
      </div>
    );
  }

}

const FreemodeContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(FreemodeContainerComponent);

export default FreemodeContainer;
