import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import TextInput from 'legacy_components/authentication/TextInput';
import TranslatedText from 'legacy_containers/TranslatedText';

const style = {
  root: {
    height: '20%',
    position: 'relative',
    top: '20%',
    width: '80%',
    left: '-11%',
  },
  center: {
    // paddingTop: '150px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    // marginTop:'30%',
    // marginRight:'80%',
  },
  content: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  label: {
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    fontSize: '30px',
    color: GeneralPalette.darkPurple,
    textAlign: 'left',
    marginBottom: '10px',
  },
  fields: {
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column'
  },
  button: {
    // backgroundColor: GeneralPalette.brightPurple,
    backgroundColor: 'gray',
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    paddingTop: '2px',
    width: '200px',
    // marginTop: '40px',
    // textAlign:'left',
    marginLeft: '-50px',
    cursor: 'pointer',
  },
  cancel: {
    backgroundColor: '#fff',
    border: '1px solid #ccc',
    color: GeneralPalette.brightPurple,
    width: '200px',
    marginTop: '20px',
    borderRadius: '55px',
    marginLeft: '-50px',
  },
  errorMessage: {
    fontFamily: 'sfns',
    color: ToolbeltPalette.compileError,
    fontWeight: 100,
    fontSize: '12px',
    marginTop: '10px',
    height: '20px'
  },
  help: {
    color: 'gray',
    marginTop: '20px',
    marginLeft: '40px'
  }
};

const PasswordComponent = ({ username, userChange, errorMessage, cancel, requestPassword }) => {
  return (
    <div style={style.root}>
      <div style={style.center}>
        <div style={style.content}>
          {/* <img style={style.image} src="static/media/logo_orig.png" /> */}
          <div style={style.fields}>
            <div style={style.label}><TranslatedText text={'Forgot Password'} />?</div>
            <TextInput text={<TranslatedText text={'Email'} />} onChange={userChange} defaultValue={username} />
            <div style={style.errorMessage}> {errorMessage} </div>
          </div>
        </div>
        <FlatButton disabled style={style.button} onClick={() => requestPassword(username)}>
          <TranslatedText text={'RETRIEVE PASSWORD'} />
        </FlatButton>
        <FlatButton style={Object.assign({}, style.button, style.cancel)} onClick={cancel}>
          <TranslatedText text={'CANCEL'} />
        </FlatButton>
        <div className="help-block" style={style.help}>
                    重要提示：若您重置密码登录失败，请参照如下方案处理： <br />
          <ul style={{ paddingLeft: '20px', listStyle: 'none' }}>
            <li>1. 请登录您的注册邮箱查看收取新的登录密码；</li>
            <li>2. 请用您的注册邮箱地址发送邮件至 castlerock@roboterra.com 获取新密码；</li>
            <li>3. 如果上述操作失败，请直接联系售后服务人员。</li>
          </ul>
                    感谢您对萝卜的支持。
                  </div>
      </div>
    </div>
  );
};

import { connect } from 'react-redux';

import { sendResetRequest, setAuthLocation, userChange, getPassword, LOGIN, resetError } from 'legacy_actions/AuthActions';

const mapStateToProps = (state, ownProps) => {
  return {
    username: state.auth.username,
    errorMessage: state.auth.loggedIn.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    cancel: () => {
      dispatch(setAuthLocation(LOGIN));
      dispatch(resetError());
    },
    userChange: (event) => {
      dispatch(userChange(event.target.value));
    },
    requestPassword: (email) => {
      dispatch(sendResetRequest(email));
    }
  };
};

const Password = connect(
    mapStateToProps,
    mapDispatchToProps,
)(PasswordComponent);

export default Password;


/*
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import CircularProgress from 'material-ui/CircularProgress';
const style = {
  right:{
    display:'flex',
    width:'80%',
    height:'800px',
    flexDirection:'row',
    backgroundColor:'#fff'
  },
  center: {
    width: '100%',
    transform: 'translateY(20%)',
    display: 'flex',
    flexDirection: 'column',
    marginLeft:'200px',
    marginTop:'130px'
  },
  p1:{
   color:'#686e84',
   fontSize:'48px',
   fontFamily:'.SF NS display'
  },
  input:{
     display:'flex',
     flexDirection:'row',
     marginLeft:'25px',
     marginTop:20,
  },
  foot:{
   display:'flex',
   flexDirection:'column',
   marginTop:15,
   alignItems:'center',
   marginLeft:'-480px'
  },
button: {
       backgroundColor: GeneralPalette.brightPurple,
       boxShadow: `0px 0px 0px 0px${GeneralPalette.purpleShadow}`,
       border: 'none',
       borderRadius: '20px',
       color: 'white',
       fontSize: '15px',
       fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
       textAlign: 'center',
       lineHeight: '35px',
       paddingTop: '2px',
       width: '200px',
       marginTop: '40px',
       cursor: 'pointer'
},
cancel: {
       backgroundColor: GeneralPalette.menuBG,
       boxShadow: `0px 0px 0px 0px${GeneralPalette.whiteShadow}`,
       color: GeneralPalette.darkPurple,
       width: '100px',
       marginTop: '20px'
   },
};

class PassWord extends Component{
    render(){
      return(
        <div style={style.right}>
          <div style={style.center}>
            <div>
                 <span style={style.p1} > <TranslatedText text={'Forgot Password?'} />
                 </span>
            </div>
            <div style={style.input}>
              <TextInput type={'Email'} text={<TranslatedText text={'Email Address'} />}/>
           </div>
           <div style={style.foot}>
             <FlatButton style={Object.assign({}, style.button, style.cancel)}>
               <TranslatedText text={'RETRIEVE PASSWORD'} />
             </FlatButton>
             <FlatButton style={Object.assign({}, style.button, style.cancel)}>
               <TranslatedText text={'CANCEL'} />
             </FlatButton>
           </div>
           </div>
        </div>
      );
    }
}
*/
