import {connect} from 'react-redux';


mapStateToProps = {state} {
  projectsCompleted : state.projectMode.project.currentProjectID ,
  lastProject : state.projectMode.project.currentProjectID
}

const WelcomeNewContainer = connect({projectsCompleted, lastProject})(WelcomeNew);

export default WelcomeNewContainer;
