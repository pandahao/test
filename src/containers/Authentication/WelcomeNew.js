import React,{Component} from 'react';
import Login from 'components/Login';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import CircularProgress from 'material-ui/CircularProgress';
import TranslatedText from 'legacy_containers/TranslatedText';
import { LangIcon } from 'legacy_assets/index';
const style = {
  root:{
    display: 'flex',
    width:'100%',
    height:'1280px',
    backgroundColor:'#50c3ff',
    flexDirection: 'row',
  },
  left:{
    display:'flex',
    width:'25%',
    height:'1280px',
    backgroundColor:'#50c3ff'
  },
  rom:{
    margin: '50px auto 0',
    width: '80%',
    height: '0',
    paddingTop: '80%',
    borderRadius: '100%',
    backgroundColor:'#fff',
    position:'relative',
    top:'19%',
    left:'49%',
  },
  img:{
    width:'250%',
    height:'250%',
    position:'absolute',
    top:'-70%',
    left:'-70%',
  },
  right:{
    display:'flex',
    width:'80%',
    height:'1280px',
    flexDirection:'row',
    backgroundColor:'#fff',
    // marginLeft:'3%'
  },
  center: {
    width: '100%',
    transform: 'translateY(20%)',
    display: 'flex',
    flexDirection: 'column',
    marginLeft:'20%',
    marginTop:'7%'
  },
  p1:{
   color:'#686E84',
   fontSize:'48px',
   fontFamily:'.SF NS display'
  },
  input:{
   display:'flex',
  flexDirection:'column',
  marginLeft:'8px',
   color:'#c2c9df',
   fontSize:14,
   fontFamily:'.Helvetica Neue Desklr',
   line:17
  },
  foot:{
   display:'flex',
   flexDirection:'row',
   marginTop:15
  },
  button: {
    backgroundColor: GeneralPalette.brightPurple,
    boxShadow: `0px 0px 0px 0px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static','Arial','Microsoft YaHei','黑体','宋体','sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    paddingTop: '2px',
    width: '150px',
    cursor: 'pointer'
  },
  lang:{
    position:'fixed',
    left:'93%',
    bottom:'82%',
  },
  tra:{
    width:'197px',
    height:'197px',
    backgroundColor:'#50c3ff',
    borderRadius:'100px',
    lineHeight:'100px',
  },
  smalltext: {
    fontSize: '14px',
    fontFamily: 'sfns',
    color: '#fff',
    marginLeft:38,
    marginTop:-75,
  },
  corner: {
    width:'100%',
    marginLeft:'20%',
    paddingTop:'70px',
    // marginTop:'50px'
  },
}
const WelcomeNew = ({projectsCompleted, lastProject}) => {
   render(){
     return(
       <div style={style.root}>
            <div style={style.left}>
              <div style={style.rom}>
               <img style={style.img} src="./static/media/logo.svg"/>
              </div>
           </div>
       <div style={style.right}>
         <div style={style.center}>
           <div>
                <span style={style.p1} > <TranslatedText text={'Welcome'} />:
                </span>
           </div>
           <div style={style.input}>
             <p><TranslatedText text={'Projects Completed'} />: {projectsCompleted}</p>
             <p><TranslatedText text={'Last Project'} />: {lastProject}</p>
          </div>
          <div style={style.foot}>
                <FlatButton style={style.button}>
                  <TranslatedText text={'TUTORIAL'} />
                </FlatButton>
          </div>
          </div>
          <div style={style.lang}>
          <div style={style.tra}>
            {
              // <div style={style.corner}>
              //   <LangIcon />
              // </div>
              // <div style={style.smalltext}><TranslatedText text={'ENGLISH'} /></div>
            }

          </div>
       </div>
       </div>
     </div>
     );
   }
}
export default WelcomeNew;
