import { connect } from 'react-redux';

import Login from 'components/Login';
import { checkLogin, setAuthLocation, userChange, passChange, resetError, updateUsername, changeLanguage } from 'legacy_actions/AuthActions';

const mapStateToProps = (state, ownProps) => {
  return {
    authLocation: state.auth.authLocation,
    errorMessage: state.auth.loggedIn.error,
    username: state.auth.username
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    checkLogin: () => {
      dispatch(checkLogin());
    },
        // not using toggle in order to preserve having more languages in the future
    setAuthLocation: (loc) => {
      dispatch(setAuthLocation(loc));
      dispatch(resetError());
    },
    userChange: (ev) => {
      dispatch(userChange(ev.target.value));
      dispatch(updateUsername(ev.target.value));
    },
    passChange: (ev) => {
      dispatch(passChange(ev.target.value));
    },
    changeLanguage: () => {
      dispatch(changeLanguage('CHINESE'));
    }
        // login : (event,user, pass) => {
        //     dispatch(beginLogin());
        //     dispatch(checkCredentials(user, pass));
        // }
  };
};

const LoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Login);

export default LoginContainer;
