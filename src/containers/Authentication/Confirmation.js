import React, { Component } from 'react';
import LoginComponent from './LoginComponent';
import FlatButton from 'material-ui/FlatButton';
import { GeneralPalette, ToolbeltPalette } from 'legacy_styles/Colors';
import CircularProgress from 'material-ui/CircularProgress';
const style = {
  right: {
    display: 'flex',
    width: '80%',
    height: '800px',
    flexDirection: 'row',
    backgroundColor: '#fff'
  },
  center: {
    width: '100%',
    transform: 'translateY(20%)',
    display: 'flex',
    flexDirection: 'column',
    marginLeft: '200px',
    marginTop: '150px'
  },
  input: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: '25px',
    marginTop: 20,
  },
  corner: {
    width: '155px',
    backgroundColor: 'fff',
    marginTop: 70,
    marginLeft: 40
  },

  foot: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: 15,
    alignItems: 'center',
    marginLeft: '-480px'
  },
  button: {
    backgroundColor: GeneralPalette.brightPurple,
    boxShadow: `0px 0px 0px 0px${GeneralPalette.purpleShadow}`,
    border: 'none',
    borderRadius: '20px',
    color: 'white',
    fontSize: '15px',
    fontFamily: ['static', 'Arial', 'Microsoft YaHei', '黑体', '宋体', 'sans-serif'],
    textAlign: 'center',
    lineHeight: '35px',
    paddingTop: '2px',
    width: '200px',
    marginTop: '40px',
    cursor: 'pointer'
  },
  cancel: {
    backgroundColor: GeneralPalette.menuBG,
    boxShadow: `0px 0px 0px 0px${GeneralPalette.whiteShadow}`,
    color: GeneralPalette.darkPurple,
    width: '100px',
    marginTop: '20px'
  },
};

class Confirmation extends Component {
  render() {
    return (
      <div style={style.right}>
        <div style={style.center}>
          <div style={style.input}>
            <TextInput type={'SendCode'} text={<TranslatedText text={'Confirmation Code'} />} />
          </div>
          <div style={style.foot}>
            <FlatButton style={Object.assign({}, style.button, style.cancel)}>
              <TranslatedText text={'CONFIRM'} />
            </FlatButton>
            <FlatButton style={Object.assign({}, style.button, style.cancel)}>
              <TranslatedText text={'RESEND'} />
            </FlatButton>
          </div>
        </div>
      </div>
    );
  }
}
export default Confirmation;
