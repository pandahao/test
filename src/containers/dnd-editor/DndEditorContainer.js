import React, { Component } from 'react';
import { connect } from 'react-redux';
import DndEditor from '../../dnd-editor';
import { setCode } from '../../dnd-editor/actions';
import { sketchChange, markDirty } from 'legacy_actions/CodepadActions';

function mapStateToProps({ freeMode, projectMode, shared }) {
  const { currentChallengeSet, currentChallengeSelected } = projectMode.challenge;
  const selectedChallenge = currentChallengeSet[currentChallengeSelected];
  const { sketch } = shared.codepad;
  let initCode = '';

  if (shared.app.erraMode && selectedChallenge) {
    initCode = selectedChallenge.content.start_template;
  } else if (typeof sketch === 'string' && !sketch.startsWith('#')) {
    initCode = sketch;
  }

  return {
    initCode
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    makeChange: (val) => {
      dispatch(sketchChange(val));
      dispatch(markDirty());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DndEditor);
