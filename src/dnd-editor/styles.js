export const colors = {
  roboCore: '#9b9b9b',
  text: '#686E84',
  func: '#f5d76e',
  led: '#87d37c',
  motor: '#ed547e',
  button: '#59abe3',
  ir: '#ef6d55',
  tape: '#7f508c',
  menuBackground: '#24262C',
  menuHighlight: '#3d404c'
};

function flexLayout() {
  const flex = {
    display: 'flex'
  };

  const center = {
    alignItems: 'center'
  };

  const column = {
    flexDirection: 'column'
  };

  return {
    flex,
    center: {
      ...flex,
      ...center
    },
    column: {
      ...flex,
      ...column
    },
    columnCenter: {
      ...flex,
      ...center,
      ...column
    }
  };
}

export const flex = flexLayout();
