import { connect } from 'react-redux';

import { clearDropZone } from '../actions';

import DragSource from './drag-source';

export default connect(
  null,
  { clearDropZone }
)(DragSource);
