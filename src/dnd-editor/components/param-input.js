import React, {Component, PropTypes} from 'react';
import {colors, flex} from '../styles';

const {arrayOf, bool, func, number, object} = PropTypes;

const styles = {
  paramBox: {
    display: 'flex',
    height: 36
  },
  input: {
    border: 'none',
    borderRadius: 6,
    boxShadow: '0 1px 6px 1px rgba(0, 0, 0, 0.5) inset',
    color: colors.text,
    fontSize: 16,
    textAlign: 'center',
    width: 50
  },
  operators: {
    color: 'white',
    ...flex.columnCenter,
    fontSize: 20,
    justifyContent: 'space-around'
  },
  operator: {
    cursor: 'pointer'
  },
  textMargin: {
    marginTop : 0.4+'em',
    marginLeft : 0.5+'em'
  }
};

export default class ParamInput extends Component {
  static propTypes = {
    range: arrayOf(number),
    style: object,
    value: number,
    zeroAsInfinite: bool,
    onChange: func.isRequired
  };

  static defaultProps = {
    infiniteSign: '∞',
    style: {},
    value: 0,
    zeroAsInfinite: false
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.value || 0
    };
  }

  componentWillReceiveProps({value = 0}) {
    this.setState({value});
  }

  updateValue(value) {
    const {range, onChange} = this.props;

    if (range) {
      value = Math.max(value, range[0]);
      value = Math.min(value, range[1]);
    }

    onChange(value);
  }

  onBlur({target}) {
    let value;

    if (this.props.valueType === 'timerBegin') {
      value = parseInt(target.value * 10) / 10;
    } else {
      value = parseInt(target.value, 10);
    }

    if (isNaN(value)) {
      value = 0;
    }

    this.updateValue(value);
  }

  render() {
    const {props, state: {value}} = this;
    const {infiniteSign, style, zeroAsInfinite} = props;
    const valueDisplay = (zeroAsInfinite && value === 0) ? infiniteSign : value;
    if (this.props.valueType != "timerBegin"){
      return (
        <div style={{...styles.paramBox, ...style}} onDoubleClick={e => e.stopPropagation()}>
          <input style={styles.input} value={valueDisplay} onBlur={e => this.onBlur(e)}
            onChange={e => this.setState({value: e.target.value})} />
          <div style={styles.operators}>
            <i className="icon-stepper-increase" style={styles.operator}
              onClick={() => this.updateValue(value + 1)} />
            <i className="icon-stepper-decrease" style={styles.operator}
              onClick={() => this.updateValue(value - 1)} />
          </div>
        </div>
      );
    }
    else{
      return (
        <div style={{...styles.paramBox, ...style}} onDoubleClick={e => e.stopPropagation()}>

          <input style={styles.input} value={valueDisplay} onBlur={e => this.onBlur(e)}
            onChange={e => this.setState({value: e.target.value})} />
          <p style={styles.textMargin}>秒</p>
        </div>

      );
    }
  }
}
