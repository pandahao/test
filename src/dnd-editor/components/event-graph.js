import React, { Component } from 'react';

const yellow = '#FDD835';

const styles = {
  eventGraph: {
    display: 'flex',
    flexDirection: 'column'
  },
  eventStart: {
    backgroundColor: yellow,
    display: 'flex',
    borderRadius: 6,
    borderBottomLeftRadius: 0
  },
  eventRow: {
    display: 'flex',
    alignItems: 'center'
  },
  eventHeader: {
    fontSize: 20,
    color: 'white',
    padding: 12
  },
  eventBody: {
    display: 'flex',
    minHeight: 60
  },
  eventBodyBorder: {
    width: 16,
    backgroundColor: yellow,
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'stretch',
    marginRight: -6
  },
  eventBodyBorderCover: {
    width: 12,
    position: 'relative',
    left: 12,
    backgroundColor: 'transparent',
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
    boxShadow: '-6px 0px 0px 6px rgb(253, 216, 53)'
  },
  commandsContainer: {
    borderRadius: 6,
    minWidth: 60,
    minHeight: 60
  },
  eventEnd: {
    backgroundColor: yellow,
    height: 16,
    borderRadius: 6,
    borderTopLeftRadius: 0
  }
};

export default class EventGraph extends Component {
  render() {
    return (
      <div style={styles.eventGraph}>
        <div style={styles.eventStart}>
          <div style={styles.eventRow}>
            <span style={styles.eventHeader}>当</span>
          </div>
        </div>

        <div style={styles.eventBody}>
          <div style={styles.eventBodyBorder}>
            <div style={styles.eventBodyBorderCover} />
          </div>
          <div style={styles.commandsContainer} />
        </div>

        <div style={styles.eventEnd} />
      </div>
    );
  }
}
