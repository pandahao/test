import React, {Component, PropTypes} from 'react';

const {func, object, string} = PropTypes;

const styles = {
  style: {
    lineHeight: 0
  }
};

export default class DragSource extends Component {
  static propTypes = {
    className: string,
    clearDropZone: func.isRequired,
    dragType: string.isRequired,
    style: object
  };

  static defaultProps = {
    className: '',
  };

  onDragStart({dataTransfer}) {
    const {dragType, value} = this.props;

    dataTransfer.dragEffect = 'copy';
    // the data value is only available in drop event
    // but data type (1st param) is available in drag over which validates the drop
    dataTransfer.setData(dragType, dragType);
    dataTransfer.setData('value', value);
  }

  render() {
    const {props} = this;
    const {children, className, clearDropZone, style} = props;

    return (
      <div draggable {...{className}} style={{...styles.style, ...style}}
        onDragStart={e => this.onDragStart(e)}
        onDragEnd={clearDropZone}>
        {children}
      </div>
    );
  }
}
