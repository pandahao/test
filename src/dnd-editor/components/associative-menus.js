import React, {Component, PropTypes} from 'react';

import Dropdown from './dropdown';

import {flex} from '../styles';

import {getAssociatedValues} from '../utils/menus';

const {any, array, bool, func, object, string} = PropTypes;

const styles = {
  associativeMenus: {
    ...flex.center
  },
  primaryDropdown: {
    marginRight: 6
  }
};

export default class AssociativeMenus extends Component {
  static propTypes = {
    primaryValue: any,
    primaryValues: array,
    secondaryNoLabel: bool,
    secondaryValue: any,
    secondaryValues: array,
    style: object,
    type: string.isRequired,
    onPrimaryChange: func,
    onSecondaryChange: func
  };

  static defaultProps = {
    primaryValue: null,
    primaryValues: [],
    secondaryNoLabel: false,
    secondaryValue: null,
    secondaryValues: null,
    style: {},
    onPrimaryChange: () => {},
    onSecondaryChange: () => {}
  };

  render() {
    const {onPrimaryChange, onSecondaryChange, primaryValue, primaryValues = [],
      secondaryNoLabel, secondaryValue, secondaryValues, style, type} = this.props;

    return (
      <div style={{...styles.associativeMenus, ...style}}>
        <Dropdown values={primaryValues} value={primaryValue}
          style={styles.primaryDropdown} onChange={onPrimaryChange} />
        <Dropdown noLabel={secondaryNoLabel} value={secondaryValue}
          values={secondaryValues || getAssociatedValues(primaryValue, type)}
          valueType={primaryValue}
          onChange={onSecondaryChange} />
      </div>
    );
  }
}
