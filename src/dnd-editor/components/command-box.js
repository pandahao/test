import React, {Component, PropTypes} from 'react';

import AssociativeMenus from './associative-menus';
import {CommandBg} from './shapes';
import Dropdown from './dropdown';
import DropItemContainer from './drop-item-container';
import IRParamsBox from './ir-params-box';
import ParamInput from './param-input';
import TerraCoreBox from './terra-core-box';

import {findAvailableComponents} from '../utils/code-selector';
import {getDisplayConfig} from '../utils/display-configs';
import {instances, irBeaconOptions, parameterizedCommands, timerParams} from '../utils/menus';
import {isZeroAsInfinite, valueRanges} from '../utils/values';

import {flex} from '../styles';

const {array, func, object} = PropTypes;

const styles = {
  commandBox(index) {
    return {
      ...flex.center,
      height: 60,
      marginBottom: 3,
      marginLeft: 3,
      marginTop: index === 0 ? 3 : 0,
      position: 'relative'
    };
  },
  bg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'flex-start'
  },
  bgBody(color) {
    return {
      backgroundColor: color,
      borderBottomRightRadius: 8,
      borderTopRightRadius: 8,
      color: 'white',
      ...flex.center,
      flexGrow: 1,
      height: 60
    };
  },
  body: {
    ...flex.center,
    padding: 6,
    position: 'relative'
  },
  fixedBody: {
    color: 'white',
    ...flex.center
  }
};

export default class CommandBox extends Component {
  static propTypes = {
    code: object.isRequired,
    command: object.isRequired,
    indices: array.isRequired,
    updateCode: func.isRequired
  };

  updateCommand(data) {
    const {indices, updateCode} = this.props;

    updateCode({
      type: 'command',
      indices,
      data
    });
  }

  updateCommandSource(value) {
    this.updateCommand({commandSource: value, commandAction: '', commandParams: []});
  }

  updateCommandAction(value) {
    this.updateCommand({commandAction: value, commandParams: []});
  }

  renderBody(commandSources) {
    const {command} = this.props;
    const {commandAction, commandSource, commandParams = []} = command;

    if (commandSources.length) {
      return (
        <AssociativeMenus type="command" noLabel
          primaryValue={commandSource} primaryValues={commandSources}
          secondaryNoLabel secondaryValue={commandAction}
          onPrimaryChange={value => this.updateCommandSource(value)}
          onSecondaryChange={value => this.updateCommandAction(value)} />
      );
    }

    const {icon} = getDisplayConfig(commandAction);

    return (
      <TerraCoreBox {...{icon}}>
        {commandAction === 'timerBegin' &&
          <ParamInput range={valueRanges[commandAction]} value={commandParams[0]} valueType={commandAction}
            onChange={
              value => this.updateCommand({commandParams: [value]})
            }
          />
        }
      </TerraCoreBox>
    );
  }

  renderParams() {
    const {command: {commandAction, commandParams = []}} = this.props;

    if (!parameterizedCommands.includes(commandAction)) {
      return null;
    }

    const style = {marginLeft: 6};
    const onChange = value => this.updateCommand({commandParams: [value]});

    if (commandAction === 'irEmit') {
      return (
        <IRParamsBox {...{commandParams, onChange, style}} />
      );
    }

    if (commandAction === 'irBeacon') {
      return (
        <Dropdown values={irBeaconOptions} value={commandParams[0]} {...{onChange, style}} />
      );
    }

    return (
      <ParamInput range={valueRanges[commandAction]} value={commandParams[0]}
        zeroAsInfinite={isZeroAsInfinite(commandAction)} {...{onChange, style}}
      />
    );
  }

  render() {
    const {code, command: {type}, indices} = this.props;
    const {color} = getDisplayConfig(type);
    const index = indices[indices.length - 1];
    const commandSources = findAvailableComponents(instances[type], code);

    return (
      <DropItemContainer {...{indices, type}}>
        <div style={styles.commandBox(index)}>
          <div style={styles.bg}>
            <CommandBg color={color} />
            <div style={styles.bgBody(color)} />
          </div>

          <div style={styles.body}>
            {this.renderBody(commandSources)}
            {this.renderParams()}
          </div>
        </div>
      </DropItemContainer>
    );
  }
}
