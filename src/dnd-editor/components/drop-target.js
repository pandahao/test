import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';

const {array, func, object, string} = PropTypes;

const styles = {
  dropZone: {
    height: 60,
    borderLeft: '4px solid #42A5F5',
    marginTop: 6,
    marginBottom: 6
  }
};

function DropZone() {
  return (
    <div style={styles.dropZone} />
  );
}

export default class DropTarget extends Component {
  static propTypes = {
    containerIndices: array,
    dnd: object.isRequired,
    dropType: string.isRequired,
    style: object,
    insertDropZone: func.isRequired,
    onDrop: func.isRequired
  };

  static defaultProps = {
    containerIndices: [],
    style: {}
  };

  renderDropZone(rows, dropType) {
    const {containerIndices, dnd} = this.props;
    const {draggingItemIndex, dropZoneIndices, dropZoneType} = dnd;

    if (dropZoneType === dropType && containerIndices.every(
      (value, i) => value === dropZoneIndices[i]
    )) {
      const dropZoneIndex = dropZoneIndices[dropZoneIndices.length - 1];
      if (draggingItemIndex === -1 ||
        dropZoneIndex < draggingItemIndex || dropZoneIndex > draggingItemIndex + 1) {
        rows.splice(dropZoneIndex, 0, <DropZone key="dropZone" />);
      }
    }

    return rows;
  }

  onDragOver(e) {
    const {previousX, previousY, props} = this;
    const {containerIndices, dropType, insertDropZone} = props;
    const {clientX, clientY, dataTransfer} = e;

    if (dataTransfer.types.includes(dropType)) {
      e.preventDefault();

      const el = ReactDOM.findDOMNode(this);
      const dndItems = Array.from(el.childNodes).filter(c => c.classList.contains('dnd-item'));

      let index = dndItems.length;
      for (let i = 0; i < dndItems.length; i++) {
        const {bottom, top} = dndItems[i].getBoundingClientRect();
        const center = (top + bottom) / 2;

        if (e.clientY < center) {
          index = i;
          break;
        }
      }

      if (Math.abs(clientX - previousX) < 5 && Math.abs(clientY - previousY) < 5) {
        // event throttling
        return;
      }

      this.previousX = clientX;
      this.previousY = clientY;

      insertDropZone({
        type: dropType,
        indices: [...containerIndices, index]
      });
    }

    e.stopPropagation();
  }

  render() {
    const {children, dropType, onDrop, style} = this.props;
    let rows = React.Children.toArray(children);
    rows = this.renderDropZone(rows, dropType);

    return (
      <div {...{style, onDrop}} onDragOver={e => this.onDragOver(e)}>
        {rows}
      </div>
    );
  }
}
