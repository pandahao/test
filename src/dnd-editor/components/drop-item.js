import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import {getDragType} from '../utils/display-configs';

const {array, func, object, string} = PropTypes;

export default class DropItem extends Component {
  static propTypes = {
    indices: array,
    removeCode: func,
    copyCode: func,
    setDraggingItemIndex: func,
    statement: object,
    type: string
  };

  onDragStart(e) {
    const {dataTransfer} = e;
    const {indices, setDraggingItemIndex, statement, type} = this.props;
    const dragType = getDragType(type);

    dataTransfer.dragEffect = 'copy';
    // the data value is only available in drop event
    // but data type (1st param) is available in drag over which validates the drop
    dataTransfer.setData(dragType, dragType);
    dataTransfer.setData('value', type);
    setDraggingItemIndex(indices[indices.length - 1]);

    e.stopPropagation();
  }

  removeCode(e) {
    const {indices, removeCode, type} = this.props;

    removeCode({
      type, indices
    });
    if(e) {
      e.stopPropagation();
    }
  }
  copyCode() {
    const {indices, copyCode, type} = this.props;
    copyCode({
      type, indices
    });
  }
  pasteCode() {
    const { pasteCode } = this.props;
    pasteCode();
  }
  componentDidMount() {
    const {remote} = require('electron')
    const {Menu, MenuItem} = remote
    let dom = ReactDOM.findDOMNode(this.refs.drop_item);
    let that = this;
    const menu = new Menu()
    menu.append(new MenuItem({label: '复制', click() { that.copyCode() }}))
    menu.append(new MenuItem({label: '粘贴', click() { that.pasteCode() }}))
    menu.append(new MenuItem({label: '删除', click() { that.removeCode() }}))

    dom.addEventListener('contextmenu', (e) => {
      e.preventDefault();
      e.stopPropagation();
      menu.popup(remote.getCurrentWindow());
    }, false)
  }

  render() {
    const {children, clearDropZone, type} = this.props;

    return (
      <ReactCSSTransitionGroup className="dnd-item"
        transitionName="dnd-item"
        component="div"
        transitionAppear
        transitionAppearTimeout={300}
        transitionLeaveTimeout={3000}
        transitionEnter={false}>
        <div ref="drop_item" draggable onDoubleClick={e => this.removeCode(e)}
          onDragStart={e => this.onDragStart(e)} onDragEnd={clearDropZone}>
          {children}
        </div>
      </ReactCSSTransitionGroup>
    );
  }
}
