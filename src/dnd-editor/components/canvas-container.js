import { connect } from 'react-redux';

import { insertCode, updateCode, pasteCode } from '../actions';

import Canvas from './canvas';

export default connect(
  ({ dndEditor: { dnd } }) => ({ dnd }),
  { insertCode, updateCode, pasteCode }
)(Canvas);
