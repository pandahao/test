import React, {Component, PropTypes} from 'react';
import AssociativeMenus from './associative-menus';
import CommandBox from './command-box';
import DropItemContainer from './drop-item-container';
import DropTargetContainer from './drop-target-container';
import {EventBottomBg, EventTitleBg} from './shapes';
import ParamInput from './param-input';

import {findAvailableComponents} from '../utils/code-selector';
import {eventSources, parameterizedEvents} from '../utils/menus';
import {isZeroAsInfinite, valueRanges} from '../utils/values';

import {colors, flex} from '../styles';

const {func, number, object} = PropTypes;

const styles = {
  eventBox: {
    ...flex.column,
    alignItems: 'flex-start'
  },
  title: {
    ...flex.center,
    position: 'relative'
  },
  titleBg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'flex-start'
  },
  titleBodyBg: {
    backgroundColor: colors.func,
    borderBottomRightRadius: 8,
    borderTopRightRadius: 8,
    height: 60,
    flexGrow: 1
  },
  titleLabel: {
    color: 'white',
    fontSize: 24,
    padding: 12,
    WebKitUserSelect: 'none'
  },
  titleBody: {
    height: 60,
    ...flex.center,
    padding: 12,
    position: 'relative'
  },
  body: {
    display: 'flex',
    minHeight: 60,
    position: 'relative',
    width: '100%'
  },
  bodyBorder: {
    backgroundColor: colors.func,
    width: 16
  },
  commandContainer: {
    flexGrow: 1,
    ...flex.column,
    alignItems: 'flex-start'
  },
  bottom: {
    lineHeight: 0,
    marginBottom: -15,
    position: 'relative',
    top: -11
  },
  paramInput: {
    marginLeft: 6
  }
};

export default class EventBox extends Component {
  static propTypes = {
    code: object.isRequired,
    dnd: object.isRequired,
    event: object.isRequired,
    index: number.isRequired,
    insertCode: func.isRequired,
    updateCode: func.isRequired
  };

  onDrop(e) {
    const {dnd, insertCode} = this.props;

    insertCode({
      indices: dnd.dropZoneIndices,
      oldIndex: dnd.draggingItemIndex,
      type: e.dataTransfer.getData('value')
    });

    e.stopPropagation();
  }

  updateEvent(data) {
    const {index, updateCode} = this.props;

    if (!data.eventData) {
      data.eventData = [];
    }

    updateCode({
      type: 'event',
      indices: [index],
      data
    });
  }

  renderParams() {
    const {event: {eventType, eventData = []}} = this.props;

    if (!parameterizedEvents.includes(eventType)) {
      return null;
    }

    return (
      <ParamInput range={valueRanges[eventType]} style={styles.paramInput} value={eventData[0]}
        zeroAsInfinite={isZeroAsInfinite(eventType)}
        infiniteSign={eventType === 'buttonPress'||'buttonRelease'||'tapeEnter'||'tapeLeave'||'timerUp' ? '*' : undefined}
        onChange={
          value => this.updateEvent({eventData: [value]})
        }
      />
    );
  }

  renderCommands() {
    const {code, event, index, updateCode} = this.props;
    const {commands = []} = event;

    return commands.filter((e) => {
      return e != null
    }).map((command, i) =>
      <CommandBox key={command.uuid} indices={[index, i]} {...{code, command, updateCode}} />
    );
  }

  render() {
    const {code, event: {eventSource, eventType}, index} = this.props;
    const sources = findAvailableComponents(eventSources, code);

    return (
      <DropItemContainer type="event" indices={[index]}>
        <div style={styles.eventBox}>
          <div style={styles.title}>
            <div style={styles.titleBg}>
              <EventTitleBg color={colors.func} />
              <div style={styles.titleBodyBg} />
            </div>

            <div style={styles.titleBody}>
              <span style={styles.titleLabel}>当</span>
              <AssociativeMenus type="event" primaryValue={eventSource} primaryValues={sources}
                secondaryNoLabel secondaryValue={eventType}
                onPrimaryChange={value => this.updateEvent({eventSource: value, eventType: ''})}
                onSecondaryChange={value => this.updateEvent({eventType: value})} />

              {this.renderParams()}
            </div>
          </div>

          <div style={styles.body}>
            <div style={styles.bodyBorder} />
            <DropTargetContainer containerIndices={[index]} dropType="command"
              style={styles.commandContainer} onDrop={e => this.onDrop(e)}>
              {this.renderCommands()}
            </DropTargetContainer>
          </div>

          <div style={styles.bottom}>
            <EventBottomBg color={colors.func} />
          </div>
        </div>
      </DropItemContainer>
    );
  }
}
