import React from 'react';
import { colors } from '../styles';
import DragSource from './drag-source-container';

import { getDragType, shapes } from '../utils/display-configs';

const styles = {
  menuItem: {
    alignItems: 'center',
    backgroundColor: colors.menuBackground,
    color: '#B0BEC5',
    display: 'flex',
    height: 72,
    padding: 24,
    minWidth: 120
  },
  menuIconContainer: {
    cursor: 'pointer',
    display: 'flex',
    alignItems: 'center'
  },
  menuItemLabel: {
    flexGrow: 1,
    textAlign: 'center',
    padding: 6,
    flexShrink: 0,
    whiteSpace: 'nowrap'
  }
};

export default function DrawerMenuItem({ label, value }) {
  const Shape = shapes[value];

  return (
    <div className="drawer-menu-item" style={styles.menuItem}>
      <DragSource
        dragType={getDragType(value)} style={styles.menuIconContainer}
        value={value}
      >
        <Shape />
        <span style={styles.menuItemLabel}>{label}</span>
      </DragSource>
    </div>
  );
}
