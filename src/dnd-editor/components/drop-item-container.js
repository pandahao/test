import { connect } from 'react-redux';

import { clearDropZone, removeCode, copyCode, pasteCode, setDraggingItemIndex } from '../actions';

import DropItem from './drop-item';

export default connect(
  null,
  { clearDropZone, removeCode, copyCode, pasteCode, setDraggingItemIndex }
)(DropItem);
