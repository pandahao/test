import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';

import {getDisplayConfig} from '../utils/display-configs';

import {colors} from '../styles';

const {any, array, bool, func, object, string} = PropTypes;

const menuBackground = {
  backgroundColor: 'white',
  boxShadow: '0 2px 6px 0px rgba(0, 0, 0, 0.5)'
};

const styles = {
  dropdown(noLabel) {
    return {
      color: colors.text,
      cursor: 'pointer',
      position: 'relative',
      minWidth: noLabel ? 60 : 100
    };
  },
  body(isMenuOpen) {
    return {
      display: 'flex',
      alignItems: 'center',
      borderTopLeftRadius: 8,
      borderTopRightRadius: 8,
      borderBottomLeftRadius: isMenuOpen ? 0 : 8,
      borderBottomRightRadius: isMenuOpen ? 0 : 8,
      ...menuBackground,
      height: 36
    };
  },
  menu: {
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    maxHeight: '30vh',
    ...menuBackground,
    overflow: 'auto',
    position: 'absolute',
    width: '100%',
    zIndex: 10
  },
  menuItem: {
    alignItems: 'center',
    display: 'flex',
    height: 36,
    justifyContent: 'center',
    padding: 4
  },
  menuItemIcon: {
    flexShrink: 0,
    flexBasis: 0,
    marginRight: 6,
    fontSize: 24
  },
  menuItemLabel: {
    flexGrow: 1,
    fontSize: 14,
    fontWeight: 600,
    whiteSpace: 'nowrap'
  },
  dropDownIcon: {
    fontSize: 30,
    marginLeft: -12 // offset the padding of the glyph icon
  },
  selectedMenuItemContainer: {
    flexGrow: 1
  },
  defaultLabel: {
    paddingLeft: 12,
    paddingRight: 12,
    whiteSpace: 'nowrap'
  }
};

function MenuItem(props) {
  const {icon, noLabel, label, onClick} = props;

  return (
    <div className="dropdown-menu-item" style={styles.menuItem} title={label} onClick={onClick}>
      <i className={icon} style={styles.menuItemIcon} />
      {!noLabel && <span style={styles.menuItemLabel}>{label}</span>}
    </div>
  );
}

export default class Dropdown extends Component {
  static propTypes = {
    noLabel: bool,
    style: object,
    value: any,
    values: array,
    valueType: string,
    onChange: func
  };

  static defaultProps = {
    noLabel: false,
    style: {},
    value: null,
    values: [],
    onChange: () => {}
  };

  constructor(props) {
    super(props);
    this.state = {
      isMenuOpen: false
    };
  }

  onChange(value) {
    const {onChange} = this.props;

    if (onChange) {
      onChange(value);
    }
    this.setMenuVisible(false);
  }

  setMenuVisible(isMenuOpen) {
    if (this.props.values.length === 0) {
      return;
    }

    if (isMenuOpen) {
      ReactDOM.findDOMNode(this).focus();
    }
    this.setState({isMenuOpen});
  }

  renderMenuItems(values) {
    const {noLabel, valueType} = this.props;

    return values.map(value =>
      <MenuItem key={value} {...getDisplayConfig(value, valueType)} {...{noLabel}}
        onClick={() => this.onChange(value)} />
    );
  }

  render() {
    const {noLabel, style, value, values, valueType} = this.props;
    const {isMenuOpen} = this.state;

    return (
      <div className="dropdown" style={{...styles.dropdown(noLabel), ...style}}
        tabIndex="0" onBlur={() => this.setMenuVisible(false)}>
        <div style={styles.body(isMenuOpen)}
          onClick={() => this.setMenuVisible(!isMenuOpen)}
          onDoubleClick={e => e.stopPropagation()}>
          <div style={styles.selectedMenuItemContainer}>
            {value ?
              <MenuItem {...getDisplayConfig(value, valueType)} {...{noLabel}} />
              : <span style={styles.defaultLabel}>选择</span>
            }
          </div>
          <i className="icon-menu-pointer" style={styles.dropDownIcon} />
        </div>

        <div className="dropdown-menu" style={styles.menu}>
          {isMenuOpen && this.renderMenuItems(values)}
        </div>
      </div>
    );
  }
}
