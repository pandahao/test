import React, {Component, PropTypes} from 'react';
import AssociativeMenus from './associative-menus';
import DropItemContainer from './drop-item-container';
import TerraCoreBox from './terra-core-box';

import {findAvailableModules, findAvailablePorts} from '../utils/code-selector';
import {attachmentModules, getAssociatedValues} from '../utils/menus';
import {valueTypes} from '../utils/values';

import {colors, flex} from '../styles';

const {func, number, object} = PropTypes;

const styles = {
  attachmentBox: {
    backgroundColor: colors.roboCore,
    borderRadius: 8,
    color: 'white',
    ...flex.center,
    height: 60,
    padding: 6,
    marginBottom: 12
  },
  headerIcon: {
    fontSize: 30,
    marginRight: 6
  },
  attachmentIcon: {
    margin: '0 6px'
  },
  attachmentMenu: {
    marginRight: 6
  }
};

export default class AttachmentBox extends Component {
  static propTypes = {
    index: number,
    attachment: object,
    updateCode: func
  };

  updateAttachment(data) {
    const {index, updateCode} = this.props;
    updateCode({
      type: 'attachment',
      indices: [index],
      data
    });
  }

  updateModule(module) {
    this.updateAttachment({module, port: null});
  }

  render() {
    const {attachment, code, index} = this.props;
    const {module = '', port} = attachment;

    const attachmentIcon = valueTypes[module] === 'motor' ?
      'icon-robocore-attach-2' : 'icon-robocore-attach-3';

    const ports = findAvailablePorts(getAssociatedValues(module, 'attachment'), code);

    return (
      <DropItemContainer type="attachment" indices={[index]}>
        <div style={styles.attachmentBox}>
          <TerraCoreBox icon={attachmentIcon} />

          <AssociativeMenus type="attachment" primaryValue={module}
            primaryValues={findAvailableModules(attachmentModules, code)}
            secondaryValue={port} secondaryValues={ports}
            onPrimaryChange={value => this.updateModule(value)}
            onSecondaryChange={value => this.updateAttachment({port: value})} />
        </div>
      </DropItemContainer>
    );
  }
}
