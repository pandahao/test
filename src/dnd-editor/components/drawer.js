import React, {Component, PropTypes} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import DrawerMenuItem from './drawer-menu-item';

import {getDisplayConfig, shapes} from '../utils/display-configs';

import {colors, flex} from '../styles';

const {array, string} = PropTypes;

const styles = {
  drawer: {
    position: 'relative',
    zIndex: 10
  },
  drawerBody(isMenuVisible) {
    return {
      backgroundColor: isMenuVisible ? colors.menuHighlight : colors.menuBackground,
      color: colors.text,
      cursor: 'pointer',
      ...flex.columnCenter,
      padding: 6,
      height: '100%',
      minWidth: 60,
      position: 'relative',
      zIndex: 20
    };
  },
  menuArrow: {
    fontSize: 30,
    margin: -8
  },
  icon: {
    ...flex.center,
    flexGrow: 1,
    margin: 4
  }
};

export default class Drawer extends Component {
  static propTypes = {
    items: array.isRequired,
    label: string.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      isMenuVisible: false
    };
  }

  renderMenus(items) {
    if (!this.state.isMenuVisible) {
      return null;
    }

    return items.map(key =>
      <DrawerMenuItem key={key} value={key} {...getDisplayConfig(key)} />
    );
  }

  setMenuVisible(isMenuVisible) {
    this.setState({isMenuVisible});
  }

  render() {
    const {icon, items, label} = this.props;
    const Shape = shapes[icon];

    return (
      <div className="drawer" style={styles.drawer}
        onMouseOver={() => this.setMenuVisible(true)}
        onMouseLeave={() => this.setMenuVisible(false)}>
        <div style={styles.drawerBody(this.state.isMenuVisible)}>
          <i className="icon-menu-pointer-up" style={styles.menuArrow} />
          <div style={styles.icon}>
            <Shape />
          </div>
          <span>{label}</span>
        </div>

        <ReactCSSTransitionGroup className="drawer-menu"
          transitionName="drawer-menu"
          component="div"
          transitionEnterTimeout={300}
          transitionLeaveTimeout={200}>
          {this.renderMenus(items)}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

