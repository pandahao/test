import React, {Component, PropTypes} from 'react';
import ParamInput from './param-input';

import {flex} from '../styles';
import {valueRanges} from '../utils/values';

const {array, func} = PropTypes;

const styles = {
  paramsBox: {
    color: 'white',
    ...flex.center,
    marginLeft: 6
  },
  icon: {
    fontSize: 24
  }
};

export default class IRParamsBox extends Component {
  static propTypes = {
    params: array,
    onChange: func.isRequired
  };

  static defaultProps = {
    params: []
  };

  render() {
    const {onChange, params: [address, irValue]} = this.props;
    const ranges = valueRanges.irEmit;

    return (
      <div style={styles.paramsBox}>
        <i className="icon-IR-address" style={styles.icon} />

        <ParamInput range={ranges[0]} value={address}
          onChange={value => onChange([value, irValue])} />

        <i className="icon-IR-value" style={styles.icon} />

        <ParamInput range={ranges[0]} value={irValue}
          onChange={value => onChange([address, value])} />
      </div>
    );
  }
}
