import React from 'react';

import Drawer from './drawer';

import { findAvailableComponentTypes } from '../utils/code-selector';
import { drawerMenuValues } from '../utils/menus';

import { colors } from '../styles';

const styles = {
  drawers: {
    backgroundColor: colors.menuBackground,
    height: 90,
    display: 'flex',
    flexShrink: 0,
    justifyContent: 'space-around'
  }
};

export default function Drawers({ code }) {
  const componentTypes = findAvailableComponentTypes(drawerMenuValues.components, code);

  return (
    <div style={styles.drawers}>
      <Drawer items={drawerMenuValues.erra} icon="erra" label="艾拉芯" />
      <Drawer items={drawerMenuValues.events} icon="events" label="事件逻辑" />
      <Drawer items={componentTypes} icon="components" label="艾拉组件" />
    </div>
  );
}
