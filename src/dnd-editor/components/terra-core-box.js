import React from 'react';

import { flex } from '../styles';

const styles = {
  terraCoreBox: {
    color: 'white',
    ...flex.center
  },
  headerIcon: {
    fontSize: 30,
    marginRight: 6
  },
  icon: {
    margin: '0 6px',
    fontSize: 24
  },
  label: {
    WebKitUserSelect: 'none'
  }
};

export default function TerraCoreBox({ children, icon }) {
  return (
    <div style={styles.terraCoreBox}>
      <i className="icon-robocore" style={styles.headerIcon} />
      <span style={styles.label}>艾拉芯</span>
      <i className={icon} style={styles.icon} />
      {children}
    </div>
  );
}
