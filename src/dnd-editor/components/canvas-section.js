import React, {Component, PropTypes} from 'react';

import DropTargetContainer from './drop-target-container';

import {colors, flex} from '../styles';

const {func, object, string} = PropTypes;

const styles = {
  canvasSection: {
    minHeight: 120,
    flexShrink: 0,
    display: 'flex',
    flexDirection: 'column'
  },
  header: {
    ...flex.center,
    color: colors.text,
    marginTop: 12,
    marginBottom: 12
  },
  border: {
    borderTop: '1px dashed',
    height: 1,
    flexGrow: 1,
    marginLeft: 12
  },
  canvasBody: {
    ...flex.column,
    alignItems: 'flex-start',
    flexGrow: 1
  }
};

export default class CanvasSection extends Component {
  static propTypes = {
    bodyStyle: object,
    dnd: object.isRequired,
    dropType: string.isRequired,
    style: object,
    title: string,
    insertCode: func.isRequired
  };

  static defaultProps = {
    bodyStyle: {},
    style: {},
    title: ''
  };

  onDrop({dataTransfer}) {
    const {dnd, insertCode} = this.props;

    insertCode({
      indices: dnd.dropZoneIndices,
      oldIndex: dnd.draggingItemIndex,
      type: dataTransfer.getData('value')
    });
  }

  render() {
    const {bodyStyle, children, dropType, style, title} = this.props;

    return (
      <div style={{...styles.canvasSection, ...style}}>
        <div style={styles.header}>
          <span>{title}</span>
          <div style={styles.border} />
        </div>
        <DropTargetContainer dropType={dropType} style={{...styles.canvasBody, ...bodyStyle}}
          onDrop={e => this.onDrop(e)}>
          {children}
        </DropTargetContainer>
      </div>
    );
  }
}
