import { connect } from 'react-redux';

import { insertDropZone } from '../actions';

import DropTarget from './drop-target';

export default connect(
  ({ dndEditor: { dnd } }) => ({ dnd }),
  { insertDropZone }
)(DropTarget);
