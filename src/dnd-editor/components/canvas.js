import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';

import AttachmentBox from './attachment-box';
import CanvasSection from './canvas-section';
import EventBox from './event-box';

import {flex} from '../styles';

const {func, object} = PropTypes;

const styles = {
  canvas: {
    backgroundColor: '#2D3038',
    ...flex.column,
    flexGrow: 1,
    overflow: 'auto',
    padding: '12px 24px'
  },
  eventCanvas: {
    flexGrow: 1
  },
  eventCanvasBody: {
    paddingBottom: 300
  }
};

export default class Canvas extends Component {
  static propTypes = {
    code: object,
    dnd: object,
    insertCode: func,
    updateCode: func
  };

  renderAttachments() {
    const {code, updateCode} = this.props;

    return code.attachments.map((attachment, i) =>
      <AttachmentBox key={attachment.uuid} index={i} {...{attachment, code, updateCode}} />
    );
  }

  renderEvents() {
    const {code, dnd, insertCode, updateCode} = this.props;

    return code.eventHandlers.map((event, i) =>
      <EventBox key={event.uuid} index={i} {...{code, dnd, event, insertCode, updateCode}} />
    );
  }

  pasteCode() {
    const { pasteCode } = this.props;
    pasteCode();
  }
  componentDidMount() {
    const {remote} = require('electron')
    const {Menu, MenuItem} = remote
    let dom = ReactDOM.findDOMNode(this.refs.canvas);
    let that = this;
    const menu = new Menu()
    menu.append(new MenuItem({label: '粘贴', click() { that.pasteCode() }}))

    dom.addEventListener('contextmenu', (e) => {
      e.preventDefault();
      menu.popup(remote.getCurrentWindow());
    }, false)
  }

  render() {
    const {dnd, insertCode} = this.props;

    return (
      <div ref="canvas" style={styles.canvas}>
        <CanvasSection title="链接艾拉组件" dropType="attachment" {...{dnd, insertCode}}>
          {this.renderAttachments()}
        </CanvasSection>

        <CanvasSection title="程序" dropType="event" {...{dnd, insertCode}}
          bodyStyle={styles.eventCanvasBody} style={styles.eventCanvas}>
          {this.renderEvents()}
        </CanvasSection>
      </div>
    );
  }
}
