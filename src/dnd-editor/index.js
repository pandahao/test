import {connect} from 'react-redux';
import React, {Component, PropTypes} from 'react';

import {resetDndEditor, setCode} from './actions';

import CanvasContainer from './components/canvas-container';
import Drawers from './components/drawers';

import {parseExternalGraph} from './utils/code-gen';

const {object, string} = PropTypes;

const styles = {
  editor: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    width: '100%'
  }
};

class DndEditor extends Component {
  static propTypes = {
    code: object,
    initCode: string
  };

  componentWillMount() {
    const {setCode, initCode} = this.props;
    if (initCode) {
      const graph = parseExternalGraph(initCode);
      setCode(graph);
    }
  }

  componentWillUnmount() {
    this.props.resetDndEditor();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.code !== this.props.code && this.props.code.attachments.length) {
      this.props.makeChange(nextProps.code);
    }
  }
  render() {
    const {code} = this.props;

    return (
      <div style={styles.editor}>
        <CanvasContainer {...{code}} />
        <Drawers {...{code}} />
      </div>
    );
  }
}

export default connect(
  ({dndEditor: {code}}) => ({code}),
  {
    resetDndEditor,
    setCode
  }
)(DndEditor);
