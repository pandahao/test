import { handleActions } from 'redux-actions';

const initialState = {
  dropZoneType: null,
  dropZoneIndices: [],
  draggingItemIndex: -1
};

export default handleActions({
  clearDropZone() {
    return { ...initialState };
  },
  insertDropZone(state, { payload: { indices, type } }) {
    return { ...state, dropZoneIndices: indices, dropZoneType: type };
  },
  setDraggingItemIndex(state, { payload: draggingItemIndex }) {
    return { ...state, draggingItemIndex };
  }
}, initialState);
