import { combineReducers } from 'redux';
import code from './code';
import dnd from './dnd';

export default combineReducers({
  code,
  dnd
});
