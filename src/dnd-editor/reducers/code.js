import { handleActions } from 'redux-actions';
import uuid from 'uuid';

import { defaultParams } from '../utils/values';

/*
attachment {
  module: string,
  port: string,
  type: string
}

event {
  commands: [],
  eventData: [],
  eventSource: string,
  eventType: string,
  type: string
}

command {
  commandAction: string,
  commandSource: string,
  commandParams: [],
  type: string
}
*/

const initialState = {
  attachments: [],
  eventHandlers: []
};

function traverse(state, type, indices, callback) {
  const sectionName = type === 'attachment' ? 'attachments' : 'eventHandlers';
  const section = state[sectionName].slice();

  if (indices.length === 1) {
    callback(section, indices[0]);
  } else if (indices.length > 1) {
    const { commands } = section[indices[0]];
    callback(commands, indices[1]);
  }

  return { ...state, ...{ [sectionName]: section } };
}

function createNewStatement(type) {
  const statement = { type, uuid: uuid() };

  if (type === 'event') {
    statement.commands = [];
  }
  if (type === 'timerBegin' || type === 'terminate') {
    statement.commandSource = 'erra';
    statement.commandAction = type;
    statement.commandParams = defaultParams[type];
  }

  return statement;
}

function updateStatement(statement, data) {
  const stmt = { ...statement, ...data };
  const { commandAction, eventType, type } = stmt;

  if (type === 'event' && eventType !== statement.eventType) {
    stmt.eventData = defaultParams[eventType];
  } else if (commandAction !== statement.commandAction) {
    stmt.commandParams = defaultParams[commandAction];
  }

  return stmt;
}

function onAttachmentRemove(state, module) {
  state.eventHandlers = state.eventHandlers.filter((eventHandler) => {
    eventHandler.commands = eventHandler.commands.filter(
      command => command.commandSource && command.commandSource !== module
    );

    return eventHandler.eventSource !== module;
  });

  return state;
}

export default handleActions({
  insertDndCode(state, { payload: { indices, oldIndex, type } }) {
    if (oldIndex >= 0) {
      return traverse(state, type, indices,
        (list, i) => {
          if (oldIndex !== i) {
            const statement = list[oldIndex];
            list.splice(oldIndex, 1);
            // deduct 1 from the inserting index of the statement is removed before the inserting index
            list.splice(oldIndex < i ? i - 1 : i, 0, statement);
          }
        }
      );
    }

    return traverse(state, type, indices,
      (list, i) => list.splice(i, 0, createNewStatement(type))
    );
  },
  removeDndCode(state, { payload: { indices, type } }) {
    let item;

    state = traverse(state, type, indices,
      (list, i) => {
        item = list[i];
        list.splice(i, 1);
      }
    );

    if (type === 'attachment') {
      return onAttachmentRemove(state, item.module);
    }

    return state;
  },
  copyDndCode(state, { payload: { indices, type } }) {
    state = traverse(state, type, indices,
      (list, i) => {
        window.DndClipBoard = {
          type,
          indices,
          item: list[i]
        };
      }
    );
    return state;
  },
  pasteDndCode(state) {
    const type = window.DndClipBoard.type;
    const indices = window.DndClipBoard.indices;
    let item = createNewStatement(window.DndClipBoard.type);
    const item_uuid = item.uuid;
    Object.assign(item, window.DndClipBoard.item);
    item.uuid = item_uuid;
    if(item.type === 'event') {
      item.commands = item.commands.map(command => {
        command.uuid = uuid();
        return command;
      });
    }
    state = traverse(state, type, indices,
      (list, i) => {
        list.push(item);
      }
    );
    return state;
  },
  resetDndEditor() {
    return initialState;
  },
  setDndCode(state, { payload }) {
    return payload;
  },
  updateDndCode(state, { payload: { data, indices, type } }) {
    let removedModule;

    state = traverse(state, type, indices,
      (list, i) => {
        // if the attachment module is updated to a different value
        if (type === 'attachment' && data.module && data.module !== list[i].module) {
          removedModule = list[i].module;
        }
        list[i] = updateStatement(list[i], data);
      }
    );

    if (removedModule) {
      onAttachmentRemove(state, removedModule);
    }

    return state;
  }
}, initialState);
