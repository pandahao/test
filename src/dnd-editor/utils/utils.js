export function range(start, end) {
  return Array(end - start + 1).fill().map((_, i) => i + start);
}

export function isEmpty(source) {
  if (!source) {
    return true;
  }

  if ('length' in source && !source.length) {
    return true;
  }

  return false;
}

export function findKey(map, value) {
  return Object.keys(map).find(key => map[key] === value);
}
