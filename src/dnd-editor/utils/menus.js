import { range } from './utils';

import { findValueConfig } from './values';

export const drawerMenuValues = {
  erra: [
    'attachment',
    'timerBegin',
    'terminate'
  ],
  events: [
    'event'
  ],
  components: [
    'motor',
    'led',
    'button',
    'tape',
    'ir'
  ]
};

export const instances = {
  button: ['button1', 'button2', 'button3'],
  ir: ['ir'],
  led: ['led1', 'led2'],
  motor: ['motor1', 'motor2'],
  tape: ['tape1', 'tape2']
};

export const attachmentModules = [
  ...instances.led,
  ...instances.motor,
  ...instances.button,
  ...instances.tape,
  ...instances.ir
];

export const eventSources = [
  'erra',
  ...attachmentModules
];

const commonPorts = range(1, 7).map(i => `PORT_${i}`);
const irActions = ['irEmit', 'irActivate', 'irDeactivate'];
const ledActions = ['ledOn', 'ledOff', 'ledSlowBlink', 'ledFastBlink', 'ledBlinkEnd',
  'ledActivate', 'ledDeactivate'];
const ledCommandActions = [...ledActions];
ledCommandActions.splice(2, 0, 'ledToggle');
export const timerParams = [0.1, 0.25, 0.5, ...range(1, 10), 30, 60, 120];
export const irBeaconOptions = ['irOn', 'irOff', 'irRed', 'irYellow', 'irGreen', 'irBlue',
  'irPurple', 'irOrange', 'irWhite'];

export const associatedValues = {
  attachment: {
    button: commonPorts,
    ir: ['IR_TRAN'],
    led: commonPorts,
    motor: ['MOTOR_A', 'MOTOR_B'],
    tape: commonPorts
  },
  event: {
    button: ['buttonPress', 'buttonRelease', 'buttonActivate', 'buttonDeactivate'],
    erra: ['launch', 'timerUp', 'terminate'],
    ir: irActions,
    led: ledActions,
    motor: ['motorRotate', 'motorReverse', 'motorStop', 'motorActivate', 'motorDeactivate'],
    tape: ['tapeEnter', 'tapeLeave', 'tapeActivate', 'tapeDeactivate']
  },
  command: {
    button: ['buttonActivate', 'buttonDeactivate'],
    ir: ['irBeacon', ...irActions],
    led: ledCommandActions,
    motor: ['motorRotate', 'motorReverse', 'motorPause', 'motorResume', 'motorActivate', 'motorDeactivate'],
    tape: ['tapeActivate', 'tapeDeactivate']
  }
};

export function getAssociatedValues(value, type) {
  return findValueConfig(associatedValues[type], value) || [];
}

export const parameterizedEvents = ['buttonPress', 'buttonRelease', 'ledBlinkEnd',
  'tapeEnter', 'tapeLeave', 'timerUp'];

export const parameterizedCommands = [
  'buttonPress', 'buttonRelease', 'irBeacon', 'ledFastBlink', 'ledSlowBlink',
  'motorRotate'
];
