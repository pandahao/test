export const valueTypes = {
  attachment: 'erra',
  button1: 'button',
  button2: 'button',
  button3: 'button',
  erra: 'erra',
  ir: 'ir',
  led1: 'led',
  led2: 'led',
  motor1: 'motor',
  motor2: 'motor',
  tape1: 'tape',
  tape2: 'tape',
  terminate: 'erra',
  timerBegin: 'timer',
  timerUp: 'timer'
};

export function findValueConfig(source, value) {
  let config = source[value];

  if (!config) {
    const type = valueTypes[value];

    if (type) {
      config = source[type];
    }
  }

  return config;
}

const max = 65535;

export const valueRanges = {
  buttonPress: [0, 100],
  buttonRelease: [0, 100],
  irEmit: [[1, max], [1, max]],
  ledBlinkEnd: [0, 10],
  ledFastBlink: [0, max],
  ledSlowBlink: [0, max],
  motorRotate: [-10, 11],
  tapeEnter: [0, 100],
  tapeLeave: [0, 100],
  timerUp: [0, max],
  timerBegin: [0, 120]
};

export const defaultParams = {
  buttonPress: [1],
  buttonRelease: [1],
  irEmit: [1, 1],
  ledFastBlink: [0],
  ledSlowBlink: [0],
  motorRotate: [0],
  tapeEnter: [1],
  tapeLeave: [1],
  timerUp: [1],
  timerBegin: [1]
};

export function isZeroAsInfinite(action) {
  return ['buttonPress', 'buttonRelease', 'tapeEnter', 'tapeLeave', 'ledFastBlink', 'timerUp', 'ledSlowBlink'].includes(action);
}
