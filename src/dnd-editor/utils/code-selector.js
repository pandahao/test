import { valueTypes } from './values';

export function findAvailableModules(modules, code) {
  return modules.filter(
    module => !code.attachments.some(attachment => attachment.module === module)
  );
}

export function findAvailablePorts(ports, code) {
  return ports.filter(
    port => !code.attachments.some(attachment => attachment.port === port)
  );
}

export function findAvailableComponentTypes(componentTypes, code) {
  if (!componentTypes) {
    return [];
  }

  const map = {};

  code.attachments.forEach(({ module }) => (map[valueTypes[module]] = 1));

  return componentTypes.filter(component => map[component]);
}

export function findAvailableComponents(components, code) {
  if (!components) {
    return [];
  }

  const map = { erra: 1 };

  code.attachments.forEach(({ module }) => (map[module] = 1));

  return components.filter(component => map[component]);
}

export function getAction(statement) {
  if (statement.type === 'event') {
    return statement.eventType;
  }

  return statement.commandAction;
}

export function getParams(statement) {
  let params;

  if (statement.type === 'event') {
    params = statement.eventData;
  } else {
    params = statement.commandParams;
  }

  return params || [];
}
