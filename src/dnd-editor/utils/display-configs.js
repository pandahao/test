import { colors } from '../styles';

import * as svg from '../components/shapes';
import { findValueConfig } from './values';

const icons = {
  attachment: 'icon-robocore-attach-3',
  button: 'icon-button',
  buttonActivate: 'icon-activate',
  buttonDeactivate: 'icon-deactivate',
  buttonPress: 'icon-button-press',
  buttonRelease: 'icon-button-release',
  erra: 'icon-robocore',
  event: 'icon-LED',
  ir: 'icon-IR',
  irActivate: 'icon-activate',
  irDeactivate: 'icon-deactivate',
  irEmit: 'icon-IR-message-emit',
  irBeacon: 'icon-IR-beacon',
  launch: 'icon-robocore-launch',
  led: 'icon-LED',
  ledActivate: 'icon-activate',
  ledBlinkEnd: 'icon-LED-stop-blink',
  ledDeactivate: 'icon-deactivate',
  ledFastBlink: 'icon-LED-fast-blink',
  ledOff: 'icon-LED-off',
  ledOn: 'icon-LED-on',
  ledSlowBlink: 'icon-LED-slow-blink',
  ledToggle: 'icon-LED-toggle',
  motor: 'icon-motor',
  motorActivate: 'icon-activate',
  motorDeactivate: 'icon-deactivate',
  motorPause: 'icon-motor-pause',
  motorResume: 'icon-motor-resume',
  motorReverse: 'icon-motor-reverse',
  motorRotate: 'icon-motor-change-speed',
  motorStop: 'icon-motor-stop',
  tape: 'icon-tape',
  tapeActivate: 'icon-activate',
  tapeDeactivate: 'icon-deactivate',
  tapeEnter: 'icon-tape-enter',
  tapeLeave: 'icon-tape-leave',
  terminate: 'icon-robocore-terminate',
  timerBegin: 'icon-robocore-time',
  timerUp: 'icon-robocore-time-up'
};

export const shapes = {
  erra: svg.ErraCoreIcon,
  attachment: svg.AttachmentIcon,
  timerBegin: svg.TimeIcon,
  terminate: svg.TerminateIcon,
  events: svg.FunctionIcon,
  event: svg.FunctionItemIcon,
  components: svg.ComponentIcon,
  motor: svg.MotorIcon,
  led: svg.LedIcon,
  button: svg.ButtonIcon,
  tape: svg.TapeIcon,
  ir: svg.IrIcon
};

const colorConfigs = {
  button: colors.button,
  erra: colors.roboCore,
  ir: colors.ir,
  led: colors.led,
  motor: colors.motor,
  tape: colors.tape,
  timer: colors.roboCore
};

const labels = {
  attachment: '连接',
  button: '按钮',
  button1: '按钮甲',
  button2: '按钮乙',
  button3: '按钮丙',
  erra: '艾拉芯',
  event: '条件判断',
  ir: '发射器',
  irBlue: '蓝色',
  irGreen: '绿色',
  irOff: '熄灭',
  irOn: '点亮',
  irOrange: '橙色',
  irPurple: '紫色',
  irRed: '红色',
  irWhite: '白色',
  irYellow: '黄色',
  led: '灯珠',
  led1: '灯珠甲',
  led2: '灯珠乙',
  motor: '电机',
  motor1: '电机甲',
  motor2: '电机乙',
  tape: '巡线',
  tape1: '巡线甲',
  tape2: '巡线乙',
  terminate: '终止',
  timerBegin: '计时'
};

export function getDisplayConfig(value, type) {
  const color = findValueConfig(colorConfigs, value);
  const icon = findValueConfig(icons, value);
  let label = findValueConfig(labels, value) || value;

  if (type === 'timerBegin' && label) {
    label += ' 秒';
  }

  return { color, icon, label };
}

export function getDragType(value) {
  if (['attachment', 'event'].includes(value)) {
    return value;
  }

  return 'command';
}
