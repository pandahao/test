import uuid from 'uuid';

import { getAction, getParams } from './code-selector';

import { findKey } from './utils';

import { findValueConfig, isZeroAsInfinite, valueTypes } from './values';

const varTypes = {
  button: 'RoboTerraButton',
  erra: 'RoboTerraRoboCore',
  ir: 'RoboTerraIRTransmitter',
  led: 'RoboTerraLED',
  motor: 'RoboTerraMotor',
  tape: 'RoboTerraTapeSensor'
};

const codeEvents = {
  buttonActivate: 'ACTIVATE',
  buttonDeactivate: 'DEACTIVATE',
  buttonPress: 'BUTTON_PRESS',
  buttonRelease: 'BUTTON_RELEASE',
  irActivate: 'ACTIVATE',
  irDeactivate: 'DEACTIVATE',
  irEmit: 'IR_MESSAGE_EMIT',
  launch: 'ROBOCORE_LAUNCH',
  ledActivate: 'ACTIVATE',
  irBeacon: 'SET_BEACON',
  ledBlinkEnd: 'BLINK_END',
  ledDeactivate: 'DEACTIVATE',
  ledFastBlink: 'FASTBLINK_BEGIN',
  ledOff: 'LED_TURNOFF',
  ledOn: 'LED_TURNON',
  ledSlowBlink: 'SLOWBLINK_BEGIN',
  motorActivate: 'ACTIVATE',
  motorDeactivate: 'DEACTIVATE',
  motorReverse: 'MOTOR_REVERSE',
  motorRotate: 'MOTOR_SPEED_CHANGE',
  motorStop: 'MOTOR_SPEED_ZERO',
  tapeActivate: 'ACTIVATE',
  tapeDeactivate: 'DEACTIVATE',
  tapeEnter: 'BLACK_TAPE_ENTER',
  tapeLeave: 'BLACK_TAPE_LEAVE',
  terminate: 'ROBOCORE_TERMINATE',
  timerUp: 'ROBOCORE_TIME_UP'
};

const codeFunctions = {
  buttonActivate: 'activate',
  buttonDeactivate: 'deactivate',
  irActivate: 'activate',
  irBeacon: 'setBeacon',
  irDeactivate: 'deactivate',
  irEmit: 'emit',
  ledActivate: 'activate',
  ledBlinkEnd: 'stopBlink',
  ledDeactivate: 'deactivate',
  ledFastBlink: 'fastBlink',
  ledOff: 'turnOff',
  ledOn: 'turnOn',
  ledSlowBlink: 'slowBlink',
  ledToggle: 'toggle',
  motorActivate: 'activate',
  motorDeactivate: 'deactivate',
  motorPause: 'pause',
  motorResume: 'resume',
  motorReverse: 'reverse',
  motorRotate: 'rotate',
  tapeActivate: 'activate',
  tapeDeactivate: 'deactivate',
  terminate: 'terminate',
  timerBegin: 'time'
};

const paramValues = {
  // timerBegin: {
  //   0.1: 'TENTH_SEC',
  //   0.25: 'QUARTER_SEC',
  //   0.5: 'HALF_SEC',
  //   1: 'ONE_SEC',
  //   2: 'TWO_SEC',
  //   3: 'THREE_SEC',
  //   4: 'FOUR_SEC',
  //   5: 'FIVE_SEC',
  //   6: 'SIX_SEC',
  //   7: 'SEVEN_SEC',
  //   8: 'EIGHT_SEC',
  //   9: 'NINE_SEC',
  //   10: 'TEN_SEC',
  //   30: 'HALF_MIN',
  //   60: 'ONE_MIN',
  //   120: 'TWO_MIN'
  // },
  irBeacon: {
    irBlue: 'BLUE',
    irGreen: 'GREEN',
    irOff: 'OFF',
    irOn: 'ON',
    irOrange: 'ORANGE',
    irPurple: 'PURPLE',
    irRed: 'RED',
    irWhite: 'WHITE',
    irYellow: 'YELLOW'
  }
};

function getParamValue(action, value) {
  const values = paramValues[action];

  if (values) {
    return values[value];
  }

  return value;
}

function getParamKey(action, paramValue) {
  const values = paramValues[action];

  if (values) {
    return findKey(values, paramValue);
  }

  return paramValue;
}

export default function generateCode(code) {
  const lines = compose(code);
  return lines.join('\n');
}

export function compose(code) {
  const factories = [genIncludes, genVars, genAttachmentFunction, genEventHandler];
  const lines = [];

  factories.forEach(factory => lines.push(...factory(code), ''));

  return lines;
}

function indent(lines) {
  return lines.map(line => `\t${line}`);
}

function genIncludes() {
  return [
    '#include "ROBOTERRA.h"'
  ];
}

function genVars({ attachments = [] }) {
  return [
    'RoboTerraRoboCore erra;',
    ...attachments.map(({ module }) => {
      const varType = findValueConfig(varTypes, module);
      return `${varType} ${module};`;
    })
  ];
}

function genAttachmentFunction({ attachments = [] }) {
  return [
    'void attachRoboTerraElectronics() {',
    ...indent(genAttachments(attachments)),
    '}'
  ];
}

function genAttachments(attachments) {
  return attachments.map(({ module, port }) =>
    `erra.attach(${module}, ${port});`
  );
}

function genEventHandler({ eventHandlers = [] }) {
  const eventHandlersCode = [];
  eventHandlers.forEach((event, i) => {
    eventHandlersCode.push(...indent(genEvent(event, i)));
  });

  return [
    'void handleRoboTerraEvent() {',
    ...eventHandlersCode,
    '}'
  ];
}

function transformParams(statement) {
  const action = getAction(statement);
  const params = getParams(statement);

  return params.map(param => getParamValue(action, param)).map(
    param => (isZeroAsInfinite(action) && !param ? '' : param));
}

function genEvent(event, index) {
  const { eventType, eventSource, commands } = event;
  const params = transformParams(event);
  const eventCodeName = codeEvents[eventType];
  const hasParams = params.length && params[0];
  const paramsCode = hasParams ? ` && EVENT.getData() == ${params[0]}` : '';
  const conditionCode = `EVENT.isFrom(${eventSource}) && EVENT.isType(${eventCodeName})${paramsCode}`;

  const commandsCode = commands.map(genCommand);

  return [
    `${index === 0 ? 'if' : 'else if'} (${conditionCode}) {`,
    ...indent(commandsCode),
    '}'
  ];
}

function genCommand(command) {
  const { commandAction, commandSource } = command;
  const functionCodeName = codeFunctions[commandAction];
  const paramsCode = transformParams(command).join(', ');
  return `${commandSource}.${functionCodeName}(${paramsCode});`;
}

// generates syntax data structure
// the external data structure differs from the reducer state as it uses CPP for eventType
// and commandAction, and does not retain internal fields like uuid and statement type
export function generateExternalGraph(code) {
  const attachments = code.attachments.map(({ module, port }) => ({ module, port }));

  function formatExternalParams(params) {
    params = params.filter(Boolean);

    if (params && params.length) {
      return params;
    }

    return undefined;
  }

  const eventHandlers = code.eventHandlers.map((event) => {
    const commands = event.commands.map(command => ({
      commandAction: codeFunctions[command.commandAction],
      commandSource: command.commandSource,
      commandParams: formatExternalParams(transformParams(command))
    }));

    return {
      commands,
      eventData: formatExternalParams(transformParams(event)),
      eventType: codeEvents[event.eventType],
      eventSource: event.eventSource
    };
  });

  return JSON.stringify({ attachments, eventHandlers }, null, 2);
}

// parses the event data / command params from external graph
function parseExternalParams(action, params) {
  if (params && params.length) {
    return params.map(param => getParamKey(action, param));
  }

  return undefined;
}

export function parseExternalGraph(graphText) {
  const graph = JSON.parse(graphText.trim());

  const attachments = graph.attachments.map(
    ({ module, port }) => ({ module, port, type: 'attachment', uuid: uuid() })
  );

  const eventHandlers = graph.eventHandlers.map((event) => {
    const commands = event.commands.map((command) => {
      const commandAction = findKey(codeFunctions, command.commandAction);

      return {
        commandAction,
        commandSource: command.commandSource,
        commandParams: parseExternalParams(commandAction, command.commandParams),
        type: valueTypes[command.commandSource],
        uuid: uuid()
      };
    });

    const eventType = findKey(codeEvents, event.eventType);

    return {
      commands,
      eventData: parseExternalParams(eventType, event.eventData),
      eventType,
      eventSource: event.eventSource,
      type: 'event',
      uuid: uuid()
    };
  });

  return { attachments, eventHandlers };
}
