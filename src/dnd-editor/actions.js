import { createAction as $ } from 'redux-actions';

export const clearDropZone = $`clearDropZone`;
export const insertDropZone = $`insertDropZone`;

export const insertCode = $`insertDndCode`;
export const removeCode = $`removeDndCode`;
export const copyCode = $`copyDndCode`;
export const pasteCode = $`pasteDndCode`;
export const updateCode = $`updateDndCode`;

export const resetDndEditor = $`resetDndEditor`;
export const setCode = $`setDndCode`;
export const setDraggingItemIndex = $`setDraggingItemIndex`;
