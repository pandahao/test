const settings = require('electron').remote.require('electron-settings');
const COMPILER_SERVER = settings.get('COMPILER_SERVER');

export const ProductionEnv = process.env.NODE_ENV === 'production';
export const SerialEnv = __SERIAL__;
export const ElectronEnv = __ELECTRON__;
export const DevEnv = __DEVELOPMENT__;
export const ChromeEnv = __CHROME__;

const gbsCompilerEndpoint = COMPILER_SERVER;

// Serving all clients from aliyun endpoints for now
export const CompilerEndPoint = gbsCompilerEndpoint;

function getCore() {
  console.log(`SerialEnv: ${SerialEnv}`);
  console.log('window.CastleRockCore', window.CastleRockCore);
  if (SerialEnv && window.CastleRockCore) {
    return new window.CastleRockCore();
  }
  return () => {};
}

export const SkylineCore = getCore();
