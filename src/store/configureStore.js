/* eslint global-require: "off"*/
import { createStore, compose, combineReducers, applyMiddleware } from 'redux';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { hashHistory } from 'react-router';
import thunk from 'redux-thunk';

import rootReducer from 'reducers';

// Devtool enhancers
const devToolEnhancer = () => {
  if (__DEVELOPMENT__) {
    if (__BROWSER__ && window.devToolsExtension) {
      return window.devToolsExtension();
    }
  }
  // Return a null function for no enhancer
  return f => f;
};
// Middleware you want to use in development:
const decideMiddlewares = () => {
  const middlewares = [];

  // Using redux-thunk middlewares
  middlewares.push(thunk);
  // Using react router middlewares
  middlewares.push(routerMiddleware(hashHistory));

  return middlewares;
};

// Compose store enhancers
const enhancer = compose(
  applyMiddleware(...decideMiddlewares()),
  devToolEnhancer()
);

// Create store
export default function configureStore(initialState) {
  // Config router reducers
  const reducer = combineReducers({
    ...rootReducer,
    routing: routerReducer
  });
  // Create redux store
  const store = createStore(reducer, initialState, enhancer);

  if (__DEVELOPMENT__ && module.hot) {
    module.hot.accept('reducers', () => {
      const changedReducer = require('reducers');
      store.replaceReducer(changedReducer);
    });
  }
  return store;
}
