const StagePalette = {
  0: '#40BAFF',
  1: '#50C3FF', // origin 01
  2: '#E6CE37',
  3: '#7A85A3',
  4: '#F89406', // origin 04
  5: '#3EB340',
  6: '#F66E56',
  7: '#9666D7',
  8: '#F3567E',
  9: '#F66E56', // 导师营地
  10: '#6293F0',
  11: '#50C3FF',
  12: '#E6CE37',
  13: '#F66E56',
  14: '#0277BD',
  15: '#0097A7',
  16: '#607D8B'
};

export const StagePicker = (id) => {
  return StagePalette[id];
};
