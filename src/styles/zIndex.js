export const zIndex = {
  low: -9999,
  mediumLow: 0,
  medium: 20,
  mediumHigh: 100,
  high: 200,
  veryHigh: 300,
  summit: 99999
};
