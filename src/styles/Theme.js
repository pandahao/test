import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { GeneralPalette } from 'legacy_styles/Colors';

const SkylineTheme = getMuiTheme({
  palette: {
    primary1Color: GeneralPalette.brightPurple,
  }
});


export default SkylineTheme;
