import { combineReducers } from 'redux';
import FreeModeReducer from './FreeModeReducer';

const freeMode = FreeModeReducer;

export default freeMode;
