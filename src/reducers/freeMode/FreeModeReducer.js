import { combineReducers } from 'redux';
import {
    LOAD_ROBOTS,
    LOAD_ROBOTS_SUCCESS,
    LOAD_ROBOTS_FAIL,
    LOAD_ROBOT_SUCCESS,
    DELETE_ROBOT_SUCCESS,
    NEW_ROBOT,
    SAVE_ROBOT_SUCCESS,
    CHANGE_FREEMODE_VIEW,
    TOGGLE_DOCUMENTATION
} from 'legacy_actions/FreeModeActions';

import SaveExistingFreeModeSessionDialog from 'components/freemode/SaveExistingFreeModeSessionDialog';
import { OPEN_SAVE_EXISTING, CLOSE_SAVE_EXISTING } from 'legacy_actions/FreeModeActions'

const showSaveExistingFreeModeSessionDialog = (state = false, action) => {
  switch (action.type) {
    case OPEN_SAVE_EXISTING:
      return true;
    case CLOSE_SAVE_EXISTING:
      return false;
    default:
      return state
  }
}

const DEFAULT_EDITING_ROBOT = {
  isSaving: false,
  item: {
    id: null,
    name: 'Untitled',
    content: null,
    created: null,
    updated: null
  }
};

const DEFAULT_ROBOTS = {
  isFetching: false,
  items: []
};

const DEFAULT_VIEW = 'list';

const editingRobot = (state = DEFAULT_EDITING_ROBOT, action) => {
  switch (action.type) {
    case LOAD_ROBOT_SUCCESS:
      return Object.assign({}, state, {
        isSaving: false,
        error: null,
        item: action.robot
      }
            );
    case DELETE_ROBOT_SUCCESS:
      return DEFAULT_EDITING_ROBOT;
    case NEW_ROBOT:
      return DEFAULT_EDITING_ROBOT;
    case SAVE_ROBOT_SUCCESS:
      return Object.assign({}, state, {
        isSaving: false,
        error: null,
        item: action.robot
      }
            );
    default:
      return state;
  }
  return state;
};

const robots = (state = DEFAULT_ROBOTS, action) => {
  switch (action.type) {
    case LOAD_ROBOTS:
      return Object.assign({}, state, {
        isFetching: true
      });
    case LOAD_ROBOTS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        items: action.items
      });
    case LOAD_ROBOTS_FAIL:
      return Object.assign({}, state, {
        isFetching: false
      });
        // reset items so that items will be fetch again
    case DELETE_ROBOT_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        items: state.items.filter((i) => { return i.id != action.robot.id; })
      });
    case SAVE_ROBOT_SUCCESS:
      return {
        isFetching: false,
        items: [action.robot, ...(function filterOutRobotById(items, id) {
          return items.filter((e) => { return e.id != id; })
        })(state.items, action.robot.id)]
      };
    default:
      return state;
  }
};

const view = (state = DEFAULT_VIEW, action) => {
  switch (action.type) {
    case CHANGE_FREEMODE_VIEW:
      return action.view;
    default:
      return state;
  }
};

const docView = (state = false, action) => {
  switch (action.type) {
    case TOGGLE_DOCUMENTATION:

      return !state;
    default:
      return state;
  }
};

const FreeModeReducer = combineReducers({
  view,
  docView,
  editingRobot,
  robots,
  showSaveExistingFreeModeSessionDialog
});
export default FreeModeReducer;
