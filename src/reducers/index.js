import { combineReducers } from 'redux';
import dndEditor from '../dnd-editor/reducers/dnd-editor';
import robot from 'reducers/robot';
import shared from 'reducers/shared';
import projectMode from 'reducers/projectMode';
import freeMode from 'reducers/freeMode';
import auth from 'reducers/auth';
import setting from 'reducers/setting';

const rootReducer = {
  dndEditor,
  robot,
  projectMode,
  freeMode,
  auth,
  setting,
  ...shared,
};

export default rootReducer;
