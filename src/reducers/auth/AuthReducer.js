import { combineReducers } from 'redux';
import { NEWPASSWORD_CHANGE, RESET, CONFIRM, LOGIN, REGISTER_ONE, REGISTER_TWO, LICENSE, PASSWORD, LOG_IN, LOG_OUT, USERNAME_CHANGE, PASSWORD_CHANGE, LOGIN_ERROR, BEGIN_LOGIN } from 'legacy_actions/AuthActions';

function authLocation(state = LOGIN, action) {
  switch (action.type) {
    case RESET:
      return RESET;
    case CONFIRM:
      return CONFIRM;
    case LOGIN:
      return LOGIN;
    case REGISTER_ONE:
      return REGISTER_ONE;
    case REGISTER_TWO:
      return REGISTER_TWO;
    case PASSWORD:
      return PASSWORD;
    case LICENSE:
      return LICENSE;
    default:
      return state;
  }
}

const init_state = {
  status: 'success',
  signedIn: false,
  error: undefined
};

function loggedIn(state = init_state, action) {
  switch (action.type) {
    case BEGIN_LOGIN:
      return Object.assign({}, state, { status: 'fetching', signedIn: false, error: undefined });
    case LOG_IN:
      return Object.assign({}, state, { status: 'success', signedIn: true, error: undefined });
    case LOG_OUT:
      return Object.assign({}, state, { status: 'success', signedIn: false, error: undefined });
    case 'AUTH_ERROR':
      return Object.assign({}, state, { status: 'success', error: action.err });
    case 'RESET_ERROR':
      return Object.assign({}, state, { error: undefined });
    default:
      return state;
  }
}

function username(state = '', action) {
  switch (action.type) {
    case USERNAME_CHANGE:
      return action.val;
    default:
      return state;
  }
}
function newPassword(state = '', action) {
  switch (action.type) {
    case NEWPASSWORD_CHANGE:
      return action.val;
    default:
      return state;
  }
}

function password(state = '', action) {
  switch (action.type) {
    case PASSWORD_CHANGE:
      return action.val;
    default:
      return state;
  }
}

const account_template = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  language: '',
  birthday: '',
  onBoardProgress: 0,
  projectsOwned: [
    {
      status: 'UNLOCKED',
      projectId: 0,
      progress: 1
    },
    {
      status: 'LOCKED',
      projectId: 1,
      progress: 0
    },
    {
      status: 'LOCKED',
      projectId: 2,
      progress: 0
    },
  ]
};

function registerAccount(state = account_template, action) {
  switch (action.type) {
    case 'UPDATE_CONFIRM':
      console.log('update');
      return Object.assign({}, state, { confirmCode: action.value });
    case 'UPDATE_USERNAME':
      return Object.assign({}, state, { username: action.value });
    case 'UPDATE_EMAIL':
      return Object.assign({}, state, { email: action.value });
    case 'UPDATE_PASS':
      return Object.assign({}, state, { password: action.value });
    case 'UPDATE_FIRSTNAME':
      return Object.assign({}, state, { firstName: action.value });
    case 'UPDATE_LASTNAME':
      return Object.assign({}, state, { lastName: action.value });
    case 'UPDATE_LANG':
      return Object.assign({}, state, { language: action.value });
    case 'UPDATE_BDAY':
      return Object.assign({}, state, { birthday: action.value });
    case 'CLEAR_INFO':
      return Object.assign({}, account_template);
    default:
      return state;
  }
}

function isChecked(state = false, action) {
  switch (action.type) {
    case 'SET_CHECK':
      return action.bool;
    default:
      return state;
  }
}

const AuthReducer = combineReducers({
  authLocation,
  loggedIn,
  username,
  password,
  registerAccount,
  isChecked,
  newPassword
});

export default AuthReducer;
