import { combineReducers } from 'redux';
import {
  DEVICE_SETUP,
  DEVICE_UPDATE,
  LAYOUT_SETUP,
  LAYOUT_UPDATE,
  LAYOUT_RESIZE,
  EVENT_RECEIVED,
  STATE_RECEIVED,
  EVENT_RESET,
  TOGGLE_CONSOLE,
  TAKE_SNAPSHOT
} from 'legacy_actions/RobotActions';

const PORT_LIMIT = 18;

const emptyLayout = () => [
  { active: false, i: 'PORT_1', w: 1, priority: 0 },
  { active: false, i: 'PORT_2', w: 1, priority: 1 },
  { active: false, i: 'PORT_3', w: 1, priority: 2 },
  { active: false, i: 'PORT_4', w: 1, priority: 3 },
  { active: false, i: 'PORT_5', w: 1, priority: 4 },
  { active: false, i: 'PORT_6', w: 1, priority: 5 },
  { active: false, i: 'PORT_7', w: 1, priority: 6 },
  { active: false, i: 'DIO_1', w: 1, priority: 7 },
  { active: false, i: 'DIO_2', w: 1, priority: 8 },
  { active: false, i: 'DIO_3', w: 1, priority: 9 },
  { active: false, i: 'DIO_4', w: 1, priority: 10 },
  { active: false, i: 'DIO_5', w: 1, priority: 11 },
  { active: false, i: 'DIO_6', w: 1, priority: 12 },
  { active: false, i: 'DIO_7', w: 1, priority: 13 },
  { active: false, i: 'DIO_8', w: 1, priority: 14 },
  { active: false, i: 'DIO_9', w: 1, priority: 15 },
  { active: false, i: 'IR_TRAN', w: 1, priority: 16 },
  { active: false, i: 'AI_1', w: 1, priority: 17 },
  { active: false, i: 'AI_2', w: 1, priority: 18 },
  { active: false, i: 'SERVO_A', w: 1, priority: 19 },
  { active: false, i: 'SERVO_B', w: 1, priority: 20 },
  { active: false, i: 'SERVO_C', w: 1, priority: 21 },
  { active: false, i: 'SERVO_D', w: 1, priority: 22 },
  { active: false, i: 'MOTOR_A', w: 1, priority: 23 },
  { active: false, i: 'MOTOR_B', w: 1, priority: 24 }
];

function calculateLayout(layout, row_limit = 6) {
  // console.log('CALCULATING LAYOUT');
  let row = 0;
  let column = 0;
  const holes = {};

  layout.sort((a, b) => {
    if (a.active && !b.active) {
      return -1;
    } else if (b.active && !a.active) {
      return 1;
    } else {
      return b.priority - a.priority;
    }
  });

  layout.forEach(p => {
    p.h = 1;
    // has set flag
    let hasSet = false;
    // check available holes in the current layout
    Object.keys(holes).forEach((k, index) => {
      if (!hasSet) {
        // Check if fit in the hole
        if (holes[k] + p.w <= row_limit) {
          p.x = holes[k];
          // k is a string, parse to a number
          p.y = parseInt(k);
          holes[k] += p.w;
          // Check if hole is full
          if (holes[k] > row_limit) {
            delete holes[k];
          }
          // Update set flag
          hasSet = true;
        }
      }
    });
    // No available holes
    if (!hasSet) {
      // check if fit in current head
      if (column + p.w > row_limit) {
        holes[row] = column;
        row += 1;
        column = 0;
      }
      p.y = row;
      p.x = column;
      column += p.w;
    }
  });

  // console.log(layout);
  return layout;
}

const mergeLayout = (new_layout, device_info) => {
  // console.log(new_layout);
  device_info.forEach(d => {
    new_layout.forEach(l => {
      if (l.i === d.port) {
        l.w = d.size;
        l.active = true;
      }
    });
  });
  return new_layout;
};

const initialLayout = () => {
  return {
    row: 6,
    layout: calculateLayout(emptyLayout())
  };
};

const initialRobot = () => {
  const result = {};
  for (let i = 1; i <= 9; i++) {
    result[`DIO_${i}`] = { hasDevice: false };
  }
  for (let i = 1; i <= 7; i++) {
    result[`PORT_${i}`] = { hasDevice: false };
  }
  result.IR_TRAN = { hasDevice: false };
  result.AI_1 = { hasDevice: false };
  result.AI_2 = { hasDevice: false };
  result.SERVO_A = { hasDevice: false, device: { angle: 0 } };
  result.SERVO_B = { hasDevice: false, device: { angle: 0 } };
  result.SERVO_C = { hasDevice: false, device: { angle: 0 } };
  result.SERVO_D = { hasDevice: false, device: { angle: 0 } };
  result.MOTOR_A = { hasDevice: false };
  result.MOTOR_B = { hasDevice: false };

  return result;
};

const setupRobot = device_info => {
  const new_robot = initialRobot();
  // console.log(device_info);
  device_info.forEach(d => {
    new_robot[d.port].hasDevice = true;
    new_robot[d.port].device = d;
  });
  return new_robot;
};

const updateRobot = (state, devices) => {
  const new_state = Object.assign({}, state);
  devices.forEach(d => {
    // SKIP ROBOCORE
    if (d.port != 'ROBOCORE') {
      new_state[d.port].hasDevice = true;
      if (d.name === 'servo') {
        const previous_device = state[d.port].device;
        // Update Current Angle
        if (previous_device.state === 2) {
          d.angle = previous_device.msg[0].value;
        } else {
          d.angle = previous_device.angle;
        }
        // console.log(d);
      }
      new_state[d.port].device = d;
    }
  });
  // console.log(new_state.SERVO_A.device);
  return new_state;
};

function robotLayout(state = initialLayout(), action) {
  switch (action.type) {
    case LAYOUT_SETUP:
      console.log('SETUP LAYOUT');
      return {
        row: action.row,
        layout: calculateLayout(mergeLayout(emptyLayout(), action.device_info), action.row)
      };
    case LAYOUT_UPDATE:
      console.log('UPDATE LAYOUT');
      return {
        row: state.row,
        layout: calculateLayout(mergeLayout([...state.layout], action.device_info), state.row)
      };
    case LAYOUT_RESIZE:
      console.log('RESIZE LAYOUT');
      return {
        row: action.row,
        layout: calculateLayout(mergeLayout([...state.layout], []), action.row)
      };
    case 'FREEMODE':
      return {
        row: 6,
        layout: calculateLayout(mergeLayout([...state.layout], []), 6)
      };
    default:
      return state;
  }
}

function virtualRobot(state = initialRobot(), action) {
  switch (action.type) {
    case DEVICE_SETUP:
      // This Action Is Only Dispatched When RoboCore Disconnects
      return setupRobot(action.device_info);
    case DEVICE_UPDATE:
      return updateRobot(state, action.device_info);
    default:
      return state;
  }
}

function eventHistory(state = [], action) {
  switch (action.type) {
    case EVENT_RECEIVED:
      // console.log('Updating Event History');
      // console.log(action.event_info);
      const new_state = [...action.event_info.reverse(), ...state];
      if (new_state.length > 50000) {
        return [];
      }

      return new_state;
    case STATE_RECEIVED:
      return [...action.state_info.reverse(), ...state];

    case EVENT_RESET:
      // console.log('Reseting Events');
      return [];
    default:
      return state;
  }
}

function showConsole(state = true, action) {
  switch (action.type) {
    case TOGGLE_CONSOLE:
      return !state;
    default:
      return state;
  }
}

const defaultSnapshot = {
  virtualRobot: null,
  eventHistory: null
};

function snapshot(state = defaultSnapshot, action) {
  switch (action.type) {
    case TAKE_SNAPSHOT:
      //console.log(action);
      return action.data;
    default:
      return state;
  }
}

const RobotReducer = combineReducers({
  robotLayout,
  virtualRobot,
  eventHistory,
  showConsole,
  snapshot
});

export default RobotReducer;

//
