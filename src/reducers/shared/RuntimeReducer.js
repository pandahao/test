import { combineReducers } from 'redux';
import {
    DISPLAY_GLOBAL_MESSAGE,
    HIDE_GLOBAL_MESSAGE,
} from 'legacy_actions/shared/RuntimeActions';

const DEFAULT_MESSAGE = {
  message: null,
  level: 'info',
  open: false
};
const message = (state = DEFAULT_MESSAGE, action) => {
  switch (action.type) {
    case DISPLAY_GLOBAL_MESSAGE:
      return Object.assign({}, state, {
        message: action.message,
        level: open.level,
        open: true
      }
            );
    case HIDE_GLOBAL_MESSAGE:
      return DEFAULT_MESSAGE;
    default:
      return state;
  }
};

export default {
  runtime: combineReducers({
    message
  })
};
