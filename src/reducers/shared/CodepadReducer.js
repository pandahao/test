import { combineReducers } from 'redux';

import {
    SKETCH_CHANGE,
    SKETCH_RESET,
    TOGGLE_TOOLBOX,
    HIDE_TOOLBOX,
    TOGGLE_CHALLENGE_BANNER,
    CLOSE_CHALLENGE_BANNER,
    TOGGLE_ERROR_BANNER,
    CLOSE_ERROR_BANNER,
    CORE_INIT_ERROR,
    CORE_INIT_SUCCESS,
    CORE_NULL,
    UPLOAD_START,
    UPLOAD_ERROR,
    UPDATE_ERROR,
    UPLOAD_SUCCESS,
    UPLOAD_NOT_AVAILABLE,
    UPDATE_NAMES,
    RESET_NAMES,
    PROCEED_CHECK,
    CANCEL_CHECK,
    MAP_SOLUTION,
    CLOSE_WARNING,
    SHOW_WARNING,
    SHOW_COMPLETION,
    CLOSE_COMPLETION,
    COMPILATION_ERROR,
    START_CONNECT,
    END_CONNECT,
    MARK_DIRTY,
    CLEAR_DIRTY,
    init_sketch
} from 'legacy_actions/CodepadActions';


const init_varNames = { ROBOCOREZ: { name: '', type: '' },
    PORT_1: { name: '', type: '' },
    PORT_2: { name: '', type: '' },
    PORT_3: { name: '', type: '' },
    PORT_4: { name: '', type: '' },
    PORT_5: { name: '', type: '' },
    PORT_6: { name: '', type: '' },
    PORT_7: { name: '', type: '' },
    DIO_1: { name: '', type: '' },
    DIO_2: { name: '', type: '' },
    DIO_3: { name: '', type: '' },
    DIO_4: { name: '', type: '' },
    DIO_5: { name: '', type: '' },
    DIO_6: { name: '', type: '' },
    DIO_7: { name: '', type: '' },
    DIO_8: { name: '', type: '' },
    DIO_9: { name: '', type: '' },
    SERVO_A: { name: '', type: '' },
    SERVO_B: { name: '', type: '' },
    SERVO_C: { name: '', type: '' },
    SERVO_D: { name: '', type: '' },
    MOTO_A: { name: '', type: '' },
    MOTO_B: { name: '', type: '' },
    AI_1: { name: '', type: '' },
    AI_2: { name: '', type: '' },
    IR_TRAN: { name: '', type: '' }
};

function variableNames(state = init_varNames, action) {
  switch (action.type) {
    case UPDATE_NAMES :
      return action.names;
    case RESET_NAMES :
      return init_varNames;
    default :
      return state;
  }
}

function solutionMapping(state = init_varNames, action) {
  switch (action.type) {
    case MAP_SOLUTION :
      return action.names;
    default:
      return state;
  }
}

function sketch(state = init_sketch, action) {
  switch (action.type) {
    case SKETCH_CHANGE:
      return action.sketch; // ? action.sketch : '';
    case SKETCH_RESET:
      return init_sketch;
    default:
      return state;
  }
}

function showToolbox(state = false, action) {
  switch (action.type) {
    case TOGGLE_TOOLBOX:
      return !state;
    case HIDE_TOOLBOX:
      return false;
    default:
      return state;
  }
}

function showNextChallengeBanner(state = false, action) {
  switch (action.type) {
    case TOGGLE_CHALLENGE_BANNER:
      return !state;
    case CLOSE_CHALLENGE_BANNER:
      return false;
    default:
      return state;
  }
}

function showErrorBanner(state = false, action) {
  switch (action.type) {
    case TOGGLE_ERROR_BANNER:
      return !state;
    case CLOSE_ERROR_BANNER:
      return false;
    default:
      return state;
  }
}

const initial_status = {
  connected: false,
  error: undefined,
  uploading: false,
  showingRun: true,
  codeCompiled: false,
  virtualrobotblur: true
};

function roboCoreStatus(state = initial_status, action) {
  switch (action.type) {
    case UPLOAD_START :
      return {
        connected: state.connected,
        error: undefined,
        uploading: true,
        virtualrobotblur: !state.connected,
        showingRun: true,
        codeCompiled: false
      };
    case UPLOAD_ERROR :
      return {
        connected: state.connected,
        error: action.error,
        uploading: false,
        virtualrobotblur: !state.connected,
        showingRun: false,
        codeCompiled: true
      };
    case COMPILATION_ERROR :
      return {
        connected: state.connected,
        error: action.error,
        uploading: false,
        virtualrobotblur: !state.connected,
        showingRun: false,
        codeCompiled: false
      };
    case UPDATE_ERROR :
      return {
        connected: state.connected,
        error: state.error,
        uploading: false,
        virtualrobotblur: !state.connected,
        showingRun: true,
        codeCompiled: true
      };
    case UPLOAD_SUCCESS :
      return {
        connected: state.connected,
        error: undefined,
        uploading: false,
        showingRun: true,
        codeCompiled: true,
        virtualrobotblur: !state.connected
      };
    case UPLOAD_NOT_AVAILABLE :
      console.warn('Upload not available outside electron');
      return state;
    case CORE_INIT_SUCCESS:
      console.log('Skyline Core Successfully initialized!');
      return {
        connected: true,
        error: state.error,
        uploading: state.uploading,
        virtualrobotblur: false,
        showingRun: true,
        codeCompiled: state.codeCompiled
      };
    case CORE_INIT_ERROR :
      console.warn('Skyline Core failed to init RoboCore for reason:', action.error);
      return {
        connected: false,
        error: action.error,
        uploading: state.uploading,
        virtualrobotblur: true,
        showingRun: true,
        codeCompiled: false
      };
    case CORE_NULL :
      console.warn('Skyline Core is not available outside Electron!');
      return state;
    default:
      return state;
  }
}

function checkingAnswers(state = false, action) {
  switch (action.type) {
    case PROCEED_CHECK:
      //console.log('PROCEED');
      return true;
    case CANCEL_CHECK:
      //console.log('CANCEL');
      return false;
    default:
      return state;
  }
}

function resetWarning(state = false, action) {
  switch (action.type) {
    case SHOW_WARNING:
      return true;
    case CLOSE_WARNING:
      return false;
    default:
      return state;
  }
}

function completionPopup(state = false, action) {
  switch (action.type) {
    case SHOW_COMPLETION:
      return true;
    case CLOSE_COMPLETION:
      return false;
    default:
      return state;
  }
}
function connectRoboCore(state=false, action){
  switch(action.type){
    case START_CONNECT:
      return true;
    case END_CONNECT :
      return false;
    default :
      return state;
  }
}

const dirty = (state=false, action) => {
  switch (action.type) {
    case MARK_DIRTY:
      return true
    case CLEAR_DIRTY:
      return false
    default:
      return state
  }
}

const CodepadReducer = { codepad: combineReducers({
  sketch,
  showToolbox,
  showNextChallengeBanner,
  showErrorBanner,
  roboCoreStatus,
  variableNames,
  checkingAnswers,
  solutionMapping,
  resetWarning,
  completionPopup,
  connectRoboCore,
  dirty
}) };

export default CodepadReducer;


//
