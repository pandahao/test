import { combineReducers } from 'redux';

import RuntimeReducer from './RuntimeReducer';
import AppReducer from './AppReducer';
import CodepadReducer from './CodepadReducer';
import DocViewReducer from './DocViewReducer';

export default {
  shared: combineReducers({
    ...RuntimeReducer,
    ...AppReducer,
    ...CodepadReducer,
    ...DocViewReducer
  })
};
