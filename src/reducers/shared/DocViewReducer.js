import { combineReducers } from 'redux';
import {
        DOC_LOAD_SUCCESS,
        DOC_LOAD_FAILED,
        TOGGLE_OPEN,
        TOGGLE_INNER_OPEN
    } from 'legacy_actions/DocViewActions';

const docs_initial_state = {
  status: 'loading',
  error: undefined
};

function docs(state = docs_initial_state, action) {
  switch (action.type) {
    case DOC_LOAD_SUCCESS :
      return Object.assign({}, state, action.data, { error: undefined, status: 'success' });
    case DOC_LOAD_FAILED :
      return Object.assign({}, state, { error: action.error, status: 'error' });
    default:
      return state;
  }
}

const open_initial_state = [
  {
    open: false,
    inner: []
  },
  {
    open: false,
    inner: []
  },
  {
    open: false,
    inner: []
  },
  {
    open: false,
    inner: []
  },
  {
    open: false,
    inner: []
  },
  {
    open: false,
    inner: []
  },
  {
    open: false,
    inner: []
  },
  {
    open: false,
    inner: []
  },
  {
    open: false,
    inner: []
  },
  {
    open: false,
    inner: []
  },
  {
    open: false,
    inner: []
  }

];

function docOpen(state = open_initial_state, action) {
  switch (action.type) {
    case TOGGLE_OPEN :
      return [
        ...state.slice(0, action.num),
        {
          open: !state[action.num].open,
          inner: []
        },
        ...state.slice(action.num + 1)
      ];
    case TOGGLE_INNER_OPEN :
      const index = state[action.num].inner.indexOf(action.node);
      if (index === -1) {
        return [
          ...state.slice(0, action.num),
          {
            open: state[action.num].open,
            inner: state[action.num].inner.concat(action.node)
          },
          ...state.slice(action.num + 1)
        ];
      }
      else {
        return [
          ...state.slice(0, action.num),
          {
            open: state[action.num].open,
            inner: [
              ...state[action.num].inner.slice(0, index),
              ...state[action.num].inner.slice(index + 1)
            ]
          },
          ...state.slice(action.num + 1)
        ];
      }
    default:
      return state;
  }
}


const DocViewReducer = { doc: combineReducers({
  docs,
  docOpen
}) };


export default DocViewReducer;
