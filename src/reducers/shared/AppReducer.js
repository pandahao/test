import { combineReducers } from 'redux';
import {handleActions} from 'redux-actions';

import { TOGGLE_DOC_VIEW, CLOSE_DOC_VIEW, CHANGE_LANGUAGE, SET_LANGUAGE } from 'legacy_actions/AppActions';
import { updateLanguage } from 'legacy_utils/i18n';
import {ChromeEnv} from 'config';

const defaultChallengeLocation = ChromeEnv ? 1 : 0

function rootLocation(state = 'HOME_VIEW', action) {
  switch (action.type) {
    case 'PROJECT_VIEW':
      return 0;
    case 'SETTINGS_VIEW':
      return 1;
    case 'FREEMODE':
      return 2;
    case 'HOME_VIEW':
      return action.type;
    default:
      return state;
  }
}

function challengeLocation(state = defaultChallengeLocation, action) {
  switch (action.type) {
    case 'GRID_VIEW':
      return defaultChallengeLocation;
    case 'LIST_VIEW':
      return 1;
    case 'OVERVIEW':
      return 2;
    case 'CHALLENGE_VIEW':
      return 3;
    default:
      return state;
  }
}

function docView(state = false, action) {
  switch (action.type) {
    case TOGGLE_DOC_VIEW:
      return !state;
    case CLOSE_DOC_VIEW:
      return false;
    default:
      return state;
  }
}

// function tutorialOnBoard(state = -1, action) {
//   switch (action.type) {
//     case 'SET_TUTORIAL_ONBOARD':
//       return action.value;
//     case 'SELECT_CHALLENGE':
//       if (action.challenge_id === 0) {
//         return 0;
//       } else if (action.challenge_id === 1) {
//         return 6;
//       }
//       else if (action.challenge_id === 2) {
//         return 8;
//       }
//     default:
//       return state;
//   }
// }

function getDefaultLanguage (){
  return localStorage.getItem('skylineDefaultLanguage') || 'CHINESE';
}
function setDefaultLanguage (lan){
  localStorage.setItem('skylineDefaultLanguage', lan);
}

function language (state=getDefaultLanguage(), action) {
  switch (action.type) {
    case CHANGE_LANGUAGE:
        // right now just toggling, but in the future
        // remove the first two lines and allow more language choices

      setDefaultLanguage(action.language);
      return action.language;

    default:
      return state;
  }
}

function setLanguage (state='CHINESE', action) {
  switch (action.type) {
    case SET_LANGUAGE:
      return action.value
    default:
      return state
  }
}

function tutorialMode(state=0, action){
  switch(action.type){
    case 'TURN_ON_TUTORIAL':
      return 1;
    case 'TURN_OFF_TUTORIAL':
      return 0;
    case 'NEXT_TUTORIAL_STEP':
      if(state < 1){
        return state;
      }
      return state+1;
    case 'PREVIOUS_TUTORIAL_STEP':
      if(state <= 1){
        return state;
      }
      return state-1;
    default:
      return state
  }
}

const erraMode = handleActions({
  TURN_OFF_ERRA: () => false,
  TURN_ON_ERRA: () => true
}, false);

const AppReducer = {
  app: combineReducers({
    rootLocation,
    challengeLocation,
    docView,
    erraMode,
    tutorialMode,
    language,
    currentLanguage: setLanguage
  })
};

export default AppReducer;
