import { combineReducers } from 'redux';

import ProjectReducer from './ProjectReducer';
import ChallengeReducer from './ChallengeReducer';

const projectMode = combineReducers({
  project: ProjectReducer,
  challenge: ChallengeReducer
});

export default projectMode;
