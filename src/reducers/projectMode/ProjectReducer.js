import { combineReducers } from 'redux';

import { SET_LOADING, PROJECT_LOAD_SUCCESS, PROJECT_LOAD_FAILED, LOAD_SINGLE_PROJECT, SELECT_PROJECT, TOGGLE_GRID_VIEW, CLOSE_GRID_VIEW, BEGIN_PROJECT, RESTART_PROJECT, CLOSE_RESET, SHOW_RESET } from 'legacy_actions/ListViewActions';
import { UPDATE_USER_PROFILE_SUCCESS } from 'legacy_actions/shared/UserActions';
import { INCREMENT_CHALLENGES_COMPLETED } from 'legacy_actions/ChallengeActions';
import { UPDATE_PROJECTS } from 'legacy_actions/RobotActions';
import { SET_USER } from 'legacy_actions/AuthActions';
import { SET_ONBOARDING, REQUEST_PROJECTS_RELOAD } from 'legacy_actions/AppActions';

const projects_initial_state = {
  status: 'loading',
  error: undefined,
  projectsData : []
};

function loading(state = -1, action) {
  switch (action.type) {
    case SET_LOADING:
      return action.loading;
    default:
      return state;
  }
}

function projects(state = projects_initial_state, action) {
  switch (action.type) {
    case LOAD_SINGLE_PROJECT :
      // console.log(action.data);
      if(action.error){
        return state;
      }
      const newState = Object.assign({},state);
      newState.projectsData = newState.projectsData.map(prj=>{
        if(prj.keyname === action.projectID){
          prj.challengeData = action.data;
          // console.log(prj);
          return prj;
        }
        return prj;
      })
      return newState;
    case PROJECT_LOAD_SUCCESS :
      return Object.assign({}, state, action.data, { error: undefined, status: 'success' });
    case PROJECT_LOAD_FAILED :
      return Object.assign({}, state, { error: action.error, status: 'error' });
    case REQUEST_PROJECTS_RELOAD:
      console.log(REQUEST_PROJECTS_RELOAD)
      return Object.assign({}, state, {error: undefined, status: 'loading'});
    default:
      return state;
  }
}

const user_initial_state = {
  status: 'loading',
  error: undefined
};

function userProgress(state = user_initial_state, action) {
  switch (action.type) {
    case SET_USER:
      return Object.assign({}, state, action.user, { status: 'success', error: undefined });
    case BEGIN_PROJECT :
      return Object.assign({}, state,
            { info: Object.assign({}, state.info,
                { projectsOwned: Object.assign([], state.info.projectsOwned,
                    { [state.info.projectsOwned.indexOf(state.info.projectsOwned.find((p) => { return p.projectId === action.projectID; }))]: Object.assign({}, state.info.projectsOwned.find((p) => { return p.projectId === action.projectID; }), { status: 'ONGOING' }) }
                ) }
            ) }
        );
    case RESTART_PROJECT :
      return Object.assign({}, state,
            { info: Object.assign({}, state.info,
                { projectsOwned: Object.assign([], state.info.projectsOwned,
                    { [action.projectIndex]: Object.assign({}, state.info.projectsOwned[action.projectIndex], { progress: 1, status: 'ONGOING' }) }
                ) }
            ) }
        );
    case UPDATE_PROJECTS :
        // if (index === state.info.projectsOwned.length - 1) {
        //     // LAST PROJECT
        //     return Object.assign({},state,
        //         {'info' : Object.assign({}, state['info'],
        //             {'projectsOwned' : Object.assign([], state['info']['projectsOwned'],
        //                 {
        //                     [index] : Object.assign({}, state.info.projectsOwned.find(p=>{return p.projectId === pid}), {'status' : 'COMPLETED'})
        //                 }
        //             )}
        //         )}
        //     );
        // }
        // ALL UNLOCKED NOW, NO NEED FOR THIS
        // else if (state.info.projectsOwned[index + 1].status !== 'LOCKED') {
        //     // NEXT PROJECT ALREADY UNLOCKED, not sure if possible but just in case...
        //     return Object.assign({},state,
        //         {'info' : Object.assign({}, state['info'],
        //             {'projectsOwned' : Object.assign([], state['info']['projectsOwned'],
        //                 {
        //                     [index] : Object.assign({}, state.info.projectsOwned[index], {'status' : 'COMPLETED'})
        //                 }
        //             )}
        //         )}
        //     );
        // }

        // COMPLETE CURRENT AND UNLOCK NEXT PROJECT if next project not unlocked

      const newProjects = state.info.projectsOwned.map((p) => {
        console.log(`${p.projectId} vs ${action.currentProjectID}`)
        if (p.projectId === action.currentProjectID) {
          localStorage.setItem(`${p.projectId}_COMPLETE`,true);
          return Object.assign({}, p, { status: 'COMPLETED' });
        } else {
          return Object.assign({}, p);
        }
      });
      return Object.assign({}, state,
            { info: Object.assign({}, state.info,
                { projectsOwned: newProjects }
            ) }
        );

    case INCREMENT_CHALLENGES_COMPLETED:
        // REMEBER;
      const newProjectsOwned = state.info.projectsOwned.map((p) => {
        if (p.projectId === action.currentProjectID) {
          localStorage.setItem(`${p.projectId}`,p.progress + 1);
          return Object.assign({}, p, { progress: p.progress + 1 });
        } else {
          return Object.assign({}, p);
        }
      });
      return Object.assign({}, state,
            { info: Object.assign({}, state.info,
                { projectsOwned: newProjectsOwned }
            ) }
        );
        /*
        Object.assign([], state['info']['projectsOwned'],
           {[action.currentProjectIndex] : Object.assign({}, currentProObj, {progress : currentProObj.progress + 1})}
       )
        */
    case SET_ONBOARDING:
      return Object.assign({}, state,
            { info: Object.assign({}, state.info, { onBoardProgress: action.value }) }
        );
    case UPDATE_USER_PROFILE_SUCCESS:
      return Object.assign({}, state,
            { info: Object.assign({}, state.info, action.profile) }
        );
    default :
      return state;
  }
}

function currentProjectID(state = -1, action) {
  switch (action.type) {
    case SELECT_PROJECT :
      return action.projectID;
    default:
      return state;
  }
}

function currentProjectIndex(state = -1, action) {
  switch (action.type) {
    case SELECT_PROJECT:
      return action.projectIndex;
    default:
      return state;
  }
}


function gridView(state = true, action) {
  switch (action.type) {
    case TOGGLE_GRID_VIEW :
      return !state;
    case CLOSE_GRID_VIEW :
      return false;
    default :
      return state;
  }
}

function resetProject(state = false, action) {
  switch (action.type) {
    case SHOW_RESET:
      return true;
    case CLOSE_RESET:
      return false;
    default:
      return state;
  }
}


const ProjectReducer = combineReducers({
  projects,
  currentProjectID,
  gridView,
  userProgress,
  resetProject,
  currentProjectIndex,
  loading
});


export default ProjectReducer;
