import { combineReducers } from 'redux';
import {
        TOGGLE_CHALLENGE_VIEW,
        SELECT_CHALLENGE,
        NEXT_CHALLENGE,
        PREVIOUS_CHALLENGE,
        SET_BLUR,
        SHOW_CHALLENGE_VIEW,
        SET_HINT,
        SET_ANSWER_SELECTED,
        SET_RESULT,
        INITIALIZE_QUIZ_RESULT,
        INITIALIZE_CHALLENGE_SET,
        TOGGLE_BUILD_VIEW,
        PROGRESS_BACK,
        PROGRESS_NEXT,
        MOVE_POINT,
        INITIALIZE_ANSWERS_SELECTED,
        SET_DELAY_PAGE_CHANGE,
        RESET_BUILD_PROGRESS
    } from 'legacy_actions/ChallengeActions';

function showChallengeContent(state = true, action) {
  switch (action.type) {
    case TOGGLE_CHALLENGE_VIEW:
      return !state;
    case 'CLOSE_CHALLENGE_VIEW':
      return false;
    case SHOW_CHALLENGE_VIEW:
      return (action.status !== 'Finished');
    default:
      return state;
  }
}

function currentChallengeSelected(state = -1, action) {
  switch (action.type) {
    case NEXT_CHALLENGE :
      if (state === action.max) {
        return state;
      }
      return state + 1;
    case PREVIOUS_CHALLENGE :
      if (state === 0) {
        return 0;
      }
      return state - 1;
    case SELECT_CHALLENGE:
            // Prevent unwanted state change here ALSO from action creators!!!
      return action.challenge_id;
    default :
      return state;
  }
}

function challengelistBlur(state = false, action) {
  switch (action.type) {
    case SET_BLUR:
      return action.blur;
    default:
      return state;
  }
}

function hintStatus(state = false, action) {
  switch (action.type) {
    case SET_HINT:
      return action.hint;
    default:
      return state;
  }
}

function quizAnswerSelected(state = {}, action) {
  switch (action.type) {
    case INITIALIZE_ANSWERS_SELECTED:
      return {};
    case SET_ANSWER_SELECTED:
      const newAnswer = {};
      newAnswer[action.question] = action.index;
      return Object.assign({}, state, newAnswer);
    default:
      return state;
  }
}

function quizResult(state = {}, action) {
  switch (action.type) {
    case INITIALIZE_QUIZ_RESULT:
      return {};
    case SET_RESULT:
      const newResult = {};
      newResult[action.question] = action.result; // will be either correct or wrong
      return Object.assign({}, state, newResult);
    default:
      return state;
  }
}

function currentChallengeSet(state = [], action) {
  switch (action.type) {
    case INITIALIZE_CHALLENGE_SET:
      const arr = [];
      if(action.value) {
        for (let i = 0; i < action.value.length; i++) {
          arr.push(Object.assign({}, action.value[i]));
        }
      }
      return arr;
    default:
      return state;
  }
}

function currentEventOrder(state = 0, action) {
  switch (action.type) {
    case 'SET_CHALLENGE_EVENT_ORDER':
      return action.newOrder;
    default:
      return state;
  }
}

function currentProgress(state = 1, action) {
  switch (action.type) {
    case MOVE_POINT:
      return action.point;
    case PROGRESS_BACK:
      if (state > 1) {
        return state - 1;
      }
      else {
        return state;
      }
    case PROGRESS_NEXT:
      if (state < action.maxpoints) {
        return state + 1;
      }
      else {
        return state;
      }
    case RESET_BUILD_PROGRESS:
      return 1;
    default:
      return state;
  }
}

function delayedPageChange(state = true, action) {
  switch (action.type) {
    case SET_DELAY_PAGE_CHANGE:
      return action.delay;
    default:
      return state;
  }
}

const ChallengeReducer = combineReducers({
  showChallengeContent,
  currentChallengeSelected,
  challengelistBlur,
  hintStatus,
  quizAnswerSelected,
  quizResult,
  currentChallengeSet,
  currentEventOrder,
  currentProgress,
  delayedPageChange
});


export default ChallengeReducer;
