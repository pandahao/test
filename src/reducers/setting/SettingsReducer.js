import { combineReducers } from 'redux';
import {
    SELECT_SETTING, ACCOUNT,PROFILE,LANGUAGE,SEND,VERSION
} from 'legacy_actions/SettingsActions';

function currentSettingSelected(state = VERSION, action) {
  switch (action.type) {
    case SELECT_SETTING:
      return action.setting_id;
    default:
      return state;
  }
}

const SettingsReducer = combineReducers({
  currentSettingSelected
});

export default SettingsReducer;
