import React, { Component } from 'react';
// Radium style support
import { StyleRoot } from 'radium';
// material-ui theme support
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SkylineTheme from 'styles/Theme';
import injectTapEventPlugin from 'react-tap-event-plugin';
// velocity ui pack
require('velocity-animate/velocity.ui');

// Suppress material-ui warnings
injectTapEventPlugin();

// The MOTHER SHIP
class App extends Component {
  render() {
    return (
      <StyleRoot>
        <MuiThemeProvider muiTheme={SkylineTheme}>
          {this.props.children}
        </MuiThemeProvider>
      </StyleRoot>
    );
  }
}


export default App;
