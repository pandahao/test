/**
 * Listens for the app launching then creates the window
 *
 * @see http://developer.chrome.com/apps/app.window.html
 */

chrome.app.runtime.onLaunched.addListener(() => {
  chrome.app.window.create('chrome.html', {
    id: 'main',
    bounds: { width: 1280, height: 800 }
  });
});
