
// chrome.storage.local.getItem = chrome.storage.local.get;
// chrome.storage.local.setItem = chrome.storage.local.set;
window.skylineStorage = Object.create(null);

window.localStorage = {
  getItem: (key) => {
    return window.skylineStorage[key] || undefined
  },
  setItem: (key,value) => {
    window.skylineStorage[key] = value;
  },
  clear: ()=>{
    window.skylineStorage = Object.create(null);
  }
};
